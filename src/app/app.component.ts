/* cSpell:disable */
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  login: boolean;
  seccionini: boolean;

  ngOnInit() {
    if (localStorage.getItem('usertoken') === null) {     
      this.seccionini = false;
      this.login = true;
    } else {    
      this.seccionini = true;
      this.login = false;
    }
  }
}
