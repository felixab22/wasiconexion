import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { FilesService } from '@services/servicio.index';
import { URL_SERVICIOS } from 'app/config/config';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  nombre: string;
  personal: any;
  date = new Date();
  anioactual: number;
  @Output() Cambiomenu: EventEmitter<any> = new EventEmitter();
  _opened: boolean = false;
  urlimg: string;
  verAtender: boolean;


  constructor(
    private router: Router,
    private _uploadFilesSrv: FilesService
  ) {

  }

  ngOnInit() {
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));
    if (autorization === 5 || autorization === 4) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }
    const anioactual = String(this.date).substr(8, 2);
    this.anioactual = parseInt(anioactual);
    const ayer = parseInt(localStorage.getItem('hoy'));
    if (ayer !== this.anioactual) {
      this.cerrarseccion();

    }
    if (localStorage.getItem('personal')) {
      this.personal = JSON.parse(localStorage.getItem('personal'));
      this.nombre = this.personal.nombrepersonal + ' ' + this.personal.apppaterno || 'Wasi';
    }
    this.imprimirFiles();
  }
  cerrarseccion() {
    console.log('hola');
    localStorage.removeItem('usertoken');
    localStorage.removeItem('personal');
    localStorage.removeItem('hoy');
    this.router.navigate(['/login']);
  }
  _toggleSidebar() {
    this._opened = !this._opened;
    this.Cambiomenu.emit(this._opened);
  }
  imprimirFiles() {
    const user: any = JSON.parse(localStorage.getItem('personal'));   
    this._uploadFilesSrv.getListaFilesxAreayPaciente('perfil', user.id_personal).subscribe(res => {
      if (res.length === 0) {
        console.log('no existe foto');
      } else {
        const imagenperfil = res[res.length - 1];
        this.urlimg = `${URL_SERVICIOS}/file/${imagenperfil}?nombrearea=perfil&codigo=${user.id_personal}`;
      }
    });


  }
}
