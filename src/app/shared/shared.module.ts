import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RouterModule } from '@angular/router';
import { NopagefoundComponent } from './nopagefound/nopagefound.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MenuComponent } from './menu/menu.component';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { SidebarModule } from 'ng-sidebar';

@NgModule({
    imports: [       
        RouterModule,
        CommonModule,        
        MDBBootstrapModule.forRoot(),
        SidebarModule.forRoot(),
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
    ],
    declarations: [
        NopagefoundComponent,
        NavbarComponent,
        MenuComponent, 
        BreadcrumbsComponent,   
    ],
    exports: [
        NopagefoundComponent, 
        NavbarComponent,
        MenuComponent, 
        BreadcrumbsComponent,        
    ],
    schemas: [NO_ERRORS_SCHEMA],
})
export class SharedModule { }
