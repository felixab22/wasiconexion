/* cSpell:disable */
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'app/core/auth.service';
declare var swal: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  verseccionini: boolean;
  recuerdame: boolean = false;
  dominios: { id: number; tipo: string; }[];
  selectedprograma1: any;
  gradodomiio1: any;
  date = new Date();
  private anioactual: string;
  constructor(
    private router: Router,
    private authService: AuthService,
  ) {
    this.anioactual = String(this.date).substr(8, 2);
    console.log(this.anioactual);

  }
  username: string;
  password: string;
  ngOnInit() {
    this.dominios = [
      { id: 1, tipo: 'Recuerdame' }
    ];
    if (localStorage.getItem('usertoken') === null) {
      this.router.navigate(['login']);
    } else {
      this.router.navigate(['wasi/bienvenido']);
      // this.verseccionini = false;
    }
    this.username = localStorage.getItem('usuario') || 'wasi';
    this.password = localStorage.getItem('password') || '';
    if (this.username.length > 1) {
      this.recuerdame = true;
    }
  }

  login(username, password, recuerdame): void {
    if (recuerdame === true) {
      localStorage.setItem('usuario', username);
      localStorage.setItem('password', password);
    } else {
      localStorage.removeItem('usuario');
      localStorage.removeItem('password');
    }
    this.authService.attemptAuth(username, password)
      .subscribe(
        data => {
          localStorage.setItem('hoy', this.anioactual);
          localStorage.setItem('personal', JSON.stringify(data.personal));
          localStorage.setItem('usertoken', data.accessToken);
          localStorage.setItem('autorization', data.authorities[0].authority);
          this.router.navigate(['wasi/bienvenido']);
        },
        error => {
          swal("Mal!", "Credenciale incorrectos!", "warning")
        }
      );
  }
}
