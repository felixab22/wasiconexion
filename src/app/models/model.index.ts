export { AtentionModel, InasistenciasModel } from "./configuration/agenda.model";

export { PersonalModel } from "./configuration/personal.model";
export {
    VentaMedicModel,
    MedicamentosModel
} from "./salud/medicamentos.model";
export { AnamnesisClinicoModel } from "./rehabilitacion/anamnesis-clinico.model";



export { DesarrolloIMDModel } from "./rehabilitacion/desarrollo-imd.model";
export { DiganosticoMedicoModel } from "./rehabilitacion/diagnostico-medico.model";
export { FichaTerapiaFisicaModel } from "./rehabilitacion/ficha-terapia.model";
export { InasistenciaModel } from "./rehabilitacion/inasistencia.model";
export { MaterialRegModel, PrestamoMaterialModel } from "./rehabilitacion/materiales.model";
export { ReferenciaImtermaModel } from "./rehabilitacion/referencia-interna.model";
export {
    AnamnesisFamiliarModel,
    EvaluacionFisicaModel,
    BiometriaModel,
    FuerzaObserModel,
    SegFuerzaModel,
    InspbocaarribaModel,
    SegMovilidadModel,
    MovilidadArticulacionesModel,
    DiagnosticoFinalModel,
    ResultadoEscalaModel,
    PlanTrabajoModel,
    EspasticidadModel,
    MarchaModel,
    PiernaModel
} from "./rehabilitacion/rehabilitacion.model";
export {
    FichaPsicologicaModel,
    EvaluacionFamiliarModel
} from "./psicologia/psicologia.model";
export {
    EducacionModel,
    EscolaridadModel,
    DesarrolloSocialModel,
    DesalenguajeModel,
    SaludActualEduModel
} from "./educacion/educacion.model";
export { EvaluacionEduModel } from "./educacion/evaluacion-edu.model";
export { UbigeoModel } from "./configuration/ubigeo.model";
export { RolModel } from "./configuration/rol.model";
export { RestResponse } from "./configuration/restResponse.model";
export { RegisUsuarioModel } from "./configuration/regis-usuario.modal";
export { ProvinciaModel } from "./configuration/provincia.model";
export { DistritoModel } from "./configuration/distrito.model";
export { DepartamentoModel } from "./configuration/departamento.model";
export { CentroDiaModel } from "./centrodia/centrodia.model";
export { ATOModel } from "./ato/ato.model";
export {
    SaludModel,
    VisitaIntegralModel,
    CuidadoAlimenModel,
    VisitaMedicaModel,
    CampaniasModel,
    AtencionCampanModel
} from "./salud/salud.model";
export { PreInscripcionModel } from "./social/preinscripcion.model";
export { ReferenciaModel } from "./social/referencia-externa.model";
export { SituacionEconomicaModel } from "./social/situacion-economica.model";
export { SituacionPacienteModel } from "./social/situacion-paciente";
export { SocioEconomicaModel } from "./social/socio-economica";
export { VisitaDomiciliarioModel } from "./social/visita-domiciliario.model";
export { CompisicionFamiliarModel } from "./social/compo-familiar.model";
export { ExpedienteModel } from "./social/expediente.model";
export { AreaModel } from "./configuration/area.model";
export { ApoderadoModel } from "./social/apoderado.model";






