export class FichaTerapiaFisicaModel {
  idfichaterapiafisica?: number;
  fregistro?: Date;  
  usuariocreacion?: string;
  fichaseguimiento?: File;
  estadoatencion?: boolean;
  idcitaterapia?: number;
}
