export class AnamnesisFamiliarModel {
  public idantefami: number;
  public anamnesisFamiliar_rehab: string;
  public problemasactuales: string;
  public parentesco: string;
  public salud_familiar: string;
  public edad_concepcion: number;
  public patologias_fam: string;
  public num_hermanos: string;
  public desc_adicional: string;
  public idfichaterapiafisica: number;
}
export class EvaluacionFisicaModel {
  public idfichaterapiafisica: number;
  public fregistro: Date;
  public updatedAt: Date;
  public usuariocreacion: string;
  public estadoatencion: boolean;
  public idexpediente: number;
}

export class BiometriaModel {
  public fbioseguno: Date;
  public fbiosegdos: Date;
  public fbiosegtres: Date;

  public idbiometria: number;
  public longitudizq: string;
  public longitudder: string;
  public diferencia: string;
  public circunfcabeza: string;
  // datos de segunda evaluacion
  public longitudizqdos: string;
  public longitudderdos: string;
  public diferenciados: string;
  public circunfcabezados: string;
  // Datos de la tercera evaluacion
  public longitudizqtres: string;
  public longituddertres: string;
  public diferenciatres: string;
  public circunfcabezatres: string;
  // otros datos
  public obslongitud: string;
  public obscircunfcabeza: string;

  public conclusion: string;
  public difizquierda: string;
  public reducciones: string;
  public observaciones: string;
  public idfichaterapiafisica: number;
}
export class FuerzaObserModel {
  public idfuerza: number;
  public obscaderaext: string;
  public obscaderaflex: string;
  public obscaderaabducglut: Date;
  public obscaderaabducext: string;
  public obscaderaabducflex: string;
  public obsrodillaext: string;
  public obsrodillaflex: string;
  public obstobillodors: string;
  public observaciones: string;
  public obstobilloplant: string;
  public obshombrosante: string;
  public obshombrosabdu: string;
  public obscodoext: string;
  public obscodoflex: string;
  public obsabdominales: string;
  public obsextensores: string;
  public conclusiones: string;
  public difizquierda: string;
  public reducciones: string;
  public obsfinal: string;
  public fichaTerapiaFisica: number;

}
export class SegFuerzaModel {
  public idfuerzaseguimiento: number;
  public numseguimientofuerza: number;
  public fseguimientofuerza: Date;
  // izquierda
  public caderaextiz: string;
  public caderaflexiz: string;
  public caderaabdglutiz: Date;
  public caderaabducextiz: string;
  public caderaabducflexiz: string;

  public rodillaextiz: string;
  public rodillaflexiz: string;
  public tobillodorsiiz: string;
  public tobilloplantiz: string;

  public hombrosanteiz: string;
  public hombrosabduiz: string;
  public codextiz: string;
  public codoflexiz: string;
  // derecha
  public caderaextder: string;
  public caderaflexder: string;
  public caderaabdglutder: string;
  public caderaabducextder: string;
  public caderaabducflexder: string;

  public rodillaextder: string;
  public rodillaflexder: string;
  public tobillodorsider: string;
  public tobilloplantder: string;

  public hombrosanteder: string;
  public hombrosabduder: string;
  public codextder: string;
  public codoflexder: string;
  // uno sola
  public abdominales: string;
  public extespalda: string;

  public fuerza: number;
}

export class InspbocaarribaModel {
  public idinspbocaarriba: number;
  public posicion: string;
  public tronco: string;
  public tono: string;
  public patrones: string;
  public movimientos: string;
  public estado_alerte: string;
  public emociones: string;
  public visto: string;
  public otro: string;
  public oido: string;
  public especifico: string;
  public conclusion: string;
  public idfichaterapiafisica: number;
}

export class SegMovilidadModel {
  public idmovilidad: number;
  public numseguimiento: number;
  public fseguimiento: Date;
  // izquierda
  public caderaextiz: string;
  public caderaflexiz: string;
  public caderaabexciz: Date;
  public caderaabfleiz: string;
  public caderarotextiz: string;
  public caderarotintiz: Date;
  public rodillaextiz: string;
  public rodillaextflexiz: string;
  public rodillaflexiz: string;
  public tobillosoleusiz: string;
  public tobillogastociz: string;
  public hombroabdiz: string;
  public hombroanteiz: string;
  public hombroexoriz: string;
  public hombroendoriz: string;
  public codoentiz: string;
  public codosupiniz: string;
  public manodorsifiz: string;
  public manoflexioniz: string;
  // Derecha
  public caderaextder: string;
  public caderaflexder: string;
  public caderaabexcder: string;
  public caderaabfleder: string;
  public caderarotextder: string;
  public caderarotintder: string;
  public rodillaextder: string;
  public rodillaextflexder: string;
  public rodillaflexder: string;
  public tobillosoleusder: string;
  public tobillogastocider: string;
  public hombroabdder: string;
  public hombroanteider: string;
  public hombroexorder: string;
  public hombroendorder: string;
  public codoentder: string;
  public codosupinder: string;
  public manodorsifder: string;
  public manoflexionder: string;

  public idmovarticulaciones: number;
}
export class MovilidadArticulacionesModel {
  // primera tabla
  public idmovarticulaciones: number;
  public obscadeext: string;
  public obscaderaflex: string;
  public obscaderaabduext: string;
  public obscaderaabduflex: string;
  public obscaderarotext: string;
  public obscaderarotint: string;
  public obsrodillaextext: string;
  public obsrodillaextflex: string;
  public obsrodillaflex: string;
  public obstobillosoleus: string;
  public obstobillogastoc: string;
  public obshombroabdu: string;
  public obshombroantef: string;
  public obshombroexor: string;
  public obshombroendor: string;
  public obscodoext: string;
  public obscodosupin: string;
  public obsmanodorsif: string;
  public obsmanoflex: string;
  // parte abajo
  public conclusion: string;
  public difIzqDerecha: string;
  public limitaciones: string;
  public obsfinal: string;
  public idfichaterapiafisica: number;
}
export class ResultadoEscalaModel {
  public idresultescala: number;
  public aims: string;
  // public gmfm: string;
  public gmfma: string;
  public gmfmb: string;
  public gmfmc: string;
  public gmfmd: string;
  public gmfme: string;
  public imgunoaims: string;
  public imgdosaims: string;
  public imgtresaims: string;
  public imgcuatroaims: string;
  public imgunogmfm: string;
  public imgdosgmfm: string;
  public gmfcs: string;
  public macs: string;
  public fichaTerapiaFisica: number;
}

export class EspasticidadModel {
  public idespasticidad: number;
  public fespseguno: Date;
  public fespsegdos: Date;


  public unocaderaiz: string;
  public unorodillaflexiz: string;
  public unorodillaextiz: string;
  public unotobilloiz: string;
  public unocodoiz: string;
  public unocaderader: string;
  public unorodillaflexder: string;
  public unorodillaextder: string;
  public unotobilloder: string;
  public unocododer: string;
  public doscaderaiz: string;
  public dosrodillaflexiz: string;
  public dosrodillaextiz: string;
  public dostobilloiz: string;
  public doscodoiz: string;
  public doscaderader: string;
  public dosrodillaflexder: string;
  public dosrodillaextder: string;
  public dostobilloder: string;
  public doscododer: string;
  /**
     * Observaciones
   */
  public obscadera: string;
  public obsrodillaflex: string;
  public obsrodillaext: string;
  public obstobillo: string;
  public obscodo: string;

  public conclusion: string;
  public difizquierda: string;
  public otro: string;
  public fichaTerapiaFisica: number;
}

export class MarchaModel {
  public idmarcha: number;
  public poscabeza: string;
  public poshombro: string;
  public difdistancia: string;
  public anchopiernas: string;
  public balanceobrazos: string;
  public dolor: string;
  public conclusionmarcha: string;
  public obsnivelact: string;
  public concluactiv: string;
  public fichaTerapiaFisica: number;
}

export class PiernaModel {
  public idmarchapierna: number;
  public posicionpierna: number;
  public fsegpierna: Date;
  public numsegpierna: number;

  // Fase de Apoyo
  // Tronco
  public troncoincap: string;
  public troncorotapyo: string;
  // Pelvis
  // public pelvissubeap: string;
  public pelviscontraap: string;
  // Cadera
  public caderarotap: string;
  public caderaabduap: string;
  // Tronco
  public pstroncoap: string;
  // Pelvis
  public pspelvisap: string;
  // Cadera
  public pscaderaap: string;
  // Rodilla
  public psrodillflexap: string;
  public psrodillhiperap: string;
  // Tobillo
  public pstobillantpieap: string;
  public pstobillpieap: string;
  // public pstobilldedosap: string;

  // Fase de Impulsion
  // tronco
  public pftroncoincimp: string;
  public pftroncorotimp: string;
  // pelvis arriba
  public pfpelvissubeimp: string;
  // public pfpelviscontraimp: string;
  // cadera
  public pfcaderarotimp: string;
  public pfcaderaabduimp: string;
  // Tronco
  public pstroncoapimp: string;
  // pelvis
  public pspelvisimp: string;
  // cadera
  public pscaderaimp: string;
  // rodilla
  public psrodillfleximp: string;
  public psrodillhiperimp: string;
  // tobillo
  // public pstobillantepieimp: string;
  // public pstobillpieimp: string;
  public pstobilldedosimp: string;

  public marcha: number;

}
export class DiagnosticoFinalModel {
  public iddiagnosticofinal: number;
  public probpacientcasa: string;
  public probpacienteduc: string;
  public probpacientsocied: string;
  public factpersonales: string;
  public factexteriores: string;
  public probterapbiom: string;
  public probterapmovili: string;
  public probterapfuerza: string;
  public probterapespecif: string;
  public probterapfinal: string;
  public probprincipal: string;
  public idfichaterapiafisica: number;
}
export class PlanTrabajoModel {
  public idplantrabajo: number;
  public probderivados: string;
  public objetivo: string;
  public plandetrabajo: string;
  public fseguimiento: Date;
  public resulseg: string;
  public iddiagnosticofinal: number;
}
