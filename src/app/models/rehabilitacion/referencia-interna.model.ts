export class ReferenciaImtermaModel {
    public idreferinterna: number;
    public motivoreferencia: string;
    public freferencia: Date;
    public idexpediente: number;
    public idarea: number;
}
