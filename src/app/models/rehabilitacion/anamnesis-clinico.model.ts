export class AnamnesisClinicoModel {
  public idanamnesis: number;
  // embarazo
  public edad_embarazo: number;
  public numero_hijos: number;
  public control_medico: String ;
  public num_emb_abortos: string;
  public compli_embarazo: string;
  public infeccion: string;
  public perdidosangre: string;
  public trauma: string;
  public crecimientofeto: string;
  public otrocompli: string;
  public usa_medicamento: string;
  public otrainformacion: string;
  // parto
  public lugar_parto: string;
  public num_meses_parto: number;
  public progreso_parto: string;
  public talla: number;
  public peso: number;
  public compli_parto: string;
  // APGAR
  public apgar: string;
  public color: string;
  public llora: string;
  public movimiento: string;
  public latido_corazon: string;
  public respiracion: string;
// PROGRESO
  public progreso: string;
  // EVALUACIONES MEDICAS
  public eval_medicas: string;
  // DESARROLLO
  public personal_parto: string;
  public compli_postparto: string;
  public idfichaterapiafisica: number;
}
