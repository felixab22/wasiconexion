export class InasistenciaModel {
  public idinasistencia: number;
  public descripcion: string;
  public accionrealizada: string;
  public finasistencia: Date;
  public idfichaterapiafisica: number;
}
