export class MaterialRegModel {
    public idmaterialortopedico: number;
    public nombre: string;
    public cantidad: number;
    public fregistro: Date;
    public modalidad: string;
    public costo: number;
}
export class PrestamoMaterialModel {
    public idprestamomaterial: number;
    public tipooperacion: boolean;
    public finiciooperacion: Date;
    public ffinprestamo: Date;
    public condicionprestamo: string;
    public monto: number;
    public estado_devolucion: boolean;
    public idmaterialortopedico: number;
    public idexpediente: number;
    public codigoboleta?: string;
}
