export class DiganosticoMedicoModel {
  public iddiagnosticomedico: number;
  public diagnostico: string;
  public nombre_medico: string;
  public fdiagnostico: Date;
  public lugar_diagnostico: string;
  public sospecha_diagnostico: string;
  public segun_sospecha: string;
  public evidencia_diagnostico: string;
  public idfichaterapiafisica: number;
}
