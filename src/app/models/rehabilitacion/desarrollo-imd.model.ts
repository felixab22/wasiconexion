// import { Validators } from '@angular/forms';
export class DesarrolloIMDModel {
  public iddesainde: number;
  public idanamnesis: number;
  // Desarrollo
  public des_boca_abajo: string;
  public des_levantar_cabeza: string;
  public des_voltearse: string;
  public des_sentarse: string;
  public des_gatear: string;
  public des_caminar: string;
  // Independencias
  public ind_transferencias: string;
  public ind_lavarse: string;
  public ind_limpio_bano: string;
  public ind_lav_diente: string;
  public ind_peinarse: string;
  public ind_vestirse: string;
  public ind_comer: string;
  public ind_tomar: string;
  public ind_hablar: string;
  public ind_ver: string;
  public ind_oir: string;
  public ind_ayudar_casa: string;
  public ind_escuela: string;
  public ind_comunicacion: string;
  public ind_amigos: string;
  public ind_pasa_tiempos: string;
}
