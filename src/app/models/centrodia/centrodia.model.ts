/* cSpell:disable */
export class CentroDiaModel {
    public idcentrodia: number;
    public fregistro: Date;
    public asistioalbergue: string;
    public terapiaanterior: string;
    public anterioroperacion: string;
    public anteriorenfer: string;
    public medicanterior: string;
    public motivoingreso: string;
    public opinionalberque: string;
    public confianzapersonal: string;
    public refertrabajo: string;

    public caractermenor: string;
    public gustajugar: string;
    public juegpref: string;
    public juegopreferido: string;

    public otros: string;
    public idcuidalim: number;
    // public idexpediente: number;
}
