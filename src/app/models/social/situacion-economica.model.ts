export class SituacionEconomicaModel {
  public id_situacionecon: number;
  public numaportantes: number;
  // entrantes
  public numdependientes: number;
  public madreing: number;
  public padreing: number;
  public abuelosing: number;
  public hermanosing: number;
  public primosing: number;
  public parientesing: number;
  public tiosing: number;
  public noparientesing: number;
  public totalingreso: number;
  // salidas
  public alimentacionegr: number;
  public viviendaegre: number;
  public educacionegre: number;
  public aguaegre: number;
  public luzegre: number;
  public telefonoegre: number;
  public pasajesegre: number;
  public otrosegre: number;
  public totalegre: number;
  public idexpediente: number;
}
