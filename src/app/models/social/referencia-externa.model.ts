export class ReferenciaModel {
  public idrefexterna: number;
  public solicitud_ayuda: string;
  public derivacion: string;
  public idexpediente: number;
}
