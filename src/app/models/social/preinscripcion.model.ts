export class PreInscripcionModel {
  public id_preinscripcion: Number;
  public nombre_paciente: string;
  public condicionpaciente: string;
  public edadpaciente: number;
  public fechanacimiento: Date;
  public diagnosticopaciente: string;
  public fechainscripcionpaciente: Date;
  public nombrepadre: string;
  public nombremadre: string;
  public ocupacion: string;
  public celular1: number;
  public celular2: number;
  public llamada1: Date;
  public llamada2: Date;
  public llamada3: Date;
  public estado: boolean;
  public Observacion: string;
}
