export class VisitaDomiciliarioModel {
  public id_visitadomiciliaria: number;
  public motivovisita: string;
  public entrevistado: string;
  public fechavisita: Date;
  public conclusionesvisita: string;
  public evidenciavisita: File;
  public idexpediente: number;
  public areavisitador: string;
  public personalvisitador: string;
}
