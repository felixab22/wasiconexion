import { ExpedienteModel } from './expediente.model';
export class ApoderadoModel extends ExpedienteModel {
  public id_apoderado: number;
  public tipo_apoderado: string; // papá o mamá
  public nombre_apoderado: string;
  public apellido_apoderado: string; // todo ya en una
  public fnacimiento_apod: Date;
  public dni_apoderado: number;
  public idioma_apoderado: string;
  public estcivil_apoderado: string;
  public nivelinst_apoderado: string;
  public departamento_apoderado: string;
  public provincia_apoderado: string;
  public distrito_apoderado: string;
  public religion_apoderado: string;
  public ocupacion_apoderado: string;
  public celular: number;
  public telefonoFijo: number;
  public condlaboral_apod: string;
  public funcion_apoderado: string;
  public idexpediente: number;
  public horariotrabajo: string;
  public adiccion: string;
  public caracter: string;
}
