export class SituacionPacienteModel {
  public idsituacionpaciente: number;
  public pacienteaceptado: boolean;
  public estado: boolean;
  public idexpediente: number;
}
