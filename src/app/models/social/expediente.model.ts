export class ExpedienteModel {
  public id_expediente: number;
  public fechaapertura: Date;
  public codigohistorial: string;
  public nombre: string;
  public primer_apellido: string;
  public segundoapellido: string;
  public foto: string;
  public sexo: string;
  public fechanacimiento: Date;
  public dni: number;
  public departamentonacimiento: string;
  public provincianacimiento: string;
  public distritonacimiento: string;
  public idioma: string;
  public id_ubigeo: number;
  public edad: number;
  public estadoexpediente: boolean;
  // aumentados
  public lugarnacimiento: string;
  public diagnostico: string;
  public edadmeses: Number;
  public edadanios: number;

}
