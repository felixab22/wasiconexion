export class CompisicionFamiliarModel {
  public id_compfamiliar: number;
  public nombre: string;
  public apellido: string;
  public edad: string;
  public parentesco: string;
  public ocupacion: string;
  public gradoinstr: string;
  public ingresomensual: number;
  public observacion: string;
  public idexpediente: number;
}
