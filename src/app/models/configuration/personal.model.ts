import { AreaModel } from './area.model';
export class PersonalModel extends AreaModel {  
  public user: number;
  public id_personal: number;
  public idarea: number;
  public nombrepersonal: string;
  public apppaterno: string;
  public appmaterno: string;
  public telefono: string;
  public fcumple: string;
  public hobby: string;
  public profesion: string;
  public estado: boolean;
  public correo?:string;
  public cargo?:string;
}
