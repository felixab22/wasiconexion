export class RegisUsuarioModel {
    public id: number;
    public username: string;
    public email: string;
    public password: string;
    public role: number;
    public name: string;
}
