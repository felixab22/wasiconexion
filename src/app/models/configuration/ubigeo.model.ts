export class UbigeoModel {
  public id_ubigeo: number;
  public id_depa: string;
  public id_prov: string;
  public id_dist: string;
}
