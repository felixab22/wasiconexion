export class HorarioModel {
  public idcitaterapia: number;
  public orden: number;
  public hora: string;
  public lunes: string;
  public martes: string;
  public miercoles: string;
  public jueves: string;
  public viernes: string;
  public lunescobro?: number;
  public martescobro?: number;
  public miercolescobro?: number;
  public juevescobro?: number;
  public viernescobro?: number;
  public idpersonal: number;
}
export class AtentionModel {
  public idasistencia: number;
  public paciente: string;
  public dia: string;
  public fecha: string;
  public descripcion: string;
  public tipoasistencia: number;
  public personal: number;
}
export class InasistenciasModel {
  public idnotificacion: number;
  public nombrepaciente: string;
  public finasistencia: string;
  public personal: number;
}