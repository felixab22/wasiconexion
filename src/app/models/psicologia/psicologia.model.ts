export class FichaPsicologicaModel {
    public idfichapsico: number;
    public motivoconsulta: string;
    public tipofamilia: string;
    public relacionpadres: string;
    public climafamiliar: string;
    public antecpsiquiatricos: string;
    public etapaPrenatal: string;
    public etapaPostnatal: string;
    public desarrMotor: string;
    public lenguaje: string;
    public desarrCognitivo: string;
    public desarrPersonal: string;
    public desarrDia: string;
    public historiaEduc: string;
    public diagnPresuntivo: string;
    public recomendaciones: string;
    public idreferenciainterna: number;
    public idexpediente: number;
}
export class EvaluacionFamiliarModel {
    public idevpsicologica: number;
    public tipo_familia: string;
    public fevaluacion: Date;
    public actrealizada: string;
    public trab_psico: string;
    public progreso: string;
    public idfichapsicologia: number;
}
