export class SaludModel {
    public idfichasalud: number;
    public diagmedico: string;
    public diagnutricini: string;
    public secdiscapacidad: string;
    public inicio: string;
    public descencadena: string;
    public observaciones: string;
    // Eval SoEc
    public enferpadecfrec: string;
    public medicadomot: string;
    public sufrealergia: string;
    // Eval SoEc
    public recibeterapia: string;
    public tiempoterap: string;
    public lugarterapia: string;
    // Eval SoEc
    public motivonoatencion: string;
    public idcuidalim: number;
}
export class VisitaIntegralModel {
    public idvisdomintegral: number;
    public numseguimiento: number;
    public fechavisita: Date;
    public edad: number;
    public criteriouno: number;
    public criteriodos: number;
    public criteriotres: number;
    public criteriocuatro: number;
    public puntajetotal: number;
    public idfichasalud: number;
}

export class CuidadoAlimenModel {
    public idcuidalim: number;
    public cuidterceros: string;
    public cuidadomenor: string;
    public apoyofamilia: string;
    public dificcuidado: string;
    public apegofamilia: string;
    public maltratofam: string;
    // centro de dia
    public aspectoscuid: string;
    public horadespierta: string;
    public vecesduerme: string;
    public apoyoact: string;

    public frecalimen: string;
    public cantalimen: string;
    public consisalimen: string;
    public comobebe: string;
    public compAlimen: string;
    public tiempolimenta: string;
    public horaalimen: string;
    // centro de dia
    public alimnotolera: string;

    public tipoalim: string;

    public apoyoalimen: string;
    // ¿en que toma los loquidos de preferencia?
    public tomamaterial: string;
    public liquidogusta: string;
    public vecestoma: string;
    public consistomar: string;
    public posicioncomer: string;
    public vecesdeposic: string;
    public vecesaseo: string;
    public vecesaseobucal: string;
    public otros: string;
    // cento de dia
    public sufreestre: string;
    public idexpediente: number;
}
export class VisitaMedicaModel {
    public idvisitamedica: number;
    public motivovisita: string;
    public fechavisita: Date;
    public estabsalud: string;
    public medicotratante: string;
    public examindicados: string;
    public resultados: string;
    public tratamiento: string;
    public otros: string;
    public idfichasalud: number;
}
export class CampaniasModel {
    public idcampaniaatencion: number;
    public nombrecampania: string;
    public servicio1: string;
    public servicio2: string;
    public servicio3: string;
    public servicio4: string;
    public servicio5: string;
    public servicio6: string;
    public fcampania?: string;
}

export class AtencionCampanModel {
    public idregistrocampania: number;
    public nombrepaciente: string;    
    public fregistro?: string;
    public motivoconsulta?: string;
    public anamnesis?: string;
    public examenfisico?: string;
    public diagnostico?: string;
    public tratamiento?: string;
    public recomendaciones?: string;
    public cita?:  string;
    public edad?:  number;
    public peso?:  number;
    public personal: number;
    public campaniaatencion: number;
}
