export class MedicamentosModel {
    public id_medicamento: number;
    public nombremedic: string;
    public costoventa: string;
    public stock: number;
    public fecha_caducidad: Date;
    public tipo_medicamento: string;
    public caracteristica: string;
    public comprado: string;
    public codigo: string;
    public salida: string;
  }
export class VentaMedicModel {
  public idmedicventa: number;
  public fecha: Date;
  public motivoadmision: string;
  public dosis: string;
  public idmedicamentos: number;
  public idexpediente: number;
}
