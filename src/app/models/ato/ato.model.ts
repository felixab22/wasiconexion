export class ATOModel {
    public idfichaato: number;
    public arthombro: string;
    public artcodo: string;
    public artmuneca: string;
    public pronosupinacion: string;
    public disocmovim: string;
    public falanges: string;
    public caminar: string;
    public atencion: string;
    public memoria: string;
    public resolproblem: string;
    public toleranesfuer: string;
    public toleranfrustra: string;
    public funcionhabla: string;
    public numhermanos: string;
    public asisteCole: string;
    public observaciones: string;
    public idreferenciainterna: number;
    public idexpediente: number;
}
