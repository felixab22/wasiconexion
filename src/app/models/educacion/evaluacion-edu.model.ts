export class EvaluacionEduModel {
    public idevalsemestral: number;
    // cada seis meses valores 0 y 1
    public numevaluacion: number;
    // las clasificaciones
    // valores 0 1 2 3 4 5 6 7
    public tipohabilidad: number;
    public item1: number;
    public item2: number;
    public item3: number;
    public item4: number;
    public item5: number;
    public item6: number;
    public item7: number;
    public item8: number;
    public item9: number;
    public item10: number;
    public item11: number;
    public item12: number;
    public item13: number;
    public item14: number;
    public item15: number;
    public item16: number;
    public item17: number;
    public item18: number;
    // observacion
    public observacion1: string;
    public observacion2: string;
    public observacion3: string;
    public observacion4: string;
    public observacion5: string;
    public observacion6: string;
    public observacion7: string;
    public observacion8: string;
    public observacion9: string;
    public observacion10: string;
    public observacion11: string;
    public observacion12: string;
    public observacion13: string;
    public observacion14: string;
    public observacion15: string;
    public observacion16: string;
    public observacion17: string;
    public observacion18: string;
    public obsinicial: string;
    public obsfinal: string;
    public idfichaeducacion: number;
}
