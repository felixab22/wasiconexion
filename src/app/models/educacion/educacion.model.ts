export class EducacionModel {
    public idfichaeduc: number;
    public personalregistro: string;
    public lenguamaterna: string;
    public gradodominiolm: string;
    public lenguauso: string;
    public gradodominiolu: string;
    public escolactual: string;
    public insteduc: string;
    public fregistro: Date;
    public nombreinfor: string;
    public relacionpaciente: string;

    public motivoentre: string;
    public diagnprevio: string;
    // agrcgados
    public pediatria: string;
    public psicologia: string;
    public genetico: string;
    public neurologia: string;
    public psicopedagogico: string;
    public terapiaocupa: string;

    // public especDiagn: string;

    public otrosdiagn: string;
    public idreferinterna: number;
    public idexpediente: number;
}
export class EscolaridadModel {
    public idescolaridad: number;
    public edadingreso: number;
    public repitecursos: string;
    public cursosrep: string;
    public motivorepite: string;
    public asistiojardin: string;
    public numcolegios: number;
    public motivocambio: string;
    public modensenanza: string;
    public difmatricula: string;
    public nivelactual: string;
    public conductadisr: string;
    public amigos: string;
    public difaprendizaje: string;
    public difparticipar: string;
    public asistereg: string;
    public asisteagrado: string;
    public necesbano: string;
    public apoyofamiliar: string;
    public necesestido: string;
    public necesalim: string;
    public obsercole: string;
    public evaldesempeno: string;
    public respdificultades: string;
    public expectativas: string;
    public respexitos: string;
    public apoyoaprendizaje: string;
    public comentarios: string;
    public idfichaeduc: number;
}

export class DesarrolloSocialModel {
    public iddesarrollosocial: number;
    public relespontanea: string;
    public explicomporta: string;
    public partactgrup: string;
    public pataletas: string;
    public trabindiv: string;
    public lenguaecolalico: string;
    public dificadapt: string;
    public movestereot: string;
    public escolaborador: string;
    public normassociales: string;
    public normasescolar: string;
    public humor: string;

    public reaccionluces: string;
    public reaccionsonido: string;
    public reaccionpersext: string;
    public obsdesocial: string;
    public idfichaeduc: number;
}
export class DesalenguajeModel {
    public iddesalenguaje: number;

    public comunicacion: string;

    public balbucea: string;
    public emitepalabras: string;
    public emitefrases: string;
    public pronunclara: string;
    public vocaliza: string;
    public relataexpe: string;
    public identobjeto: string;
    public respcoherentes: string;
    public identpersonas: string;
    // en html
    public perdidalenguaje: string;
    public comprconcep: string;
    public obslenguaje: string;
    public idfichaeduc: number;
}

export class SaludActualEduModel {
    public idsaludactual: number;
    public vacunasdia: string;
    public epilepsia: string;
    public probcardiaco: string;
    public trantornconduc: string;
    public trastornomotor: string;
    public probbroncoresp: string;
    public enfercontagio: string;
    public transtoremoci: string;
    public paraplejia: string;
    public perdauditiva: string;
    public perdvisual: string;
    public alimentacion: string;
    public peso: string;
    public sufreinsomio: string;
    public sueno: string;
    public duermecompania: string;
    public obsSalud: string;
    public recibntratam: string;
    public horasduerme: string;
    public otro: string;
    public idfichaeduc: number;
}
