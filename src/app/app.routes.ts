import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './core/auth.guard';
import { ComponentsComponent } from './components/components.component';
import { NopagefoundComponent } from './shared/nopagefound/nopagefound.component';
import { BienvenidoComponent } from './components/configuracion/bienvenido/bienvenido.component';
import { ListauserComponent } from './components/configuracion/listauser.component';
import { MaterialesComponent } from './components/rehabilitacion/materiales/materiales.component';
import { UserFormComponent } from './components/configuracion/user-form.component';
import { EvalSocioEconomicaComponent } from './components/social/eval-socio-economica.component';
import { IncripcionComponent } from './components/social/incripcion.component';
import { InscripcionFormComponent } from './components/social/inscripcion-form.component';
import { ListaAptosComponent } from './components/social/lista-aptos.component';
import { HistorialPacienteComponent } from './components/configuracion/historial-paciente.component';
import { ListaMedicamentosComponent } from './components/salud/lista-medicamentos.component';
import { MedicamentosFormsComponent } from './components/salud/medicamentos-forms.component';
import { Estadistica1Component } from './components/configuracion/estadistica1.component';
import { HorarioreahComponent } from './components/configuracion/horarioreah/horarioreah.component';
import { RolPersonalComponent } from './components/configuracion/rol-personal.component';
import { AreaListarComponent } from './components/configuracion/area-listar/area-listar.component';
import { ExpedienteComponent } from './components/social/expediente-list.component';
import { ExpedienteFormComponent } from './components/social/expediente-form.component';
import { ListapComponent } from './components/rehabilitacion/listap/listap.component';
import { ListaaComponent } from './components/ato/listaa/listaa.component';
import { ReferenciadosComponent } from './components/psicologia/referenciados/referenciados.component';
import { ReferenciasaludComponent } from './components/salud/referenciasalud/referenciasalud.component';
import { ListaeComponent } from './components/educacion/listae/listae.component';
import { ListadiaComponent } from './components/centrodia/listadia/listadia.component';
import { ApoderadoComponent } from './components/social/apoderado.component';
import { AbrirExpedienteComponent } from './components/social/abrir-expediente.component';
import { ReferenciapComponent } from './components/rehabilitacion/referenciap/referenciap.component';
import { EvarFisicaFormComponent } from './components/rehabilitacion/evar-fisica-form.component';
import { PsicologiaComponent } from './components/psicologia/psicologia.component';
import { SaludComponent } from './components/salud/salud.component';
import { AtoComponent } from './components/ato/ato.component';
import { EducacionComponent } from './components/educacion/educacion.component';
import { EvaluacionComponent } from './components/educacion/evaluacion/evaluacion.component';
import { CentrodiaComponent } from './components/centrodia/centrodia.component';
import { IniciarAgendaComponent } from './components/configuracion/iniciar-agenda/iniciar-agenda.component';
import { LoginComponent } from './shared/login/login.component';
import { CampaniasComponent } from '@components/salud/companias/campanias.component';
import { ListamatComponent } from '@components/rehabilitacion/listamat/listamat.component';
import { CorreoComponent } from '@components/configuracion/correo.component';
import { PerfilComponent } from '@components/configuracion/perfil/perfil.component';

const appRoutes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  {
    path: 'wasi',
    component: ComponentsComponent,
    canActivate: [AuthGuard],
    children:
      [
        { path: 'bienvenido', component: BienvenidoComponent, data: { titulo: 'Bienvenidos' } },
        { path: 'perfil', component: PerfilComponent, data: { titulo: 'Bienvenidos' } },
        { path: 'ListarUsuario', component: ListauserComponent, data: { titulo: 'Lista de Usuarios' } },
        { path: 'materialesrehabilitacion', component: MaterialesComponent, data: { titulo: 'Prueba' } },
        { path: 'CrearUsuario', component: CorreoComponent, data: { titulo: 'Prueba' } },
        { path: 'EditUsuario/:id', component: UserFormComponent, data: { titulo: 'Prueba' } },
        { path: 'Evaluacionsocioeconomica', component: EvalSocioEconomicaComponent, data: { titulo: 'Prueba' } },
        { path: 'Fichadeinscripcion', component: IncripcionComponent, data: { titulo: 'Prueba' } },
        { path: 'Fichadeinscripcion/CrearFichadeinscripcion', component: InscripcionFormComponent, data: { titulo: 'Prueba' } },
        { path: 'Listaaptossocial', component: ListaAptosComponent, data: { titulo: 'Prueba' } },
        { path: 'historialgeneral', component: HistorialPacienteComponent, data: { titulo: 'Prueba' } },
        { path: 'Listamedicamento', component: ListaMedicamentosComponent, data: { titulo: 'Prueba' } },
        { path: 'Crearmedicamento', component: MedicamentosFormsComponent, data: { titulo: 'Prueba' } },
        { path: 'Campanias', component: CampaniasComponent, data: { titulo: 'Prueba' } },
        { path: 'Gestionamteriales', component: ListamatComponent, data: { titulo: 'Prueba' } },
        { path: 'Estadistica', component: Estadistica1Component, data: { titulo: 'Prueba' } },
        { path: 'horario', component: HorarioreahComponent, data: { titulo: 'Prueba' } },
        { path: 'Iniciar', component: IniciarAgendaComponent, data: { titulo: 'Prueba' } },
        { path: 'Asignarrol', component: RolPersonalComponent, data: { titulo: 'Prueba' } },
        { path: 'Areaslistar', component: AreaListarComponent, data: { titulo: 'Prueba' } },
        { path: 'HistoriaSocial', component: ExpedienteComponent, data: { titulo: 'Prueba' } },
        { path: 'historialgeneral/CrearHistoriaSocial', component: ExpedienteFormComponent, data: { titulo: 'Prueba' } },
        // { path: 'Visitas', component: VisitaComponent, data: { titulo: 'Prueba' } },
        { path: 'Listarehabilitacion', component: ListapComponent, data: { titulo: 'Prueba' } },
        { path: 'listaATO', component: ListaaComponent, data: { titulo: 'Prueba' } },
        { path: 'Refparapsicologia', component: ReferenciadosComponent, data: { titulo: 'Prueba' } },
        { path: 'refparasalud', component: ReferenciasaludComponent, data: { titulo: 'Prueba' } },
        { path: 'Listaeducacion', component: ListaeComponent, data: { titulo: 'Prueba' } },
        { path: 'Listacentrodia', component: ListadiaComponent, data: { titulo: 'Prueba' } },
        { path: 'Paciente/Apoderado/:idExpediente/detalle', component: ApoderadoComponent, data: { titulo: 'Prueba' } },
        { path: 'Listaaptossocial/:historial/aceptarpaciente', component: AbrirExpedienteComponent, data: { titulo: 'Prueba' } },
        { path: 'historialgeneral/:historial/aceptarpaciente', component: AbrirExpedienteComponent, data: { titulo: 'Prueba' } },
        { path: 'Listarehabilitacion/:historial/referenciar', component: ReferenciapComponent, data: { titulo: 'Prueba' } },
        { path: 'Listarehabilitacion/HorarioRehabilitacion/:historial/evaluacionFisica', component: EvarFisicaFormComponent, data: { titulo: 'Prueba' } },
        { path: 'Listaaptossocial/Fichadeinscripcion/CrearFichadeinscripcion/:historial/evaluacionFisica', component: EvarFisicaFormComponent, data: { titulo: 'Prueba' } },
        { path: 'Refparapsicologia/paciente/:historial/evaluacionPsicologica', component: PsicologiaComponent, data: { titulo: 'Prueba' } },
        { path: 'refparasalud/paciente/:historial/evaluacionSalud', component: SaludComponent, data: { titulo: 'Prueba' } },
        { path: 'Listaeducacion/paciente/:historial/evaluacionEducacion', component: EducacionComponent, data: { titulo: 'Prueba' } },
        { path: 'Listaeducacion/paciente/:historial/evaluacionSemestral', component: EvaluacionComponent, data: { titulo: 'Prueba' } },
        { path: 'listaATO/paciente/:historial/evaluacionAto', component: AtoComponent, data: { titulo: 'Prueba' } },
        { path: 'Listacentrodia/paciente/:historial/evaluacionCentrodia', component: CentrodiaComponent, data: { titulo: 'Prueba' } },
        { path: '**', component: NopagefoundComponent }
      ]
  },
];

export const APP_ROUTES = RouterModule.forRoot(appRoutes, { useHash: true });
