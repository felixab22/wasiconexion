import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { URL_SERVICIOS } from '../config/config';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {

    api = URL_SERVICIOS;

  constructor(
    private http: HttpClient,
    public router: Router,
  ) {
  }
  
  attemptAuth(usernameOrEmail: string, password: string): Observable<any> {
    const credentials = {usernameOrEmail: usernameOrEmail, password: password};    
    return this.http.post(this.api + '/api/auth/signin', credentials);
  }
    

}
