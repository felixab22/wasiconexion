import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'guion'
})
export class GuionPipe implements PipeTransform {
  singuion: any;

  transform(value: any, args?: any): any {
    if (value === null || value === undefined) {
      return 'null';
    } else {
      for (let i = 0; i < value.length; i++) {
        value = value.replace(/_/, " ");
      }
      this.singuion = value;
      return this.singuion;
    }

  }

}

