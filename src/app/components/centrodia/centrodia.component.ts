/* cSpell:disable */
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OK } from '../../models/configuration/httpstatus';
declare var swal: any;
import { 
  CentroDiaService, 
  CuidadoAlimenService, 
  ExpedienteService, 
  ApoderadoService,
  AnamnesisClinicoService, 
  EvaluacionFisicaService
 } from '@services/servicio.index';
import { 
  AnamnesisClinicoModel, 
  EvaluacionFisicaModel, 
  ApoderadoModel, 
  CuidadoAlimenModel, 
  ExpedienteModel, 
  CentroDiaModel
 } from '@models/model.index';

@Component({
  selector: 'app-centrodia',
  templateUrl: './centrodia.component.html',
  styleUrls: ['./centrodia.component.scss']
})
export class CentrodiaComponent implements OnInit {
  idterapiafisica: number;
  anamnecis: AnamnesisClinicoModel;
  evaluacionfisica: EvaluacionFisicaModel;
  evalcentrodia: CentroDiaModel;
  padre: ApoderadoModel;
  madre: ApoderadoModel;
  alimentacion: CuidadoAlimenModel;
  public expedientes: Array<ExpedienteModel>;
  VerCargarDesarrollo: boolean;
  verAlimentacion: boolean;
  idalimentacion: number;
  botonHistorial: boolean;
  botonDNI: boolean;
  verxHistorial: boolean;
  verxDNI: boolean;
  MadreExiste: boolean;
  PadreExiste: boolean;
  idExpediente: any;
  verpaciente: boolean;  
  public isValid: boolean = true;  
  private message: string = '';
  verAtender = false;
  constructor(
    private centridiaSrv: CentroDiaService,
    private AlimentSRV: CuidadoAlimenService,
    private expedienteSrv: ExpedienteService,
    private apoderadoSrv: ApoderadoService,
    private SrvAnamanecis: AnamnesisClinicoService,
    private SrvEvalFisica: EvaluacionFisicaService,
    private route: ActivatedRoute,   
  ) {
    if (sessionStorage.getItem('evalcentrodia')) {
      this.evalcentrodia = JSON.parse(sessionStorage.getItem('evalcentrodia'));
    } else {
      this.evalcentrodia = new CentroDiaModel();
    }
    if (sessionStorage.getItem('alimentacion')) {
      this.alimentacion = JSON.parse(sessionStorage.getItem('alimentacion'));
    } else {
      this.alimentacion = new CuidadoAlimenModel();
    }
    if (sessionStorage.getItem('padre')) {
      this.padre = JSON.parse(sessionStorage.getItem('padre'));
    } else {
      this.padre = new ApoderadoModel();
    }
    if (sessionStorage.getItem('madre')) {
      this.madre = JSON.parse(sessionStorage.getItem('madre'));
    } else {
      this.madre = new ApoderadoModel();
    }
  }

  ngOnInit() {
    this.verAlimentacion = false;
    this.VerCargarDesarrollo = false;  
    this.PadreExiste = false;
    this.MadreExiste = false;
    const dato = this.route.snapshot.paramMap.get('historial');    
    this.verpaciente = false;
    this.imprimiExpedientexHistorial(dato);    
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));       
    if(autorization === 7 || autorization === 18  || autorization === 5  || autorization === 4 ) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }
  }  
  
  imprimiExpedientexHistorial(historial) {
    this.expedienteSrv.getBusquedaxHistorial(historial).subscribe(
      (data: any[]) => {
        if (data[0] === undefined) {                 
          swal('Mal!', 'No existe el código de Historial!', 'warning');
        } else {                 
          this.verpaciente = true;
          this.expedientes = data[0];
          this.idExpediente = data[0].id_expediente;
          this.verxHistorial = false;
          this.verxDNI = false;
          this.botonDNI = false;
          this.botonHistorial = false;
          this.imprimirAlimentacion(this.idExpediente);          
          this.imprimirFichaterapiaFisica(this.idExpediente);
          this.imprimirpadres(this.idExpediente);
        }
      });
  }
  imprimirFichaterapiaFisica(dato) {
    this.SrvEvalFisica.getEvaluacionFisicaporexpediente(dato).subscribe(res => {
      if (res[0] !== undefined) {
        this.evaluacionfisica = res[0];
        this.evaluacionfisica.fregistro = res[0].fregistro;
        this.idterapiafisica = this.evaluacionfisica.idfichaterapiafisica;
        this.cargarAnamnecis(this.idterapiafisica);
      } else {
        this.evaluacionfisica = new EvaluacionFisicaModel();        
      }
      if (this.evaluacionfisica.idfichaterapiafisica !== 0) {

      }
    });
  }
  imprimirAlimentacion(dato) {
    this.verAlimentacion = true;
    this.AlimentSRV.getCuidadoAlimenxExpediente(dato).subscribe(res => {
      if (res[0] === undefined) {
        this.alimentacion = new CuidadoAlimenModel();
        console.log('no existe alimentacion');
        } else {        
        this.alimentacion = res[0];        
        this.idalimentacion = res[0].idcuidalim;
        this.imprimirCentrodeDia(this.idalimentacion);
      }
    });
  }
  imprimirpadres(dato) {
    this.apoderadoSrv.getApoderadoxExpediente(dato).subscribe(res => {
      if (res !== undefined) {
        if (res[0].tipo_apoderado === 'Padre') {
          this.PadreExiste = true;
          this.padre = res[0];
          if (res[1] !== undefined) {
            this.MadreExiste = true;
            this.madre = res[1];
          }
        } else {
          this.madre = res[0];
          this.MadreExiste = true;
          if (res[1] !== undefined) {
            this.PadreExiste = true;
            this.padre = res[1];
          }
        }
        if (res[0].tipo_apoderado === 'Apoderado') {
          this.PadreExiste = true;
          this.padre = res[0];
        }
      }
    });
  }
  cargarAnamnecis(dato) {
    this.SrvAnamanecis.getAnamnesisxidfichaterapiafisica(dato).subscribe(res => {
      if (res[0] === undefined) {
        this.VerCargarDesarrollo = false;
        this.anamnecis = new AnamnesisClinicoModel();
      } else {
        this.anamnecis = res[0];
        this.VerCargarDesarrollo = true;

      }
    });
  }
  imprimirCentrodeDia(dato) {

    this.centridiaSrv.getcuidadoYalimentacion(dato).subscribe(res => {
      if (res[0] === undefined) {               
        this.evalcentrodia = new CentroDiaModel();
      } else {
        this.evalcentrodia = res[0]; 
         
      }
    });
  } 
  saveOrUpdateCentrodia(): void {   
    this.evalcentrodia.idcuidalim = this.idalimentacion;
    if (this.isValid) {
      this.centridiaSrv.saveOrUpdateEvalCentroDia(this.evalcentrodia).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Guardado!', 'success');        
        } else {
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }

}

