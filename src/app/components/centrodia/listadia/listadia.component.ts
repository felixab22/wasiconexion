/* cSpell:disable */
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { RefInternaService } from '@services/servicio.index';
import { ReferenciaImtermaModel } from '@models/model.index';
declare var swal: any;
@Component({
  selector: 'app-listadia',
  templateUrl: './listadia.component.html',
  styleUrls: ['./listadia.component.scss']
})
export class ListadiaComponent implements OnInit {

  pacientes: Array<ReferenciaImtermaModel>;
  public p = 5;
  public term: any;
  public binding: any;

  constructor(
    private referenciaSrv: RefInternaService,

  ) {

  }
  ngOnInit() {
    const idpscologia = 7;
    this.imprimirReferenciaEducacion(idpscologia);

  }

  imprimirReferenciaEducacion(dato) {
    this.referenciaSrv.getReferenciaxArea(dato).subscribe(res => {
      if (res !== undefined) {
        this.pacientes = res;
      } else {
        swal("Mal!", "No existe pacientes!", "warning")
      }
    });
  }

  createPDF() {
    var sTable = document.getElementById('idcentrodia').innerHTML;
    var style = "<style>";
    style = style + "table {width: 100%;font: 11px Calibri;}";
    style = style + "table, th, td {border: solid 1px #DDD; border-collapse: collapse;}";
    style = style + "th {color: blue;}";
    style = style + "td {color: black;}";
    style = style + ".none {display: none;}";
    style = style + "padding: 2px 3px;text-align: center;}";
    style = style + "</style>";
    // CREATE A WINDOW OBJECT.
    var win = window.open('', '', 'height=700,width=700');
    win.document.write('<html><head>');
    win.document.write('<title>FICHA DE CENTRO DE DIA</title>');   // <title> FOR PDF HEADER.
    win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');
    win.document.close(); 	// CLOSE THE CURRENT WINDOW.
    win.print();    // PRINT THE CONTENTS.
  }

  exportTableToExcel(tableID, filename = '') {
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

    // Specify file name
    filename = filename ? filename + '.xls' : 'excel_data.xls';

    // Create download link element
    downloadLink = document.createElement("a");

    document.body.appendChild(downloadLink);

    if (navigator.msSaveOrOpenBlob) {
      var blob = new Blob(['\ufeff', tableHTML], {
        type: dataType
      });
      navigator.msSaveOrOpenBlob(blob, filename);
    } else {
      // Create a link to the file
      downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

      // Setting the file name
      downloadLink.download = filename;

      //triggering the function
      downloadLink.click();
    }
  }
}
