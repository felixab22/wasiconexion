/* cSpell:disable */
import { Component, OnInit } from '@angular/core';
import { PrestamoMaterialModel } from '@models/model.index';
import { PrestamoMaterialService } from '@services/servicio.index';
declare var swal: any;
@Component({
  selector: 'app-listamat',
  templateUrl: './listamat.component.html',
  styles: []
})
export class ListamatComponent implements OnInit {
  prestados: Array<PrestamoMaterialModel>;
  alquileres: Array<PrestamoMaterialModel>;
  vendidos: Array<PrestamoMaterialModel>;
  p = 1;
  public isValid: boolean = true;
  private message: string = '';
  public term1: any;
  public term2: any;
  public binding: any;
  prestado: PrestamoMaterialModel;
  constructor(
    private prestadoSrv: PrestamoMaterialService,

  ) {
    this.prestado = new PrestamoMaterialModel();
  }
  ngOnInit() {
    this.imprimirPrestamo();
    this.ImprimirMaterAlquiler();
    this.imprimirVendido();
  }
  imprimirPrestamo() {
    this.prestadoSrv.getMaterGarantia().subscribe(res => {
      if (res !== undefined) {
        this.prestados = res;
      }
    });
  }
  ImprimirMaterAlquiler() {
    this.prestadoSrv.getMaterAlquiler().subscribe(res => {
      if (res !== undefined) {
        this.alquileres = res;
      }
    });
  }
  imprimirVendido() {
    this.prestadoSrv.getMaterVendido().subscribe(res => {
      if (res !== undefined) {
        this.vendidos = res;
      }
    });
  }
  devuelto(prestado: PrestamoMaterialModel) {
    this.prestado = prestado;
    swal({
      title: "Registrar Devolución?",
      text: "la Persona devolvio ya el material prestado!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          this.prestado.estado_devolucion = true;
          this.saveOrUpdatePrestamo();
          swal("Material devuelto!", {
            icon: "success",
          });
        } else {
          swal("Cancelado!");
        }
      });
  }
  saveOrUpdatePrestamo(): void {
    if (this.isValid) {
      this.prestadoSrv.saveOrUpdatePrestamoMaterial(this.prestado).subscribe(res => {
        if (res.responseCode === 200) {
          this.imprimirPrestamo();
          this.prestado = new PrestamoMaterialModel();
        } else {
          swal('Mal!', 'Los campos con * son obligatorios', 'warning');
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }

  createPDF() {
    var sTable = document.getElementById('idlistamateria').innerHTML;
    var style = "<style>";
    style = style + "table {width: 100%;font: 11px Calibri;}";
    style = style + "table, th, td {border: solid 1px #DDD; border-collapse: collapse;}";
    style = style + "th {color: blue;}";
    style = style + "td {color: black;}";
    style = style + ".none {display: none;}";
    style = style + "padding: 2px 3px;text-align: center;}";
    style = style + "</style>";
    // CREATE A WINDOW OBJECT.
    var win = window.open('', '', 'height=700,width=700');
    win.document.write('<html><head>');
    win.document.write('<title>FICHA DE EDUCACION</title>');   // <title> FOR PDF HEADER.
    win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');
    win.document.close(); 	// CLOSE THE CURRENT WINDOW.
    win.print();    // PRINT THE CONTENTS.
  }

  exportTableToExcel(tableID, filename = '') {
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

    // Specify file name
    filename = filename ? filename + '.xls' : 'excel_data.xls';

    // Create download link element
    downloadLink = document.createElement("a");

    document.body.appendChild(downloadLink);

    if (navigator.msSaveOrOpenBlob) {
      var blob = new Blob(['\ufeff', tableHTML], {
        type: dataType
      });
      navigator.msSaveOrOpenBlob(blob, filename);
    } else {
      // Create a link to the file
      downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

      // Setting the file name
      downloadLink.download = filename;

      //triggering the function
      downloadLink.click();
    }
  }

}
