/* cSpell:disable */
import { Component, OnInit, Input } from '@angular/core';
import { OK } from '../../../models/configuration/httpstatus';
import { MarchaModel, PiernaModel } from '@models/model.index';
import { MarchaService, PiernaService } from '@services/servicio.index';
declare var swal: any;

@Component({
  selector: 'app-pierna',
  templateUrl: './pierna.component.html',
  styleUrls: ['./pierna.component.scss']
})
export class PiernaComponent implements OnInit {
  @Input() idterapiafisica: number;
  VerGuardar3: boolean;
  Actualizar3: boolean;
  izbotonoculpatarsegund: boolean;
  izbotonoculpatarprimer: boolean;
  izbotonsegundseg: boolean;
  izbotonprimerseg: boolean;
  botonoculpatarsegund: boolean;
  botonoculpatarprimer: boolean;
  botonsegundseg: boolean;
  botonprimerseg: boolean;
  terceraparteiz: boolean;
  segundaparteiz: boolean;
  primeraparteiz: boolean;
  terceraparte: boolean;
  segundaparte: boolean;
  primeraparte: boolean;
  VerGuardar5: boolean;
  Actualizar5: boolean;
  VerGuardar4: boolean;
  Actualizar4: boolean;
  VerGuardar2: boolean;
  Actualizar2: boolean;
  VerGuardar1: boolean;
  Actualizar1: boolean;
  VerGuardar0: boolean;
  Actualizar0: boolean;
  idmarcha: number;
  verPiernas: boolean;
  marcha: MarchaModel;
  piernade0: PiernaModel;
  piernade1: PiernaModel;
  piernade2: PiernaModel;
  piernaiz0: PiernaModel;
  piernaiz1: PiernaModel;
  piernaiz2: PiernaModel;
  public isValid: boolean = true;
  private message: string = '';
  verSegundoSeguimientoDerecha: boolean;
  verPrimerSeguimientoDerecha: boolean;
  verSegundoSeguimientoIzquierda: boolean;
  verPrimerSeguimientoIzquierda: boolean;
  date = new Date();
  anioactual: string;
  verAtender = false;
  aniosretornos: any;
  constructor(
    private marchaSrv: MarchaService,
    private piernaSrv: PiernaService,
  ) {
    // marcha
    this.anioactual = String(this.date).substr(11, 4);
    if (sessionStorage.getItem('marcha')) {
      this.marcha = JSON.parse(sessionStorage.getItem('marcha'));
    } else {
      this.marcha = new MarchaModel();
    }
    // pierna derecha
    if (sessionStorage.getItem('piernade0')) {
      this.piernade0 = JSON.parse(sessionStorage.getItem('piernade0'));
    } else {
      this.piernade0 = new PiernaModel();
    }
    if (sessionStorage.getItem('piernade1')) {
      this.piernade1 = JSON.parse(sessionStorage.getItem('piernade1'));
    } else {
      this.piernade1 = new PiernaModel();
    }

    if (sessionStorage.getItem('piernade2')) {
      this.piernade2 = JSON.parse(sessionStorage.getItem('piernade2'));
    } else {
      this.piernade2 = new PiernaModel();
    }
    // pierna izquierda
    if (sessionStorage.getItem('piernaiz0')) {
      this.piernaiz0 = JSON.parse(sessionStorage.getItem('piernaiz0'));
    } else {
      this.piernaiz0 = new PiernaModel();
    }
    if (sessionStorage.getItem('piernaiz1')) {
      this.piernaiz1 = JSON.parse(sessionStorage.getItem('piernaiz1'));
    } else {
      this.piernaiz1 = new PiernaModel();
    }
    if (sessionStorage.getItem('piernaiz2')) {
      this.piernaiz2 = JSON.parse(sessionStorage.getItem('piernaiz2'));
    } else {
      this.piernaiz2 = new PiernaModel();
    }
  }

  ngOnInit() {
    this.marchaSrv.getAniosListaMarcha(this.idterapiafisica).subscribe(res => {
      this.aniosretornos = res;
    });
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));
    if (autorization === 12 || autorization === 16 || autorization === 5 || autorization === 4) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }

    this.verPiernas = false;
    this.primeraparte = true;
    this.segundaparte = false;
    this.terceraparte = false;

    this.primeraparteiz = true;
    this.segundaparteiz = false;
    this.terceraparteiz = false;
    this.VerGuardar4 = true;

    this.verPrimerSeguimientoDerecha = false;
    this.verSegundoSeguimientoDerecha = false;
    this.verPrimerSeguimientoIzquierda = false;
    this.verSegundoSeguimientoIzquierda = false;
    this.botonprimerseg = true;
    this.botonsegundseg = false;
    this.botonoculpatarprimer = false;
    this.botonoculpatarsegund = false;
    this.izbotonprimerseg = true;
    this.izbotonsegundseg = false;
    this.izbotonoculpatarprimer = false;
    this.izbotonoculpatarsegund = false;
    this.imprimiMarcha(this.anioactual);
  }

  PrimerSeguimientoDerecha() {
    this.verPrimerSeguimientoDerecha = true;
    this.primeraparte = false;
    this.segundaparte = true;
    this.terceraparte = false;

    this.botonprimerseg = false;
    this.botonsegundseg = true;
    this.botonoculpatarprimer = true;
    this.botonoculpatarsegund = false;
  }
  noprimerpSeguimientoDerecha() {
    this.verPrimerSeguimientoDerecha = false;
    this.primeraparte = true;
    this.segundaparte = false;
    this.terceraparte = false;

    this.botonprimerseg = true;
    this.botonsegundseg = false;
    this.botonoculpatarprimer = false;
    this.botonoculpatarsegund = false;
  }
  segundoSeguimientoDerecha() {
    this.verSegundoSeguimientoDerecha = true;
    this.primeraparte = false;
    this.segundaparte = false;
    this.terceraparte = true;

    this.botonprimerseg = false;
    this.botonsegundseg = false;
    this.botonoculpatarprimer = false;
    this.botonoculpatarsegund = true;
  }
  nosegundoSeguimientoDerecha() {
    this.verSegundoSeguimientoDerecha = false;
    this.primeraparte = false;
    this.segundaparte = true;
    this.terceraparte = false;

    this.botonprimerseg = false;
    this.botonsegundseg = true;
    this.botonoculpatarprimer = true;
    this.botonoculpatarsegund = false;
  }
  // botoenez izquierda
  PrimerSeguimientoIzquierda() {
    this.verPrimerSeguimientoIzquierda = true;
    this.primeraparteiz = false;
    this.segundaparteiz = true;
    this.terceraparteiz = false;

    this.izbotonprimerseg = false;
    this.izbotonsegundseg = true;
    this.izbotonoculpatarprimer = true;
    this.izbotonoculpatarsegund = false;
  }
  noprimerpSeguimientoIzquierda() {
    this.verPrimerSeguimientoIzquierda = false;
    this.primeraparteiz = true;
    this.segundaparteiz = false;
    this.terceraparteiz = false;

    this.izbotonprimerseg = true;
    this.izbotonsegundseg = false;
    this.izbotonoculpatarprimer = false;
    this.izbotonoculpatarsegund = false;
  }
  segundoSeguimientoIzquierda() {
    this.verSegundoSeguimientoIzquierda = true;
    this.primeraparteiz = false;
    this.segundaparteiz = false;
    this.terceraparteiz = true;

    this.izbotonprimerseg = false;
    this.izbotonsegundseg = false;
    this.izbotonoculpatarprimer = false;
    this.izbotonoculpatarsegund = true;
  }
  nosegundoSeguimientoIzquierda() {
    this.verSegundoSeguimientoIzquierda = false;
    this.primeraparteiz = false;
    this.segundaparteiz = true;
    this.terceraparteiz = false;

    this.izbotonprimerseg = false;
    this.izbotonsegundseg = true;
    this.izbotonoculpatarprimer = true;
    this.izbotonoculpatarsegund = false;
  }
  setNewbiometriaanios(valor) {
    const anio = valor.substr(0, 4);
    if (this.anioactual !== anio) {
      this.verAtender = false;
    } else {
      this.verAtender = true;
    }
    this.imprimiMarcha(anio);
  }
  imprimiMarcha(anio) {
    this.marchaSrv.getMarchaxFichaterapiafisica(this.idterapiafisica, anio).subscribe(res => {
      if (res[0] === undefined) {
        this.marcha = new MarchaModel();
      } else {
        this.marcha = res[0];
        this.idmarcha = res[0].idmarcha;
        this.verPiernas = true;
        this.imprimirpiernaizquierda();
        this.imprimirpiernaderecha();
      }
    });
  }
  imprimirpiernaizquierda() {
    this.piernaSrv.getPernaxMarchayPosicion(this.idmarcha, 1).subscribe(res => {
      if (res[0] === undefined) {
        this.piernaiz0 = new PiernaModel();
        this.Actualizar3 = false;
        this.VerGuardar3 = true;
      } else {
        this.piernaiz0 = res[0];
        this.Actualizar3 = true;
        this.VerGuardar3 = false;

        if (res[1] === undefined) {
          this.piernaiz1 = new PiernaModel();
          this.Actualizar4 = false;
          this.VerGuardar4 = true;
        } else {
          this.piernaiz1 = res[1];
          this.Actualizar4 = true;
          this.VerGuardar4 = false;
          if (res[2] === undefined) {
            this.piernaiz2 = new PiernaModel();
            this.Actualizar5 = false;
            this.VerGuardar5 = true;
          } else {
            this.piernaiz2 = res[2];
            this.Actualizar5 = true;
            this.VerGuardar5 = false;
          }
        }
      }
    });
  }

  imprimirpiernaderecha() {
    this.piernaSrv.getPernaxMarchayPosicion(this.idmarcha, 0).subscribe(res => {
      if (res[0] === undefined) {
        this.piernade0 = new PiernaModel();
        this.Actualizar0 = false;
        this.VerGuardar0 = true;
      } else {
        this.piernade0 = res[0];
        this.Actualizar0 = true;
        this.VerGuardar0 = false;

        if (res[1] === undefined) {
          this.piernade1 = new PiernaModel();
          this.Actualizar1 = false;
          this.VerGuardar1 = true;
        } else {
          this.piernade1 = res[1];
          this.Actualizar1 = true;
          this.VerGuardar1 = false;

          if (res[2] === undefined) {
            this.piernade2 = new PiernaModel();
            this.Actualizar2 = false;
            this.VerGuardar2 = true;
          } else {
            this.piernade2 = res[2];
            this.Actualizar2 = true;
            this.VerGuardar2 = false;
          }
        }
      }
    });
  }

  saveOrUpdateMarcha(): void {
    this.marcha.fichaTerapiaFisica = this.idterapiafisica;
    if (this.isValid) {
      this.marchaSrv.saveOrUpdateSegMarcha(this.marcha).subscribe(res => {
        if (res.responseCode === OK) {
          swal("Bien!", "Guardado!", "success");
          this.imprimiMarcha(this.anioactual);
        } else {
          this.message = res.message;
          // this.isValid = false;
          swal('Mal!', 'Los campos con * son obligatorios', 'warning');
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }

  saveOrUpdateMarchaSinActualizar(): void {
    this.marcha.fichaTerapiaFisica = this.idterapiafisica;
    if (this.isValid) {
      this.marchaSrv.saveOrUpdateSegMarcha(this.marcha).subscribe(res => {
        if (res.responseCode === OK) {
          swal("Bien!", "Guardado!", "success");
        } else {
          this.message = res.message;
          // this.isValid = false;
          swal('Mal!', 'Los campos con * son obligatorios', 'warning');
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }

  saveOrUpdatepiernaderecha0(): void {
    this.piernade0.marcha = this.idmarcha;
    this.piernade0.posicionpierna = 0;
    this.piernade0.numsegpierna = 0;
    if (this.isValid) {
      this.piernaSrv.saveOrUpdatePierna(this.piernade0).subscribe(res => {
        if (res.responseCode === OK) {
          swal("Bien!", "Guardado!", "success").then(() => {
            this.saveOrUpdateMarchaSinActualizar();
          });
        } else {
          this.message = res.message;
          // this.isValid = false;
          swal('Mal!', 'Los campos con * son obligatorios', 'warning');
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  saveOrUpdatepiernaderecha1(): void {
    this.piernade1.marcha = this.idmarcha;
    this.piernade1.posicionpierna = 0;
    this.piernade1.numsegpierna = 1;
    if (this.isValid) {
      this.piernaSrv.saveOrUpdatePierna(this.piernade1).subscribe(res => {
        if (res.responseCode === OK) {
          swal("Bien!", "Guardado!", "success").then(() => {
            this.saveOrUpdateMarchaSinActualizar();
          });
        } else {
          this.message = res.message;
          // this.isValid = false;
          swal('Mal!', 'Los campos con * son obligatorios', 'warning');
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  saveOrUpdatepiernaderecha2(): void {
    this.piernade2.marcha = this.idmarcha;
    this.piernade2.posicionpierna = 0;
    this.piernade2.numsegpierna = 2;
    if (this.isValid) {
      this.piernaSrv.saveOrUpdatePierna(this.piernade2).subscribe(res => {
        if (res.responseCode === OK) {
          swal("Bien!", "Guardado!", "success").then(() => {
            this.saveOrUpdateMarchaSinActualizar();
          });
        } else {
          this.message = res.message;
          // this.isValid = false;
          swal('Mal!', 'Los campos con * son obligatorios', 'warning');
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }

  saveOrUpdatepiernaizquierda0(): void {
    this.piernaiz0.marcha = this.idmarcha;
    this.piernaiz0.posicionpierna = 1;
    this.piernaiz0.numsegpierna = 0;
    if (this.isValid) {
      this.piernaSrv.saveOrUpdatePierna(this.piernaiz0).subscribe(res => {
        if (res.responseCode === OK) {
          swal("Bien!", "Guardado!", "success").then(() => {
            this.saveOrUpdateMarchaSinActualizar();
          });
        } else {
          this.message = res.message;
          // this.isValid = false;
          swal('Mal!', 'Los campos con * son obligatorios', 'warning');
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }

  saveOrUpdatepiernaizquierda1(): void {
    this.piernaiz1.marcha = this.idmarcha;
    this.piernaiz1.posicionpierna = 1;
    this.piernaiz1.numsegpierna = 1;
    if (this.isValid) {
      this.piernaSrv.saveOrUpdatePierna(this.piernaiz1).subscribe(res => {
        if (res.responseCode === OK) {
          swal("Bien!", "Guardado!", "success").then(() => {
            this.saveOrUpdateMarchaSinActualizar();
          });
        } else {
          this.message = res.message;
          // this.isValid = false;
          swal('Mal!', 'Los campos con * son obligatorios', 'warning');
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }

  saveOrUpdatepiernaizquierda2(): void {
    this.piernaiz2.marcha = this.idmarcha;
    this.piernaiz2.posicionpierna = 1;
    this.piernaiz2.numsegpierna = 2;
    if (this.isValid) {
      this.piernaSrv.saveOrUpdatePierna(this.piernaiz2).subscribe(res => {
        if (res.responseCode === OK) {
          swal("Bien!", "Guardado!", "success").then(() => {
            this.saveOrUpdateMarchaSinActualizar();
          });
        } else {
          this.message = res.message;
          // this.isValid = false;
          swal('Mal!', 'Los campos con * son obligatorios', 'warning');
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }

}
