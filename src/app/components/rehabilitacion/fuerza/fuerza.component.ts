/* cSpell:disable */
import { Component, OnInit, Input, ViewContainerRef } from '@angular/core';
import { OK } from '../../../models/configuration/httpstatus';
import { SegFuerzaModel, FuerzaObserModel } from '@models/model.index';
import { FuerzaService, SegFuerzaService } from '@services/servicio.index';
declare var swal: any;
@Component({
  selector: 'app-fuerza',
  templateUrl: './fuerza.component.html',
  styleUrls: ['./fuerza.component.scss']
})
export class FuerzaComponent implements OnInit {
  @Input() idterapiafisica: number;
  primeraparte: boolean;
  terceraparte: boolean;
  segundaparte: boolean;
  VerGuardar2: boolean;
  VerGuardar1: boolean;
  VerGuardar0: boolean;
  idfuerza: number;
  verFuerza: boolean;
  verBotoniniliza: boolean;
  obsefuerza: FuerzaObserModel;
  segfuerza0: SegFuerzaModel;
  segfuerza1: SegFuerzaModel;
  segfuerza2: SegFuerzaModel;
  versegundoSeguimientoFuerza: boolean;
  verPrimerSeguimientoFuerza: boolean;
  public isValid: boolean = true;
  private message: string = '';
  boton1: boolean;
  boton2: boolean;
  boton3: boolean;
  boton4: boolean;
  verAtender = false;
  aniosretornos: FuerzaObserModel[];
  date = new Date();
  anioactual: string;
  constructor(
    private fuerzaSrv: FuerzaService,
    private segfuerzaSrv: SegFuerzaService,
  ) {
    this.anioactual = String(this.date).substr(11, 4);
    if (sessionStorage.getItem('obsefuerza')) {
      this.obsefuerza = JSON.parse(sessionStorage.getItem('obsefuerza'));
    } else {
      this.obsefuerza = new FuerzaObserModel();
    }
    // primer seguimiento
    if (sessionStorage.getItem('segfuerza0')) {
      this.segfuerza0 = JSON.parse(sessionStorage.getItem('segfuerza0'));
    } else {
      this.segfuerza0 = new SegFuerzaModel();
    }
    // segundo seguimiento
    if (sessionStorage.getItem('segfuerza1')) {
      this.segfuerza1 = JSON.parse(sessionStorage.getItem('segfuerza1'));
    } else {
      this.segfuerza1 = new SegFuerzaModel();
    }
    // tercer seguimiento
    if (sessionStorage.getItem('segfuerza2')) {
      this.segfuerza2 = JSON.parse(sessionStorage.getItem('segfuerza2'));
    } else {
      this.segfuerza2 = new SegFuerzaModel();
    }
  }

  ngOnInit() {
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));
    if (autorization === 12 || autorization === 16 || autorization === 5 || autorization === 4) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }
    this.fuerzaSrv.getListaAniosFuerza(this.idterapiafisica).subscribe(res => {
      this.aniosretornos = res;
    });
    this.boton1 = true;
    this.boton2 = false;
    this.boton3 = false;
    this.boton4 = false;
    this.verPrimerSeguimientoFuerza = false;
    this.versegundoSeguimientoFuerza = false;
    this.verFuerza = false;
    this.imprimirObserMovilidad(this.anioactual);
    this.primeraparte = true;
    this.segundaparte = false;
    this.terceraparte = false;
  }
  PrimerSeguimientoFuerza() {
    this.verPrimerSeguimientoFuerza = true;
    this.boton1 = false;
    this.boton2 = true;
    this.boton3 = false;
    this.boton4 = true;
    this.primeraparte = false;
    this.segundaparte = true;
    this.terceraparte = false;
  }
  noprimerpSeguimientoFuerza() {
    this.boton1 = true;
    this.boton2 = false;
    this.boton3 = false;
    this.boton4 = false;
    this.verPrimerSeguimientoFuerza = false;
    this.primeraparte = true;
    this.segundaparte = false;
    this.terceraparte = false;
  }
  segundoSeguimientoFuerza() {
    this.boton1 = false;
    this.boton2 = false;
    this.boton3 = true;
    this.boton4 = false;
    this.versegundoSeguimientoFuerza = true;
    this.primeraparte = false;
    this.segundaparte = false;
    this.terceraparte = true;
  }
  nosegundoSeguimientoFuerza() {
    this.boton4 = true;
    this.boton3 = false;
    this.boton2 = true;
    this.versegundoSeguimientoFuerza = false;
    this.primeraparte = false;
    this.segundaparte = true;
    this.terceraparte = false;
  }
  inicializarFuerza() {
    this.saveOrUpdateFuerza();
  }
  setNewbiometriaanios(valor) {
    const anio = valor.substr(0, 4);
    if (this.anioactual !== anio) {
      this.verAtender = false;
    } else {
      this.verAtender = true;
    }
    this.imprimirObserMovilidad(anio);
  }
  imprimirObserMovilidad(anio) {
    this.fuerzaSrv.getFuerzaxidfichaterapia(this.idterapiafisica, anio).subscribe(res => {
      if (res[0] === undefined) {
        this.verFuerza = false;
        this.verBotoniniliza = true;
      } else {
        this.obsefuerza = res[0];
        this.idfuerza = res[0].idfuerza;
        this.verFuerza = true;
        this.verBotoniniliza = false;
        this.imprimirseguimiento(this.idfuerza);
      }
    });
  }
  imprimirseguimiento(idfuerza) {
    this.segfuerzaSrv.getseguiFuerzaxFuerza(idfuerza).subscribe(res => {
      if (res[0] === undefined) {
        this.segfuerza0 = new SegFuerzaModel();
      } else {
        this.segfuerza0 = res[0];
        if (res[1] === undefined) {
          this.segfuerza1 = new SegFuerzaModel();
        } else {
          this.segfuerza1 = res[1];
        } if (res[2] === undefined) {
          this.segfuerza2 = new SegFuerzaModel();
        } else {
          this.segfuerza2 = res[2];

        }
      }
    });
  }
  saveOrUpdateFuerza(): void {
    this.obsefuerza.fichaTerapiaFisica = this.idterapiafisica;
    if (this.isValid) {
      this.fuerzaSrv.saveOrUpdateFuerzaobs(this.obsefuerza).subscribe(res => {
        if (res.responseCode === OK) {
          swal("Bien!", "Guardado!", "success");
          // this.imprimirseguimiento();
          this.imprimirObserMovilidad(this.anioactual);
        } else {
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  saveOrUpdateFuerzasinrelogear(): void {
    this.obsefuerza.fichaTerapiaFisica = this.idterapiafisica;
    if (this.isValid) {
      this.fuerzaSrv.saveOrUpdateFuerzaobs(this.obsefuerza).subscribe(res => {
        if (res.responseCode === OK) {
          swal("Bien!", "Guardado!", "success");

        } else {
          swal('Mal!', 'Los campos con * son obligatorios', 'warning');
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }

  saveOrUpdateSegFuerza0(): void {
    this.segfuerza0.fuerza = this.idfuerza;
    this.segfuerza0.numseguimientofuerza = 0;
    if (this.isValid) {
      this.segfuerzaSrv.saveOrUpdateSegFuerza(this.segfuerza0).subscribe(res => {
        if (res.responseCode === OK) {
          // swal("Bien!", "Guardado!", "success");        
          this.saveOrUpdateFuerzasinrelogear();
        } else {
          swal('Mal!', 'Los campos con * son obligatorios', 'warning');
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  saveOrUpdateSegFuerza1(): void {
    this.segfuerza1.fuerza = this.idfuerza;
    this.segfuerza1.numseguimientofuerza = 1;
    if (this.isValid) {
      this.segfuerzaSrv.saveOrUpdateSegFuerza(this.segfuerza1).subscribe(res => {
        if (res.responseCode === OK) {
          swal("Bien!", "Guardado Fuerza!", "success");

          this.saveOrUpdateFuerzasinrelogear();
        } else {
          swal('Mal!', 'Los campos con * son obligatorios', 'warning');
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  saveOrUpdateSegFuerza2(): void {
    this.segfuerza2.fuerza = this.idfuerza;
    this.segfuerza2.numseguimientofuerza = 2;
    if (this.isValid) {
      this.segfuerzaSrv.saveOrUpdateSegFuerza(this.segfuerza2).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Guardado!', 'success').then(() => {
            this.saveOrUpdateFuerzasinrelogear();
          });


        } else {
          swal('Mal!', 'Los campos con * son obligatorios', 'warning');
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
}
