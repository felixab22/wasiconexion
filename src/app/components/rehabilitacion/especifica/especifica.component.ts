/* cSpell:disable */
import { Component, OnInit, Input, ViewContainerRef } from '@angular/core';
import { OK } from '../../../models/configuration/httpstatus';
import { EspasticidadModel } from '@models/model.index';
import { EspasticidadService } from '@services/servicio.index';
declare var swal: any;
@Component({
  selector: 'app-especifica',
  templateUrl: './especifica.component.html',
  styleUrls: ['./especifica.component.scss']
})
export class EspecificaComponent implements OnInit {
  @Input() idterapiafisica: number;
  espasticidad: EspasticidadModel;
  verPrimerSeguimientoEspecifica: boolean;
  public isValid: boolean = true;
  private message: string = '';
  verprimerboton: boolean;
  verAtender = false;
  anioactual: string;
  date = new Date();
  aniosretornos: any;
  constructor(
    private espastiSrv: EspasticidadService,
  ) {
    this.anioactual = String(this.date).substr(11, 4);
    if (sessionStorage.getItem('espasticidad')) {
      this.espasticidad = JSON.parse(sessionStorage.getItem('espasticidad'));
    } else {
      this.espasticidad = new EspasticidadModel();
    }
  }
  ngOnInit() {
    this.espastiSrv.getAniosListaEspasticidad(this.idterapiafisica).subscribe(res => {
      this.aniosretornos = res;
    });
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));
    if (autorization === 12 || autorization === 16 || autorization === 5 || autorization === 4) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }
    this.verprimerboton = true;
    this.imprimirEspasticidad(this.anioactual);
    this.verPrimerSeguimientoEspecifica = false;
  }
  PrimerSeguimientoEspecifica() {
    this.verprimerboton = false;
    this.verPrimerSeguimientoEspecifica = true;
  }
  noprimerpSeguimientoEspecifica() {
    this.verprimerboton = true;
    this.verPrimerSeguimientoEspecifica = false;
  }
  setNewbiometriaanios(valor) {
    const anio = valor.substr(0, 4);
    if (this.anioactual !== anio) {
      this.verAtender = false;
    } else {
      this.verAtender = true;
    }
    this.imprimirEspasticidad(anio);
  }

  imprimirEspasticidad(anio) {
    this.espastiSrv.getEspasticidadxFichaterapiafisica(this.idterapiafisica, anio).subscribe(res => {
      if (res[0] === undefined) {
        this.espasticidad = new EspasticidadModel();
      } else {
        this.espasticidad = res[0];
      }
    });
  }
  saveOrUpdateEspasticidad(): void {
    this.espasticidad.fichaTerapiaFisica = this.idterapiafisica;
    if (this.isValid) {
      this.espastiSrv.saveOrUpdateSegEspasticidad(this.espasticidad).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Guardado!', 'success').then(() => {
            this.imprimirEspasticidad(this.anioactual);
          });
        } else {
          swal("Mal!", "Los campos con * son obligatorios!", "warning");
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
}
