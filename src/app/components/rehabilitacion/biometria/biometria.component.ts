/* cSpell:disable */
import { Component, OnInit, Input } from '@angular/core';
import { OK } from '../../../models/configuration/httpstatus';
import { BiometriaModel } from '@models/model.index';
import { BiometriaService } from '@services/servicio.index';
declare var swal: any;

@Component({
  selector: 'app-biometria',
  templateUrl: './biometria.component.html',
  styleUrls: ['./biometria.component.scss']
})
export class BiometriaComponent implements OnInit {
  versegundoboton: boolean;
  verprimerboton: boolean;

  @Input() idterapiafisica: number;
  biometria: BiometriaModel;
  versegundoBiometria: boolean;
  verPrimerBiometria: boolean;
  public isValid: boolean = true;
  private message: string = '';
  resultado1: number;
  resultado2: number;
  resultado3: number;
  noverprimerboton: boolean;
  noversegundoboton: boolean;
  verAtender = false;
  anioactual: string;
  date = new Date();
  aniosretornos: any[];
  constructor(
    private biometriaSRV: BiometriaService,
  ) {
    if (sessionStorage.getItem('biometria')) {
      this.biometria = JSON.parse(sessionStorage.getItem('biometria'));
    } else {
      this.biometria = new BiometriaModel();
    }
    this.anioactual = String(this.date).substr(11, 4);
  }
  ngOnInit() {
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));
    if (autorization === 12 || autorization === 16 || autorization === 5 || autorization === 4) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }

    const dato = this.idterapiafisica;
    this.noverprimerboton = false;
    this.noversegundoboton = false;
    this.verPrimerBiometria = false;
    this.versegundoBiometria = false;
    this.verprimerboton = true;
    this.versegundoboton = false;
    this.imprimirBiometria(dato);
    this.biometriaSRV.getlistadeaniosBiomatria(dato).subscribe(res => {
      this.aniosretornos = res;
    })

  }
  verprimerSeguimientoBiometria() {
    this.noverprimerboton = true;
    this.verPrimerBiometria = true;
    this.verprimerboton = false;
    this.versegundoboton = true;
    this.noversegundoboton = false;
  }
  noverprimerSeguimientoBiometria() {
    this.versegundoboton = false;
    this.noverprimerboton = false;
    this.verPrimerBiometria = false;
    this.verprimerboton = true;
    this.noversegundoboton = false;
  }
  versegundoSeguimientoBiometria() {
    this.noverprimerboton = false;
    this.versegundoBiometria = true;
    this.versegundoboton = false;
    this.noversegundoboton = true;
  }
  noversegundoSeguimientoBiometria() {
    this.noverprimerboton = true;
    this.versegundoBiometria = false;
    this.versegundoboton = true;
    this.noversegundoboton = false;
  }
  setNewbiometriaanios(valor) {
    const anio = valor.substr(0, 4);
    if (this.anioactual !== anio) {
      this.verAtender = false;
    } else {
      this.verAtender = true;
    }
    this.imprimirBiometria(this.idterapiafisica, anio);
  }
  imprimirBiometria(dato, anio = this.anioactual) {
    this.resultado1 = 0;
    this.resultado2 = 0;
    this.resultado3 = 0;
    this.biometriaSRV.getEvaluacionBiometrixterapiafisica(dato, anio).subscribe(res => {
      if (res[0] === undefined) {
        this.biometria = new BiometriaModel();
        this.sumar1();
        this.sumar2();
        this.sumar3();
      } else {
        this.biometria = res[0];
      }
    });
  }
  sumar1() {
    if (this.biometria.longitudizq !== null && this.biometria.longitudder) {
      // tslint:disable-next-line:radix
      this.resultado1 = parseInt(this.biometria.longitudizq) - parseInt(this.biometria.longitudder);
      if (this.resultado1 >= 0) {
        this.biometria.diferencia = String(this.resultado1);
      } else {
        this.biometria.diferencia = String(this.resultado1 * -1);
      }
    }
  }
  sumar2() {
    if (this.biometria.longitudizqdos !== null && this.biometria.longitudderdos) {
      // tslint:disable-next-line:radix
      this.resultado2 = parseInt(this.biometria.longitudizqdos) - parseInt(this.biometria.longitudderdos);
      if (this.resultado2 >= 0) {
        this.biometria.diferenciados = String(this.resultado2);
      } else {
        this.biometria.diferenciados = String(this.resultado2 * -1);
      }
    }
  }
  sumar3() {
    if (this.biometria.longitudizqtres !== null && this.biometria.longituddertres) {
      this.resultado3 = parseInt(this.biometria.longitudizqtres) - parseInt(this.biometria.longituddertres);
      if (this.resultado3 >= 0) {
        this.biometria.diferenciatres = String(this.resultado3);
      } else {
        this.biometria.diferenciatres = String(this.resultado3 * -1);
      }
    }
  }
  saveOrUpdateBiometria(): void {
    this.biometria.idfichaterapiafisica = this.idterapiafisica;
    if (this.isValid) {
      this.biometriaSRV.saveOrUpdateBiometria(this.biometria).subscribe(res => {
        if (res.responseCode === OK) {
          swal("Bien!", "Guardado!", "success");
        } else {
          this.message = res.message;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }

}
