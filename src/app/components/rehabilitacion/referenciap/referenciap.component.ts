/* cSpell:disable */
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OK } from '../../../models/configuration/httpstatus';
declare var swal: any;

import {
  ExpedienteService,
  AreaService,
  RefInternaService
} from '@services/servicio.index';
import { ReferenciaImtermaModel, AreaModel } from '@models/model.index';
@Component({
  selector: 'app-referenciap',
  templateUrl: './referenciap.component.html',
  styles: []
})
export class ReferenciapComponent implements OnInit {
  areaselect: any;
  referencias: Array<ReferenciaImtermaModel>;
  public isValid: boolean = true;
  private message: string = '';
  verpaciente: boolean;
  idExpediente: number;
  paciente: any;
  areas: Array<AreaModel>;
  referencia1: ReferenciaImtermaModel;
  validado: boolean;
  constructor(
    private route: ActivatedRoute,
    private expedienteSrv: ExpedienteService,
    private areaSrv: AreaService,
    private referenciaSrv: RefInternaService
  ) {
    if (sessionStorage.getItem('referencia1')) {
      this.referencia1 = JSON.parse(sessionStorage.getItem('referencia1'));
    } else {
      this.referencia1 = new ReferenciaImtermaModel();
    }
  }

  ngOnInit() {
    const dato = this.route.snapshot.paramMap.get('historial');
    this.imprimiExpedientexHistorial(dato);
    this.imprimirArea();
    this.verpaciente = false;
    this.validado = true;
  }
  imprimiExpedientexHistorial(historial1) {
    this.expedienteSrv.getBusquedaxHistorial(historial1).subscribe(
      (data: any[]) => {
        if (data !== undefined) {
          this.paciente = data[0];
          this.idExpediente = data[0].id_expediente;
          // this.verpaciente = true;
          this.listarReferenciaxExpediente();
        }
      });
  }
  imprimirArea() {
    this.areaSrv.getAreareferenciados().subscribe(dato => {
      if (dato !== undefined) {
        this.areas = dato;
      }
    });
  }

  onSelectionChangearea(area: AreaModel) {
    this.referencia1.idarea = Number(area.id_area);
    this.referencia1.idexpediente = this.idExpediente;
    this.validado = false;
  }
  onEditreferencia(ref: ReferenciaImtermaModel) {
    this.verpaciente = true;
    this.referencia1 = ref;
    console.log(this.areas);
    for (let i = 1; i < this.areas.length; i++) {
      const element = this.areas[i];
      if (this.areas[i].id_area === ref.idarea) {
        this.areaselect = this.areas[i].descripcionarea;
        console.log(this.areaselect);
      }
    }
  }
  eliminarrefe(id: number) {
    if (confirm('¿Está seguro que desea eliminar?')) {
      this.referenciaSrv.deleteRefeInterna(id);
      swal('Eliminado!', 'Guardado!', 'info');
      this.referencias = this.referencias.filter(u => id !== u.idreferinterna);
    }
  }
  saveOrUpdateReferencia1(): void {
    if (this.isValid) {
      this.referenciaSrv.saveOrUpdateRefInterna(this.referencia1).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Referencia guardado!', 'success');
          this.referencia1 = new ReferenciaImtermaModel();
          this.listarReferenciaxExpediente();
          this.verpaciente = false;
        } else {
          this.message = res.message;
          // this.isValid = false;
          swal('Mal!', 'Puede ya existir la referencia', 'warning');
          // window.location.reload();
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }

  listarReferenciaxExpediente() {
    this.referenciaSrv.getReferenciaxExpediente(this.idExpediente).subscribe(res => {
      if (res !== undefined) {
        this.referencias = res;
      }
    });
  }
  nuevoferencia() {
    this.verpaciente = true;
  }
  cancelar() {
    this.verpaciente = false;
    this.referencia1 = new ReferenciaImtermaModel();
    // this.referencia1.motivoreferencia = '';
  }

}
