/* cSpell:disable */
import { Component, OnInit, Input, ViewContainerRef } from '@angular/core';
import { OK } from '../../models/configuration/httpstatus';
import {
  MovilidadArticulacionesModel,
  SegMovilidadModel,
  ExpedienteModel
} from '@models/model.index';
import {
  MovilidadArticulacionesService,
  SegMovilidadArtiService
} from '@services/servicio.index';
declare var swal: any;

@Component({
  selector: 'app-mov-articulaciones',
  templateUrl: './mov-articulaciones.component.html',
  styles: []
})
export class MovArticulacionesComponent implements OnInit {
  VerGuardar2: boolean;
  Actualizar2: boolean;
  VerGuardar1: boolean;
  Actualizar1: boolean;
  terceraparte: boolean;
  segundaparte: boolean;
  primeraparte: boolean;
  Actualizar0: boolean;
  VerGuardar0: boolean;
  idmovimiento: number;
  verBotoniniliza: boolean;
  verMovilidad: boolean;
  obsmovilidad: MovilidadArticulacionesModel;
  segmovilidad0: SegMovilidadModel;
  segmovilidad1: SegMovilidadModel;
  segmovilidad2: SegMovilidadModel;
  public isValid: boolean = true;
  private message: string = '';
  @Input() expedientes: ExpedienteModel;
  @Input() idterapiafisica: number;
  versegundoSeguimiento: boolean;
  verPrimerSeguimiento: boolean;
  noverprimerboton: boolean;
  noversegundoboton: boolean;
  verAtender = false;
  aniosretornos: any;
  anioactual: string;
  date = new Date();
  constructor(
    private movilidadSrv: MovilidadArticulacionesService,
    private segmovimientoSrv: SegMovilidadArtiService,
  ) {
    this.anioactual = String(this.date).substr(11, 4);
    if (sessionStorage.getItem('obsmovilidad')) {
      this.obsmovilidad = JSON.parse(sessionStorage.getItem('obsmovilidad'));
    } else {
      this.obsmovilidad = new MovilidadArticulacionesModel();
    }
    // primer seguimiento
    if (sessionStorage.getItem('segmovilidad0')) {
      this.segmovilidad0 = JSON.parse(sessionStorage.getItem('segmovilidad0'));
    } else {
      this.segmovilidad0 = new SegMovilidadModel();
    }
    // segundo seguimiento
    if (sessionStorage.getItem('segmovilidad1')) {
      this.segmovilidad1 = JSON.parse(sessionStorage.getItem('segmovilidad1'));
    } else {
      this.segmovilidad1 = new SegMovilidadModel();
    }
    // tercer seguimiento
    if (sessionStorage.getItem('segmovilidad2')) {
      this.segmovilidad2 = JSON.parse(sessionStorage.getItem('segmovilidad2'));
    } else {
      this.segmovilidad2 = new SegMovilidadModel();
    }
  }

  ngOnInit() {
    this.movilidadSrv.getlistaAniosMovilidad(this.idterapiafisica).subscribe(res => {
      this.aniosretornos = res;
    });
    this.verPrimerSeguimiento = false;
    this.versegundoSeguimiento = false;
    this.verMovilidad = false;
    this.imprimirObserMovilidad(this.anioactual);
    this.primeraparte = true;
    this.VerGuardar0 = true;
    this.segundaparte = false;
    this.terceraparte = false;
    this.VerGuardar1 = true;
    this.VerGuardar2 = true;
    this.noverprimerboton = false;
    this.noversegundoboton = false;
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));
    if (autorization === 12 || autorization === 16 || autorization === 5 || autorization === 4) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }

  }
  setNewbiometriaanios(valor) {
    const anio = valor.substr(0, 4);
    if (this.anioactual !== anio) {
      this.verAtender = false;
    } else {
      this.verAtender = true;
    }
    this.imprimirObserMovilidad(anio);
  }

  verprimerSeguimientoMovimiento() {
    this.noversegundoboton = false;
    this.noverprimerboton = true;
    this.verPrimerSeguimiento = true;
    this.VerGuardar0 = false;
    this.primeraparte = false;
    this.segundaparte = true;
    this.terceraparte = false;
  }
  noverprimerpSeguimientoMovimiento() {
    this.noversegundoboton = false;
    this.noverprimerboton = false;
    this.verPrimerSeguimiento = false;
    this.VerGuardar0 = true;
    this.primeraparte = true;
    this.segundaparte = false;
    this.terceraparte = false;
  }
  versegundoSeguimientoMovimiento() {
    this.noversegundoboton = true;
    this.noverprimerboton = false;
    this.versegundoSeguimiento = true;
    this.VerGuardar0 = false;
    this.primeraparte = false;
    this.segundaparte = false;
    this.terceraparte = true;
  }
  noversegundoSeguimientoMovimiento() {
    this.noversegundoboton = false;
    this.noverprimerboton = true;
    this.versegundoSeguimiento = false;
    this.VerGuardar0 = true;
    this.segundaparte = true;
    this.primeraparte = true;
    this.terceraparte = false;
  }
  inicializarMovilida() {
    this.saveOrUpdateMovilidad();
  }

  imprimirObserMovilidad(anios) {
    this.movilidadSrv.getMovilidadxidfichaterapiafisica(this.idterapiafisica, anios).subscribe(res => {
      if (res[0] === undefined) {
        this.verMovilidad = false;
        this.verBotoniniliza = true;
      } else {
        this.obsmovilidad = res[0];
        this.idmovimiento = res[0].idmovarticulaciones;
        this.verMovilidad = true;
        this.verBotoniniliza = false;
        this.imprimirSegmito();
      }
    });
  }

  imprimirSegmito() {
    this.segmovimientoSrv.getMovilidadxidmovilidadArtia(this.idmovimiento).subscribe(res => {
      if (res[0] === undefined) {
        this.segmovilidad0 = new SegMovilidadModel();
      } else {
        this.segmovilidad0 = res[0];
        this.Actualizar0 = true;
        this.VerGuardar0 = false;

        if (res[1] === undefined) {
          this.segmovilidad1 = new SegMovilidadModel();
          this.Actualizar1 = false;
          this.VerGuardar1 = true;
        } else {
          this.segmovilidad1 = res[1];
          this.Actualizar1 = true;
          this.VerGuardar1 = false;

        } if (res[2] === undefined) {
          this.segmovilidad2 = new SegMovilidadModel();
          this.Actualizar2 = false;
          this.VerGuardar2 = true;
        } else {
          this.segmovilidad2 = res[2];
          this.Actualizar2 = true;
          this.VerGuardar2 = false;
        }
      }
    });
  }

  saveOrUpdateMovilidad(): void {
    this.obsmovilidad.idfichaterapiafisica = this.idterapiafisica;
    if (this.isValid) {
      this.movilidadSrv.saveOrUpdateMovilidad(this.obsmovilidad).subscribe(res => {
        if (res.responseCode === 200) {
          if (this.obsmovilidad.obscadeext === null) {
            swal("Bien!", "Guardado!", "success").then(() => {
              this.imprimirObserMovilidad(this.anioactual);
              this.verMovilidad = true;
            });
          } else {
            this.imprimirObserMovilidad(this.anioactual);
            this.verMovilidad = true;
            // this.toastr.info('Guardado Observaciones', 'Bien!!');
          }
        } else {
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }

  saveOrUpdateSegMovilidad0(): void {
    this.segmovilidad0.idmovarticulaciones = this.idmovimiento;
    this.segmovilidad0.numseguimiento = 0;
    if (this.isValid) {
      this.segmovimientoSrv.saveOrUpdateMovilidad(this.segmovilidad0).subscribe(res => {
        if (res.responseCode === OK) {
          swal("Bien!", "Guardado!", "success").then(() => {
            this.saveOrUpdateMovilidad();
          });
        } else {
          swal('Mal!', 'Los campos con * son obligatorios', 'warning');
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }

  saveOrUpdateSegMovilidad1(): void {
    this.segmovilidad1.idmovarticulaciones = this.idmovimiento;
    this.segmovilidad1.numseguimiento = 1;
    if (this.isValid) {
      this.segmovimientoSrv.saveOrUpdateMovilidad(this.segmovilidad1).subscribe(res => {
        if (res.responseCode === OK) {
          swal("Bien!", "Guardado!", "success").then(() => {
            this.saveOrUpdateMovilidad();
          });
        } else {
          swal('Mal!', 'Los campos con * son obligatorios', 'warning');
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  saveOrUpdateSegMovilidad2(): void {
    this.segmovilidad2.idmovarticulaciones = this.idmovimiento;
    this.segmovilidad2.numseguimiento = 2;
    if (this.isValid) {
      this.segmovimientoSrv.saveOrUpdateMovilidad(this.segmovilidad2).subscribe(res => {
        if (res.responseCode === OK) {
          swal("Bien!", "Guardado!", "success").then(() => {
            this.saveOrUpdateMovilidad();
          });
        } else {
          swal('Mal!', 'Los campos con * son obligatorios', 'warning');
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
}
