/* cSpell:disable */
import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Input } from '@angular/core';
import { DiganosticoMedicoModel } from '@models/model.index';
import { DiganosticoMedicoService } from '@services/servicio.index';
declare var swal: any;

@Component({
  selector: 'app-diagnostico',
  templateUrl: './diagnostico.component.html',
  styles: []
})
export class DiagnosticoComponent implements OnInit { 
  @Input() idterapiafisica: number;
  diagnostico: DiganosticoMedicoModel; 
  public isValid: boolean = true;
  private message: string = '';
  guardado: boolean;
  verAtender = false;
  constructor(   
    private servicioDiagnostico: DiganosticoMedicoService,    
  ) {
   
    if (sessionStorage.getItem('diagnostico')) {
      this.diagnostico = JSON.parse(sessionStorage.getItem('diagnostico'));
    } else {
      this.diagnostico = new DiganosticoMedicoModel();
    }
  }
  ngOnInit() {
    this.guardado = false;
    this.getDiagnosticoMedico();
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));       
    if(autorization === 12 || autorization === 16  || autorization === 5  || autorization === 4 ) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }
  }

  getDiagnosticoMedico() {
    this.servicioDiagnostico.getDiganosticoMedicoxfichaterapia(this.idterapiafisica).subscribe(res => {
      if (res[0] === undefined) {
        this.diagnostico = new DiganosticoMedicoModel();
      } else {
        this.diagnostico = res[0];        
      }
    });
  }
  handleFileSelect(evt) {
    const files = evt.target.files;
    const file = files[0];

    if (files && file) {
      const reader = new FileReader();
      reader.onload = this.hendleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  hendleReaderLoaded(readerEvt) {
    const binaryString = readerEvt.target.result;
    this.diagnostico.evidencia_diagnostico = btoa(binaryString);
  }

  saveOrUpdateDiagnostico(): void {
    this.diagnostico.idfichaterapiafisica = this.idterapiafisica;
    if (this.isValid) {
      this.servicioDiagnostico.saveOrUpdateDiganosticoMedico(this.diagnostico).subscribe(res => {
        if (res.responseCode === 200) {
          swal("Bien!", "Guardado!", "success").then(() => {
            this.getDiagnosticoMedico();
          });  
        } else {
          this.message = res.message;
          // this.isValid = false;
        }
        console.log(res);
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
}
