/* cSpell:disable */
import { Component, OnInit, Input } from '@angular/core';
import { OK } from '../../models/configuration/httpstatus';
import { 
  AnamnesisClinicoService,
  DesarrolloIMDService, 
  AnamnecisFamiliarService, 
  ReferenciaService 
} from '@services/servicio.index';
import { 
  ExpedienteModel, 
  ReferenciaModel, 
  AnamnesisFamiliarModel, 
  AnamnesisClinicoModel, 
  DesarrolloIMDModel
 } from '@models/model.index';
 declare var swal: any;

@Component({
  selector: 'app-anamnecis',
  templateUrl: './anamnecis.component.html',
  styles: []
})
export class AnamnecisComponent implements OnInit {
  @Input() expedientes: ExpedienteModel;
  @Input() idterapiafisica: number;
  referencia: ReferenciaModel;
  Anamnesisfamiliar: AnamnesisFamiliarModel;
  verBotonguardar: boolean;
  iddesarrollo: number;
  verBotonEditar: boolean;
  VerCargarDesarrollo: boolean;
  idanamnecis: number;
  anamnecis: AnamnesisClinicoModel;
  desarrollo: DesarrolloIMDModel;
  listaAnanmecis: Array<AnamnesisClinicoModel>;
  public isValid: boolean = true; 
  private message: string = '';
  guardado: boolean;
  guardado2: boolean;
  guardado1: boolean;
  verAtender = false;
  constructor(
    private SrvAnamanecis: AnamnesisClinicoService,
    private desarrolloSrv: DesarrolloIMDService,
    private anamnesifamiliarSrv: AnamnecisFamiliarService,
    private referenciaSrv: ReferenciaService,    
  ) {  
    if (sessionStorage.getItem('anamnecis')) {
      this.anamnecis = JSON.parse(sessionStorage.getItem('anamnecis'));
    } else {
      this.anamnecis = new AnamnesisClinicoModel();
    }
    if (sessionStorage.getItem('desarrollo')) {
      this.desarrollo = JSON.parse(sessionStorage.getItem('desarrollo'));
    } else {
      this.desarrollo = new DesarrolloIMDModel();
    }
    if (sessionStorage.getItem('Anamnesisfamiliar')) {
      this.Anamnesisfamiliar = JSON.parse(sessionStorage.getItem('desarrollo'));
    } else {
      this.Anamnesisfamiliar = new AnamnesisFamiliarModel();
    }
    if (sessionStorage.getItem('referencia')) {
      this.referencia = JSON.parse(sessionStorage.getItem('referencia'));
    } else {
      this.referencia = new ReferenciaModel();
    }
  }
  ngOnInit() {
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));       
    if(autorization === 12 || autorization === 16  || autorization === 5  || autorization === 4 ) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }
    this.guardado = false;
    this.guardado1 = false;
    this.guardado2 = false;
    this.VerCargarDesarrollo = false;
    this.verBotonEditar = false;
    this.verBotonguardar = true;
    this.cargarAnamnecis();
  }
  cargarAnamnecis() {
    this.SrvAnamanecis.getAnamnesisxidfichaterapiafisica(this.idterapiafisica).subscribe(res => {
      if (res[0] === undefined) {
        this.VerCargarDesarrollo = false;
        this.anamnecis = new AnamnesisClinicoModel();
      } else {
        this.anamnecis = res[0];
        this.idanamnecis = this.anamnecis.idanamnesis;
        this.cargarReferencia();
        this.VerCargarDesarrollo = true;
        this.cargarDesarollo();
        this.imprimirAmanecisfamiliar();
      }
    });
  }
  cargarReferencia() {
    this.referenciaSrv.getSituaReferencia(this.expedientes.id_expediente).subscribe(res => {
      if (res[0] !== undefined) {        
        this.referencia = res[0];
      }
    });
  }
  cargarDesarollo() {
    this.desarrolloSrv.getDesarrolloIMDxidfichaterapiafisica(this.idanamnecis).subscribe(res => {
      if (res[0] === undefined) {
        this.desarrollo = new DesarrolloIMDModel();
      } else {
        this.desarrollo = res[0];
        this.cargarReferencia();
        this.iddesarrollo = this.desarrollo.iddesainde;
        this.verBotonguardar = false;
        this.verBotonEditar = true;
      }
    });
  }

  imprimirAmanecisfamiliar() {
    this.anamnesifamiliarSrv.getAnamnecisFamiliarxidfichaterapiafisica(this.idterapiafisica).subscribe(res => {
      if (res[0] === undefined) {
        this.Anamnesisfamiliar = new AnamnesisFamiliarModel();
      } else {
        this.Anamnesisfamiliar = res[0];
      }
    });
  }
  saveOrUpdateAnamnecis(): void {
    this.anamnecis.idfichaterapiafisica = this.idterapiafisica;
    if (this.isValid) {
      this.SrvAnamanecis.saveOrUpdateAnamnesisClinico(this.anamnecis).subscribe(res => {
        if (res.responseCode === 200) {
          swal('Bien!', 'Guardado!', 'success').then(() => { 
            this.guardado = true;         
           this.cargarAnamnecis();
         });
        } else {
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  saveOrUpdateAnamnecisFamiliars(): void {
    this.Anamnesisfamiliar.idfichaterapiafisica = this.idterapiafisica;
    if (this.isValid) {
      this.anamnesifamiliarSrv.saveOrUpdateAnamnecisFamiliar(this.Anamnesisfamiliar).subscribe(res => {
        if (res.responseCode === OK) {
          swal("Bien!", "Guardado!", "success");          
        } else {
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  saveOrUpdateDesarrollo(): void {
    this.desarrollo.idanamnesis = this.idanamnecis;
    if (this.isValid) {
      this.desarrolloSrv.saveOrUpdateDesarrolloIMD(this.desarrollo).subscribe(res => {
        if (res.responseCode === OK) {
          swal("Bien!", "Guardado!", "success")         
        } else {
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
}
