import { URL_SERVICIOS } from '../../config/config';
import { FileUploader } from 'ng2-file-upload';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  ExpedienteModel,
  EvaluacionFisicaModel
} from '@models/model.index';
import {
  EvaluacionFisicaService,
  ExpedienteService,
  ApoderadoService,
  FilesService
} from '@services/servicio.index';
declare var swal: any;
const URL = URL_SERVICIOS + '/upload';
@Component({
  selector: 'app-evar-fisica-form',
  templateUrl: './evar-fisica-form.component.html',
  styles: []
})
export class EvarFisicaFormComponent implements OnInit {
  documentnames: Array<any>;
  public uploader: FileUploader = new FileUploader({ url: URL });
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;
  verAtender = false
  personal: string;
  verOrtasTerapias: boolean;
  idterapiafisica: number;
  evaluacionfisica: EvaluacionFisicaModel;
  listarevaluacionfisica: Array<EvaluacionFisicaModel>;
  padres: any[];
  expedientepadres: any;
  verpaciente: boolean;
  dni: any;
  idExpediente: any;
  public expedientes: Array<ExpedienteModel>;
  historial: any;
  verxHistorial: boolean;
  verxDNI: boolean;
  public isValid: boolean = true;
  private message: string = '';
  codigoHistorial: string;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private SrvEvalFisica: EvaluacionFisicaService,
    private expedienteSrv: ExpedienteService,
    private padresSRv: ApoderadoService,
    private _uploadFilesSrv: FilesService,
  ) {
    if (sessionStorage.getItem('evaluacionfisica')) {
      this.evaluacionfisica = JSON.parse(sessionStorage.getItem('evaluacionfisica'));
    } else {
      this.evaluacionfisica = new EvaluacionFisicaModel();
    }
  }

  ngOnInit() {
    this.codigoHistorial = this.route.snapshot.paramMap.get('historial');
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));
    if (autorization === 12 || autorization === 16 || autorization === 5 || autorization === 4) {
      this.verAtender = true
    } else {
      this.verAtender = false
    }
    this.verxHistorial = false;
    this.verOrtasTerapias = false;
    this.verxDNI = false;
    this.verpaciente = false;
    this.imprimiExpedientexHistorial(this.codigoHistorial);
  }
  buscarxDNI() {
    this.verxDNI = true;
    this.verxHistorial = false;
  }
  Buscarxhistorial() {
    this.verxHistorial = true;
    this.verxDNI = false;
  }
  imprimiExpedientexHistorial(historial1) {
    this.expedienteSrv.getBusquedaxHistorial(historial1)
      .subscribe(
        (data: any) => {
          this.expedientes = data[0];
          this.idExpediente = data[0].id_expediente;
          this.verpaciente = true;
          this.imprimirpadres();
          this.imprimirFichaterapiaFisica(this.idExpediente);
        });
  }

  imprimirFichaterapiaFisica(dato) {
    const date = new Date();
    this.SrvEvalFisica.getEvaluacionFisicaporexpediente(dato).subscribe(res => {
      if (res[0] !== undefined) {
        this.evaluacionfisica = res[0];
        this.evaluacionfisica.fregistro = res[0].fregistro;
        this.evaluacionfisica.idexpediente = res[0].idexpediente.id_expediente;
        this.verOrtasTerapias = true;
        this.idterapiafisica = this.evaluacionfisica.idfichaterapiafisica
        if (String(res[0].updatedAt).substr(0, 4) !== String(date).substr(11, 4)) {
          this.evaluacionfisica.updatedAt = date;
          console.log(this.evaluacionfisica);
          this.saveOrUpdateEvaluacionFisica();
        }

      } else {
        this.evaluacionfisica = new EvaluacionFisicaModel();
      }
    });
  }
  imprimirpadres() {
    this.expedientepadres = this.idExpediente;
    this.padresSRv.getApoderadoxExpediente(this.expedientepadres)
      .subscribe(
        (data6: any[]) => {
          if (data6 !== undefined) {
            this.padres = data6;
          }
        });
  }
  imprimirFiles() {
    console.log(this.codigoHistorial);
    this._uploadFilesSrv.getListaFilesxAreayPaciente('rehabilitacion', this.codigoHistorial).subscribe(res => {
      if (res.length === 0) {
        swal("error!", "No existen archivos!", "warning");
      } else {
        this.documentnames = res;
      }
    })
  }
  descargar(nombreArchivo) {
    window.open(`${URL_SERVICIOS}/file/${nombreArchivo}?nombrearea=rehabilitacion&codigo=${this.codigoHistorial}`);
  }
  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
    for (let i = 0; i < this.uploader.queue.length; i++) {
      this.uploader.queue[i].file.name = "rehabilitacion-" + this.codigoHistorial + "-" + this.uploader.queue[i].file.name;
    }
  }
  eliminararchivo(documento) {
    console.log(documento);
    swal({
      title: "Esta seguro?",
      text: "Usted esta por eliminar el archivo!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          this._uploadFilesSrv.deleteFilesxcodigo(documento, 'rehabilitacion', this.codigoHistorial).subscribe(res => {
            if (res) {
              this.imprimirFiles();
              swal("Horario eliminado", {
                icon: "success",
              });
            }
          });

        } else {
          swal("Sin acciones!");
        }
      });
  }

  saveOrUpdateEvaluacionFisica() {
    this.evaluacionfisica.idexpediente = this.idExpediente;
    this.evaluacionfisica.estadoatencion = true;
    if (this.isValid) {
      this.SrvEvalFisica.saveOrUpdateEvaluacionFisica(this.evaluacionfisica).subscribe(res => {
        if (res.responseCode === 200) {
          swal("Bien!", "Guardado!", "success").then(() => {
            this.imprimirFichaterapiaFisica(this.idExpediente);
          });
        } else {
          this.message = res.message;
          // this.isValid = false;
          console.log('error');
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
}
