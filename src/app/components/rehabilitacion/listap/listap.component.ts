/* cSpell:disable */
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { OK } from '../../../models/configuration/httpstatus';
import {
  PrestamoMaterialModel,
  MaterialRegModel,
  EvaluacionFisicaModel
} from '@models/model.index';
import {
  MaterialRegService,
  EvaluacionFisicaService,
  PrestamoMaterialService
} from '@services/servicio.index';
declare var swal: any;
@Component({
  selector: 'app-listap',
  templateUrl: './listap.component.html',
  styles: []
})
export class ListapComponent implements OnInit {
  public isValid: boolean = true;
  private message: string = '';
  prestamo: PrestamoMaterialModel;
  prestamos: { id: number; tipo: string; }[];
  ventas: { id: number; tipo: string; }[];
  p = 1;
  m = 1;
  verAtender = false;
  public term: any;
  termo: any;
  public binding: any;
  materiales: Array<MaterialRegModel>;
  material: MaterialRegModel;
  listapacientes: Array<EvaluacionFisicaModel>;
  verfin: boolean;
  verTabla: boolean;
  constructor(
    private materialSrv: MaterialRegService,
    private evalfisicaSrv: EvaluacionFisicaService,
    private preestamoSrv: PrestamoMaterialService,

  ) {
    if (sessionStorage.getItem('prestamo')) {
      this.prestamo = JSON.parse(sessionStorage.getItem('prestamo'));
    } else {
      this.prestamo = new PrestamoMaterialModel();
    }
    this.material = new MaterialRegModel();
  }
  ngOnInit() {
    this.verTabla = true;
    this.verfin = true;
    this.ventas = [
      { id: 0, tipo: 'Venta' },
      { id: 1, tipo: 'Garantia' },
      { id: 2, tipo: 'Alquiler' },
    ];
    this.prestamos = [
      { id: 2, tipo: 'Prestamo' }
    ];
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));
    if (autorization === 12 || autorization === 16 || autorization === 5 || autorization === 4) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }
    this.imprimirlistarehabilitacion();
    this.imprimirMateriales();
  }
  imprimirlistarehabilitacion() {
    this.evalfisicaSrv.getEvaluacionFisica().subscribe(res => {
      if (res !== undefined) {
        // console.log(res);
        this.listapacientes = res.sort((a: any, b: any) => parseFloat(a.codigohistorial) - parseFloat(b.codigohistorial));
      }
    });
  }
  imprimirMateriales() {
    this.materialSrv.getMaterialReg().subscribe(res => {
      if (res !== undefined) {
        this.materiales = res;
      }
    });
  }
  materialSelect(material: MaterialRegModel) {
    if (material.cantidad === 0) {
      swal('Mal!', 'No tiene materiales', 'warning');
    } else {
      this.verTabla = false;
      this.material = material;
      this.prestamo.idmaterialortopedico = material.idmaterialortopedico;
      this.prestamo.estado_devolucion = false;
      this.material = material;
      console.log(this.material);

    }
  }
  regresasTabla() {
    this.verTabla = true;
    this.material = new MaterialRegModel();
  }
  capturaPaciente(paciente: any) {

    this.prestamo.idexpediente = paciente.id_expediente;
  }
  onSelectionChangediagnosticoprevio(select) {
    if (select.tipo === 'Venta') {
      this.verfin = false;
    } else {
      this.verfin = true;
    }
    this.prestamo.tipooperacion = select.id;

  }
  cancelPrestamo() {
    this.regresasTabla();
    this.prestamo = new PrestamoMaterialModel();
    Array.from(document.querySelectorAll('[name=radiogroupventa]')).forEach((x: any) => x.checked = false);
  }
  saveOrUpdatePrestamo(): void {
    if (this.isValid) {
      this.preestamoSrv.saveOrUpdatePrestamoMaterial(this.prestamo).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Guardado!', 'success').then(() => {
            this.saveOrUpdateMateriales();
            this.prestamo = new PrestamoMaterialModel();
            this.regresasTabla();
            Array.from(document.querySelectorAll('[name=radiogroupventa]')).forEach((x: any) => x.checked = false);
          });
        } else {
          swal('Mal!', 'Los campos con * son obligatorios', 'warning');
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  saveOrUpdateMateriales(): void {
    this.material.cantidad = this.material.cantidad - 1;
    if (this.isValid) {
      this.materialSrv.saveOrUpdateMaterialReg(this.material).subscribe(res => {
        if (res.responseCode === OK) {
          console.log('reducido en materiales');
        } else {
          swal('Mal!', 'Los campos con * son obligatorios', 'warning');
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }

  createPDF() {
    var sTable = document.getElementById('tab').innerHTML;

    var style = "<style>";
    style = style + "table {width: 100%;font: 12px Calibri;}";
    style = style + "table, th, td {border: solid 1px #DDD; border-collapse: collapse;}";
    style = style + "th {color: blue;}";
    style = style + "td {color: black;}";
    style = style + ".none {display: none;}";
    style = style + "padding: 2px 3px;text-align: center;}";
    style = style + "</style>";

    // CREATE A WINDOW OBJECT.
    var win = window.open('', '', 'height=700,width=700');

    win.document.write('<html><head>');
    win.document.write('<title>PACIENTES ADMITIDOS EN REHABILITACION</title>');   // <title> FOR PDF HEADER.
    win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');

    win.document.close(); 	// CLOSE THE CURRENT WINDOW.

    win.print();    // PRINT THE CONTENTS.
  }

}
