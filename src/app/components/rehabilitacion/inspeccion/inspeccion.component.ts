/* cSpell:disable */
import { Component, OnInit, Input } from '@angular/core';
import { OK } from '../../../models/configuration/httpstatus';
import { InspbocaarribaModel } from '@models/model.index';
import { BocaArribaService } from '@services/servicio.index';
declare var swal: any;

@Component({
  selector: 'app-inspeccion',
  templateUrl: './inspeccion.component.html',
  styles: []
})
export class InspeccionComponent implements OnInit {
  verBotonguardar: boolean;
  verBotonEditar: boolean;
  @Input() idterapiafisica: number;
  bocaarriba: InspbocaarribaModel;
  public isValid: boolean = true;
  private message: string = '';
  verAtender = false;
  anioactual: string;
  date = new Date();
  aniosretornos: any[];
  constructor(
    private bocaarribaSRV: BocaArribaService,
  ) {
    if (sessionStorage.getItem('bocaarriba')) {
      this.bocaarriba = JSON.parse(sessionStorage.getItem('bocaarriba'));
    } else {
      this.bocaarriba = new InspbocaarribaModel();
    }
    this.anioactual = String(this.date).substr(11, 4);
  }
  ngOnInit() {

    this.verBotonEditar = false;
    this.verBotonguardar = true;
    const dato = this.idterapiafisica;
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));
    if (autorization === 12 || autorization === 16 || autorization === 5 || autorization === 4) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }
    this.imprimirEvaluacion(dato);
    this.imprimiranios(dato);
  }
  setNewanioBoca(valor) {
    const anio = valor.substr(0, 4);
    if (this.anioactual !== anio) {
      this.verAtender = false;
    } else {
      this.verAtender = true;
    }

    this.imprimirEvaluacion(this.idterapiafisica, anio);
  }

  imprimirEvaluacion(dato, anio = this.anioactual) {
    this.bocaarribaSRV.getBocaArribaxfichaterapia(dato, anio).subscribe(res => {
      if (res[0] === undefined) {
        // this.toastr.warning('nueva evaluacion');
        this.bocaarriba = new InspbocaarribaModel();
      } else {
        this.bocaarriba = res[0];
      }
      if (this.bocaarriba.idinspbocaarriba !== 0) {
        this.verBotonguardar = false;
        this.verBotonEditar = true;
      } else {
        this.verBotonEditar = false;
        this.verBotonguardar = true;
        swal('Mal!', 'necesita crear Anamnecis', 'warning');
      }
    });
  }
  imprimiranios(dato) {
    this.bocaarribaSRV.getlistadeaniosBocaarriba(dato).subscribe(res => {
      this.aniosretornos = res;
    })
  }

  saveOrUpdateBocaArriba(): void {
    this.bocaarriba.idfichaterapiafisica = this.idterapiafisica;
    if (this.isValid) {
      this.bocaarribaSRV.saveOrUpdateBocaArriba(this.bocaarriba).subscribe(res => {
        if (res.responseCode === OK) {
          swal("Bien!", "Guardado!", "success");;
        } else {
          this.message = res.message;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
}
