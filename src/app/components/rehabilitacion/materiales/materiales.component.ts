/* cSpell:disable */
import { Component, OnInit } from '@angular/core';
import { OK } from '../../../models/configuration/httpstatus';
import { MaterialRegModel } from '@models/model.index';
import { MaterialRegService } from '@services/servicio.index';
declare var swal: any;

@Component({
  selector: 'app-materiales',
  templateUrl: './materiales.component.html',
  styles: []
})
export class MaterialesComponent implements OnInit {
  p = 1;
  verAtender = false;
  public term: any;
  public binding: any;
  modalselect: string;
  vermateria: boolean;
  material: MaterialRegModel;
  materiales: Array<MaterialRegModel>;
  public isValid: boolean = true;
  private message: string = '';
  modalidades: { id: number; tipo: string; }[];
  fechahoy: any;
  today: string;
  constructor(
    private materialSrv: MaterialRegService,
  ) {
    if (sessionStorage.getItem('material')) {
      this.material = JSON.parse(sessionStorage.getItem('material'));
    } else {
      this.material = new MaterialRegModel();
    }
    this.today = new Date().toISOString().substr(0, 10);
    this.fechahoy = this.today;
  }
  ngOnInit() {
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));
    if (autorization === 12 || autorization === 16 || autorization === 5 || autorization === 4) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }
    this.modalidades = [
      { id: 1, tipo: 'Donación' },
      { id: 2, tipo: 'Compra' }
    ];
    this.imprimirMateriales();
  }
  radioSelectmodalidad(select) {
    console.log(select);
    this.material.modalidad = select.tipo;
  }
  imprimirMateriales() {
    this.vermateria = false;
    this.materialSrv.getMaterialReg().subscribe(res => {
      if (res !== undefined) {
        this.materiales = res;
      }
    });
  }
  CrearMateriales() {
    this.vermateria = true;
    this.material = new MaterialRegModel();
    this.material.fregistro = this.fechahoy;

  }
  cancelar() {
    this.vermateria = false;
    this.material = new MaterialRegModel();
    this.material.fregistro = this.fechahoy;
  }
  editar(material: MaterialRegModel) {
    this.vermateria = true;
    this.material = material;
    this.modalselect = material.modalidad;
    this.material.fregistro = this.fechahoy;
  }
  saveOrUpdateMateriales(): void {
    if (this.isValid) {
      this.materialSrv.saveOrUpdateMaterialReg(this.material).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Guardado!', 'success').then(() => {
            this.imprimirMateriales();
            this.vermateria = false;
          });
        } else {
          swal('Mal!', 'Los campos con * son obligatorios', 'warning');
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }

  createPDF() {
    var sTable = document.getElementById('idmaterialesreha').innerHTML;
    var style = "<style>";
    style = style + "table {width: 100%;font: 11px Calibri;}";
    style = style + "table, th, td {border: solid 1px #DDD; border-collapse: collapse;}";
    style = style + "th {color: blue;}";
    style = style + "td {color: black;}";
    style = style + ".none {display: none;}";
    style = style + "padding: 2px 3px;text-align: center;}";
    style = style + "</style>";
    // CREATE A WINDOW OBJECT.
    var win = window.open('', '', 'height=700,width=700');
    win.document.write('<html><head>');
    win.document.write('<title>LISTA DE MATERIALES</title>');   // <title> FOR PDF HEADER.
    win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');
    win.document.close(); 	// CLOSE THE CURRENT WINDOW.
    win.print();    // PRINT THE CONTENTS.
  }

  exportTableToExcel(tableID, filename = '') {
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

    // Specify file name
    filename = filename ? filename + '.xls' : 'excel_data.xls';

    // Create download link element
    downloadLink = document.createElement("a");

    document.body.appendChild(downloadLink);

    if (navigator.msSaveOrOpenBlob) {
      var blob = new Blob(['\ufeff', tableHTML], {
        type: dataType
      });
      navigator.msSaveOrOpenBlob(blob, filename);
    } else {
      // Create a link to the file
      downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

      // Setting the file name
      downloadLink.download = filename;

      //triggering the function
      downloadLink.click();
    }
  }
}
