/* cSpell:disable */
import { Component, OnInit, Input, ViewContainerRef } from '@angular/core';
import { OK } from '../../../models/configuration/httpstatus';
import { DiagnosticoFinalModel, PlanTrabajoModel } from '@models/model.index';
import { DiagnosticoFinalService, PlanTrabajoService } from '@services/servicio.index';
declare var swal: any;

@Component({
  selector: 'app-final',
  templateUrl: './final.component.html',
  styleUrls: ['./final.component.scss']
})
export class FinalComponent implements OnInit {
  @Input() idterapiafisica: number;
  Actualizarplan: boolean;
  VerGuardarplan: boolean;
  verPlanTrabajo: boolean;
  iddiagfinal: number;
  diagnofinal: DiagnosticoFinalModel;
  plantrabajo: PlanTrabajoModel;
  planestrabajo: Array<PlanTrabajoModel>;
  public isValid: boolean = true;
  private message: string = '';
  Actualizar: boolean;
  VerGuardar: boolean;
  verAtender = false;
  anioactual: string;
  date = new Date();
  aniosretornos: any;
  constructor(
    private diagnofinalSrv: DiagnosticoFinalService,
    private plantrabaSrv: PlanTrabajoService,
  ) {
    this.anioactual = String(this.date).substr(11, 4);
    if (sessionStorage.getItem('diagnofinal')) {
      this.diagnofinal = JSON.parse(sessionStorage.getItem('diagnofinal'));
    } else {
      this.diagnofinal = new DiagnosticoFinalModel();
    }
    if (sessionStorage.getItem('plantrabajo')) {
      this.plantrabajo = JSON.parse(sessionStorage.getItem('plantrabajo'));
    } else {
      this.plantrabajo = new PlanTrabajoModel();
    }
  }

  ngOnInit() {
    this.diagnofinalSrv.getAniosListaDiagnosticoFinal(this.idterapiafisica).subscribe(res => {
      this.aniosretornos = res;
    });
    this.VerGuardar = true;
    this.Actualizar = false;
    this.VerGuardarplan = true;
    this.Actualizarplan = false;
    this.imprimirDiagnosticoFinal(this.anioactual);
    this.verPlanTrabajo = false;
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));
    if (autorization === 12 || autorization === 16 || autorization === 5 || autorization === 4) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }
  }
  setNewbiometriaanios(valor) {
    const anio = valor.substr(0, 4);
    if (this.anioactual !== anio) {
      this.verAtender = false;
    } else {
      this.verAtender = true;
    }
    this.imprimirDiagnosticoFinal(anio);
  }
  imprimirDiagnosticoFinal(anio) {
    this.diagnofinalSrv.getDiagnosticoFinalxFichafisica(this.idterapiafisica, anio).subscribe(res => {
      if (res[0] === undefined) {
        this.diagnofinal = new DiagnosticoFinalModel();
      } else {
        this.diagnofinal = res[0];
        this.iddiagfinal = res[0].iddiagnosticofinal;
        this.imprimirPlandeTrabajo();
        this.Actualizar = true;
        this.VerGuardar = false;
        this.verPlanTrabajo = true;
      }
    });
  }
  imprimirPlandeTrabajo() {
    this.plantrabaSrv.getDiagnosticoFinalxDiagFinal(this.iddiagfinal).subscribe(res => {
      if (res[0] === undefined) {
        this.plantrabajo = new PlanTrabajoModel();
      } else {
        this.planestrabajo = res;
      }
    });
  }
  cancelareventoDiagnostico() {
    this.diagnofinal = new DiagnosticoFinalModel();
  }
  cancelarevento() {
    this.plantrabajo = new PlanTrabajoModel();
    this.VerGuardarplan = true;
    this.Actualizarplan = false;
  }
  saveOrUpdateDiagnosticoFinal(): void {
    this.diagnofinal.idfichaterapiafisica = this.idterapiafisica;
    if (this.isValid) {
      this.diagnofinalSrv.saveOrUpdateDiagnosticoFinal(this.diagnofinal).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Guardado!', 'success').then(() => {
            this.imprimirDiagnosticoFinal(this.anioactual);
          });
        } else {
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  saveOrUpdatePlanTrabajo(): void {
    this.plantrabajo.iddiagnosticofinal = this.iddiagfinal;
    if (this.isValid) {
      this.plantrabaSrv.saveOrUpdateDiagnosticoFinal(this.plantrabajo).subscribe(res => {
        if (res.responseCode === OK) {
          swal("Bien!", "Guardado!", "success").then(() => {
            this.imprimirPlandeTrabajo();
            this.plantrabajo = new PlanTrabajoModel();
            this.VerGuardarplan = true;
            this.Actualizarplan = false;
          });
        } else {
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  editplan(plan) {
    this.plantrabajo = plan;
    this.VerGuardarplan = false;
    this.Actualizarplan = true;
  }
}
