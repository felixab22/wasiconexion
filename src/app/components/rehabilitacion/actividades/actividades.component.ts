/* cSpell:disable */
import { Component, OnInit, Input } from '@angular/core';
import { OK } from '../../../models/configuration/httpstatus';
import { ResultadoEscalaModel } from '../../../models/rehabilitacion/rehabilitacion.model';
import { ResultadoEscalaService } from '@services/servicio.index';
declare var swal: any;
@Component({
  selector: 'app-actividades',
  templateUrl: './actividades.component.html',
  styleUrls: ['./actividades.component.scss']
})
export class ActividadesComponent implements OnInit {

  @Input() idterapiafisica: number;
  resultado: ResultadoEscalaModel;
  public isValid: boolean = true;
  private message: string = '';
  total: number = 0;
  verAtender = false;
  anioactual: string;
  date = new Date();
  aniosretornos: any[];
  constructor(
    private resultadosSrv: ResultadoEscalaService,
  ) {
    this.anioactual = String(this.date).substr(11, 4);
    if (sessionStorage.getItem('resultado')) {
      this.resultado = JSON.parse(sessionStorage.getItem('resultado'));
    } else {
      this.resultado = new ResultadoEscalaModel();
    }
  }
  ngOnInit() {
    this.resultadosSrv.getAniosListaResultado(this.idterapiafisica).subscribe(res => {
      this.aniosretornos = res;
    });
    this.imprimirResultados(this.anioactual);
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));
    if (autorization === 12 || autorization === 16 || autorization === 5 || autorization === 4) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }
  }
  setNewbiometriaanios(valor) {
    const anio = valor.substr(0, 4);
    if (this.anioactual !== anio) {
      this.verAtender = false;
    } else {
      this.verAtender = true;
    }
    this.imprimirResultados(anio);
  }

  imprimirResultados(anios) {
    this.resultadosSrv.getResultadoEscalaxFichaterapiafisica(this.idterapiafisica, anios).subscribe(res => {
      if (res[0] === undefined) {
        this.resultado = new ResultadoEscalaModel();
      } else {
        this.sumartotal();
        this.resultado = res[0];
      }
    });
  }
  sumartotal() {
    // tslint:disable-next-line:radix
    const numa = parseInt(this.resultado.gmfma);
    const numb = parseInt(this.resultado.gmfmb);
    const numc = parseInt(this.resultado.gmfmc);
    const numd = parseInt(this.resultado.gmfmd);
    const nume = parseInt(this.resultado.gmfme);

    if (isNaN(numa) && isNaN(numb) && isNaN(numc) && isNaN(numd) && isNaN(nume)) {
      // this.toastr.error('solo resiven numeros', 'Error!!!');
      return false;
    } else {
      this.total = parseInt(this.resultado.gmfma) + parseInt(this.resultado.gmfmb) + parseInt(this.resultado.gmfmc) +
        parseInt(this.resultado.gmfmd) + parseInt(this.resultado.gmfme);
    }

  }

  saveOrUpdateResultado(): void {
    this.resultado.fichaTerapiaFisica = this.idterapiafisica;
    if (this.isValid) {
      this.resultadosSrv.saveOrUpdateSegResultadoEscala(this.resultado).subscribe(res => {
        if (res.responseCode === OK) {
          swal("Bien!", "Guardado!", "success");
        } else {
          swal("Mal!", "No admitido!", "warning");
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }

  // imagen 1
  handleFileSelectimagen1(evt) {
    const files = evt.target.files;
    const file = files[0];

    if (files && file) {
      const reader = new FileReader();
      reader.onload = this.hendleReaderLoaded1.bind(this);
      reader.readAsBinaryString(file);
    }
  }
  hendleReaderLoaded1(readerEvt) {
    const binaryString = readerEvt.target.result;
    this.resultado.imgunoaims = btoa(binaryString);
  }
  // imagen 2
  handleFileSelectimagen2(evt) {
    const files = evt.target.files;
    const file = files[0];

    if (files && file) {
      const reader = new FileReader();
      reader.onload = this.hendleReaderLoaded2.bind(this);
      reader.readAsBinaryString(file);
    }
  }
  hendleReaderLoaded2(readerEvt) {
    const binaryString = readerEvt.target.result;
    this.resultado.imgdosaims = btoa(binaryString);
  }
  // imagen 3
  handleFileSelectimagen3(evt) {
    const files = evt.target.files;
    const file = files[0];

    if (files && file) {
      const reader = new FileReader();
      reader.onload = this.hendleReaderLoaded3.bind(this);
      reader.readAsBinaryString(file);
    }
  }
  hendleReaderLoaded3(readerEvt) {
    const binaryString = readerEvt.target.result;
    this.resultado.imgtresaims = btoa(binaryString);
  }
  // imagen 4
  handleFileSelectimagen4(evt) {
    const files = evt.target.files;
    const file = files[0];

    if (files && file) {
      const reader = new FileReader();
      reader.onload = this.hendleReaderLoaded4.bind(this);
      reader.readAsBinaryString(file);
    }
  }
  hendleReaderLoaded4(readerEvt) {
    const binaryString = readerEvt.target.result;
    this.resultado.imgcuatroaims = btoa(binaryString);
  }
  // imagen 5
  handleFileSelectimagen5(evt) {
    const files = evt.target.files;
    const file = files[0];

    if (files && file) {
      const reader = new FileReader();
      reader.onload = this.hendleReaderLoaded5.bind(this);
      reader.readAsBinaryString(file);
    }
  }
  hendleReaderLoaded5(readerEvt) {
    const binaryString = readerEvt.target.result;
    this.resultado.imgunogmfm = btoa(binaryString);
  }
  // imagen 6
  handleFileSelectimagen6(evt) {
    const files = evt.target.files;
    const file = files[0];

    if (files && file) {
      const reader = new FileReader();
      reader.onload = this.hendleReaderLoaded6.bind(this);
      reader.readAsBinaryString(file);
    }
  }
  hendleReaderLoaded6(readerEvt) {
    const binaryString = readerEvt.target.result;
    this.resultado.imgdosgmfm = btoa(binaryString);
  }

  // imagen 7
  handleFileSelectimagen7(evt) {
    const files = evt.target.files;
    const file = files[0];

    if (files && file) {
      const reader = new FileReader();
      reader.onload = this.hendleReaderLoaded7.bind(this);
      reader.readAsBinaryString(file);
    }
  }
  hendleReaderLoaded7(readerEvt) {
    const binaryString = readerEvt.target.result;
    this.resultado.imgdosaims = btoa(binaryString);
  }
}
