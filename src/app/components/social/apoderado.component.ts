/* cSpell:disable */
import { Component, OnInit, Input, Output, ViewContainerRef } from '@angular/core';
import { EventEmitter } from 'events';
import { OK } from '../../models/configuration/httpstatus';
declare var swal: any;
import { 
  ApoderadoService, 
  UbigeoService, 
  EdadService 
} from '@services/servicio.index';
import { 
  DepartamentoModel, 
  ApoderadoModel, 
  ExpedienteModel 
} from '@models/model.index';

@Component({
  selector: 'app-apoderado',
  templateUrl: './apoderado.component.html',
  styles: []
})
export class ApoderadoComponent implements OnInit {
  nivelInstruciones: { id: number; descripcion: string; }[];
  idiomas: { id: number; descripcion: string; }[];
  apoderados: { id: number; descripcion: string; }[];
  estadoCiviles: { id: number; descripcion: string; }[];
  religiones: { id: number; descripcion: string; }[];
  condicionLaborales: { id: number; descripcion: string; }[];
  ocupaciones: { id: number; descripcion: string; }[];
  mostrarboton: boolean;
  apoderadoocupacion: string;
  apoderadocondicionLaboral: string;
  apoderadoreligion: string;
  apoderadoIdioma: string;
  apoderadoTipo: string;
  verOtroRadioReligionMadre: boolean;
  verOtroRadioIdiomaMadre: boolean;
  verOtroRadioOcupacionMadre: boolean;
  verOtroRadioCondicionlabMadre: boolean;
  public isValid: boolean = true;  
  private message: string = '';
  ubigeoDepartamentos: DepartamentoModel[];
  padres: Array<ApoderadoModel>;
  verCrearPadres: boolean;
  expedientepadres: number;
  i: number;
  valor11: any;
  provi: any;
  ubigeoDistritos: any[];
  ubigeoProvincias: any[];
  valor1: any;
  depa: any;
  verProvincia: boolean;
  verDistrito: boolean;
  @Input() expediente: ExpedienteModel;
  @Input() submitText: string;
  @Output() onSubmit: EventEmitter = new EventEmitter();
  private adicion: string = '';
  private caracter: string = '';
  selectedprograma1: string[] = [];
  public apoderadomadre: ApoderadoModel;
  igualar1: string;
  edades: any;
  edadmuestra: string;  
  verAtender = false;
  constructor(
    private apoderadoSrv: ApoderadoService,    
    private ubicacionSrv: UbigeoService,
    public _edadymesSrv: EdadService,    
  ) {    
    if (sessionStorage.getItem('apoderadomadre')) {
      this.apoderadomadre = JSON.parse(sessionStorage.getItem('apoderadomadre'));
      console.log('Madre retorno' + this.apoderadomadre);
    } else {
      this.apoderadomadre = new ApoderadoModel();
    }
  }

  ngOnInit() {
    this.imprimirpadres();
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));       
    if(autorization === 6  || autorization === 5  || autorization === 4 ) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }
    this.apoderados = [
      { id: 1, descripcion: 'Padre ' },
      { id: 2, descripcion: 'Madre' },
      { id: 3, descripcion: 'Apoderado' }];

    this.idiomas = [
      { id: 4, descripcion: 'Castellano' },
      { id: 5, descripcion: 'Quechua' },
      { id: 6, descripcion: 'Aimara' },
      { id: 7, descripcion: 'Otro' }];

    this.estadoCiviles = [
      { id: 8, descripcion: 'Soltero(a)' },
      { id: 9, descripcion: 'Casado(a)' },
      { id: 10, descripcion: 'Divorsiado(a)' },
      { id: 11, descripcion: 'Viudo(a)' },
      { id: 12, descripcion: 'Conviviente' }
    ];

    this.nivelInstruciones = [
      { id: 13, descripcion: 'Sin estudios' },
      { id: 14, descripcion: 'Inicial' },
      { id: 15, descripcion: 'Primaria completo' },
      { id: 46, descripcion: 'Primaria incompleto' },
      { id: 16, descripcion: 'Secundaria completo' },
      { id: 47, descripcion: 'Secundaria incompleto' },
      { id: 17, descripcion: 'Superior incompleto'},
      { id: 45, descripcion: 'Superior completo'},
      { id: 18, descripcion: 'Estudiante universitario' },
      ];

    this.religiones = [
      { id: 20, descripcion: 'Ateo' },
      { id: 21, descripcion: 'Católico' },
      { id: 22, descripcion: 'Evangélica' },
      { id: 23, descripcion: 'Testigos de Jehová' },
      { id: 24, descripcion: 'Mormones' },
      { id: 25, descripcion: 'Adventistas' },
      { id: 26, descripcion: 'Otro' }];

    this.condicionLaborales = [
      { id: 27, descripcion: 'Eventual' },
      { id: 28, descripcion: 'Estable' },
      { id: 29, descripcion: 'Desempleado' },
      { id: 30, descripcion: 'Independiente formal' },
      { id: 31, descripcion: 'Independienten informal' },
      { id: 32, descripcion: 'Jubilado o pensionista' },
      { id: 33, descripcion: 'Otro' }];

    this.ocupaciones = [
      { id: 34, descripcion: 'Profesional' },
      { id: 35, descripcion: 'Obrero(a)' },
      { id: 36, descripcion: 'Empleado(a)' },
      { id: 37, descripcion: 'Comerciante formal' },
      { id: 38, descripcion: 'Comerciante informal' },
      { id: 39, descripcion: 'Ambulante' },
      { id: 40, descripcion: 'Estudiante' },
      { id: 41, descripcion: 'Trabajo doméstico' },
      { id: 42, descripcion: 'Agricultor(a)' },
      { id: 43, descripcion: 'Su casa' },
      { id: 44, descripcion: 'Otro' }];

    this.verOtroRadioReligionMadre = false;
    this.verOtroRadioIdiomaMadre = false;
    this.verOtroRadioOcupacionMadre = false;
    this.verOtroRadioCondicionlabMadre = false;
    this.verDistrito = false;
    this.verProvincia = false;
    this.verCrearPadres = false;
    this.mostrarboton = true;
    this.loadDepartamento();

  }

  calcularedadymes() {     
    this.edades = JSON.parse(this._edadymesSrv.calcularedad(this.apoderadomadre.fnacimiento_apod).toString())[0] ;
    const a = this.edades.edad;
    const m = this.edades.meses;
    if ( this.edades.meses === 0) {
      this.edadmuestra = a + ' años';
    } else if ( this.edades.meses === 1 ) {
      this.edadmuestra = a + ' años y ' + m + ' mes ' ;
    } else {
      this.edadmuestra = a + ' años y ' + m + ' meses ' ;
    }
  }
  onSelectionChangeapoderado(apoderado) {
    this.apoderadomadre.tipo_apoderado = apoderado.descripcion;   

  }
  
  private loadDepartamento(): void {
    this.ubicacionSrv.getDepartamento().subscribe(res => {
      this.ubigeoDepartamentos = res;
    });
  }

  CrearPadres() {
    this.verCrearPadres = true;
    this.apoderadomadre.idexpediente = this.expediente.id_expediente;
  }
  setNewDepartamentoMadre(departamento) {
    this.depa = departamento;
    console.log(this.depa);
    this.ubicacionSrv.getprovinciaxDepartamento(departamento)
      .subscribe(
        (data1: any[]) => {
          this.valor1 = data1[0].id_depa;
          this.ubigeoProvincias = data1;
          this.verProvincia = true;

          this.ubicacionSrv.getDepartamento()
            .subscribe(
              (datadepa: any[]) => {
                this.apoderadomadre.departamento_apoderado = datadepa[this.valor1 - 1].nombre_departamento;
                console.log(datadepa[this.valor1 - 1].nombre_departamento);
              });
        }
      );
  }
  chenckboxChangeocupacion(programa1) {
    if (this.selectedprograma1.indexOf(programa1.value) === -1) {
      this.selectedprograma1.push(programa1.value);
      this.igualar1 = this.selectedprograma1.toString();
    } else {
      this.selectedprograma1.splice(this.selectedprograma1.indexOf(programa1.value), 1);
      this.igualar1 = this.selectedprograma1.toString();
    }
  }
  igualarocupacion() {
    this.apoderadomadre.ocupacion_apoderado = this.igualar1;
  }
  setNewProvinciaMadre(provincia) {
    this.provi = provincia;
    this.ubicacionSrv.getDistritoxprovincia(provincia)
      .subscribe(
        (data11: any[]) => {
          this.ubigeoDistritos = data11;
          this.valor11 = data11[0].id_prov;
          this.verDistrito = true;
          this.ubicacionSrv.getprovinciaxDepartamento(this.depa)
            .subscribe(
              (data1prov: any[]) => {
                this.i = 0;
                do {
                  if (data1prov[this.i].id_prov === this.valor11) {
                    this.apoderadomadre.provincia_apoderado = data1prov[this.i].nombre_provincia;
                    this.i = 200;
                  }
                  this.i++;
                } while (this.i < 200);
              }
            );
        }
      );
  }
  setNewDistritoMadre(distro1) {
    this.apoderadomadre.distrito_apoderado = distro1;
  }

  imprimirpadres() {
    this.expedientepadres = this.expediente.id_expediente;
    this.apoderadoSrv.getApoderadoxExpediente(this.expedientepadres)
      .subscribe(
        (data6: any[]) => {
          if (data6[0] === undefined) {
            this.apoderadomadre = new ApoderadoModel();
          } else {
            this.verCrearPadres = false;
            this.padres = data6;
            if (this.padres.length === 3) {
              this.mostrarboton = false;
            } else {
              this.mostrarboton = true;
            }
          }
        });
  }
  onSelectionChangeReligionMadre(religion) {
    if (religion.descripcion !== 'Otro') {
      this.apoderadomadre.religion_apoderado = religion.descripcion;
      this.verOtroRadioReligionMadre = false;
    } else {
      this.verOtroRadioReligionMadre = true;
    }
  }
  desseleccionarradiogroupReligion() {
    this.apoderadomadre.religion_apoderado = null;
    Array.from(document.querySelectorAll('[name=radiogroupReligion]')).forEach((x: any) => x.checked  = false);
    this.verOtroRadioReligionMadre = false;
  }

  onSelectionChangeIdiomaMadre(entry1) {
    if (entry1.descripcion !== 'Otro') {
      this.apoderadomadre.idioma_apoderado = entry1.descripcion;
      this.verOtroRadioIdiomaMadre = false;
    } else {
      this.verOtroRadioIdiomaMadre = true;
    } 
  }
  desseleccionarradioidioma() {
    this.apoderadomadre.idioma_apoderado = null;
    Array.from(document.querySelectorAll('[name=radiogroupIdioma]')).forEach((x: any) => x.checked  = false);
    this.verOtroRadioIdiomaMadre = false;
  }
  setNewNivelInstruccionMadre(nivel1: any): void {
    console.log(nivel1);
    this.apoderadomadre.nivelinst_apoderado = nivel1;
  }
  setNewEstadoCivilMadre(estado: any): void {
    console.log(estado);
    this.apoderadomadre.estcivil_apoderado = estado;
  }
  onSelectionChangeocupacionMadre(ocupacion) {
    if (ocupacion.descripcion !== 'Otro') {
      this.apoderadomadre.ocupacion_apoderado = ocupacion.descripcion;
      this.verOtroRadioOcupacionMadre = false;
    } else {
      this.verOtroRadioOcupacionMadre = true;
    }
  }
  onSelectionChangecondicionLaboralMadre(condicionLaboral) {
    if (condicionLaboral.descripcion !== 'Otro') {
      this.apoderadomadre.condlaboral_apod = condicionLaboral.descripcion;
      console.log(this.apoderadomadre.condlaboral_apod);
      this.verOtroRadioCondicionlabMadre = false;
    } else {
      this.verOtroRadioCondicionlabMadre = true;
    }
  }
  desseleccionarradiogroupcondicionLaboralMadre() {
    this.apoderadomadre.condlaboral_apod = null;
    Array.from(document.querySelectorAll('[name=radiogroupcondicionLaboralMadre]')).forEach((x: any) => x.checked  = false);
    this.verOtroRadioCondicionlabMadre = false;
  }
  onEditApoderado(padre) {
    this.verCrearPadres = true;
    this.apoderadomadre = padre;
    this.apoderadoTipo = padre.tipo_apoderado;
    this.apoderadoIdioma = padre.idioma_apoderado;
    this.apoderadoreligion = padre.religion_apoderado;
    this.apoderadocondicionLaboral = padre.condlaboral_apod;
    this.apoderadoocupacion = padre.ocupacion_apoderado;
    this.calcularedadymes();
  }
  cancelar() {
    this.apoderadomadre = new ApoderadoModel();
    this.verCrearPadres = false;
  }
  saveOrUpdateMadre(): void {
    this.apoderadomadre.adiccion = this.adicion;
    this.apoderadomadre.adiccion = this.caracter;
    if (this.isValid) {
      this.apoderadoSrv.saveOrUpdate(this.apoderadomadre).subscribe(res => {
        if (res.responseCode === OK) {
          swal("Bien!", "Guardado!", "success").then(() => {
            this.imprimirpadres();
          });
          this.apoderadomadre = new ApoderadoModel();
        } else {
          this.message = res.message;
          // this.isValid = false;
          this.verCrearPadres = false;
          this.imprimirpadres();
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  } 
}

