/* cSpell:disable */
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { ExpedienteModel } from '@models/model.index';
import { ExpedienteService, UbigeoService } from '@services/servicio.index';
declare var swal: any;

@Component({
  selector: 'app-expediente-form',
  templateUrl: './expediente-form.component.html',
  styles: []
})
export class ExpedienteFormComponent implements OnInit {
  depa: any;
  ubigeoDistritos: any[];
  ubigeoProvincias: any[];
  ubigeoDepartamentos: any[];
  verOtroRadioIdiomaMadre: boolean;  
  sexoseleccionado: string;
  valor2: any;
  valor1: any;
  provi: any;
  public i: number;
  public j: number;
  public expediente: ExpedienteModel;
  public sexos = [];
  public selectedEntry;
  public entry: ExpedienteModel;
  public verDistrito: boolean;
  public verProvincia: boolean;  
  public isValid: boolean = true;
  private message: string = '';
  idiomaselect: string;
  nacimiento_anio: number;
  aperturaanio: string;
  nacimientoanio: string;
  apertura_anio: number;
  nacimiento_mes: number;
  apertura_mes: number;
  apertura_dia: number;
  nacimiento_dia: number;
  d: number;
  m: number;
  a: number;
  diagnosticos: { id: number; tipo: string; }[];
  verOtrodiagnostico: boolean;
  idiomas: { id: number; descripcion: string; }[];
  today: any;
  validado: boolean;
  diagnosticselect: string;
  encontro: boolean;
  verAtender = false;
  constructor(
    private createExpedienteService: ExpedienteService,
    private router: Router,
    private ubigeoServ: UbigeoService,    
    public vcr: ViewContainerRef,    
  ) {
    
    if (sessionStorage.getItem('expediente')) {
      this.expediente = JSON.parse(sessionStorage.getItem('expediente'));
      this.sexoseleccionado = this.expediente.sexo;
      if (this.expediente.idioma !== 'Otro') {
        this.idiomaselect = this.expediente.idioma;
      } else {
        this.verOtroRadioIdiomaMadre = true;
      }
      if (this.expediente.diagnostico === 'Parálisis cerebral' || this.expediente.diagnostico === 'Síndrome de down' ||
      this.expediente.diagnostico === 'Displasia de cadera' || this.expediente.diagnostico === 'Espina bífida' ) {
        this.diagnosticselect = this.expediente.diagnostico;
      } else {
        this.diagnosticselect = 'Otro';
        this.verOtrodiagnostico = true;
      }
    } else {
      this.today = new Date().toISOString().substr(0, 10);          
      this.expediente = new ExpedienteModel();
      this.expediente.fechaapertura = this.today;
    }
  }

  ngOnInit() {
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));       
    if(autorization === 6  || autorization === 5  || autorization === 4 ) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }
this.validado = true;
    this.idiomas = [
      { id: 20, descripcion: 'No habla' },
      { id: 21, descripcion: 'Castellano' },
      { id: 22, descripcion: 'Quechua' },
      { id: 23, descripcion: 'Aimara' },
      { id: 24, descripcion: 'Otro' }];

    this.sexos = [
      { id: 1, tipo: 'Femenino' },
      { id: 2, tipo: 'Masculino' }
    ];

    this.diagnosticos = [
      { id: 3, tipo: 'Parálisis cerebral' },
      { id: 4, tipo: 'Síndrome de down' },
      { id: 5, tipo: 'Displasia de cadera' },
      { id: 6, tipo: 'Espina bífida' },
      { id: 7, tipo: 'Otro' }
    ];
    this.loadDepartamento();
    this.verDistrito = false;
    this.verProvincia = false;
    // this.verOtrodiagnostico = false;
  }
  cancelar() {
    this.expediente = new ExpedienteModel();
    sessionStorage.removeItem('expediente');
    this.router.navigate(['wasi/historialgeneral']);
  }
 
  public saveOrUpdate(): void {
    if (this.expediente.codigohistorial !== null) {
      this.createExpedienteService.saveOrUpdate(this.expediente).subscribe(res => {
        if (res.responseCode === 200) {
          swal("Bien!", "Guardado!", "success").then(() => {
            this.router.navigate(['wasi/historialgeneral']);         
          });         
        } 
        if (res.responseCode === 400 || res.responseCode === 500 ) {          
          swal("Mal!", "Verificque campos obligatorios!", "warning");          
        }
        else {
          this.message = res.message;
          // this.isValid = false;
        }

      });
    } else {
       swal('Mal!', 'Los campos con * son obligatorios', 'warning');      
      // this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }


  private loadDepartamento(): void {
    this.ubigeoServ.getDepartamento().subscribe(res => {
      this.ubigeoDepartamentos = res;
    });
  }
  onSelectionChange(entry) {
    this.selectedEntry = entry.tipo;
    this.expediente.sexo = this.selectedEntry;
  }
  deseleccionarradioSexo() {      
    this.expediente.sexo = null;   
    Array.from(document.querySelectorAll('[name=radiogroupsexo]')).forEach((x: any) => x.checked  = false);    
  }
  setNewDepartamento(departamento) {
    this.depa = departamento;
    this.ubigeoServ.getprovinciaxDepartamento(this.depa)
      .subscribe(
        (data: any[]) => {
          this.valor1 = data[0].id_depa;
          this.ubigeoProvincias = data;
          this.verProvincia = true;

          this.ubigeoServ.getDepartamento()
            .subscribe(
              (datadepa: any[]) => {
                this.expediente.departamentonacimiento = datadepa[this.valor1 - 1].nombre_departamento;
              });
        }
      );
  }
  onSelectionChangeIdiomaMadre(entry1) {
    if (entry1.descripcion !== 'Otro') {
      this.expediente.idioma = entry1.descripcion;
      this.verOtroRadioIdiomaMadre = false;
    } else {
      this.verOtroRadioIdiomaMadre = true;
    }

  }

  setNewProvincia(provincia) {
    this.provi = provincia;
    this.ubigeoServ.getDistritoxprovincia(provincia)
      .subscribe(
        (data2: any[]) => {
          this.ubigeoDistritos = data2;
          this.valor2 = data2[0].id_prov;
          this.verDistrito = true;
          this.ubigeoServ.getprovinciaxDepartamento(this.depa)
            .subscribe(
              (data2prov: any[]) => {
                this.i = 0;
                do {
                  if (data2prov[this.i].id_prov === this.valor2) {
                    this.expediente.provincianacimiento = data2prov[this.i].nombre_provincia;
                    this.i = 200;
                  }
                  this.i++;
                } while (this.i < 200);
              }
            );
        }
      );
  }
  setNewDistrito(provincia) {
    this.expediente.distritonacimiento = provincia;

  }
  calcularedad() {
    console.log('nacimiento:', this.expediente.fechanacimiento);

    this.aperturaanio = String(this.expediente.fechaapertura);
    this.nacimientoanio = String(this.expediente.fechanacimiento);
    this.nacimiento_anio = parseInt(this.nacimientoanio.substr(0, 4));
    this.apertura_anio = parseInt(this.aperturaanio.substr(0, 4));
    this.nacimiento_mes = parseInt(this.nacimientoanio.substr(5, 2));
    this.apertura_mes = parseInt(this.aperturaanio.substr(5, 2));
    this.nacimiento_dia = parseInt(this.nacimientoanio.substr(8, 2));
    this.apertura_dia = parseInt(this.aperturaanio.substr(8, 2));

    this.d = this.apertura_dia - this.nacimiento_dia;
    this.m = this.apertura_mes - this.nacimiento_mes;
    this.a = this.apertura_anio - this.nacimiento_anio;

    if (this.m === 1) {
      this.expediente.edadmeses =  1;      
    } else  if (this.m === 0) {
      // this.m = this.m - 1;
      if (this.d < 0) {
        this.a = this.a - 1;
        this.expediente.edadmeses = 12 - 1;
      } else if (this. d >= 0) {
        this.expediente.edadmeses = 0;
      }
    } else if (this.m < 0) {       
        this.a = this.a - 1;
        this.expediente.edadmeses = 12 - Math.abs(this.m);
      } else {
        this.expediente.edadmeses = this.m;
      }    
    this.expediente.edad = this.a;
    
  }

  onSelectionChangeDiagnostico(diagnostico) {
    if (diagnostico.tipo === 'Otro') {
      this.verOtrodiagnostico = true;
    } else {
      this.verOtrodiagnostico = false;
      this.expediente.diagnostico = diagnostico.tipo;
    }
  }
  handleFileSelect(evt) {
    const files = evt.target.files;
    const file = files[0];

    if (files && file) {
      const reader = new FileReader();
      reader.onload = this.hendleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }
  hendleReaderLoaded(readerEvt) {
    const binaryString = readerEvt.target.result;
    this.expediente.foto = btoa(binaryString);
  }
  
}
