/* cSpell:disable */
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { OK } from '../../models/configuration/httpstatus';
import { PreInscripcionModel } from '@models/model.index';
import { PreinscripcionService } from '@services/servicio.index';
declare var swal: any;

@Component({
  selector: 'app-inscripcion-form',
  templateUrl: './inscripcion-form.component.html',
  styles: []
})
export class InscripcionFormComponent implements OnInit {
  selectcondicion: string;
  diagnosticselect: string;
  verOtrodiagnostico: boolean;
  diagnosticos: { id: number; tipo: string; }[];
  public selectedEntry;
  public isValid: boolean = true;
  private message: string = '';
  condiciones: { id: number; condicionpaciente: string; }[];
  public preinscripcion: PreInscripcionModel;
  verAtender = false;
  constructor(
    private createPreinscService: PreinscripcionService,
    private router: Router,    
  ) {
    
    if (sessionStorage.getItem('preinscripcion')) {
      this.preinscripcion = JSON.parse(sessionStorage.getItem('preinscripcion'));
      this.selectcondicion = this.preinscripcion.condicionpaciente;
      if (this.preinscripcion.diagnosticopaciente ===  'Parálisis cerebral' || this.preinscripcion.diagnosticopaciente === 'Síndrome de down' ||
      this.preinscripcion.diagnosticopaciente === 'Displasia de cadera' || this.preinscripcion.diagnosticopaciente === 'Espina bífida' ) {
        this.diagnosticselect = this.preinscripcion.diagnosticopaciente;
      } else {
        this.diagnosticselect = 'Otro';
        this.verOtrodiagnostico = true;
      }
    } else {
      this.preinscripcion = new PreInscripcionModel();
    }
  }

  ngOnInit() {
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));       
    if(autorization === 6  || autorization === 5  || autorization === 4 ) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }
    
    this.condiciones = [
      { id: 1, condicionpaciente: 'Regular' },
      { id: 2, condicionpaciente: 'Nuevo' }];

    this.diagnosticos = [
      { id: 3, tipo: 'Parálisis cerebral' },
      { id: 4, tipo: 'Síndrome de down' },
      { id: 5, tipo: 'Displasia de cadera' },
      { id: 6, tipo: 'Espina bífida' },
      { id: 7, tipo: 'Otro' }
    ];
  }
  cancelar() {
    this.preinscripcion = new PreInscripcionModel();
    this.router.navigate(['wasi/Fichadeinscripcion']);
    sessionStorage.clear();
  }
  public saveOrUpdate(): void {
    this.preinscripcion.estado = true;
    if (this.isValid) {
      this.createPreinscService.saveOrUpdate(this.preinscripcion).subscribe(res => {
        if (res.responseCode === OK) {
          sessionStorage.clear();
          swal('Bien!', 'Guardado!', 'success').then(() => {
            this.router.navigate(['wasi/Fichadeinscripcion']);
          }); 
        } else {
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  onSelectionChange(entry) {
    this.preinscripcion.estado = true;
    this.selectedEntry = entry.condicionpaciente;    
    this.preinscripcion.condicionpaciente = this.selectedEntry;
  }
  onSelectionChangeDiagnostico(diagnostico) {
    if (diagnostico.tipo === 'Otro') {
      this.verOtrodiagnostico = true;
    } else {
      this.verOtrodiagnostico = false;
      this.preinscripcion.diagnosticopaciente = diagnostico.tipo;
    }
  }
}
