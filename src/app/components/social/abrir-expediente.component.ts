/* cSpell:disable */
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { OK } from '../../models/configuration/httpstatus';
import { ActivatedRoute, Router } from '@angular/router';
import { 
  ExpedienteModel, 
  SituacionPacienteModel 
} from '@models/model.index';
import { 
  ExpedienteService, 
  SituaPacientecService 
} from '@services/servicio.index';
declare var swal: any;

@Component({
  selector: 'app-abrir-expediente',
  templateUrl: './abrir-expediente.component.html',
  styles: []
})
export class AbrirExpedienteComponent implements OnInit {  
  historial1: string;
  pacienteaceptadopersona: string;
  estadopersona: string;
  estadopacientes: { code: number, id: boolean; descripcion: string; }[];
  admitidopacientes: { code: number, id: boolean; descripcion: string; }[];
  verestadoModificar: boolean;
  public isValid: boolean = true; 
  private message: string = '';
  midfaceptado: boolean;
  midfestado: boolean;
  verpaciente: boolean;
  dni: any;
  idExpediente: any;
  expedientes: Array<ExpedienteModel>;
  historial: any;
  verxHistorial: boolean;
  verxDNI: boolean;
  public estadopacienteadmitido: SituacionPacienteModel;
  verAtender = false;
  constructor(
    private router: Router,
    private expedienteSrv: ExpedienteService,
    private route: ActivatedRoute,
    private http: HttpClient,  
    private situapaciSrv: SituaPacientecService
  ) {    
    if (sessionStorage.getItem('estadopacienteadmitido')) {
      this.estadopacienteadmitido = JSON.parse(sessionStorage.getItem('estadopacienteadmitido'));
    } else {
      this.estadopacienteadmitido = new SituacionPacienteModel();
    }
  }
  ngOnInit() {
    const dato = this.route.snapshot.paramMap.get('historial');
    this.estadopersona = '';
    this.pacienteaceptadopersona = '';
    this.verpaciente = false;
    this.verestadoModificar = false;
    this.imprimiExpedientexHistorial(dato);
    this.admitidopacientes = [
      { code: 1, id: true, descripcion: 'Admitido ' },
      { code: 2, id: false, descripcion: 'No admitido' }];
    this.estadopacientes = [
      { code: 3, id: true, descripcion: 'Habilitar ' },
      { code: 4, id: false, descripcion: 'Deshabilitar' }];
      const autorization = JSON.parse(localStorage.getItem('idautorizado'));       
    if(autorization === 6 || autorization === 5  || autorization === 4 ) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }
  }
  buscarxDNI() {
    this.verxDNI = true;
    this.verxHistorial = false;
  }
  Buscarxhistorial() {
    this.verxHistorial = true;
    this.verxDNI = false;
  }

  imprimiExpedientexHistorial(historial1) {
    this.expedienteSrv.getBusquedaxHistorial(historial1)
      .subscribe(
        (data: any[]) => {
          this.expedientes = data[0];
          this.idExpediente = data[0].id_expediente;
          this.verpaciente = true;
          this.estadopacienteadmitido.idexpediente = data[0].id_expediente;

          this.situapaciSrv.getSituaPaciente(this.idExpediente).subscribe(res => {
            if (res === undefined) {
              this.estadopacienteadmitido = new SituacionPacienteModel();
            } else {
              this.estadopacienteadmitido.idsituacionpaciente = res[0].idsituacionpaciente;
              this.midfaceptado = res[0].pacienteaceptado;
              this.estadopacienteadmitido.pacienteaceptado = res[0].pacienteaceptado;
              this.midfestado = res[0].estado;
              this.estadopacienteadmitido.estado = res[0].estado;
              this.estadopacienteadmitido.pacienteaceptado = res[0].pacienteaceptado;
              if (res[0].estado === false) {
                this.estadopersona = 'Deshabilitar';
              } else {
                this.estadopersona = 'Habilitar';
              }
              if (res[0].pacienteaceptado === false) {
                this.pacienteaceptadopersona = 'no Admitido';
              } else {
                this.pacienteaceptadopersona = 'Admitido';
              }
            }
          }
          );
        });

  }
  verModificar() {
    this.verestadoModificar = true;
  }
  onSelectionChangeadmitidopaciente(admitidopaciente) {
    this.estadopacienteadmitido.pacienteaceptado = admitidopaciente.id;
  }
  Cancelar() {
    this.estadopacienteadmitido = new SituacionPacienteModel();
    sessionStorage.removeItem('estadopacienteadmitido');
    this.router.navigate(['wasi/historialgeneral']);
  }
  onSelectionChangeestadopaciente(estadopaciente) {
    this.estadopacienteadmitido.estado = estadopaciente.id;
  }

  validacion() {
    if (this.idExpediente === 0) {
      return false;
    } else {
      return true;
    }

  }
  saveOrUpdateEstadoPaciente(): void {
    if (this.isValid) {
      this.situapaciSrv.saveOrUpdateSituaPaciente(this.estadopacienteadmitido).subscribe(res => {
        if (res.responseCode === OK) {
          swal("Bien!", "Guardado!", "success").then(() => {
            this.router.navigate(['wasi/historialgeneral']);
          });
        } else {
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }

}
