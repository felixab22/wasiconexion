import { FileUploader } from 'ng2-file-upload';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { OK } from '../../models/configuration/httpstatus';
import { Router } from '@angular/router';
import {
  SocioEconomicaModel,
  ReferenciaModel,
  SituacionPacienteModel,
  CompisicionFamiliarModel,
  ExpedienteModel,
  SituacionEconomicaModel,
  DepartamentoModel,
  ProvinciaModel,
  DistritoModel
} from '@models/model.index';

import {
  CompoFamiliarService,
  SocioEconomicaService,
  UbigeoService,
  ExpedienteService,
  SituaEconomicService,
  SituaPacientecService,
  ReferenciaService,
  FilesService
} from '@services/servicio.index';
import { URL_SERVICIOS } from 'app/config/config';
declare var swal: any;
const URL = URL_SERVICIOS + '/upload';
@Component({
  selector: 'app-eval-socio-economica',
  templateUrl: './eval-socio-economica.component.html',
  styles: []
})
export class EvalSocioEconomicaComponent implements OnInit {
  documentnames: Array<any>;
  public uploader: FileUploader = new FileUploader({ url: URL });
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;
  igualar8: string;
  igualar7: string;
  igualar6: string;
  igualar5: string;
  igualar4: string;
  igualar3: string;
  igualar2: string;
  igualar: string;
  encontro: boolean;
  chekeadozona: string;
  verOtroZonaRural: boolean;
  necesitaCertificados6: { id: number; descripcion: string; }[];
  necesitaCertificados5: { id: number; descripcion: string; }[];
  necesitaCertificados4: { id: number; descripcion: string; }[];
  necesitaCertificados3: { id: number; descripcion: string; }[];
  necesitaCertificados2: { id: number; descripcion: string; }[];
  necesitaCertificados1: { id: number; descripcion: string; }[];
  apoderados: { id: number; descripcion: string; }[];
  programasociales: { id: number; descripcion: string; }[];
  pcdOrganizaciones: { id: number; descripcion: string; }[];
  cuentaconCertificados: { id: number; descripcion: string; }[];
  necesitaCertificadoS: { id: number; descripcion: string; }[];
  hogarColaboras: { id: number; descripcion: string; }[];
  habilidadesDestrezas: { id: number; descripcion: string; }[];
  participaActividades: { id: number; descripcion: string; }[];
  tipoIntitucines: { id: number; descripcion: string; }[];
  nivelEducAlcansados: { id: number; descripcion: string; }[];
  tipoSeguroSaludes: { id: number; descripcion: string; }[];
  otrosTipoProblemas: { id: number; descripcion: string; }[];
  adquirioDiscapacidades: { id: number; descripcion: string; }[];
  bienesposeeHogares: { id: number; descripcion: string; }[];
  conbustibleMasUsos: { id: number; descripcion: string; }[];
  servHiguienicos: { id: number; descripcion: string; }[];
  tipoAlumbrados: { id: number; descripcion: string; }[];
  abasteAguas: { id: number; descripcion: string; }[];
  ambientesViviendas: { id: number; descripcion: string; }[];
  matPredominanentesPisos: { id: number; descripcion: string; }[];
  matPredominanenteTechos: { id: number; descripcion: string; }[];
  tipoViviendas: { id: number; descripcion: string; }[];
  caracterisViviendas: { id: number; descripcion: string; }[];
  tenenciaViviendas: { id: number; descripcion: string; }[];
  zonaRurales: { id: number; descripcion: string; }[];
  zonaUrbanas: { id: number; descripcion: string; }[];
  probleFamiliares: { id: number; descripcion: string; }[];
  tipoFamiliares: { id: number; descripcion: string; }[];
  nivelInstruciones: { id: number; descripcion: string; }[];
  parentescos: { id: number; descripcion: string; }[];
  botonHistorial: boolean;
  botonDNI: boolean;
  evaluacionnueva: number;
  public listarReferencias: Array<ReferenciaModel>;
  public referencia: ReferenciaModel; 
  verEvaluacionSocioeconomica: boolean;
  verActualizar: boolean;
  verGuardar: boolean;
  chekeadotipoAlumbrado: string;
  chekeadocaracterisVivienda: string;
  chekeadoservHiguienico: string;
  chekeadotipoVivienda: string;
  chekeadoabasteAgua: string;
  chekeadoconbustibleMasUsa: string;
  chekeadoadquirioDiscapacidad: string;
  chekeadotipoSeguroSalud: string;
  chekeadonivelEducAlcansado: string;
  chekeadotipoIntitucin: string;
  chekeadonecesitaCertificado: string;
  chekeadonecesitaCertificado1: string;
  chekeadonecesitaCertificado2: string;
  chekeadonecesitaCertificado3: string;
  chekeadonecesitaCertificado4: string;
  chekeadomatPredominanentesPiso: string;
  chekeadozonaRural: string;
  chekeadomatPredominanenteTecho: string;
  chekeadotenenciaVivienda: string;
  chekeadozonaUrbana: string;
  chekeadoprobleFamiliar: string;
  chekeadotipoFamiliar: string;
  public respuesta: SocioEconomicaModel[];
  public situaPaciente: SituacionPacienteModel;
  otrosProblemas: string;
  verOtroPresetProble: boolean;
  OtrosProblemas1: string;
  verOtroCumbustible: boolean;
  verOtroTipoAgua: boolean;
  verTipoAlumbrado: boolean;
  verServhh: boolean;
  verCaractVivienda: boolean;
  verTipoVivienda: boolean;
  verMateriaPiso: boolean;
  verMateriaTecho: boolean;
  verCuentosDormitorios: boolean;
  totalegreso: number;
  totalingreso: number;
  verCrearVisita: boolean;
  verxHistorial: boolean;

  verxDNIthis: boolean;
  verxDNI: boolean;
  historial: any;
  k: number;
  valor33: any;
  valor3: any;
  valor22: any;
  valor11: any;
  verDistritopadre: boolean;
  verProvinciapadre: boolean;
  Expediente: any;
  verCrearFamilia: boolean;
  familiares: Array<CompisicionFamiliarModel>;

  selectedotrosTip: string[] = [];
  selectepartbienes: string[] = [];
  viviendasOtros: string;
  cantDormitorios: string;
  verOtroRadiotenenciaVivienda: boolean;
  verOtroRadiozonaUrbanas: boolean;
  verOtroRadioprobleFamiliar: boolean;
  verOtroRadiotipoFamiliar: boolean;
  provi3: any;
  provi2: any;
  verDistritoFamilia: boolean;
  depa2: any;
  depa3: any;
  verProvinciafamilia: boolean;
  verOtroRadioprogramasocial: boolean;
  selectedprograma1: string[] = [];

  verOtroRadioterapiaotrolugar: boolean;
  verOtroRadioestimulacion: boolean;
  verOtroRadiopcdOrganizacion: boolean;
  verOtroRadioNecesitaortopedico: boolean;
  verOtroRadionecesitaCertificado: boolean;
  selectedpcd: string[] = [];
  selectedcuentacon: string[] = [];
  selectedhabilidades: string[] = [];
  selectedhogare: string[] = [];
  selecteparticipaAct: string[] = [];
  verOtroRadiocuentaconCertificados: boolean;

  verOtroRadiohabilidadesDestreza: boolean;
  verOtroRadiohogarColabora: boolean;
  verOtroRadioparticipaActividade: boolean;
  verOtroRadiotipoIntitucin: boolean;
  verOtroRadionivelEducAlcansado: boolean;

  term: string;
  verOtroRadiotipoSeguroSalud: boolean;
  verOtroRadioadquirioDiscapacidad: boolean;
  verOtroRadioCondicionlabMadre: boolean;
  verOtroRadioOcupacionMadre: boolean;
  verOtroRadioReligionMadre: boolean;
  verOtroRadioIdiomaMadre: boolean;
  selectIdiomamadre: number;
  selectedAmbienteVivienda: string[] = [];


  public idExpediente: number;
  public evaluacionSocioeconomica: SocioEconomicaModel;
  public familiar: CompisicionFamiliarModel;
  public expediente: ExpedienteModel;
  public economia: SituacionEconomicaModel;


  public ubigeoDepartamentos: Array<DepartamentoModel>;
  public ubigeoProvincias: Array<ProvinciaModel>;
  public ubigeoDistritos: Array<DistritoModel>;
  public expedientes: Array<ExpedienteModel>;
  guardado = false;
  public isValid: boolean = true;
  private message: string = '';
  valor2: any;
  valor1: any;
  provi: any;
  public depa: DepartamentoModel;
  public i: number;
  public j: number;
  public dni: number;
  public idiomas = [];
  public estadoCiviles = [];
  public religiones = [];
  problemFamiliares = [];
  UbicionCasas = [];
  selectedEntry = [];

  public verpaciente: boolean;
  public verMadre1: boolean;
  public verPadre1: boolean;
  public verDistrito: boolean;
  public verProvincia: boolean;
  public verDistrito1: boolean;
  public verProvincia1: boolean;
  public verzonaUrbana: boolean;
  public verzonaRural: boolean;
  selectedtipoproble: any;
  guardado1: boolean;
  guardado2: boolean;
  guardado3: boolean;
  verAtender = false;
  codigoHistorial: any;
  personaeval=0;
  constructor(
    private familiaSrv: CompoFamiliarService,
    private socioecoSRV: SocioEconomicaService,
    private ubigeoServ: UbigeoService,
    private expedienteSrv: ExpedienteService,
    private createSituaEconomicServ: SituaEconomicService,
    private createSituaPacienteServ: SituaPacientecService,
    private referenciSrv: ReferenciaService,
    private _router: Router,
    private _uploadFilesSrv: FilesService
  ) {
    if (sessionStorage.getItem('evaluacionSocioeconomica')) {
      this.evaluacionSocioeconomica = JSON.parse(sessionStorage.getItem('evaluacionSocioeconomica'));
    } else {
      this.evaluacionSocioeconomica = new SocioEconomicaModel();
    }
    if (sessionStorage.getItem('evaluar')) {
      this.personaeval = JSON.parse(sessionStorage.getItem('evaluar'));
     
      
    } 
    if (sessionStorage.getItem('familiar')) {
      this.familiar = JSON.parse(sessionStorage.getItem('familiar'));
    } else {
      this.familiar = new CompisicionFamiliarModel();
    }
    if (sessionStorage.getItem('economia')) {
      this.economia = JSON.parse(sessionStorage.getItem('economia'));
    } else {
      this.economia = new SituacionEconomicaModel();
    }
    if (sessionStorage.getItem('situaPaciente')) {
      this.situaPaciente = JSON.parse(sessionStorage.getItem('situaPaciente'));
    } else {
      this.situaPaciente = new SituacionPacienteModel();
    }
    if (sessionStorage.getItem('referencia')) {
      this.referencia = JSON.parse(sessionStorage.getItem('referencia'));
    } else {
      this.referencia = new ReferenciaModel();
    }
  }

  ngOnInit() {
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));
    if (autorization === 6 || autorization === 5 || autorization === 4) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }

    this.guardado = false;
    this.guardado1 = false;
    this.guardado2 = false;
    this.guardado3 = false;
    this.term = '';
    this.cantDormitorios = '';
    this.viviendasOtros = '';
    this.totalingreso = 0;
    this.totalegreso = 0;
    this.idExpediente = 0;
    this.loadDepartamento();
    this.verpaciente = false;
    this.verMadre1 = false;
    this.verPadre1 = false;
    this.verDistrito = false;
    this.verProvincia = false;
    this.verDistrito1 = false;
    this.verProvincia1 = false;
    this.verzonaRural = true;
    this.verzonaUrbana = true;
    this.verProvinciafamilia = false;
    this.verDistritoFamilia = false;
    this.verCrearFamilia = false;
    this.verDistritopadre = false;
    this.verMateriaPiso = false;
    this.verDistritopadre = false;
    // otro
    this.verOtroRadioIdiomaMadre = false;
    this.verOtroRadioReligionMadre = false;
    this.verOtroRadioOcupacionMadre = false;
    this.verOtroRadioCondicionlabMadre = false;
    this.verOtroRadioadquirioDiscapacidad = false;
    this.verOtroRadiotipoSeguroSalud = false;
    this.verOtroRadionivelEducAlcansado = false;
    this.verOtroRadiotipoIntitucin = false;
    this.verOtroRadioparticipaActividade = false;
    this.verOtroRadiohogarColabora = false;
    this.verOtroRadiohabilidadesDestreza = false;
    this.verOtroRadiocuentaconCertificados = false;
    this.verOtroRadionecesitaCertificado = false;
    this.verOtroRadioNecesitaortopedico = false;
    this.verOtroRadiopcdOrganizacion = false;
    this.verOtroRadioestimulacion = false;
    this.verOtroRadioterapiaotrolugar = false;
    this.verOtroRadioprogramasocial = false;
    this.verOtroRadiotipoFamiliar = false;
    this.verOtroRadioprobleFamiliar = false;
    this.verOtroRadiozonaUrbanas = false;
    this.verOtroZonaRural = false;
    this.verOtroRadiotenenciaVivienda = false;
    this.verCuentosDormitorios = false;
    this.verMateriaTecho = false;
    this.verTipoVivienda = false;
    this.verCaractVivienda = false;
    this.verServhh = false;
    this.verTipoAlumbrado = false;
    this.verOtroTipoAgua = false;
    this.verOtroCumbustible = false;
    this.verOtroPresetProble = false;
    this.OtrosProblemas1 = '';

    this.parentescos = [
      { id: 1, descripcion: 'Abuelo(a)' },
      { id: 2, descripcion: 'Bisabuelo(a)' },
      { id: 3, descripcion: 'Cuñado(a)' },
      { id: 4, descripcion: 'Hermano(a)' },
      { id: 5, descripcion: 'Primo(a)' },
      { id: 6, descripcion: 'Sobrino(a)' },
      { id: 194, descripcion: 'Tío(a)' },
    ];


    this.nivelInstruciones = [
      { id: 7, descripcion: 'Sin estudios' },
      { id: 8, descripcion: 'Inicial' },
      { id: 9, descripcion: 'Primaria' },
      { id: 10, descripcion: 'Secundaria' },
      { id: 11, descripcion: 'Tecnico superior' },
      { id: 12, descripcion: 'Universitario' },
      { id: 13, descripcion: 'Otro' }];

    this.nivelInstruciones = [
      { id: 7, descripcion: 'Sin estudios' },
      { id: 8, descripcion: 'Inicial' },
      { id: 9, descripcion: 'Primaria completo' },
      { id: 10, descripcion: 'Primaria incompleto' },
      { id: 11, descripcion: 'Secundaria completo' },
      { id: 12, descripcion: 'Secundaria incompleto' },
      { id: 13, descripcion: 'Superior incompleto' },
      { id: 195, descripcion: 'Superior completo' },
      { id: 196, descripcion: 'Estudiante universitario' },
    ];

    this.tipoFamiliares = [
      { id: 14, descripcion: 'Nuclear' },
      { id: 15, descripcion: 'Incompleta' },
      { id: 16, descripcion: 'Extensa' },
      { id: 17, descripcion: 'Reconstituida' },
      { id: 18, descripcion: 'Adoptiva' },
      { id: 19, descripcion: 'Otro' }];

    this.probleFamiliares = [
      { id: 20, descripcion: 'Violencia Familiar' },
      { id: 21, descripcion: 'Maltrato Infantil' },
      // { id: 22, descripcion: 'Problemas de Adicción(Alcohol,drogas,etc)' },
      { id: 22, descripcion: 'Problemas de Adicción' },
      { id: 23, descripcion: 'Abandono familia' },
      { id: 24, descripcion: 'Antecedentes penales' },
      { id: 25, descripcion: 'Otro' }];

    this.zonaUrbanas = [
      { id: 26, descripcion: 'Ciudad' },
      { id: 27, descripcion: 'AA.HH.' },
      { id: 28, descripcion: 'Asoc. De Vivienda' },
      { id: 29, descripcion: 'Villa' },
      { id: 30, descripcion: 'Invasión' },
      { id: 31, descripcion: 'Otro' }];

    this.zonaRurales = [
      { id: 32, descripcion: 'Pueblo' },
      { id: 33, descripcion: 'Anexo' },
      { id: 34, descripcion: 'Centro Poblado' },
      { id: 35, descripcion: 'Comunidad' },
      { id: 36, descripcion: 'Distrito' },
      { id: 37, descripcion: 'Otro' }];

    this.tenenciaViviendas = [
      { id: 38, descripcion: 'Alquilada' },
      { id: 39, descripcion: 'Alojada' },
      { id: 40, descripcion: 'Propia' },
      { id: 41, descripcion: 'Guarniania' },
      { id: 42, descripcion: 'Invasión' },
      { id: 43, descripcion: 'Otro' }];

    this.caracterisViviendas = [
      { id: 44, descripcion: 'Unifamiliar' },
      { id: 45, descripcion: 'Multifamiliar' },
      { id: 46, descripcion: 'Otro' }];

    this.tipoViviendas = [
      { id: 47, descripcion: 'Material Noble y acabado' },
      { id: 48, descripcion: 'Material Noble y en construccion' },
      { id: 49, descripcion: 'Mixta(material noble + adobe)' },
      { id: 50, descripcion: 'Adobe' },
      { id: 51, descripcion: 'Rústico' },
      { id: 52, descripcion: 'Provisional' },
      { id: 53, descripcion: 'Precario' },
      { id: 54, descripcion: 'Otro' }];

    this.matPredominanenteTechos = [
      { id: 55, descripcion: 'Concreto Armado' },
      { id: 56, descripcion: 'Calamina' },
      { id: 57, descripcion: 'Teja' },
      { id: 58, descripcion: 'Madera' },
      { id: 59, descripcion: 'Eternet' },
      { id: 60, descripcion: 'Estera' },
      { id: 61, descripcion: 'Otro' }];

    this.matPredominanentesPisos = [
      { id: 62, descripcion: 'Tierra' },
      { id: 63, descripcion: 'Cemento' },
      { id: 64, descripcion: 'Madera' },
      { id: 65, descripcion: 'Losetas' },
      { id: 66, descripcion: 'Parquet' },
      { id: 67, descripcion: 'Otro' }];

    this.ambientesViviendas = [
      { id: 68, descripcion: 'Cocina' },
      { id: 69, descripcion: 'Sala' },
      { id: 70, descripcion: 'Patio' },
      { id: 71, descripcion: 'Dormitorios....' }];

    this.abasteAguas = [
      { id: 72, descripcion: 'Red Pública de vivienda' },
      { id: 73, descripcion: 'Red Pública fuera de vivienda' },
      { id: 74, descripcion: 'Pozo' },
      { id: 75, descripcion: 'Río' },
      { id: 76, descripcion: 'Entubada' },
      { id: 77, descripcion: 'Otro' }];

    this.tipoAlumbrados = [
      { id: 78, descripcion: 'Electricidad' },
      { id: 79, descripcion: 'Velas' },
      { id: 80, descripcion: 'Gas' },
      { id: 81, descripcion: 'Otro' }];

    this.servHiguienicos = [
      { id: 82, descripcion: 'Desagüe' },
      { id: 83, descripcion: 'Letrina' },
      { id: 84, descripcion: 'Pozo Séptico' },
      { id: 85, descripcion: 'Improvisado' },
      { id: 86, descripcion: 'Otro' }];

    this.conbustibleMasUsos = [
      { id: 87, descripcion: 'Gas' },
      { id: 88, descripcion: 'Electricidad' },
      { id: 89, descripcion: 'Kerosene' },
      { id: 90, descripcion: 'Leña' },
      { id: 91, descripcion: 'Otro' }];

    this.bienesposeeHogares = [
      { id: 92, descripcion: 'Televisión' },
      { id: 93, descripcion: 'Radio/Equipo de Sonido' },
      { id: 94, descripcion: 'Teléfono Fijo' },
      { id: 95, descripcion: 'Cocina' },
      { id: 96, descripcion: 'Refrigeradora' },
      { id: 97, descripcion: 'Computadora' },
      { id: 98, descripcion: 'Internet' },
      { id: 99, descripcion: 'Cable' },
      { id: 100, descripcion: 'Microondas' },
      { id: 101, descripcion: 'Otro' }];

    this.adquirioDiscapacidades = [
      { id: 102, descripcion: 'Nacimiento' },
      { id: 103, descripcion: 'Accidente de Tránsito' },
      { id: 104, descripcion: 'Problema Congénito' },
      { id: 105, descripcion: 'Enfermedad' },
      { id: 106, descripcion: 'Negligencia Médica' },
      { id: 107, descripcion: 'Violencia Política' },
      { id: 108, descripcion: 'Internet' },
      { id: 109, descripcion: 'Otro' }];

    this.otrosTipoProblemas = [
      { id: 110, descripcion: 'Epilepsia' },
      { id: 111, descripcion: 'Asma' },
      { id: 112, descripcion: 'Alergias' },
      { id: 113, descripcion: 'Mal nutrición' },
      { id: 114, descripcion: 'Hepatitis' },
      { id: 115, descripcion: 'Violencia' },
      { id: 116, descripcion: 'Sobrepeso' },
      { id: 117, descripcion: 'Convulsiones' },
      { id: 193, descripcion: 'Estrabismo' },
      { id: 118, descripcion: 'Estreñimiento' },
      { id: 119, descripcion: 'Infección urinaria' },
      { id: 120, descripcion: 'Otro' }];

    this.tipoSeguroSaludes = [
      { id: 121, descripcion: 'Seguro Integral' },
      { id: 122, descripcion: 'ESSALUD' },
      { id: 123, descripcion: 'Hospital' },
      { id: 124, descripcion: 'Salidad de la PNP' },
      { id: 125, descripcion: 'Seguro Privado - Clínica' },
      { id: 126, descripcion: 'Otro' }];

    this.nivelEducAlcansados = [
      { id: 270, descripcion: 'Inicial' },
      { id: 128, descripcion: 'Primaria' },
      { id: 129, descripcion: 'Secundaria' },
      { id: 130, descripcion: 'Superior Universitario' },
      { id: 131, descripcion: 'Superior no Universitario' },
      { id: 132, descripcion: 'CEO' },
      { id: 133, descripcion: 'Otro' }];

    this.tipoIntitucines = [
      { id: 134, descripcion: 'Estatal' },
      { id: 135, descripcion: 'Particular' },
      { id: 136, descripcion: 'Especial' },
      { id: 137, descripcion: 'No estudia' }];

    this.participaActividades = [
      { id: 138, descripcion: 'Educación Física' },
      { id: 139, descripcion: 'Vóley' },
      { id: 140, descripcion: 'Básquet' },
      { id: 141, descripcion: 'Futbol' },
      { id: 142, descripcion: 'Danza' },
      { id: 143, descripcion: 'Teatro' },
      { id: 144, descripcion: 'Canto' },
      { id: 145, descripcion: 'Otro' }];

    this.habilidadesDestrezas = [
      { id: 146, descripcion: 'Pinta' },
      { id: 147, descripcion: 'Dibuja' },
      { id: 148, descripcion: 'Arma cubos' },
      { id: 149, descripcion: 'Otro' }];

    this.hogarColaboras = [
      { id: 150, descripcion: 'Limpia' },
      { id: 151, descripcion: 'Ordena' },
      { id: 152, descripcion: 'Barrer' },
      { id: 153, descripcion: 'Regar las plantas' },
      { id: 154, descripcion: 'Pelar las verduras' },
      { id: 155, descripcion: 'Lavar los servicios' },
      { id: 156, descripcion: 'Lavar su ropa' },
      { id: 157, descripcion: 'Tender la cama' },
      { id: 158, descripcion: 'Asearse' },
      { id: 159, descripcion: 'Otro' }];

    this.necesitaCertificados1 = [
      { id: 160, descripcion: 'Si' },
      { id: 161, descripcion: 'No' }];

    this.necesitaCertificados2 = [
      { id: 183, descripcion: 'Si' },
      { id: 184, descripcion: 'No' }];

    this.necesitaCertificados3 = [
      { id: 185, descripcion: 'Si' },
      { id: 186, descripcion: 'No' }];

    this.necesitaCertificados4 = [
      { id: 187, descripcion: 'Si' },
      { id: 188, descripcion: 'No' }];

    this.necesitaCertificados5 = [
      { id: 189, descripcion: 'Si' },
      { id: 190, descripcion: 'No' }];

    this.necesitaCertificados6 = [
      { id: 191, descripcion: 'Si' },
      { id: 192, descripcion: 'No' }];

    this.cuentaconCertificados = [
      { id: 162, descripcion: 'Certificado de Discapacidad ' },
      { id: 163, descripcion: 'Resolución Ministerial' },
      { id: 164, descripcion: 'Carnet de Discapacidad' },
      { id: 165, descripcion: 'Está Inscrito en CONADIS' },
      { id: 166, descripcion: 'Otro' }];

    this.pcdOrganizaciones = [
      { id: 167, descripcion: 'CONADIS ' },
      { id: 168, descripcion: 'OMAPED' },
      { id: 169, descripcion: 'Otro' }];

    this.programasociales = [
      { id: 170, descripcion: 'Asoc. De Epilépticos de Ayac. -A.E.A' },
      { id: 171, descripcion: 'Comisión de Salud Mental' },
      { id: 172, descripcion: 'Programa Integral de Nutrición' },
      { id: 173, descripcion: 'Programas de Vaso de Leche' },
      { id: 174, descripcion: 'Cuna Más' },
      { id: 175, descripcion: 'Programas de Intervención Temprana-PRITE' },
      { id: 176, descripcion: 'Programa de estimulación temprana' },
      { id: 177, descripcion: 'Programa JUNTOS' },
      { id: 178, descripcion: 'Comedor Popular' },
      { id: 179, descripcion: 'Otro' }];

    this.apoderados = [
      { id: 180, descripcion: 'Padre ' },
      { id: 181, descripcion: 'Madre' },
      { id: 182, descripcion: 'Apoderado' }];

    this.verGuardar = true;
    this.verActualizar = false;
    this.verEvaluacionSocioeconomica = false;
    this.verxHistorial = false;
    this.verxDNI = false;
    this.verCrearVisita = false;
    this.botonDNI = true;
    this.botonHistorial = true;
    this.verzonaRural = false;
    this.verzonaUrbana = false;
    // this.selectsituacionEcon();
      if(this.personaeval!==0) {
        this.imprimiExpedientexHistorial(this.personaeval);
      }

  }

  private loadDepartamento(): void {
    this.ubigeoServ.getDepartamento().subscribe(res => {
      this.ubigeoDepartamentos = res;
    });
  }
  buscarxDNI() {
    this.verxDNI = true;
    this.verxHistorial = false;
  }
  Buscarxhistorial() {
    this.verxHistorial = true;
    this.verxDNI = false;
  }
  imprimiExpedientexHistorial(historial) {
    this.expedienteSrv.getBusquedaxHistorial(historial).subscribe(
      (data: any[]) => {
        if (data[0] === undefined) {
          swal('Error!', 'No existe el Código!', 'success').then(() => {
            this._router.navigate(['wasi/HistoriaSocial']);
          });
        } else {
          this.verpaciente = true;
          this.expedientes = data[0];
          this.idExpediente = data[0].id_expediente;
          this.codigoHistorial = data[0].codigohistorial;
          this.cargardatosevaluacion(this.idExpediente);
          this.verxHistorial = false;
          this.verxDNI = false;
          this.botonDNI = false;
          this.botonHistorial = false;
          this.imprimirSituacionEconimica();
          sessionStorage.removeItem('evaluar');
        }
      });
  }

  imprimiExpedientexDNI(dni) {
    this.expedienteSrv.getBusquedaDNI(dni).subscribe(
      (data: any[]) => {
        if (data[0] === undefined) {
          swal('Error!', 'No existe el Código!', 'success').then(() => {
            this._router.navigate(['wasi/HistoriaSocial']);
          });
        } else {
          this.expedientes = data[0];
          this.idExpediente = data[0].id_expediente;
          this.codigoHistorial = data[0].codigohistorial;
          this.verpaciente = true;
          this.evaluacionSocioeconomica.idexpediente = data[0].id_expediente;
          this.cargardatosevaluacion(this.idExpediente);
          this.verxHistorial = false;
          this.verxDNI = false;
          this.botonDNI = false;
          this.botonHistorial = false;
          this.imprimirSituacionEconimica();
        }
      });
  }
  public recorrido(dato: string, donde: { id: number; descripcion: string; }[]): boolean {
    this.encontro = false;
    for (let h = 0; h < donde.length; h++) {
      if (donde[h].descripcion === dato) {
        this.encontro = true;
        h = donde.length;
      } else {
        this.encontro = false;
      }
    }
    return this.encontro;
  }
  cargardatosevaluacion(dato) {
    this.verEvaluacionSocioeconomica = true;
    this.socioecoSRV.getSocioEconxexpediente(dato).subscribe(res => {
      if (res[0] !== undefined) {
        this.respuesta = res;
        console.log(res);
        this.evaluacionSocioeconomica.createdAt = this.respuesta[0].createdAt;
        this.evaluacionSocioeconomica.pagosemanal = this.respuesta[0].pagosemanal;
        this.evaluacionSocioeconomica.pagomensual = this.respuesta[0].pagomensual;
        this.evaluacionSocioeconomica = this.respuesta[0];

        if (this.respuesta[0].tipoFamilia !== null) {
          if (this.recorrido(this.respuesta[0].tipoFamilia, this.tipoFamiliares) === true) {
            this.chekeadotipoFamiliar = this.respuesta[0].tipoFamilia;
          } else {
            this.chekeadotipoFamiliar = 'Otro';
            this.verOtroRadiotipoFamiliar = true;
          }
        }
        if (this.respuesta[0].probFami !== null) {
          if (this.recorrido(this.respuesta[0].probFami, this.probleFamiliares) === true) {
            this.chekeadoprobleFamiliar = this.respuesta[0].probFami;
          } else {
            this.chekeadoprobleFamiliar = 'Otro';
            this.verOtroRadioprobleFamiliar = true;
          }
        }
        if (this.respuesta[0].zonaUrbana !== null) {
          this.chekeadozona = 'Zona Urbana';
          this.verzonaUrbana = true;
          if (this.recorrido(this.respuesta[0].zonaUrbana, this.zonaUrbanas) === true) {
            this.chekeadozonaUrbana = this.respuesta[0].zonaUrbana;
          } else {
            this.chekeadozonaUrbana = 'Otro';
            this.verOtroRadiozonaUrbanas = true;
          }
        }
        if (this.respuesta[0].zonaRural !== null) {
          this.chekeadozona = 'Zona Rural';
          this.verzonaRural = true;
          if (this.recorrido(this.respuesta[0].zonaRural, this.zonaRurales) === true) {
            this.chekeadozonaRural = this.respuesta[0].zonaRural;
          } else {
            this.chekeadozonaRural = 'Otro';
            this.verOtroZonaRural = true;
          }
        }
        if (this.respuesta[0].tenenciaVivienda !== null) {
          if (this.recorrido(this.respuesta[0].tenenciaVivienda, this.tenenciaViviendas) === true) {
            this.chekeadotenenciaVivienda = this.respuesta[0].tenenciaVivienda;
          } else {
            this.chekeadotenenciaVivienda = 'Otro';
            this.verOtroRadiotenenciaVivienda = true;
          }
        }
        if (this.respuesta[0].matPredTecho !== null) {
          if (this.recorrido(this.respuesta[0].matPredTecho, this.matPredominanenteTechos) === true) {
            this.chekeadomatPredominanenteTecho = this.respuesta[0].matPredTecho;
          } else {
            this.chekeadomatPredominanenteTecho = 'Otro';
            this.verMateriaTecho = true;
          }
        }
        if (this.respuesta[0].matPredPiso !== null) {
          if (this.recorrido(this.respuesta[0].matPredPiso, this.matPredominanentesPisos) === true) {
            this.chekeadomatPredominanentesPiso = this.respuesta[0].matPredPiso;
          } else {
            this.chekeadomatPredominanentesPiso = 'Otro';
            this.verMateriaPiso = true;
          }
        }
        if (this.respuesta[0].tipoVivienda !== null) {
          if (this.recorrido(this.respuesta[0].tipoVivienda, this.tipoViviendas) === true) {
            this.chekeadotipoVivienda = this.respuesta[0].tipoVivienda;
          } else {
            this.chekeadotipoVivienda = 'Otro';
            this.verTipoVivienda = true;
          }
        }
        if (this.respuesta[0].caractVivienda !== null) {
          if (this.recorrido(this.respuesta[0].caractVivienda, this.caracterisViviendas) === true) {
            this.chekeadocaracterisVivienda = this.respuesta[0].caractVivienda;
          } else {
            this.chekeadocaracterisVivienda = 'Otro';
            this.verCaractVivienda = true;
          }
        }
        if (this.respuesta[0].tipoSshh !== null) {
          if (this.recorrido(this.respuesta[0].tipoSshh, this.servHiguienicos) === true) {
            this.chekeadoservHiguienico = this.respuesta[0].tipoSshh;
          } else {
            this.chekeadoservHiguienico = 'Otro';
            this.verServhh = true;
          }
        }
        if (this.respuesta[0].tipoAlumbrado !== null) {
          if (this.recorrido(this.respuesta[0].tipoAlumbrado, this.tipoAlumbrados) === true) {
            this.chekeadotipoAlumbrado = this.respuesta[0].tipoAlumbrado;
          } else {
            this.chekeadotipoAlumbrado = 'Otro';
            this.verTipoAlumbrado = true;
          }
        }
        if (this.respuesta[0].abastAgua !== null) {
          if (this.recorrido(this.respuesta[0].abastAgua, this.abasteAguas) === true) {
            this.chekeadoabasteAgua = this.respuesta[0].abastAgua;
          } else {
            this.chekeadoabasteAgua = 'Otro';
            this.verOtroTipoAgua = true;
          }
        }
        if (this.respuesta[0].combCocinar !== null) {
          if (this.recorrido(this.respuesta[0].combCocinar, this.conbustibleMasUsos) === true) {
            this.chekeadoconbustibleMasUsa = this.respuesta[0].combCocinar;
          } else {
            this.chekeadoconbustibleMasUsa = 'Otro';
            this.verOtroCumbustible = true;
          }
        }
        if (this.respuesta[0].fechaDiscapac !== null) {
          if (this.recorrido(this.respuesta[0].fechaDiscapac, this.adquirioDiscapacidades) === true) {
            this.chekeadoadquirioDiscapacidad = this.respuesta[0].fechaDiscapac;
          } else {
            this.chekeadoadquirioDiscapacidad = 'Otro';
            this.verOtroRadioadquirioDiscapacidad = true;
          }
        }
        if (this.respuesta[0].tipoSeguro !== null) {
          if (this.recorrido(this.respuesta[0].tipoSeguro, this.tipoSeguroSaludes) === true) {
            this.chekeadotipoSeguroSalud = this.respuesta[0].tipoSeguro;
          } else {
            this.chekeadotipoSeguroSalud = 'Otro';
            this.verOtroRadiotipoSeguroSalud = true;
          }
        }
        if (this.respuesta[0].nivelEduc !== null) {
          if (this.recorrido(this.respuesta[0].nivelEduc, this.nivelEducAlcansados) === true) {
            this.chekeadonivelEducAlcansado = this.respuesta[0].nivelEduc;
          } else {
            this.chekeadonivelEducAlcansado = 'Otro';
            this.verOtroRadionivelEducAlcansado = true;
          }
        }
        if (this.respuesta[0].tipoInstiAsiste !== null) {
          if (this.recorrido(this.respuesta[0].tipoInstiAsiste, this.tipoIntitucines) === true) {
            this.chekeadotipoIntitucin = this.respuesta[0].tipoInstiAsiste;
          } else {
            this.chekeadotipoIntitucin = 'Otro';
            this.verOtroRadiotipoIntitucin = true;
          }
        }
        if (this.respuesta[0].necCertifDiscap !== null) {
          this.chekeadonecesitaCertificado = this.respuesta[0].necCertifDiscap;
        }
        if (this.respuesta[0].poseeMatortopedico !== null) {
          if (this.respuesta[0].poseeMatortopedico !== 'No') {
            this.chekeadonecesitaCertificado1 = 'Si';
            this.verOtroRadionecesitaCertificado = true;
          } else {
            this.chekeadonecesitaCertificado1 = this.respuesta[0].poseeMatortopedico;
          }
        }
        if (this.respuesta[0].requiereMatortopedico !== null) {
          if (this.respuesta[0].requiereMatortopedico !== 'No') {
            this.chekeadonecesitaCertificado2 = 'Si';
            this.verOtroRadioNecesitaortopedico = true;
          } else {
            this.chekeadonecesitaCertificado2 = this.respuesta[0].requiereMatortopedico;
          }
        }
        if (this.respuesta[0].terapia_actual !== null) {
          this.chekeadonecesitaCertificado3 = this.respuesta[0].terapia_actual;
          if (this.respuesta[0].terapia_actual === 'Si') {
            this.verOtroRadioterapiaotrolugar = true;
          }
        }
        if (this.respuesta[0].terapiaAnterior !== null) {
          if (this.respuesta[0].terapiaAnterior !== 'No') {
            this.chekeadonecesitaCertificado4 = 'Si';
            this.verOtroRadioestimulacion = true;
          } else {
            this.chekeadonecesitaCertificado4 = this.respuesta[0].terapiaAnterior;
          }
        }
        
        this.imprimifamiliar();
        // this.imprimiVisitas();
        this.imprimirReferencias();
        this.verGuardar = false;
        this.verActualizar = true;
      } else {
        this.evaluacionSocioeconomica = new SocioEconomicaModel();
        this.verGuardar = true;
        this.verActualizar = false;
      }

    }
    );
  }


  onSelectionChange(entry) {
    this.selectedEntry = entry.descripcion;
  }

  setNewDepartamentoFamiliar(departamento) {
    this.depa3 = departamento;
    this.ubigeoServ.getprovinciaxDepartamento(this.depa3)
      .subscribe(
        (data3: any[]) => {
          this.valor3 = data3[0].id_depa;
          this.ubigeoProvincias = data3;
          this.verProvinciafamilia = true;

          this.ubigeoServ.getDepartamento()
            .subscribe(
              (datadepa: any[]) => {
                this.evaluacionSocioeconomica.departamento_domicilio = datadepa[this.valor3 - 1].nombre_departamento;
              });
        }
      );
  }

  setNewProvinciaFamilia(provincia) {
    this.provi3 = provincia;
    this.ubigeoServ.getDistritoxprovincia(this.provi3)
      .subscribe(
        (data3: any[]) => {
          this.ubigeoDistritos = data3;
          this.valor33 = data3[0].id_prov;
          this.verDistritoFamilia = true;
          this.ubigeoServ.getprovinciaxDepartamento(this.depa3)
            .subscribe(
              (data33prov: any[]) => {
                this.k = 0;
                do {
                  if (data33prov[this.k].id_prov === this.valor33) {
                    this.evaluacionSocioeconomica.provincia_domicilio = data33prov[this.k].nombre_provincia;
                    this.k = 200;
                  }
                  this.k++;
                } while (this.k < 200);
              }
            );
        }
      );
  }
  setNewDistritoFamilia(distr) {
    this.evaluacionSocioeconomica.distrito_domicilio = distr;
  }
  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
    for (let i = 0; i < this.uploader.queue.length; i++) {
      this.uploader.queue[i].file.name = 'social-' + this.codigoHistorial + '-' + this.uploader.queue[i].file.name;
    }
  }
  imprimirFiles() {
    this._uploadFilesSrv.getListaFilesxAreayPaciente('social', this.codigoHistorial).subscribe(res => {
      if (res.length === 0) {
        swal('error!', 'No existen archivos!', 'warning');
      } else {
        this.documentnames = res;
      }
    });
  }
  descargar(nombreArchivo) {
    window.open(`${URL_SERVICIOS}/file/${nombreArchivo}?nombrearea=social&codigo=${this.codigoHistorial}`);
  }
  eliminararchivo(documento) {
    console.log(documento);
    swal({
      title: 'Esta seguro?',
      text: 'Usted esta por eliminar el archivo!',
      icon: 'warning',
      buttons: true,
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          this._uploadFilesSrv.deleteFilesxcodigo(documento, 'social', this.codigoHistorial).subscribe(res => {
            if (res) {
              this.imprimirFiles();
              swal('Horario eliminado', {
                icon: 'success',
              });
            }
          });

        } else {
          swal('Sin acciones!');
        }
      });
  }
  onSelectionChangeTipoFamilia(entrya) {
    if (entrya.descripcion !== 'Otro') {
      this.evaluacionSocioeconomica.tipoFamilia = entrya.descripcion;
      this.verOtroRadiotipoFamiliar = false;
    } else {
      this.verOtroRadiotipoFamiliar = true;
    }
  }
  desseleccionarradioTipoFamiliar() {
    this.evaluacionSocioeconomica.tipoFamilia = null;
    this.verOtroRadiotipoFamiliar = false;
    Array.from(document.querySelectorAll('[name=radiogrouptipofami]')).forEach((x: any) => x.checked = false);
  }
  onSelectionChangeProbleFamilia(problefamilia) {
    if (problefamilia.descripcion !== 'Otro') {
      this.evaluacionSocioeconomica.probFami = problefamilia.descripcion;
      this.verOtroRadioprobleFamiliar = false;
    } else {
      this.verOtroRadioprobleFamiliar = true;
    }
  }
  desseleccionarradioprobleFamiliar() {
    this.evaluacionSocioeconomica.probFami = null;
    Array.from(document.querySelectorAll('[name=radiogroupprboble]')).forEach((x: any) => x.checked = false);
    this.verOtroRadioprobleFamiliar = false;
  }
  OculparZonaRural() {
    this.verzonaRural = false;
    this.verzonaUrbana = true;
    this.evaluacionSocioeconomica.zona_vivienda = String('Zona Urbana');
  }
  onSelectionChangeZonaUrbana(zonaurbana) {
    this.evaluacionSocioeconomica.zonaRural = null;
    this.verOtroZonaRural = false;
    if (zonaurbana.descripcion !== 'Otro') {
      this.evaluacionSocioeconomica.zonaUrbana = zonaurbana.descripcion;
      this.verOtroRadiozonaUrbanas = false;
    } else {
      this.verOtroRadiozonaUrbanas = true;
    }
  }
  onSelectionChangeTenenciaVivienda(vivienda) {
    if (vivienda.descripcion !== 'Otro') {
      this.evaluacionSocioeconomica.tenenciaVivienda = vivienda.descripcion;
      this.verOtroRadiotenenciaVivienda = false;
    } else {
      this.verOtroRadiotenenciaVivienda = true;
    }
  }
  desseleccionarradiotenenciaVivienda() {
    this.evaluacionSocioeconomica.tenenciaVivienda = null;
    Array.from(document.querySelectorAll('[name=radiogrutenenciaVivienda]')).forEach((x: any) => x.checked = false);
    this.verOtroRadiotenenciaVivienda = false;
  }
  OculparZonaUrbana() {
    this.verzonaRural = true;
    this.verzonaUrbana = false;
    this.evaluacionSocioeconomica.zona_vivienda = String('Zona Rural');
  }
  onSelectionChangeZonaRural(zonarural) {
    this.verOtroZonaRural = false;
    this.verOtroRadiozonaUrbanas = false;
    this.evaluacionSocioeconomica.zonaUrbana = null;
    if (zonarural.descripcion !== 'Otro') {
      this.evaluacionSocioeconomica.zonaRural = zonarural.descripcion;
      this.verzonaUrbana = false;
    } else {
      this.verOtroZonaRural = true;
    }
  }
  chenckboxChangeAmbienteVivienda(ambiente) {
    if (this.selectedAmbienteVivienda.indexOf(ambiente.value) === -1) {
      this.selectedAmbienteVivienda.push(ambiente.value);
      this.igualar = this.selectedAmbienteVivienda.toString();
    } else {
      this.selectedAmbienteVivienda.splice(this.selectedAmbienteVivienda.indexOf(ambiente.value), 1);
      this.igualar = this.selectedAmbienteVivienda.toString();
    }
    if (ambiente !== 'Dormitorio') {
      this.verCuentosDormitorios = true;
    } else {
      this.verCuentosDormitorios = false;
    }
  }
  igualardominioambientesVivienda() {
    this.evaluacionSocioeconomica.numambVivienda = this.igualar;
  }
  onSelectionChangematPredominanenteTecho(matetecho) {
    if (matetecho.descripcion !== 'Otro') {
      this.evaluacionSocioeconomica.matPredTecho = matetecho.descripcion;
      this.verMateriaTecho = false;
    } else {
      this.verMateriaTecho = true;
    }
  }
  desseleccionarradiomatPredominanenteTecho() {
    this.evaluacionSocioeconomica.matPredTecho = null;
    Array.from(document.querySelectorAll('[name=radiogroupmatPredominanenteTecho]')).forEach((x: any) => x.checked = false);
    this.verMateriaTecho = false;
  }
  onSelectionChangeconbustibleMasUsa(conbustible) {
    if (conbustible.descripcion !== 'Otro') {
      this.evaluacionSocioeconomica.combCocinar = conbustible.descripcion;
      this.verOtroCumbustible = false;
    } else {
      this.verOtroCumbustible = true;
    }
  }
  desseleccionarradioconbustibleMasUsa() {
    this.evaluacionSocioeconomica.combCocinar = null;
    Array.from(document.querySelectorAll('[name=radiogroupconbustibleMasUsa]')).forEach((x: any) => x.checked = false);
    this.verOtroCumbustible = false;
  }
  onSelectionChangeabasteAgua(abasteAgua) {
    if (abasteAgua.descripcion !== 'Otro') {
      this.evaluacionSocioeconomica.abastAgua = abasteAgua.descripcion;
      this.verOtroTipoAgua = false;
    } else {
      this.verOtroTipoAgua = true;
    }
  }
  desseleccionarradioabasteAgua() {
    this.evaluacionSocioeconomica.abastAgua = null;
    Array.from(document.querySelectorAll('[name=radiogruabasteAgua]')).forEach((x: any) => x.checked = false);
    this.verOtroTipoAgua = false;
  }
  onSelectionChangematPredominanentesPiso(matPredominanentesPiso) {
    if (matPredominanentesPiso.descripcion !== 'Otro') {
      this.evaluacionSocioeconomica.matPredPiso = matPredominanentesPiso.descripcion;
      this.verMateriaPiso = false;
    } else {
      this.verMateriaPiso = true;
    }
  }
  desseleccionarradiomatPredominanentesPiso() {
    this.evaluacionSocioeconomica.matPredPiso = null;
    Array.from(document.querySelectorAll('[name=radiogrupmatPredominanentesPiso]')).forEach((x: any) => x.checked = false);
    this.verMateriaPiso = false;
  }
  chenckboxChangebienesposeeHogar(bienes) {
    if (this.selectepartbienes.indexOf(bienes.value) === -1) {
      this.selectepartbienes.push(bienes.value);
      this.igualar2 = this.selectepartbienes.toString();
      this.verOtroRadioparticipaActividade = false;
    } else {
      this.selectepartbienes.splice(this.selectepartbienes.indexOf(bienes.value), 1);
      this.igualar2 = this.selectepartbienes.toString();
      this.verOtroRadioparticipaActividade = true;
    }
  }
  igualarbienesposeeHogar() {
    this.evaluacionSocioeconomica.bienesHogar = this.igualar2;
  }
  onSelectionChangetipoVivienda(tipoVivienda) {
    if (tipoVivienda.descripcion !== 'Otro') {
      this.evaluacionSocioeconomica.tipoVivienda = tipoVivienda.descripcion;
      this.verTipoVivienda = false;
    } else {
      this.verTipoVivienda = true;
    }
  }
  desseleccionarradiotipoVivienda() {
    this.evaluacionSocioeconomica.tipoVivienda = null;
    Array.from(document.querySelectorAll('[name=radiogruptipoVivienda]')).forEach((x: any) => x.checked = false);
    this.verTipoVivienda = false;
  }
  onSelectionChangeservHiguienico(servicio) {
    if (servicio.descripcion !== 'Otro') {
      this.evaluacionSocioeconomica.tipoSshh = servicio.descripcion;
      this.verServhh = false;
    } else {
      this.verServhh = true;
    }
  }
  desseleccionarradioserviciohhss() {
    this.evaluacionSocioeconomica.tipoSshh = null;
    Array.from(document.querySelectorAll('[name=radiogroupserviciohhss]')).forEach((x: any) => x.checked = false);
    this.verServhh = false;
  }
  onSelectionChangetipoAlumbrado(tipoAlumbrado) {
    if (tipoAlumbrado.descripcion !== 'Otro') {
      this.evaluacionSocioeconomica.tipoAlumbrado = tipoAlumbrado.descripcion;
      this.verTipoAlumbrado = false;
    } else {
      this.verTipoAlumbrado = true;
    }
  }
  desseleccionarradiotipoAlumbrado() {
    this.evaluacionSocioeconomica.tipoAlumbrado = null;
    Array.from(document.querySelectorAll('[name=radiogruptipoAlumbrado]')).forEach((x: any) => x.checked = false);
    this.verTipoAlumbrado = false;
  }
  onSelectionChangecaracterisVivienda(caracterisVivienda) {
    if (caracterisVivienda.descripcion !== 'Otro') {
      this.evaluacionSocioeconomica.caractVivienda = caracterisVivienda.descripcion;
      this.verCaractVivienda = false;
    } else {
      this.verCaractVivienda = true;
    }
  }
  desseleccionarradiocaracterisVivienda() {
    this.evaluacionSocioeconomica.caractVivienda = null;
    Array.from(document.querySelectorAll('[name=radiogrupcaracterisVivienda]')).forEach((x: any) => x.checked = false);
    this.verCaractVivienda = false;
  }
  onSelectionChangeadquirioDiscapacidad(adquirioDiscapacidad) {
    if (adquirioDiscapacidad.descripcion !== 'Otro') {
      this.evaluacionSocioeconomica.fechaDiscapac = adquirioDiscapacidad.descripcion;
      this.verOtroRadioadquirioDiscapacidad = false;
    } else {
      this.verOtroRadioadquirioDiscapacidad = true;
    }
  }
  desseleccionarradioadquirioDiscapacidad() {
    this.evaluacionSocioeconomica.caractVivienda = null;
    Array.from(document.querySelectorAll('[name=radiogroupadquirioDiscapacidad]')).forEach((x: any) => x.checked = false);
    this.verOtroRadioadquirioDiscapacidad = false;
  }
  onSelectionChangetipoSeguroSalud(tipoSeguroSalud) {
    if (tipoSeguroSalud.descripcion !== 'Otro') {
      this.evaluacionSocioeconomica.tipoSeguro = tipoSeguroSalud.descripcion;
      this.verOtroRadiotipoSeguroSalud = false;
    } else {
      this.verOtroRadiotipoSeguroSalud = true;
    }
  }
  desseleccionarradiotipoSeguroSalud() {
    this.evaluacionSocioeconomica.tipoSeguro = null;
    Array.from(document.querySelectorAll('[name=radiogrouptipoSeguroSalud]')).forEach((x: any) => x.checked = false);
    this.verOtroRadiotipoSeguroSalud = false;
  }
  onSelectionChangenivelEducAlcansado(nivelEducAlcansado) {
    if (nivelEducAlcansado.descripcion !== 'Otro') {
      this.evaluacionSocioeconomica.nivelEduc = nivelEducAlcansado.descripcion;
      this.verOtroRadionivelEducAlcansado = false;
    } else {
      this.verOtroRadionivelEducAlcansado = true;
    }
  }
  desseleccionarradionivelEducAlcansado() {
    this.evaluacionSocioeconomica.nivelEduc = null;
    Array.from(document.querySelectorAll('[name=radiogroupnivelEducAlcansado]')).forEach((x: any) => x.checked = false);
    this.verOtroRadionivelEducAlcansado = false;
  }
  onSelectionChangetipoIntitucin(tipoIntitucin) {
    if (tipoIntitucin.descripcion !== 'No estudia') {
      this.evaluacionSocioeconomica.tipoInstiAsiste = tipoIntitucin.descripcion;
      this.verOtroRadiotipoIntitucin = false;
    } else {
      this.verOtroRadiotipoIntitucin = true;
    }
  }
  desseleccionarradiotipoIntitucin() {
    this.evaluacionSocioeconomica.tipoInstiAsiste = null;
    Array.from(document.querySelectorAll('[name=radiogrouptipoIntitucin]')).forEach((x: any) => x.checked = false);
    this.verOtroRadiotipoIntitucin = false;
  }
  chenckboxChangeparticipaActividade(participaAct) {
    if (this.selecteparticipaAct.indexOf(participaAct.value) === -1) {
      this.selecteparticipaAct.push(participaAct.value);
      this.igualar4 = this.selecteparticipaAct.toString();
      this.verOtroRadioparticipaActividade = false;
    } else {
      this.selecteparticipaAct.splice(this.selecteparticipaAct.indexOf(participaAct.value), 1);
      this.igualar4 = this.selecteparticipaAct.toString();
      this.verOtroRadioparticipaActividade = true;
    }
  }
  igualarexamplecheckparticipaActividade() {
    this.evaluacionSocioeconomica.partActivCole = this.igualar4;
  }
  chenckboxChangehogarColabora(hogare) {
    if (this.selectedhogare.indexOf(hogare.value) === -1) {
      this.selectedhogare.push(hogare.value);
      this.igualar5 = this.selectedhogare.toString();
      this.verOtroRadiohogarColabora = false;
    } else {
      this.selectedhogare.splice(this.selectedhogare.indexOf(hogare.value), 1);
      this.igualar5 = this.selectedhogare.toString();
      this.verOtroRadiohogarColabora = true;
    }
  }
  igualarexamplehogarColabora() {
    this.evaluacionSocioeconomica.colaboraHogar = this.igualar5;
  }
  chenckboxChangehabilidadesDestreza(habilidades) {
    if (this.selectedhabilidades.indexOf(habilidades.value) === -1) {
      this.selectedhabilidades.push(habilidades.value);
      this.igualar3 = this.selectedhabilidades.toString();
      this.verOtroRadiohabilidadesDestreza = false;
    } else {
      this.selectedhabilidades.splice(this.selectedhabilidades.indexOf(habilidades.value), 1);
      this.igualar3 = this.selectedhabilidades.toString();
      this.verOtroRadiohabilidadesDestreza = true;
    }
  }
  igualarhabilidadesDestreza() {
    this.evaluacionSocioeconomica.habilDestreza = this.igualar3;
  }
  onSelectionChangenecesitaCertificado(necesitaCertificado) {
    this.evaluacionSocioeconomica.necCertifDiscap = necesitaCertificado.descripcion;
  }
  desseleccionarnecesitaCertificado() {
    this.evaluacionSocioeconomica.necCertifDiscap = null;
    Array.from(document.querySelectorAll('[name=radiogrounecesitaCertificado]')).forEach((x: any) => x.checked = false);
  }
  chenckboxChangecuentaconCertificados(cuentacon) {
    if (this.selectedcuentacon.indexOf(cuentacon.value) === -1) {
      this.selectedcuentacon.push(cuentacon.value);
      this.igualar6 = this.selectedcuentacon.toString();
      this.verOtroRadiocuentaconCertificados = false;
    } else {
      this.selectedcuentacon.splice(this.selectedcuentacon.indexOf(cuentacon.value), 1);
      this.igualar6 = this.selectedcuentacon.toString();
      this.verOtroRadiocuentaconCertificados = true;
    }
  }
  igualarcuentaconCertificado() {
    this.evaluacionSocioeconomica.tipoCertifDisc = this.igualar6;
  }


  chenckboxChangeotrosTipoProblema(otrosTip) {
    if (this.selectedotrosTip.indexOf(otrosTip.value) === -1) {
      this.selectedotrosTip.push(otrosTip.value);
      this.otrosProblemas = this.selectedotrosTip.toString();
    } else {
      this.selectedotrosTip.splice(this.selectedotrosTip.indexOf(otrosTip.value), 1);
      this.otrosProblemas = this.selectedotrosTip.toString();
    }
  }
  igualarexampleModalLabel() {
    this.evaluacionSocioeconomica.otrosProblem = this.otrosProblemas;
  }
  chenckboxChangepcdOrganizacion(pcd) {
    if (this.selectedpcd.indexOf(pcd.value) === -1) {
      this.selectedpcd.push(pcd.value);
      this.igualar7 = this.selectedpcd.toString();
    } else {
      this.selectedpcd.splice(this.selectedpcd.indexOf(pcd.value), 1);
      this.igualar7 = this.selectedpcd.toString();
    }
  }
  igualarpcdOrganizacion() {
    this.evaluacionSocioeconomica.pcd = this.igualar7;
  }
  onSelectionChangematerialortopedico(necesitaCertificado) {
    if (necesitaCertificado.descripcion !== 'Si') {
      this.evaluacionSocioeconomica.poseeMatortopedico = necesitaCertificado.descripcion;
      this.verOtroRadionecesitaCertificado = false;
    } else {
      this.verOtroRadionecesitaCertificado = true;
    }
  }
  desseleccionarmaterialortopedico() {
    this.evaluacionSocioeconomica.poseeMatortopedico = null;
    Array.from(document.querySelectorAll('[name=radiogroumaterialortopedico]')).forEach((x: any) => x.checked = false);
    this.verOtroRadionecesitaCertificado = false;
  }
  onSelectionChangeradiogrouNecesitaortopedico(necesita1) {
    if (necesita1.descripcion !== 'Si') {
      this.evaluacionSocioeconomica.requiereMatortopedico = necesita1.descripcion;
      this.verOtroRadioNecesitaortopedico = false;
    } else {
      this.verOtroRadioNecesitaortopedico = true;
    }
  }
  desseleccionarradiogrouNecesitaortopedico() {
    this.evaluacionSocioeconomica.requiereMatortopedico = null;
    Array.from(document.querySelectorAll('[name=radiogrouNecesitaortopedico]')).forEach((x: any) => x.checked = false);
    this.verOtroRadioNecesitaortopedico = false;
  }
  // ¿Recibió terapia física/ estimulacion temprana antes?
  onSelectionChangeradiogroupestimulacion(necesita2) {
    if (necesita2.descripcion !== 'Si') {
      this.evaluacionSocioeconomica.terapiaAnterior = necesita2.descripcion;
      this.verOtroRadioestimulacion = false;
    } else {
      this.verOtroRadioestimulacion = true;
    }
  }
  desseleccionarradiogroupestimulacion() {
    this.evaluacionSocioeconomica.terapiaAnterior = null;
    Array.from(document.querySelectorAll('[name=radiogroupestimulacion]')).forEach((x: any) => x.checked = false);
    this.verOtroRadioestimulacion = false;
  }

  // ¿Recibe terapia física ahora en otro lugar?
  onSelectionChangeradiogroupterapiaotrolugar(necesita3) {
    if (necesita3.descripcion !== 'Si') {
      this.evaluacionSocioeconomica.terapia_actual = necesita3.descripcion;
      this.verOtroRadioterapiaotrolugar = false;
    } else {
      this.evaluacionSocioeconomica.terapia_actual = necesita3.descripcion;
      this.verOtroRadioterapiaotrolugar = true;
    }
  }
  desseleccionarradiogroupterapiaotrolugar() {
    this.evaluacionSocioeconomica.terapia_actual = null;
    Array.from(document.querySelectorAll('[name=radiogroupterapiaotrolugar]')).forEach((x: any) => x.checked = false);
    this.verOtroRadioterapiaotrolugar = false;
  }
  chenckboxChangeprogramasocial(programa1) {
    if (this.selectedprograma1.indexOf(programa1.value) === -1) {
      this.selectedprograma1.push(programa1.value);
      this.igualar8 = this.selectedprograma1.toString();
      this.verOtroRadioprogramasocial = false;
    } else {
      this.selectedprograma1.splice(this.selectedprograma1.indexOf(programa1.value), 1);
      this.igualar8 = this.selectedprograma1.toString();
      this.verOtroRadioprogramasocial = true;
    }
  }
  igualarprogramasocial() {
    this.evaluacionSocioeconomica.programaInscrito = this.igualar8;
  }
  Crearfamiliar() {
    this.familiar.idexpediente = this.idExpediente;
    this.verCrearFamilia = true;
  }
  setNewparentescoFamiliar(familiar: any): void {
    this.familiar.parentesco = familiar;
  }
  setNewNivelInstruccionFamiliar(nived: any): void {
    this.familiar.gradoinstr = nived;
  }

  imprimifamiliar() {
    this.familiaSrv.getComposisionFamiliar(this.idExpediente).subscribe((data6: any[]) => {
      this.verCrearFamilia = false;
      this.familiares = data6;
    });
  }
  imprimirSituacionEconimica() {
    this.createSituaEconomicServ.getSituaEconomic(this.idExpediente).subscribe(res => {
      if (res[0] !== undefined) {
        this.economia = res[0];
        this.sumaeconomiaingreso();
        this.sumaeconomiaegreso();
      } else {
        this.economia = new SituacionEconomicaModel();
        this.economia.abuelosing = 0;
        this.economia.aguaegre = 0;
        this.economia.alimentacionegr = 0;
        this.economia.educacionegre = 0;
        this.economia.hermanosing = 0;
        this.economia.luzegre = 0;
        this.economia.madreing = 0;
        this.economia.noparientesing = 0;
        this.economia.otrosegre = 0;
        this.economia.padreing = 0;
        this.economia.parientesing = 0;
        this.economia.pasajesegre = 0;
        this.economia.primosing = 0;
        this.economia.telefonoegre = 0;
        this.economia.tiosing = 0;
        this.economia.viviendaegre = 0;
        this.economia.idexpediente = this.idExpediente;
      }
    });
  }
  imprimirReferencias() {
    this.referenciSrv.getSituaReferencia(this.idExpediente).subscribe(res => {
      if (res[0] !== undefined) {
        this.referencia = res[0];
      }
    });
  }


  
 
  eliminarfamilia(id: number) {
    if (confirm('¿Está seguro que desea eliminar el Familiar?')) {
      this.familiaSrv.deleteCompoFamiliar(id);
      swal('Eliminado!', 'El familiar fue elimnado!', 'info');
      this.familiares = this.familiares.filter(u => id !== u.id_compfamiliar);
    }
  }
  cancelar() {
    this.familiar = new CompisicionFamiliarModel;
    this.verCrearFamilia = false;
  }
  saveOrUpdateFamiliar() {
    if (this.isValid) {
      this.familiaSrv.saveOrUpdate(this.familiar).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Familia Guardado!', 'success');
          this.imprimifamiliar();
          this.familiar = new CompisicionFamiliarModel;
        } else {
          this.message = res.message;
          // // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }

  onEditfamilia(fam) {
    this.verCrearFamilia = true;
    this.familiar = fam;

  }
  saveOrUpdateReferencia() {
    this.referencia.idexpediente = this.idExpediente;
    if (this.isValid) {
      this.referenciSrv.saveOrUpdateReferencia(this.referencia).subscribe(res => {
        if (res.responseCode === OK) {
          this.guardado1 = true;
          swal('Bien!', 'Guardado Referencia!', 'success');
          this.imprimirReferencias();
        } else {
          this.message = res.message;
          // // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  saveOrUpdateEvaluacionSocioEconomica(): void {
    this.situaPaciente.idexpediente = this.idExpediente;
    this.situaPaciente.estado = false;
    this.situaPaciente.pacienteaceptado = false;
    this.evaluacionSocioeconomica.idexpediente = this.idExpediente;
    if (this.isValid) {
      this.socioecoSRV.saveOrUpdateEvalSocioeco(this.evaluacionSocioeconomica).subscribe(res => {
        console.log(res);
        if (res.responseCode === 200) {
          swal('Bien!', 'Guardado!', 'success');
        }
        if (res.responseCode === 400) {
          swal('Mal!', 'Verificque campos obligatorios!', 'warning');
        } else {
          this.message = res.message;
          // // // this.isValid = false;
        }
      });
      this.createSituaPacienteServ.saveOrUpdateSituaPaciente(this.situaPaciente).subscribe(res1 => {
        console.log(res1);
        if (res1.responseCode === 200) {

        } else {
          // // // this.isValid = false;
        }
      }
      );
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  UpdateEvaluacionSocioEconomica(): void {
    this.evaluacionSocioeconomica.idexpediente = this.idExpediente;
    console.log(this.evaluacionSocioeconomica);
    if (this.isValid) {
      this.socioecoSRV.saveOrUpdateEvalSocioeco(this.evaluacionSocioeconomica).subscribe(res => {
        console.log(res);        
        if (res.responseCode === 200) {
          swal('Bien!', 'Actualizado!', 'success');
        }
        if (res.responseCode === 400) {
          swal('Mal!', 'Verificque campos obligatorios!', 'warning');
        } else {
          this.message = res.message;
          // // // this.isValid = false;
          // console.log('false');          
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }



  saveOrUpdateEconomia(): void {
    if (this.economia.totalingreso === null || this.economia.totalegre === null) {
      this.economia.totalingreso = this.economia.noparientesing + this.economia.parientesing + this.economia.padreing +
        this.economia.primosing + this.economia.tiosing + this.economia.hermanosing + this.economia.abuelosing + this.economia.madreing;
      this.economia.totalegre = this.economia.otrosegre + this.economia.pasajesegre + this.economia.telefonoegre
        + this.economia.luzegre + this.economia.aguaegre + this.economia.educacionegre + this.economia.viviendaegre +
        this.economia.alimentacionegr;
    }
    if (this.isValid) {
      this.createSituaEconomicServ.saveOrUpdateeconomia(this.economia).subscribe(res => {
        if (res.responseCode === 200) {
          swal('Bien!', 'Guardadi!', 'success');
          this.verCrearVisita = false;
        } else {
          this.message = res.message;
          // // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }

  sumaeconomiaingreso() {
    this.economia.totalingreso = this.economia.noparientesing + this.economia.parientesing + this.economia.padreing +
      this.economia.primosing + this.economia.tiosing + this.economia.hermanosing + this.economia.abuelosing + this.economia.madreing;
  }
  sumaeconomiaegreso() {
    this.economia.totalegre = this.economia.otrosegre + this.economia.pasajesegre + this.economia.telefonoegre
      + this.economia.luzegre + this.economia.aguaegre + this.economia.educacionegre +
      this.economia.viviendaegre + this.economia.alimentacionegr;
  }
}
