/* cSpell:disable */
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Time } from '@angular/common';
import {
  SituaPacientecService,
  UserService
} from '@services/servicio.index';
import {
  PersonalModel,
  ExpedienteModel
} from '@models/model.index';


@Component({
  selector: 'app-lista-aptos',
  templateUrl: './lista-aptos.component.html',
  styles: []
})

export class ListaAptosComponent implements OnInit {
  vermodal: boolean;
  horaterapia: Time;
  horafinalizacion: Time;
  public isValid: boolean = true;
  public message: string = '';
  p = 1;
  nombrepaciente: string;
  colorpersonal: string;
  public term: any;
  public binding: any;
  pacientes: Array<ExpedienteModel>;
  pacient: ExpedienteModel;
  public personales: Array<PersonalModel>;
  verAtender = false;
  verAtendersocial = false;
  constructor(
    private situapacienteSrv: SituaPacientecService,
    private usuarios: UserService,
  ) {

  }
  ngOnInit() {
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));
    if (autorization === 16 || autorization === 12 || autorization === 5 || autorization === 4) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }
    if (autorization === 6 || autorization === 5 || autorization === 4) {
      this.verAtendersocial = true;
    } else {
      this.verAtendersocial = false;
    }

    this.loadPacientesAptos();
    this.vermodal = false;
  }
  seleccionadoPaciente(paciente: ExpedienteModel) {
    this.vermodal = true;
    this.pacient = paciente;
    this.nombrepaciente = this.pacient.nombre;
    // this.cita.idexpediente = this.pacient.id_expediente;
    this.cargarUsuarios();
  }
  private loadPacientesAptos(): void {
    this.situapacienteSrv.getSituaPacientelista().subscribe(res => {
      if (res[0] !== undefined) {
        this.pacientes = res;
      }
    });
  }

  cargarUsuarios() {
    this.usuarios.getPersonalRehabilitacion().subscribe(res => {
      if (res[0] !== undefined) {
        this.personales = res;

      }
    });
  }
  createPDF() {
    var sTable = document.getElementById('idlistaaptos').innerHTML;
    var style = "<style>";
    style = style + "table {width: 100%;font: 11px Calibri;}";
    style = style + "table, th, td {border: solid 1px #DDD; border-collapse: collapse;}";
    style = style + "th {color: blue;}";
    style = style + "td {color: black;}";
    style = style + ".none {display: none;}";
    style = style + "padding: 2px 3px;text-align: center;}";
    style = style + "</style>";
    // CREATE A WINDOW OBJECT.
    var win = window.open('', '', 'height=700,width=700');
    win.document.write('<html><head>');
    win.document.write('<title>LISTA DE APTOS</title>');   // <title> FOR PDF HEADER.
    win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');
    win.document.close(); 	// CLOSE THE CURRENT WINDOW.
    win.print();    // PRINT THE CONTENTS.
  }

  exportTableToExcel(tableID, filename = '') {
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

    // Specify file name
    filename = filename ? filename + '.xls' : 'excel_data.xls';

    // Create download link element
    downloadLink = document.createElement("a");

    document.body.appendChild(downloadLink);

    if (navigator.msSaveOrOpenBlob) {
      var blob = new Blob(['\ufeff', tableHTML], {
        type: dataType
      });
      navigator.msSaveOrOpenBlob(blob, filename);
    } else {
      // Create a link to the file
      downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

      // Setting the file name
      downloadLink.download = filename;

      //triggering the function
      downloadLink.click();
    }
  }


}
