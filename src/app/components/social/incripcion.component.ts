/* cSpell:disable */
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { OK } from '../../models/configuration/httpstatus';
import { Router } from '@angular/router';
import { PreInscripcionModel } from '@models/model.index';
import { PreinscripcionService } from '@services/servicio.index';
declare var swal: any;

@Component({
  selector: 'app-incripcion',
  templateUrl: './incripcion.component.html',
  styles: []
})
export class IncripcionComponent implements OnInit {
  public isValid: boolean = true;
  private message: string = '';
  daraltas: { id: number; tipo: string; }[];
  darbajas: { id: number; tipo: string; }[];
  term: string;
  public preinscripcion: PreInscripcionModel;
  public preinscripciones: Array<PreInscripcionModel>;
  public entry: PreInscripcionModel;
  p = 1;
  selectalta: { id: number; tipo: string; }[];
  activos: { id: number; tipo: string; }[];
  verbada: boolean;
  verAtender = false;
  constructor(
    private router: Router,
    private preinscripcionServ: PreinscripcionService,
  ) {
  }
  ngOnInit() {
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));
    if (autorization === 6 || autorization === 5 || autorization === 4) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }
    this.imprimirPreinscripcion();
    this.verbada = true;
    this.term = '';
    this.darbajas = [
      { id: 1, tipo: 'dar de baja' }];
    this.daraltas = [
      { id: 2, tipo: 'dar de alta' }];

    this.selectalta = [
      { id: 2, tipo: 'Aceptados' },
      { id: 2, tipo: 'Anulados' }
    ];

    this.activos = [
      { id: 2, tipo: 'Activos' },
      { id: 2, tipo: 'Inactivos' }
    ];
  }
  private imprimirPreinscripcion(): void {
    this.preinscripcionServ.getPreinscripcion().subscribe(res => {
      if (res[0] !== undefined) {


        this.preinscripciones = res;
      }
    });
  }
  private imprimirPreinscripcionlosdebaja(): void {
    this.preinscripcionServ.getPreinscripciondebaja().subscribe(res => {
      if (res[0] !== undefined) {
        this.preinscripciones = res;
      }
    });
  }
  public deletePreinscripcion(id_preinscripcion: number): void {
    if (confirm('¿Está seguro que desea eliminar el Preinscripcion?')) {
      this.preinscripcionServ.deletePreinscripcion(id_preinscripcion);
      swal('Eliminado!', 'Inscrpción eliminado!', 'info');
      this.preinscripciones = this.preinscripciones.filter(us => id_preinscripcion !== us.id_preinscripcion);
    }
  }
  public editPreinscripcion(preinscripcion: PreInscripcionModel): void {
    sessionStorage.setItem('preinscripcion', JSON.stringify(preinscripcion));
    this.router.navigate(['wasi/Fichadeinscripcion/CrearFichadeinscripcion']);

  }
  insertarpreinscripcion(preinscripcion) {
    this.preinscripcion = preinscripcion;
  }
  public saveOrUpdate(estado): void {
    this.preinscripcion.estado = estado;
    console.log(this.preinscripcion);
    if (this.isValid) {
      this.preinscripcionServ.saveOrUpdate(this.preinscripcion).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Guardado!', 'success');
          if (this.verbada === true) {
            this.imprimirPreinscripcion();
          }
          if (this.verbada === false) {
            this.imprimirPreinscripcionlosdebaja();
          }

          // this.router.navigate(['wasi/Fichadeinscripcion']);
        } else {
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  SelecActivos(item) {
    if (item === 'Activos') {
      this.imprimirPreinscripcion();
      this.verbada = true;
    }
    if (item === 'Inactivos') {
      this.imprimirPreinscripcionlosdebaja();
      this.verbada = false;
    }
  }

  createPDF() {
    var sTable = document.getElementById('preinscription').innerHTML;
    var style = '<style>';
    style = style + 'table {width: 100%;font: 11px Calibri;}';
    style = style + 'table, th, td {border: solid 1px #DDD; border-collapse: collapse;}';
    style = style + 'th {color: blue;}';
    style = style + 'td {color: black;}';
    style = style + '.none {display: none;}';
    style = style + 'padding: 2px 3px;text-align: center;}';
    style = style + '</style>';
    // CREATE A WINDOW OBJECT.
    var win = window.open('', '', 'height=700,width=700');
    win.document.write('<html><head>');
    win.document.write('<title>FICHA DE PREINSCRIPCION</title>');   // <title> FOR PDF HEADER.
    win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');
    win.document.close(); 	// CLOSE THE CURRENT WINDOW.
    win.print();    // PRINT THE CONTENTS.
  }

  exportTableToExcel(tableID, filename = '') {
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

    // Specify file name
    filename = filename ? filename + '.xls' : 'excel_data.xls';

    // Create download link element
    downloadLink = document.createElement('a');

    document.body.appendChild(downloadLink);

    if (navigator.msSaveOrOpenBlob) {
      var blob = new Blob(['\ufeff', tableHTML], {
        type: dataType
      });
      navigator.msSaveOrOpenBlob(blob, filename);
    } else {
      // Create a link to the file
      downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

      // Setting the file name
      downloadLink.download = filename;

      //triggering the function
      downloadLink.click();
    }
  }
}
