/* cSpell:disable */
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { ExpedienteModel } from '@models/model.index';
import { ExpedienteService } from '@services/servicio.index';

@Component({
  selector: 'app-expediente',
  templateUrl: './expediente-list.component.html',
  styles: []
})
export class ExpedienteComponent implements OnInit {
  public expedientes: Array<ExpedienteModel>;
  public expediente: ExpedienteModel;  
  public isValid: boolean = true;
  private message: string = '';
  p = 1;
  public term: any;
  public binding: any;
  verAtender: boolean;
  constructor(
    private expedienteSocialServ: ExpedienteService,
    private router: Router,   
  ) {    
  }
  ngOnInit(): void {    
    this.loadExpediente();
  }
  private loadExpediente(): void {
    this.expedienteSocialServ.getExpedienteActivos().subscribe(res => {
      if (res[0] !== undefined) {       
        this.expedientes = res.sort((a: any, b: any) => parseFloat(a.codigohistorial) - parseFloat(b.codigohistorial));
      }
    });
  }
  evaluar(expediente) {
    console.log(expediente);
    sessionStorage.setItem('evaluar',JSON.stringify(expediente));   
    this.router.navigate(['/wasi/Evaluacionsocioeconomica']);    
  }  
 
}
