import { OnInit, Component } from '@angular/core';
import { RegisUsuarioService, RolUsuarioService } from '@services/servicio.index';
import { Router, ActivatedRoute } from '@angular/router';
import { RegisUsuarioModel } from '@models/model.index';
declare var swal: any;
@Component({
    selector: 'app-estadistica1',
    templateUrl: './correo.component.html',
    styles: []
})

export class CorreoComponent implements OnInit {
    public isValid: boolean = true;
    private message: string = '';
    usupassword: RegisUsuarioModel;
    roles: any;
    correos: any[];
    verCrearuser: boolean;
    verAtender: boolean;
    constructor(
        private _userpasswordSrv: RegisUsuarioService,
        private _router: Router,
        private rolService: RolUsuarioService,
    ) {
        this.usupassword = new RegisUsuarioModel();
    }

    ngOnInit() {
        const autorization = JSON.parse(localStorage.getItem('idautorizado'));
        if (autorization === 5 || autorization === 4) {
            this.verAtender = true;
        } else {
            this.verAtender = false;
        }
        this.loadRol();
        this.getListaUser();
        this.verCrearuser = false;
    }
    selectRoles(rol) {
        this.usupassword.role = parseInt(rol);
    }
    private loadRol(): void {
        this.rolService.getRol().subscribe(res => {
            if (res[0] !== undefined) {
                this.roles = res;
            }
        });
    }
    getListaUser() {
        this._userpasswordSrv.getListaUser().subscribe(res => {
            if (res[0] !== undefined) {
                this.correos = res;
            }
        });
    }
    editarinfoUser(item) {
        this._router.navigate(['wasi/EditUsuario', item.id]);
    }
    cambiarUser(item) {
        this.verCrearuser = true;
        this.usupassword.id = item.id;
        this.usupassword.username = item.username;
        this.usupassword.email = item.email;
        this.usupassword.name = item.name;
        this.usupassword.password = '';
        this.usupassword.role = item.roles[0].idrol;
    }
    crearUsuario(): void {
        console.log(this.usupassword);

        if (this.usupassword.name !== null && this.usupassword.password !== null) {
            this._userpasswordSrv.saveOrUpdateUsuario(this.usupassword).subscribe(res => {
                if (res.responseCode = 200) {
                    swal('Bien!', 'Guardado!', 'success').then(() => {
                        this.getListaUser();
                        this.verCrearuser = false;
                    });
                } else {
                    this.message = res.message;
                    // this.isValid = false;
                    swal('Mal!', 'Revise las partes obligatorias!', 'warning');
                }
            });
        } else {
            this.message = 'Los campos con * son obligatorios';
            // this.isValid = false;
        }
        sessionStorage.clear();
    }
}
