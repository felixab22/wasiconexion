import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { OK } from '../../models/configuration/httpstatus';
import { UserService, AreaService, RegisUsuarioService } from '@services/servicio.index';
import { PersonalModel, AreaModel } from '@models/model.index';
declare var swal: any;
@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styles: []
})
export class UserFormComponent implements OnInit {
  areaSelect: any;
  areaSelecte: any;
  public user: PersonalModel;
  public event;
  public selectedEntry;
  public entry: AreaModel;
  public areas: Array<AreaModel>;
  public area: AreaModel;
  public isValid: boolean = true;
  public message: string = '';
  validado: boolean;
  estadoPacientes: { id: number; descripcion: string; }[];
  estadoSelecte: any;
  verAtender = false;
  idcorreo: number;
  constructor(
    private createUserService: UserService,
    private router: Router,
    private _route: ActivatedRoute,
    private areaService: AreaService,
    private _regisuser: RegisUsuarioService,

  ) {
    if (sessionStorage.getItem('user')) {
      this.user = JSON.parse(sessionStorage.getItem('user'));
      this.areaSelect = this.user.idarea;
      this.areaSelecte = this.areaSelect.descripcionarea;
      if (this.user.estado === false) {
        this.estadoSelecte = 'Inactivo';
      } else {
        this.estadoSelecte = 'Activo';
      }
    } else {
      this.user = new PersonalModel();
    }
  }
  ngOnInit() {
    if (sessionStorage.getItem('user')) {
      this.validado = false;
    } else {
      this.validado = true;
    }
    this.loadArea();
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));
    this.idcorreo = parseInt(this._route.snapshot.paramMap.get('id'));


    this._regisuser.getPersonaxIduser(this.idcorreo).subscribe((res: any) => {
      if (res[0] === null) {
        this.user = new PersonalModel();
      } else {
        this.user = res;
        this.areaSelecte = res.idarea.descripcionarea;
        if (this.user.estado === false) {
          this.estadoSelecte = 'Inactivo';
        } else {
          this.estadoSelecte = 'Activo';
        }
      }
    });

    if (autorization === 5 || autorization === 4) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }

    this.estadoPacientes = [
      { id: 1, descripcion: 'Activo' },
      { id: 0, descripcion: 'Inactivo' }];
  }
  cancelar() {
    this.user = new PersonalModel();
    this.router.navigate(['wasi/ListarUsuario']);
    sessionStorage.clear();
  }
  onSelectionChangeestadoPacientes(estado: any) {
    this.user.estado = estado.id;
  }
  public saveOrUpdate(): void {
    this.user.user = this.idcorreo;
    if (this.isValid) {
      this.createUserService.saveOrUpdate(this.user).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Guardado!', 'success').then(() => {
            this.router.navigate(['wasi/ListarUsuario']);
          });
        } else {
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }

  private loadArea(): void {
    this.areaService.getArea().subscribe(res => {
      this.areas = res;
    });
  }
  onSelectionChange(entry: AreaModel) {
    this.validado = false;
    this.selectedEntry = entry.id_area;
    this.user.idarea = this.selectedEntry;
  }  
}
