/* cSpell:disable */
import { Component, OnInit } from '@angular/core';
import {
  UserService,
  HorarioService,
  AtentionService,
  InasistenciasService
} from '@services/servicio.index';
import { PersonalModel, AtentionModel, InasistenciasModel } from '@models/model.index';
import { HorarioModel } from '@models/configuration/agenda.model';

declare var swal: any;


@Component({
  selector: 'app-iniciar-agenda',
  templateUrl: './iniciar-agenda.component.html',
  styleUrls: ['./iniciar-agenda.component.scss']
})
export class IniciarAgendaComponent implements OnInit {
  public isValid: boolean = true;
  public message: string = '';
  newhorario: HorarioModel;
  horario: HorarioModel;
  horarios: Array<HorarioModel>;
  public personales: Array<PersonalModel>;
  idPersonal: any;
  verCrear: boolean;
  verTabla: boolean;
  boton: boolean;
  atenciones: { id: number; descripcion: string; }[];
  atention: AtentionModel;
  verbotonatender: boolean;
  inasistenciaMdl: InasistenciasModel;
  veredithora: boolean;
  constructor(
    private _usuariosSrv: UserService,
    public _horarioSrv: HorarioService,
    public _inasistenciaSrv: InasistenciasService,
    public _atentionSrv: AtentionService,

  ) {
    this.horario = new HorarioModel();
    this.newhorario = new HorarioModel();
    this.inasistenciaMdl = new InasistenciasModel();

    this.atenciones = [
      { id: 1, descripcion: 'Llegó' },
      { id: 2, descripcion: 'Llegó tarde' },
      { id: 3, descripcion: 'No llegó pero llamó' },
      { id: 4, descripcion: 'No llegó ni llamó' },
    ];

    if (sessionStorage.getItem('atention')) {
      this.atention = JSON.parse(sessionStorage.getItem('atention'));
    } else {
      this.atention = new AtentionModel();
    }
  }
  ngOnInit() {
    this.verbotonatender = true;
    this.boton = false;
    this.verCrear = false;
    this.verTabla = false;
    this.veredithora = false;
    this.cargarUsuarios();
  }
  cargarUsuarios() {
    this._usuariosSrv.getPersonalRehabilitacion().subscribe(res => {
      if (res[0] !== undefined) {
        this.personales = res;
      }
    });
  }
  capturarDatos(persona, dia) {
    const fecha = new Date().toLocaleString();
    this.atention.fecha = String(fecha);
    this.atention.descripcion = null;
    this.atention.tipoasistencia = null;
    this.verbotonatender = true;
    Array.from(document.querySelectorAll('[name=radiogruatencion]')).forEach((x: any) => x.checked = false);
    this.atention.paciente = persona;
    this.inasistenciaMdl.nombrepaciente = persona;
    this.inasistenciaMdl.finasistencia = String(fecha);
    this.atention.dia = dia;

  }

  public saveOrUpdateAtender(): void {
    console.log(this.atention);
    if (this.isValid) {
      this._atentionSrv.saveOrUpdateAtention(this.atention).subscribe(res => {
        if (res.responseCode === 200) {
          swal('Bien!', 'El paciente fue atendido!', 'success');
        } else {
          this.message = res.message;
          // this.isValid = false;
        }
      });
      if (this.atention.tipoasistencia === 4) {
        this._inasistenciaSrv.saveOrUpdateInasistencia(this.inasistenciaMdl).subscribe(res => {
          if (res) {
          } else {
            this.message = res.message;
            // this.isValid = false;
          }
        });
      }
    } else {
      this.message = 'Los campos con * son obligatorios';
    }


    sessionStorage.clear();
  }

  cancelar() {
    this.atention = new AtentionModel();
    Array.from(document.querySelectorAll('[name=radiogruatencion]')).forEach((x: any) => x.checked = false);
    this.verbotonatender = true;
  }
  onSelectionChangeabasteatenciones(atencion) {
    this.atention.descripcion = atencion.descripcion;
    this.atention.tipoasistencia = atencion.id;
    this.verbotonatender = false;
  }
  setPersonal(personal: any) {
    localStorage.setItem('horario', personal);
    this.idPersonal = personal;
    this.atention.personal = parseInt(personal);
    this.inasistenciaMdl.personal = parseInt(personal);    
    this._horarioSrv.getHorarioxidpersonal(personal).subscribe((res: Array<HorarioModel>) => {
      if (res.length === 0) {
        this.horario = new HorarioModel();
        this.verCrear = true;
        this.verTabla = false;
        this.veredithora = false;
      } else {
        this.horarios = res.sort((a: any, b: any) => parseFloat(a.orden) - parseFloat(b.orden));
        this.verCrear = false;
        this.verTabla = true;
        this.veredithora = true;
      }
    });
  }
  mostrar() {
    if (this.boton === false) {
      this.boton = true;
      return;
    } else {
      this.boton = false;
      return;
    }
  }
  editar(hora: any) {
    console.log(hora);
    this.newhorario = hora;
    this.newhorario.idpersonal = parseInt(localStorage.getItem('horario'));
  }
  GenerarCalendario() {
    let i = 7;
    do {
      this.horario.orden = i;
      const j = String(i) + ':00 a ' + String(i + 1) + ':00';
      this.horario.hora = j;
      this.horario.lunes = 'Libre';
      this.horario.martes = 'Libre';
      this.horario.miercoles = 'Libre';
      this.horario.jueves = 'Libre';
      this.horario.viernes = 'Libre';
      this.horario.idpersonal = parseInt(this.idPersonal);
      if (this.isValid) {
        this._horarioSrv.saveOrUpdateHorario(this.horario).subscribe(res => {
          if (res.responseCode === 200) {
            console.log('creado: ' + j);
          } else {
            this.message = res.message;
            // this.isValid = false;
          }
        });
      } else {
        this.message = 'Los campos con * son obligatorios';
      }
      i++;
    } while (i <= 19);
    this.setPersonal(this.horario.idpersonal);
  }

  public saveOrUpdateHorario(): void {
    if (this.isValid) {
      this._horarioSrv.saveOrUpdateHorario(this.newhorario).subscribe(res => {
        if (res.responseCode === 200) {
          swal('Bien!', 'Horario modificado!', 'success');
          console.log('actualizado');
          this.boton = false;
        } else {
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
  }
  eliminarhorario() {
    console.log(this.idPersonal);
    swal({
      title: 'Esta seguro?',
      text: 'Usted esta por eliminar el horario!',
      icon: 'warning',
      buttons: true,
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          this._horarioSrv.deleteAgendaxPersonal(this.idPersonal);
          swal('Horario eliminado', {
            icon: 'success',
          });
        } else {
          swal('Sin acciones!');
        }
      });
  }
}
