/* cSpell:disable */
import { Component, OnInit, } from '@angular/core';
import { InasistenciaService } from '@services/servicio.index';
import { AtentionModel } from '@models/model.index';
declare var swal: any;

@Component({
  selector: 'app-horarioreah',
  templateUrl: './horarioreah.component.html',
  styleUrls: ['./horarioreah.component.scss']
})
export class HorarioreahComponent implements OnInit {
  listados: Array<AtentionModel>;
  listadosgenerales: Array<any>;
  id: number;
  public term: any;
  constructor(
    public _inasistenciasSrv: InasistenciaService,
  ) {  }
  ngOnInit() {
    this.imprimirInasistencias();
    this._inasistenciasSrv.getListaInasistenciasDetalle().subscribe(res => {
      this.listadosgenerales = res;
    });
  }

  imprimirInasistencias() {
    this._inasistenciasSrv.getListaInasistencias().subscribe((res: Array<any>) => {
      this.listados = res.sort((a: any, b: any) => parseInt(b.idnotificacion) - parseInt(a.idnotificacion));
      console.log(this.listados);
      
    });
  }

  devuelto(persona: any) {
    const id = parseInt(persona.idnotificacion);
    console.log(id);
    swal({
      title: 'Eliminar?',
      text: 'Eliminar el record de inasistencias!',
      icon: 'warning',
      buttons: true,
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          this._inasistenciasSrv.deleteNotifications(id);
          swal('Inasistencia Eliminada!', {
            icon: 'success',
          });
          this.imprimirInasistencias();
        } else {
          swal('Cancelado!');
        }
      });
  }

}
