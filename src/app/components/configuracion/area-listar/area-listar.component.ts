/* cSpell:disable */
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { OK } from '../../../models/configuration/httpstatus';
import { AreaService } from '@services/servicio.index';
import { AreaModel } from '@models/model.index';
declare var swal: any;
@Component({
  selector: 'app-area-listar',
  templateUrl: './area-listar.component.html',
  styleUrls: ['./area-listar.component.scss']
})

export class AreaListarComponent implements OnInit {
  verBtnactualizar: boolean;
  verBtnguardar: boolean;
  verCrearArea: boolean;
  public areas: Array<AreaModel>;
  public area: AreaModel;
  public isValid: boolean = true;
  private message = '';
  verAtender = false;
  constructor(
    private areaService: AreaService,
    private router: Router,
  ) {
    if (sessionStorage.getItem('area')) {
      this.area = JSON.parse(sessionStorage.getItem('area'));
    } else {
      this.area = new AreaModel();
    }
  }

  ngOnInit() {
  const autorization = JSON.parse(localStorage.getItem('idautorizado'));
    if (autorization === 5 || autorization === 4) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }
    this.loadArea();
    this.verCrearArea = false;
    this.verBtnguardar = true;
    this.verBtnactualizar = false;
  }
  private loadArea(): void {
    this.areaService.getArea().subscribe(res => {
      if (res[0] !== undefined) {
        this.areas = res;
      }
    });
  }
  public editArea(area: AreaModel): void {
    sessionStorage.setItem('area', JSON.stringify(area));
    this.router.navigate(['/Areascrear']);
  }

  onEditArea(area) {
    this.verCrearArea = true;
    this.area = area;
    this.verBtnguardar = false;
    this.verBtnactualizar = true;
  }
  public deleteArea(id_area: number): void {
    if (confirm('¿Está seguro que desea eliminar el Area?')) {
      this.areaService.deleteArea(id_area);
      swal('Eliminado!', 'Area Eliminado!');
      this.areas = this.areas.filter(u => id_area !== u.id_area);
    }
  }
  CrearArea() {
    this.verCrearArea = true;
    this.verBtnguardar = true;
    this.verBtnactualizar = false;
  }

  public saveOrUpdateArea(): void {
    if (this.isValid) {
      this.areaService.saveOrUpdate(this.area).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Guardado!', 'success').then(() => {
            this.loadArea();
            this.verCrearArea = false;
            this.area = new AreaModel();
          });

        } else {
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
}
