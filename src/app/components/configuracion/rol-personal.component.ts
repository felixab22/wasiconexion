/* cSpell:disable */
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RolUsuarioService } from '@services/servicio.index';
import { RolModel } from '@models/model.index';

@Component({
  selector: 'app-rol-personal',
  templateUrl: './rol-personal.component.html',
  styles: []
})
export class RolPersonalComponent implements OnInit {
  verBtnactualiar: boolean;
  verBtnguarda: boolean;
  verCrearRol: boolean;
  public roles: Array<RolModel>;
  public rol: RolModel;
  public isValid: boolean = true; 
  private message: string = '';
  constructor(
    private rolService: RolUsuarioService,
    private router: Router,    
  ) {    
    if (sessionStorage.getItem('rol')) {
      this.rol = JSON.parse(sessionStorage.getItem('rol'));
    } else {
      this.rol = new RolModel();
    }
  }

  ngOnInit() {
    this.loadRol();   
  }
 
  private loadRol(): void {
    this.rolService.getRol().subscribe(res => {
      if (res[0] !== undefined) {
        this.roles = res;
      }
    });
  }  
}
