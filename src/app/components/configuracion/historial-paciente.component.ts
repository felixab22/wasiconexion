import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ExpedienteService } from '@services/servicio.index';
import { ExpedienteModel } from '@models/model.index';
declare var swal: any;
@Component({
  selector: 'app-historial-paciente',
  templateUrl: './historial-paciente.component.html',
  styles: []
})
export class HistorialPacienteComponent implements OnInit {
  estadoexpedientegeneral: string;
  estadopersonal: boolean;
  public expedientes: Array<ExpedienteModel>;
  public expediente: ExpedienteModel;
  public isValid: boolean = true;
  private message: string = '';
  p = 1;
  public term: any;
  public binding: any;
  verAtender = false;
  constructor(
    private router: Router,
    private expedienteSocialServ: ExpedienteService,
  ) {

  }
  ngOnInit(): void {
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));
    if (autorization === 6 || autorization === 5 || autorization === 4) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }
    this.loadExpediente();
  }
  private loadExpediente(): void {
    this.expedienteSocialServ.getExpediente().subscribe(res => {
      if (res[0] !== undefined) {
        //ordenar
        this.expedientes = res.sort((a: any, b: any) => parseFloat(a.codigohistorial) - parseFloat(b.codigohistorial));
      } else {
        swal("Mal!", "No existen Expediente!", "warning");
      }
    });
  }
  public editExpediente(expediente: ExpedienteModel): void {
    sessionStorage.setItem('expediente', JSON.stringify(expediente));
    this.router.navigate(['wasi/historialgeneral/CrearHistoriaSocial']);
  }
  public deleteExpediente(id: number): void {
    this.expedienteSocialServ.deleteExpediente(id);
  }
  crearExpediente() {
    this.router.navigate(['wasi/historialgeneral/CrearHistoriaSocial']);
  }

  createPDF() {
    var sTable = document.getElementById('histogeneral').innerHTML;
    var style = "<style>";
    style = style + "table {width: 100%;font: 11px Calibri;}";
    style = style + "table, th, td {border: solid 1px #DDD; border-collapse: collapse;}";
    style = style + "th {color: blue;}";
    style = style + "td {color: black;}";
    style = style + ".none {display: none;}";
    style = style + "padding: 2px 3px;text-align: center;}";
    style = style + "</style>";
    // CREATE A WINDOW OBJECT.
    var win = window.open('', '', 'height=700,width=700');
    win.document.write('<html><head>');
    win.document.write('<title>HISTORIA SOCIAL GENERAL</title>');   // <title> FOR PDF HEADER.
    win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');

    win.document.close(); 	// CLOSE THE CURRENT WINDOW.

    win.print();    // PRINT THE CONTENTS.
  }
}
