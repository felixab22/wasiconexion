import { Component, OnInit, Input, Output, EventEmitter, Inject } from '@angular/core';
import { GraficasService } from '@services/servicio.index';
import { DOCUMENT } from '@angular/platform-browser';
@Component({
  selector: 'app-graficas',
  templateUrl: './graficas.component.html',
  styleUrls: ['./graficas.component.scss']
})
export class GraficasComponent {

  @Output() cambioValor: EventEmitter<any> = new EventEmitter();
  menus: any[];

  constructor(
    @Inject(DOCUMENT) private _document,
    private _graficasSrv: GraficasService
  ) {
    this.menus = this._graficasSrv.menus;
  }
  emitValor(item: any, link: any) {  
    const selectores:any = this._document.getElementsByClassName('selector');
    for(const ref of selectores) {
      ref.classList.remove('colorAzul'); 
     }
    link.classList.add('colorAzul');    

    if (item.id === 1) {
      this._graficasSrv.getgraficoTipoFamilia().subscribe(res => {        
        this.cambioValor.emit(res);
      });
    }
    if (item.id === 2) {
      this._graficasSrv.getgraficoUbicVivienda().subscribe(res => {        
        this.cambioValor.emit(res);
      });
    }
    if (item.id === 3) {
      this._graficasSrv.getgraficoTenenciaVivienda().subscribe(res => {        
        this.cambioValor.emit(res);
      });
    }
    if (item.id === 4) {
      this._graficasSrv.getgraficoTipoSeguro().subscribe(res => {        
        this.cambioValor.emit(res);
      });
    }
    if (item.id === 5) {
      this._graficasSrv.getgraficoNivelEducatito().subscribe(res => {        
        this.cambioValor.emit(res);
      });
    }
    if (item.id === 6) {
      this._graficasSrv.getgraficoInsAsiste().subscribe(res => {        
        this.cambioValor.emit(res);
      });
    }
    if (item.id === 7) {
      this._graficasSrv.getgraficoNecesiCertificado().subscribe(res => {        
        this.cambioValor.emit(res);
      });
    }
    if (item.id === 8) {
      this._graficasSrv.getgraficxDiagnostico().subscribe(res => {        
        this.cambioValor.emit(res);
      });
    }
    if (item.id === 9) {
      this._graficasSrv.getgraficxEdad().subscribe(res => {        
        this.cambioValor.emit(res);
      });
    }
    if (item.id === 10) {
      this._graficasSrv.getgraficaxSexo().subscribe(res => {        
        this.cambioValor.emit(res);
      });
    }
    
  }

}
