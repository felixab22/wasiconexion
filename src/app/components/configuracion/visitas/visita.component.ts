import { Component, OnInit, Input } from '@angular/core';
import { VisitaDomiciliarioModel } from '@models/model.index';
import { VisitaService, ExpedienteService } from '@services/servicio.index';

declare var swal: any;
@Component({
    selector: 'app-visita',
    templateUrl: './visita.component.html',
    styles: []
})
export class VisitaComponent implements OnInit {
    @Input() idExpediente = 0;
    visitasDom: VisitaDomiciliarioModel;
    public visitasDomicilliarias: Array<VisitaDomiciliarioModel>;
    public isValid: boolean = true;
    private message: string = '';
    verCrearVisita: boolean;
    verxDNI: boolean;
    verxHistorial: boolean;
    expedientes: any;
    // idExpediente: any;
    verpaciente: boolean;
    botonDNI: boolean;
    botonHistorial: boolean;
    constructor(
        private visitasService: VisitaService,
        private expedienteSrv: ExpedienteService,
    ) {
        if (sessionStorage.getItem('visitasDom')) {
            this.visitasDom = JSON.parse(sessionStorage.getItem('visitasDom'));
        } else {
            this.visitasDom = new VisitaDomiciliarioModel();
        }
    }
    ngOnInit(): void {
        this.imprimiVisitas();
    }
    // buscarxDNI() {
    //     this.verxDNI = true;
    //     this.verxHistorial = false;
    // }
    // Buscarxhistorial() {
    //     this.verxHistorial = true;
    //     this.verxDNI = false;
    // }
    // imprimiExpedientexHistorial(historial) {
    //     this.expedienteSrv.getBusquedaxHistorial(historial).subscribe(
    //         (data: any[]) => {
    //             if (data[0] === undefined) {
    //                 swal('Error!', 'No existe el Código!', 'success');
    //             } else {
    //                 this.verpaciente = true;
    //                 this.expedientes = data[0];
    //                 this.idExpediente = data[0].id_expediente;
    //                 //   this.verxHistorial = false;
    //                 //   this.verxDNI = false;
    //                 this.imprimiVisitas();

    //             }
    //         });
    // }
    // imprimiExpedientexDNI(dni) {
    //     this.expedienteSrv.getBusquedaDNI(dni).subscribe(
    //         (data: any[]) => {
    //             if (data[0] === undefined) {
    //                 swal('Error!', 'No existe el Código!', 'success');
    //             } else {
    //                 this.verpaciente = true;
    //                 this.expedientes = data[0];
    //                 this.idExpediente = data[0].id_expediente;
    //                 this.imprimiVisitas();
    //             }
    //         });
    // }
    imprimiVisitas() {
        this.visitasService.getVisita(this.idExpediente).subscribe(res => {
            if (res[0] !== undefined) {
                console.log(res);
                this.visitasDomicilliarias = res;
                this.verCrearVisita = false;
            } else {
                this.visitasDom = new VisitaDomiciliarioModel();
            }
        });
    }
    onEditarVisica(visit) {
        this.verCrearVisita = true;
        this.visitasDom = visit;
    }
    CrearVisitas() {
        const persona = JSON.parse(localStorage.getItem('personal'));
        this.verCrearVisita = true;
        this.visitasDom.idexpediente = this.idExpediente;
        this.visitasDom.personalvisitador = persona.nombrepersonal + ' ' + persona.apppaterno;
        this.visitasDom.areavisitador = persona.idarea.descripcionarea;

    }
    saveOrUpdateVisita(): void {
        if (this.isValid) {
            this.visitasService.saveOrUpdateVisita(this.visitasDom).subscribe(res => {
                if (res.responseCode === 200) {

                    swal('Bien!', 'Guardado visita!', 'success').then(() => {
                        this.verCrearVisita = false;
                        this.visitasDom = new VisitaDomiciliarioModel;
                        this.imprimiVisitas();
                    });
                } else {
                    this.message = res.message;
                    // this.isValid = false;
                }
            });
        } else {
            this.message = 'Los campos con * son obligatorios';
        }
        sessionStorage.clear();
    }

}
