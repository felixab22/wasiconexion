/* cSpell:disable */
import { Component, OnInit } from '@angular/core';
import { RolUsuarioService } from '@services/servicio.index';

@Component({
  selector: 'app-bienvenido',
  templateUrl: './bienvenido.component.html',
  styleUrls: ['./bienvenido.component.scss']
})
export class BienvenidoComponent implements OnInit {


  constructor(
    public _roleSrv: RolUsuarioService
  ) { }

  ngOnInit() {
    const autorization = localStorage.getItem('autorization');
    const rolbuscado = this._roleSrv.buscarroles(autorization);
    console.log(rolbuscado[0].idrol);
    localStorage.setItem('idautorizado', JSON.stringify(rolbuscado[0].idrol));

  }

}
