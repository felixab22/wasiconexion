import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService, FilesService } from '@services/servicio.index';
import { PersonalModel } from '@models/model.index';
import { FileUploader } from 'ng2-file-upload';
import { URL_SERVICIOS } from 'app/config/config';
declare var swal: any;
const URL = URL_SERVICIOS + '/upload';
@Component({
    selector: 'app-user-form',
    templateUrl: './perfil.component.html',
    styles: []
})
export class PerfilComponent implements OnInit {

    public uploader: FileUploader = new FileUploader({ url: URL });
    public hasBaseDropZoneOver: boolean = false;
    public hasAnotherDropZoneOver: boolean = false;
    imagentemp: string;
    public user: any;
    public newuser: PersonalModel;
    public isValid: boolean = true;
    public message: string = '';
    validado: boolean;
    estadoPacientes: { id: number; descripcion: string; }[];
    estadoSelecte: any;
    verAtender = false;
    idcorreo: number;
    ImagenSubir: File;
    imagenperfil: any;
    urlimg: any;
    constructor(
        private createUserService: UserService,
        private router: Router,
        private _uploadFilesSrv: FilesService
    ) {
        this.ImagenSubir = null;       
        this.newuser = new PersonalModel();        
    }
    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('personal'));
        
        this.imprimirFiles();
        
    }
    cancelar() {
        this.user = new PersonalModel();
        this.router.navigate(['/wasi/bienvenido']);
    }    
    
    public saveOrUpdate(): void {
        this.newuser.id_personal = this.user.id_personal;
        this.newuser.nombrepersonal = this.user.nombrepersonal;
        this.newuser.appmaterno = this.user.appmaterno;
        this.newuser.apppaterno = this.user.apppaterno;
        this.newuser.idarea = this.user.idarea.id_area;
        this.newuser.user = this.user.user.id;        
        this.newuser.fcumple = this.user.fcumple;
        this.newuser.hobby = this.user.hobby;
        this.newuser.profesion = this.user.profesion;
        this.newuser.telefono = this.user.telefono;
        this.newuser.correo = this.user.user.email;        
        this.newuser.cargo = this.user.user.roles[0].descripcionrol;        
        this.newuser.estado = this.user.estado;        
        console.log(this.newuser);    

        if (this.isValid) {
            this.createUserService.saveOrUpdate(this.newuser).subscribe(res => {
                if (res.responseCode === 200) {
                    swal('Bien!', 'Guardado!', 'success');
                } else {
                    this.message = res.message;
                    // this.isValid = false;
                }
            });
        } else {
            this.message = 'Los campos con * son obligatorios';
        }
        sessionStorage.clear();
    }
    public fileOverBase(e: any): void {
        // this.eliminiar();
        this.hasBaseDropZoneOver = e;
        for (let i = 0; i < this.uploader.queue.length; i++) {
            const nombre = this.uploader.queue[i].file.name.split('.');
            const extencion = nombre[nombre.length - 1];

            if(extencion === 'png' || extencion === 'jpg' || extencion === 'PNG' || extencion === 'JPG' ) {
                this.uploader.queue[i].file.name = 'perfil-' + this.user.id_personal + '-wasi.' + extencion;
            } else {
                swal('error!', 'Extencion de archivo no valido', 'warning');
            }
        }
    }
    imprimirFiles() {
        this._uploadFilesSrv.getListaFilesxAreayPaciente('perfil', this.user.id_personal).subscribe(res => {
            if (res.length === 0) {
                swal('error!', 'No existen archivos!', 'warning');
            } else {
                // console.log(res.length);
                this.imagenperfil = res[res.length-1];
                this.urlimg = `${URL_SERVICIOS}/file/${this.imagenperfil}?nombrearea=perfil&codigo=${this.user.id_personal}`;
            }
        });
    }
    eliminiar() {
        this._uploadFilesSrv.deleteFilesxcodigo('wasi', 'perfil', this.user.id_personal).subscribe(res => {
            if (res) {
                console.log('paso eliminar');
            }
        });
    } 

}
