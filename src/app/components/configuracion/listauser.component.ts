/* cSpell:disable */
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
declare var swal: any;

import {
  UserService,
  RolUsuarioService,
  RegisUsuarioService,
  AreaService,
  FilesService
} from '@services/servicio.index';
import {
  RegisUsuarioModel,
  PersonalModel,
  RolModel,
  AreaModel
} from '@models/model.index';
import { URL_SERVICIOS } from 'app/config/config';

@Component({
  selector: 'app-listauser',
  templateUrl: './listauser.component.html',
  styles: []
})

export class ListauserComponent implements OnInit {
  usupassword: RegisUsuarioModel;
  public users: Array<PersonalModel>;
  public areas: Array<AreaModel>;
  public roles: Array<RolModel>;
  public isValid: boolean = true;
  private message: string = '';
  areaSelect: any;
  areaSelecte: any;
  public user: PersonalModel;
  public selectedEntry;
  validado: boolean;
  display = ' none ';
  activos: { id: number; tipo: string; }[];
  verAtender = false;
  urlimg: string;
  info: PersonalModel;
  constructor(
    private userService: UserService,
    private areaService: AreaService,
    private _uploadFilesSrv: FilesService,
    private router: Router,

  ) {
    if (sessionStorage.getItem('usupassword')) {
      this.usupassword = JSON.parse(sessionStorage.getItem('usupassword'));
    } else {
      this.usupassword = new RegisUsuarioModel();
      this.info = new PersonalModel();
    }
  }
  ngOnInit() {
    this.urlimg = '../../../assets/images/1.jpg';
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));
    if (autorization === 5 || autorization === 4) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }
    this.loadUser();
    this.loadArea();
    this.validado = true;
    this.activos = [
      { id: 2, tipo: 'Activos' },
      { id: 2, tipo: 'Inactivos' }
    ];
  }
  SelecActivos(item) {
    if (item === 'Activos') {
      this.userService.getPersonalActivo().subscribe(res => {
        this.users = res;
      });

    }
    if (item === 'Inactivos') {
      this.userService.getPersonalInactivos().subscribe(res => {
        this.users = res;
      });
    }
  }
  private loadUser(): void {
    this.userService.getUser().subscribe(res => {
      if (res[0] === undefined) {
        swal('Error!', 'No existe Usuario!', 'warning');
      } else {
        this.users = res;
      }
    });
  }
  public editUser(user: any): void {
    sessionStorage.setItem('user', JSON.stringify(user));
    this.router.navigate(['wasi/EditUsuario', user.user.id]);
  }
  public deleteUser(user: PersonalModel): void {
    if (confirm('¿Está seguro que desea eliminar?')) {
      this.userService.deleteUser(user);
      this.users = this.users.filter(u => user.id_personal !== u.id_personal);
      swal('Alerta!!', '...Usuario Eliminado !');
    }
  }
  CreandoUsuario() {
    this.router.navigate(['wasi/CrearUsuario']);

  }
  loadArea(): void {
    this.areaService.getArea().subscribe(res => {
      this.areas = res;
    });
  }
  cancelar() {
    this.display = 'none';
  }
  information(persona: any) {
    this.info = persona;
    console.log(this.info);
    this.imprimirFiles(persona.id_personal);

  }
  imprimirFiles(idpersona: number) {
    this._uploadFilesSrv.getListaFilesxAreayPaciente('perfil', idpersona).subscribe((res: any) => {
      if (res.status === 500) {
        console.log('error');

        this.urlimg = '../../../assets/images/1.jpg';
      } else {
        // console.log(res.length);
        const imagenperfil = res[res.length - 1];
        this.urlimg = `${URL_SERVICIOS}/file/${imagenperfil}?nombrearea=perfil&codigo=${idpersona}`;
      }
    });
  }
  okinformation() {
    this.urlimg = '../../../assets/images/1.jpg';
  }



}
