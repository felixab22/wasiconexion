/* cSpell:disable */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OK } from '../../models/configuration/httpstatus';
declare var swal: any;
import { 
  EducacionModel, 
  DesalenguajeModel, 
  DesarrolloSocialModel, 
  EscolaridadModel, 
  SaludActualEduModel, 
  ExpedienteModel,
  CompisicionFamiliarModel } from '@models/model.index';
import { 
  ExpedienteService, 
  RefInternaService, 
  EducacionService, 
  CompoFamiliarService, 
  DesalenguajeService,
   DesarrolloSocialService, 
   EscolaridadService, 
   SaludActualEduService 
  } from '@services/servicio.index';

@Component({
  selector: 'app-educacion',
  templateUrl: './educacion.component.html',
  styles: [],
})
export class EducacionComponent implements OnInit {
  gradodomiio2: string;
  verotrosEscolaridad: boolean;
  luces: { id: number; tipo: string; }[];
  sonidos: { id: number; tipo: string; }[];
  extranios: { id: number; tipo: string; }[];
  idEducacion: number;
  public desalenguaje: DesalenguajeModel;
  public desarrsocial: DesarrolloSocialModel;
  public escolaridad: EscolaridadModel;
  public saluteducacion: SaludActualEduModel;
  otrolenguaje: boolean;
  secomunican: { id: number; tipo: string; }[];
  verDiagnostico: boolean;
  dominios2: { id: number; tipo: string; }[];
  gradodomiio1: string;
  selectedprograma1: string[] = [];
  dominios: { id: number; tipo: string; }[];
  opciones42: { id: number; tipo: string; }[];
  alimentos: { id: number; tipo: string; }[];
  opciones13: { id: number; tipo: string; }[];
  opciones14: { id: number; tipo: string; }[];
  opciones15: { id: number; tipo: string; }[];
  opciones16: { id: number; tipo: string; }[];
  opciones17: { id: number; tipo: string; }[];
  opciones18: { id: number; tipo: string; }[];
  opciones19: { id: number; tipo: string; }[];
  opciones20: { id: number; tipo: string; }[];
  opciones21: { id: number; tipo: string; }[];
  opciones22: { id: number; tipo: string; }[];
  opciones23: { id: number; tipo: string; }[];
  opciones24: { id: number; tipo: string; }[];
  opciones25: { id: number; tipo: string; }[];
  opciones26: { id: number; tipo: string; }[];
  opciones27: { id: number; tipo: string; }[];
  opciones28: { id: number; tipo: string; }[];
  opciones29: { id: number; tipo: string; }[];
  opciones30: { id: number; tipo: string; }[];
  opciones31: { id: number; tipo: string; }[];
  opciones32: { id: number; tipo: string; }[];
  opciones34: { id: number; tipo: string; }[];
  opciones33: { id: number; tipo: string; }[];
  opciones35: { id: number; tipo: string; }[];
  opciones36: { id: number; tipo: string; }[];
  opciones37: { id: number; tipo: string; }[];
  opciones38: { id: number; tipo: string; }[];
  opciones39: { id: number; tipo: string; }[];
  opciones40: { id: number; tipo: string; }[];
  opciones41: { id: number; tipo: string; }[];
  opciones12: { id: number; tipo: string; }[];
  opciones11: { id: number; tipo: string; }[];
  opciones10: { id: number; tipo: string; }[];
  opciones9: { id: number; tipo: string; }[];
  opciones8: { id: number; tipo: string; }[];
  opciones7: { id: number; tipo: string; }[];
  opciones6: { id: number; tipo: string; }[];
  opciones5: { id: number; tipo: string; }[];
  opciones4: { id: number; tipo: string; }[];
  opciones3: { id: number; tipo: string; }[];
  opciones2: { id: number; tipo: string; }[];
  opciones1: { id: number; tipo: string; }[];
  futuroescolar: { id: number; tipo: string; }[];
  respuestaexito: { id: number; tipo: string; }[];
  apoyoaprendisaje: { id: number; tipo: string; }[];
  respuestadificultades: { id: number; tipo: string; }[];
  desempenioescolar: { id: number; tipo: string; }[];
  vestidos: { id: number; tipo: string; }[];
  banios: { id: number; tipo: string; }[];
  modalidades: { id: number; tipo: string; }[];
  duermes: { id: number; tipo: string; }[];
  insomios: { id: number; tipo: string; }[];
  suenios: { id: number; tipo: string; }[];
  pesos: { id: number; tipo: string; }[];
  alimentaciones: { id: number; tipo: string; }[];
  opciones: { id: number; tipo: string; }[];
  idExpediente: any;
  otrodesalenguaje: string;
  verpaciente: boolean;
  evalueducacion: EducacionModel;
  public familiares: Array<CompisicionFamiliarModel>;
  public expedientes: Array<ExpedienteModel>;  
  public isValid: boolean = true;
  private message: string = '';
  verAtender = false;
  constructor(
    private expedienteSrv: ExpedienteService,
    private referenciaSrv: RefInternaService,
    private EducacionSrv: EducacionService,
    private familiaSrv: CompoFamiliarService,
    private desalenguajeSrv: DesalenguajeService,
    private desasocialSrv: DesarrolloSocialService,
    private escolaridadSrv: EscolaridadService,
    private saludeducacionSrv: SaludActualEduService,
    private route: ActivatedRoute,
      ) {    
    if (sessionStorage.getItem('evalpsicologico')) {
      this.evalueducacion = JSON.parse(sessionStorage.getItem('evalpsicologico'));
    } else {
      this.evalueducacion = new EducacionModel();
    }
    if (sessionStorage.getItem('desalenguaje')) {
      this.desalenguaje = JSON.parse(sessionStorage.getItem('desalenguaje'));
    } else {
      this.desalenguaje = new DesalenguajeModel();
    }
    if (sessionStorage.getItem('desarrsocial')) {
      this.desarrsocial = JSON.parse(sessionStorage.getItem('desarrsocial'));
    } else {
      this.desarrsocial = new DesarrolloSocialModel();
    }
    if (sessionStorage.getItem('escolaridad')) {
      this.escolaridad = JSON.parse(sessionStorage.getItem('escolaridad'));
    } else {
      this.escolaridad = new EscolaridadModel();
    }
    if (sessionStorage.getItem('saluteducacion')) {
      this.saluteducacion = JSON.parse(sessionStorage.getItem('saluteducacion'));
    } else {
      this.saluteducacion = new SaludActualEduModel();
    }
  }
  ngOnInit() {
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));       
    if(autorization === 8  || autorization === 5  || autorization === 4 ) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }
    this.verDiagnostico = false;
    this.otrolenguaje = false;
    this.verotrosEscolaridad = false;
    this.opciones1 = [
      { id: 1, tipo: 'Si' },
      { id: 2, tipo: 'No' }
    ];
    this.opciones2 = [
      { id: 3, tipo: 'Si' },
      { id: 4, tipo: 'No' }
    ];
    this.opciones3 = [
      { id: 5, tipo: 'Si' },
      { id: 6, tipo: 'No' }
    ];
    this.opciones4 = [
      { id: 7, tipo: 'Si' },
      { id: 8, tipo: 'No' }
    ];
    this.opciones5 = [
      { id: 9, tipo: 'Si' },
      { id: 10, tipo: 'No' }
    ];
    this.opciones6 = [
      { id: 11, tipo: 'Si' },
      { id: 12, tipo: 'No' }
    ];
    this.opciones7 = [
      { id: 15, tipo: 'Si' },
      { id: 16, tipo: 'No' }
    ];
    this.opciones8 = [
      { id: 13, tipo: 'Si' },
      { id: 14, tipo: 'No' }
    ];
    this.opciones9 = [
      { id: 17, tipo: 'Si' },
      { id: 18, tipo: 'No' }
    ];
    this.opciones10 = [
      { id: 19, tipo: 'Si' },
      { id: 20, tipo: 'No' }
    ];
    this.opciones11 = [
      { id: 21, tipo: 'Si' },
      { id: 22, tipo: 'No' }
    ];
    this.opciones12 = [
      { id: 23, tipo: 'Si' },
      { id: 24, tipo: 'No' }
    ];
    this.opciones = [
      { id: 25, tipo: 'Si' },
      { id: 26, tipo: 'No' }
    ];
    this.opciones13 = [
      { id: 27, tipo: 'Si' },
      { id: 28, tipo: 'No' }
    ];
    this.opciones14 = [
      { id: 29, tipo: 'Si' },
      { id: 30, tipo: 'No' }
    ];
    this.opciones15 = [
      { id: 31, tipo: 'Si' },
      { id: 32, tipo: 'No' }
    ];
    this.opciones16 = [
      { id: 33, tipo: 'Si' },
      { id: 34, tipo: 'No' }
    ];
    this.opciones17 = [
      { id: 35, tipo: 'Si' },
      { id: 36, tipo: 'No' }
    ];
    this.opciones18 = [
      { id: 37, tipo: 'Si' },
      { id: 38, tipo: 'No' }
    ];
    this.opciones19 = [
      { id: 39, tipo: 'Si' },
      { id: 40, tipo: 'No' }
    ];
    this.opciones20 = [
      { id: 41, tipo: 'Si' },
      { id: 42, tipo: 'No' }
    ];
    this.opciones21 = [
      { id: 43, tipo: 'Si' },
      { id: 44, tipo: 'No' }
    ];
    this.opciones22 = [
      { id: 45, tipo: 'Si' },
      { id: 46, tipo: 'No' }
    ];
    this.opciones23 = [
      { id: 47, tipo: 'Si' },
      { id: 48, tipo: 'No' }
    ];
    this.opciones24 = [
      { id: 49, tipo: 'Si' },
      { id: 50, tipo: 'No' }
    ];
    this.opciones25 = [
      { id: 100, tipo: 'Si' },
      { id: 101, tipo: 'No' }
    ];
    this.opciones26 = [
      { id: 102, tipo: 'Si' },
      { id: 103, tipo: 'No' }
    ];
    this.opciones27 = [
      { id: 104, tipo: 'Si' },
      { id: 105, tipo: 'No' }
    ];
    this.opciones28 = [
      { id: 106, tipo: 'Si' },
      { id: 107, tipo: 'No' }
    ];
    this.opciones29 = [
      { id: 108, tipo: 'Si' },
      { id: 109, tipo: 'No' }
    ];
    this.opciones30 = [
      { id: 110, tipo: 'Si' },
      { id: 111, tipo: 'No' }
    ];
    this.opciones31 = [
      { id: 112, tipo: 'Si' },
      { id: 113, tipo: 'No' }
    ];
    this.opciones32 = [
      { id: 114, tipo: 'Si' },
      { id: 115, tipo: 'No' }
    ];
    this.opciones33 = [
      { id: 116, tipo: 'Si' },
      { id: 117, tipo: 'No' }
    ];
    this.opciones34 = [
      { id: 118, tipo: 'Si' },
      { id: 119, tipo: 'No' }
    ];
    this.opciones35 = [
      { id: 120, tipo: 'Si' },
      { id: 121, tipo: 'No' }
    ];
    this.opciones36 = [
      { id: 122, tipo: 'Si' },
      { id: 123, tipo: 'No' }
    ];
    this.opciones37 = [
      { id: 124, tipo: 'Si' },
      { id: 125, tipo: 'No' }
    ];
    this.opciones38 = [
      { id: 126, tipo: 'Si' },
      { id: 127, tipo: 'No' }
    ];
    this.opciones39 = [
      { id: 128, tipo: 'Si' },
      { id: 129, tipo: 'No' }
    ];

    this.opciones40 = [
      { id: 130, tipo: 'Si' },
      { id: 131, tipo: 'No' }
    ];
    this.opciones41 = [
      { id: 132, tipo: 'Si' },
      { id: 133, tipo: 'No' }
    ];
    this.opciones42 = [
      { id: 138, tipo: 'Si' },
      { id: 139, tipo: 'No' }
    ];
    this.alimentaciones = [
      { id: 55, tipo: 'Normal' },
      { id: 56, tipo: 'Con apetito' },
      { id: 57, tipo: 'Sin apetito' },
      { id: 58, tipo: 'Otro' }
    ];
    this.pesos = [
      { id: 59, tipo: 'Normal' },
      { id: 60, tipo: 'Bajo de peso' },
      { id: 61, tipo: 'Obesidad' }
    ];
    this.suenios = [
      { id: 62, tipo: 'Normal' },
      { id: 63, tipo: 'Inquiero' },
      { id: 64, tipo: 'Dificultad' }
    ];
    this.insomios = [
      { id: 65, tipo: 'Insomnio' },
      { id: 66, tipo: 'Pesadillas' },
      { id: 67, tipo: 'Terrores nocturnos' },
      { id: 68, tipo: 'Sonambulismo' },
      { id: 69, tipo: 'Despierta de Buen humor' },
    ];
    this.duermes = [
      { id: 70, tipo: 'Solo' },
      { id: 71, tipo: 'Acompañado' }
    ];
    // escolaridad;
    this.modalidades = [
      { id: 72, tipo: 'Regular' },
      { id: 73, tipo: 'Especial' }
    ];

    this.banios = [
      { id: 74, tipo: 'Solo' },
      { id: 75, tipo: 'Acompañado' },
      { id: 76, tipo: 'Usa pañal' }
    ];
    this.vestidos = [
      { id: 77, tipo: 'Solo' },
      { id: 78, tipo: 'Con apoyo' }
    ];
    this.alimentos = [
      { id: 135, tipo: 'Solo' },
      { id: 136, tipo: 'Con apoyo' }
    ];
    // ultimos
    this.desempenioescolar = [
      { id: 79, tipo: 'Satisfactorio' },
      { id: 80, tipo: 'Insatisfactorio' }
    ];
    this.respuestadificultades = [
      { id: 81, tipo: 'Apoyo' },
      { id: 82, tipo: 'Castigo' },
      { id: 83, tipo: 'Indiferencia' },
      { id: 84, tipo: 'Compasión' },
      { id: 85, tipo: 'Tensión' },
      { id: 86, tipo: 'Otros' }
    ];
    this.respuestaexito = [
      { id: 87, tipo: 'Elogio' },
      { id: 88, tipo: 'Muestra de Afecto' },
      { id: 89, tipo: 'Indiferencia' },
      { id: 90, tipo: 'Premio material' },
      { id: 91, tipo: 'Otros' }
    ];
    this.apoyoaprendisaje = [
      { id: 92, tipo: 'Madre' },
      { id: 93, tipo: 'Padre' },
      { id: 94, tipo: 'Hermanos' },
      { id: 95, tipo: 'Otros familiares' },
      { id: 96, tipo: 'Otros profesionales' }
    ];
    this.futuroescolar = [
      { id: 97, tipo: 'Alto' },
      { id: 98, tipo: 'Mediana' },
      { id: 99, tipo: 'Baja' }
    ];
    // termina ultimo
    this.dominios = [
      { id: 140, tipo: 'Comprende' },
      { id: 141, tipo: 'Habla' },
      { id: 142, tipo: 'Lee' },
      { id: 143, tipo: 'Escribe' }
    ];
    this.dominios2 = [
      { id: 144, tipo: 'Comprende' },
      { id: 145, tipo: 'Habla' },
      { id: 146, tipo: 'Lee' },
      { id: 147, tipo: 'Escribe' }
    ];
    this.secomunican = [
      { id: 148, tipo: 'Oral' },
      { id: 149, tipo: 'Gestual' },
      { id: 150, tipo: 'Mixto' },
      { id: 151, tipo: 'Otro' }
    ];

    this.luces = [
      { id: 152, tipo: 'Natural' },
      { id: 153, tipo: 'Desmesurado' }
    ];
    this.sonidos = [
      { id: 154, tipo: 'Natural' },
      { id: 155, tipo: 'Desmesurado' }
    ];
    this.extranios = [
      { id: 156, tipo: 'Natural' },
      { id: 157, tipo: 'Desmesurado' }
    ];
    const dato = this.route.snapshot.paramMap.get('historial');
    console.log(dato);
    this.verpaciente = false;
    this.imprimiExpedientexHistorial(dato);
  }
  imprimiExpedientexHistorial(histoiral) {
    this.expedienteSrv.getBusquedaxHistorial(histoiral).subscribe(
      (data: any[]) => {
        console.log(data);
        this.expedientes = data[0];
        this.idExpediente = data[0].id_expediente;
        this.verpaciente = true;
        this.imprimirReferencia(this.idExpediente);
        this.imprimifamiliar(this.idExpediente);
        this.imprimirEducacion(this.idExpediente);
      });
  }
  imprimirReferencia(dato) {
    this.referenciaSrv.getReferenciaxExpediente(dato).subscribe(res => {
      if (res !== undefined) {
        this.evalueducacion.idreferinterna = res[0].idreferinterna;
      } else {
        this.evalueducacion.idreferinterna = 0;
      }
    });
  }
  imprimirEducacion(dato) {
    this.EducacionSrv.getEducacionxExpediente(dato).subscribe(res => {
      if (res[0] === undefined) {
        this.evalueducacion = new EducacionModel();
      } else {
        this.verotrosEscolaridad = true;
        this.evalueducacion = res[0];
        this.idEducacion = res[0].idfichaeduc;
        this.imprimirSaludEducacion(this.idEducacion);
        this.imprimirDesarrolloleguanje(this.idEducacion);
        this.imprimirDesarrollosocial(this.idEducacion);
        this.imprimirEscolaridad(this.idEducacion);
      }
    });
  }
  imprimirSaludEducacion(dato) {
    this.saludeducacionSrv.getSaludxfichaeduc(dato).subscribe(res => {
      if (res[0] === undefined) {
        this.saluteducacion = new SaludActualEduModel();
      } else {
        this.saluteducacion = res[0];
      }
    });
  }
  imprimirDesarrolloleguanje(dato) {
    this.desalenguajeSrv.getDesalenguajexfichaeduc(dato).subscribe(res => {
      if (res[0] === undefined) {
        this.desalenguaje = new DesalenguajeModel();
      } else {
        this.desalenguaje = res[0];
      }
    });
  }
  imprimirDesarrollosocial(dato) {
    this.desasocialSrv.getDesarrolloSocialfichaeduc(dato).subscribe(res => {
      if (res[0] === undefined) {
        this.desarrsocial = new DesarrolloSocialModel();
      } else {
        this.desarrsocial = res[0];
      }
    });
  }
  imprimirEscolaridad(dato) {
    this.escolaridadSrv.getEscolaridadaxEducacion(dato).subscribe(res => {
      if (res[0] === undefined) {
        this.escolaridad = new EscolaridadModel();
      } else {
        this.escolaridad = res[0];
      }
    });
  }
  imprimifamiliar(dato) {
    this.familiaSrv.getComposisionFamiliar(dato)
      .subscribe(
        (data: any[]) => {
          this.familiares = data;
        });
  }

  chenckboxChangedominio(dominio1) {
    if (this.selectedprograma1.indexOf(dominio1.value) === -1) {
      this.selectedprograma1.push(dominio1.value);
      this.gradodomiio1 = this.selectedprograma1.toString();     
    } else {
      this.selectedprograma1.splice(this.selectedprograma1.indexOf(dominio1.value), 1);
      this.gradodomiio1 = this.selectedprograma1.toString();      
    }
  }
  chenckboxChangedominio2(dominio2) {
    if (this.selectedprograma1.indexOf(dominio2.value) === -1) {
      this.selectedprograma1.push(dominio2.value);
      this.gradodomiio2 = this.selectedprograma1.toString();

    } else {
      this.selectedprograma1.splice(this.selectedprograma1.indexOf(dominio2.value), 1);
      this.gradodomiio2 = this.selectedprograma1.toString();

    }
  }
  igualardominio1() {
    this.evalueducacion.gradodominiolu = this.gradodomiio1;
  }
  igualardominio2() {
    this.evalueducacion.gradodominiolm = this.gradodomiio2;
  }
  onSelectionChangediagnosticoprevio(diagnostico) {
    this.evalueducacion.diagnprevio = diagnostico.tipo;
    if (diagnostico.tipo === 'Si') {
      this.verDiagnostico = true;
    } else if (diagnostico.tipo === 'No') {
      this.verDiagnostico = false;
    }
  }
  onSelectionChangesecomunica(secomunica) {
    if (secomunica.tipo !== 'Otro') {
      this.desalenguaje.comunicacion = secomunica.tipo;
      this.otrolenguaje = false;
    }
  }

  // desarrollo del lenguaje

  onSelectionChangebalbucea(balbucea) {
    this.desalenguaje.balbucea = balbucea.tipo;
  }
  desseleccionarradiogroupbalbucea() {
    this.desalenguaje.balbucea = null;    
    Array.from(document.querySelectorAll('[name=radiogroupbalbucea]')).forEach((x: any) => x.checked  = false);
  }

  onSelectionChangepalabra(palabra) {
    this.desalenguaje.emitepalabras = palabra.tipo;
  }
  desseleccionarradiogrouppalabra() {
    this.desalenguaje.emitepalabras = null;    
    Array.from(document.querySelectorAll('[name=radiogrouppalabra]')).forEach((x: any) => x.checked  = false);
  }

  onSelectionChangeemite(emite) {
    this.desalenguaje.emitefrases = emite.tipo;
  }
  desseleccionarradiogroupemite() {
    this.desalenguaje.emitefrases = null;    
    Array.from(document.querySelectorAll('[name=radiogroupemite]')).forEach((x: any) => x.checked  = false);
  }
  onSelectionChangeclara(clara) {
    this.desalenguaje.pronunclara = clara.tipo;
  }
  desseleccionarradiogrouclara() {
    this.desalenguaje.pronunclara = null;    
    Array.from(document.querySelectorAll('[name=radiogrouclara]')).forEach((x: any) => x.checked  = false);
  }
  onSelectionChangevocaliza(vocaliza) {
    this.desalenguaje.vocaliza = vocaliza.tipo;
  }
  desseleccionarradiogroupvocaliza() {
    this.desalenguaje.vocaliza = null;    
    Array.from(document.querySelectorAll('[name=radiogroupvocaliza]')).forEach((x: any) => x.checked  = false);
  }
  onSelectionChangerelata(relata) {
    this.desalenguaje.relataexpe = relata.tipo;
  }
  desseleccionarradiogrouprelata() {
    this.desalenguaje.relataexpe = null;    
    Array.from(document.querySelectorAll('[name=radiogrouprelata]')).forEach((x: any) => x.checked  = false);
  }
  onSelectionChangeobjeto(objeto) {
    this.desalenguaje.identobjeto = objeto.tipo;
  }
  desseleccionarradiogroupobjeto() {
    this.desalenguaje.identobjeto = null;    
    Array.from(document.querySelectorAll('[name=radiogroupobjeto]')).forEach((x: any) => x.checked  = false);
  }
  onSelectionChangecoherente(coherente) {
    this.desalenguaje.respcoherentes = coherente.tipo;
  }
  desseleccionarradiogroupcoherente() {
    this.desalenguaje.respcoherentes = null;    
    Array.from(document.querySelectorAll('[name=radiogroupcoherente]')).forEach((x: any) => x.checked  = false);
  }
  onSelectionChangepersona(persona) {
    this.desalenguaje.identpersonas = persona.tipo;
  }
  desseleccionarradiogroupopersona() {
    this.desalenguaje.identpersonas = null;    
    Array.from(document.querySelectorAll('[name=radiogroupopersona]')).forEach((x: any) => x.checked  = false);
  }
  onSelectionChangeconceptos(conceptos) {
    this.desalenguaje.comprconcep = conceptos.tipo;
  }
  desseleccionarradiogroupoconceptos() {
    this.desalenguaje.comprconcep = null;    
    Array.from(document.querySelectorAll('[name=radiogroupoconceptos]')).forEach((x: any) => x.checked  = false);
  }
  // Desarrollo Social
  onSelectionChangeespontaneamente(espontaneamente) {
    this.desarrsocial.relespontanea = espontaneamente.tipo;
  }
  desseleccionarradiogroupoespontaneamente() {
    this.desarrsocial.relespontanea = null;    
    Array.from(document.querySelectorAll('[name=radiogroupoespontaneamente]')).forEach((x: any) => x.checked  = false);
  }

  onSelectionChangerazones(razones) {
    this.desarrsocial.explicomporta = razones.tipo;
  }
  desseleccionarradiogrouporazones() {
    this.desarrsocial.explicomporta = null;    
    Array.from(document.querySelectorAll('[name=radiogrouporazones]')).forEach((x: any) => x.checked  = false);
  }

  onSelectionChangegrupales(grupales) {
    this.desarrsocial.partactgrup = grupales.tipo;
  }
  desseleccionarradiogroupogrupales() {
    this.desarrsocial.partactgrup = null;    
    Array.from(document.querySelectorAll('[name=radiogroupogrupales]')).forEach((x: any) => x.checked  = false);
  }
  onSelectionChangepataletas(pataletas) {
    this.desarrsocial.pataletas = pataletas.tipo;
  }
  desseleccionarradiogroupopataletas() {
    this.desarrsocial.pataletas = null;    
    Array.from(document.querySelectorAll('[name=radiogroupopataletas]')).forEach((x: any) => x.checked  = false);
  }
  onSelectionChangeindividual(individual) {
    this.desarrsocial.trabindiv = individual.tipo;
  }
  desseleccionarradiogroupoindividual() {
    this.desarrsocial.trabindiv = null;    
    Array.from(document.querySelectorAll('[name=radiogroupoindividual]')).forEach((x: any) => x.checked  = false);
  }
  onSelectionChangeecolatico(ecolatico) {
    this.desarrsocial.lenguaecolalico = ecolatico.tipo;
  }
  desseleccionarradiogroupoecolatico() {
    this.desarrsocial.lenguaecolalico = null;    
    Array.from(document.querySelectorAll('[name=radiogroupoecolatico]')).forEach((x: any) => x.checked  = false);
  }
  onSelectionChangeadaptarse(adaptarse) {
    this.desarrsocial.dificadapt = adaptarse.tipo;
  }
  desseleccionarradiogroupoadaptarse() {
    this.desarrsocial.dificadapt = null;    
    Array.from(document.querySelectorAll('[name=radiogroupoadaptarse]')).forEach((x: any) => x.checked  = false);
  }  
  onSelectionChangeestereotipado(estereotipado) {
    this.desarrsocial.movestereot = estereotipado.tipo;
  }
  desseleccionarradiogroupoestereotipado() {
    this.desarrsocial.movestereot = null;    
    Array.from(document.querySelectorAll('[name=radiogroupoestereotipado]')).forEach((x: any) => x.checked  = false);
  }  
  onSelectionChangecolaborador(conceptos) {
    this.desarrsocial.escolaborador = conceptos.tipo;
  }
  desseleccionarradiogroupocolaborador() {
    this.desarrsocial.escolaborador = null;    
    Array.from(document.querySelectorAll('[name=radiogroupocolaborador]')).forEach((x: any) => x.checked  = false);
  } 
  onSelectionChangenorma(norma) {
    this.desarrsocial.normassociales = norma.tipo;
  }
  desseleccionarradiogrouponorma() {
    this.desarrsocial.normassociales = null;    
    Array.from(document.querySelectorAll('[name=radiogrouponorma]')).forEach((x: any) => x.checked  = false);
  } 
  onSelectionChangeescolare(escolare) {
    this.desarrsocial.normasescolar = escolare.tipo;
  }
  desseleccionarradiogroupoescolare() {
    this.desarrsocial.normasescolar = null;    
    Array.from(document.querySelectorAll('[name=radiogroupoescolare]')).forEach((x: any) => x.checked  = false);
  }  
  onSelectionChangehumor(humor) {
    this.desarrsocial.humor = humor.tipo;
  }
  desseleccionarradiogroupohumor() {
    this.desarrsocial.humor = null;    
    Array.from(document.querySelectorAll('[name=radiogroupohumor]')).forEach((x: any) => x.checked  = false);
  } 

  onSelectionChangeluces(luz) {
    this.desarrsocial.reaccionluces = luz.tipo;
  }
  desseleccionarradiogroupoluces() {
    this.desarrsocial.reaccionluces = null;    
    Array.from(document.querySelectorAll('[name=radiogroupoluces]')).forEach((x: any) => x.checked  = false);
  }   

  onSelectionChangesonido(sonido) {
    this.desarrsocial.reaccionsonido = sonido.tipo;
  }
  desseleccionarradiogrouposonido() {
    this.desarrsocial.reaccionsonido = null;    
    Array.from(document.querySelectorAll('[name=radiogrouposonido]')).forEach((x: any) => x.checked  = false);
  } 
  
  onSelectionChangeextranio(extranio) {
    this.desarrsocial.reaccionpersext = extranio.tipo;
  }
  desseleccionarradiogroupoextranio() {
    this.desarrsocial.reaccionsonido = null;    
    Array.from(document.querySelectorAll('[name=radiogroupoextranio]')).forEach((x: any) => x.checked  = false);
  } 
  // Estado de salud actual o la estudiante

  onSelectionvacuna(vacuna) {
    this.saluteducacion.vacunasdia = vacuna.tipo;
  }
  desseleccionarradiogroupovacuna() {
    this.saluteducacion.vacunasdia = null;    
    Array.from(document.querySelectorAll('[name=radiogroupovacuna]')).forEach((x: any) => x.checked  = false);
  }

  onSelectionChangeepilepsia(epilepsia) {
    this.saluteducacion.epilepsia = epilepsia.tipo;
  }
  desseleccionarradiogroupoepilepsia() {
    this.saluteducacion.epilepsia = null;    
    Array.from(document.querySelectorAll('[name=radiogroupoepilepsia]')).forEach((x: any) => x.checked  = false);
  }
  onSelectionChangecardiaco(cardiaco) {
    this.saluteducacion.probcardiaco = cardiaco.tipo;
  }
  desseleccionarradiogroupocardiaco() {
    this.saluteducacion.probcardiaco = null;    
    Array.from(document.querySelectorAll('[name=radiogroupocardiaco]')).forEach((x: any) => x.checked  = false);
  }
  onSelectionChangeconductual(conductual) {
    this.saluteducacion.trantornconduc = conductual.tipo;
  }
  desseleccionarradiogroupoconductual() {
    this.saluteducacion.trantornconduc = null;    
    Array.from(document.querySelectorAll('[name=radiogroupoconductual]')).forEach((x: any) => x.checked  = false);
  }
  onSelectionChangemotor(motor) {
    this.saluteducacion.trastornomotor = motor.tipo;
  }
  desseleccionarradiogroupomotorr() {
    this.saluteducacion.trastornomotor = null;    
    Array.from(document.querySelectorAll('[name=radiogroupomotorr]')).forEach((x: any) => x.checked  = false);
  }
  onSelectionChangebronco(bronco) {
    this.saluteducacion.probbroncoresp = bronco.tipo;
  }
  desseleccionarradiogroupobronco() {
    this.saluteducacion.probbroncoresp = null;    
    Array.from(document.querySelectorAll('[name=radiogroupobronco]')).forEach((x: any) => x.checked  = false);
  }
  onSelectionChangeinfecto(infecto) {
    this.saluteducacion.enfercontagio = infecto.tipo;
  }
  desseleccionarradiogroupoinfector() {
    this.saluteducacion.enfercontagio = null;    
    Array.from(document.querySelectorAll('[name=radiogroupoinfector]')).forEach((x: any) => x.checked  = false);
  }
  onSelectionChangeemocional(emocional) {
    this.saluteducacion.transtoremoci = emocional.tipo;
  }
  desseleccionarradiogroupoemocional() {
    this.saluteducacion.transtoremoci = null;    
    Array.from(document.querySelectorAll('[name=radiogroupoemocional]')).forEach((x: any) => x.checked  = false);
  }
  onSelectionChangeparaplejia(paraplejia) {
    this.saluteducacion.paraplejia = paraplejia.tipo;
  }
  desseleccionarradiogroupoparaplejia() {
    this.saluteducacion.paraplejia = null;    
    Array.from(document.querySelectorAll('[name=radiogroupoparaplejia]')).forEach((x: any) => x.checked  = false);
  }
  onSelectionChangeauditiva(auditiva) {
    this.saluteducacion.perdauditiva = auditiva.tipo;
  }
  desseleccionarradiogroupoauditiva() {
    this.saluteducacion.perdauditiva = null;    
    Array.from(document.querySelectorAll('[name=radiogroupoauditiva]')).forEach((x: any) => x.checked  = false);
  }
  onSelectionChangevisual(visual) {
    this.saluteducacion.perdvisual = visual.tipo;
  }
  desseleccionarradiogroupovisual() {
    this.saluteducacion.perdvisual = null;    
    Array.from(document.querySelectorAll('[name=radiogroupovisual]')).forEach((x: any) => x.checked  = false);
  }
  onSelectionChangealimenta(alimenta) {
    this.saluteducacion.alimentacion = alimenta.tipo;
  }
  desseleccionarradiogroupalimenta() {
    this.saluteducacion.alimentacion = null;    
    Array.from(document.querySelectorAll('[name=radiogroupalimenta]')).forEach((x: any) => x.checked  = false);
  }
  onSelectionChangepeso(peso) {
    this.saluteducacion.peso = peso.tipo;
  }
  desseleccionarradiogroupopeso() {
    this.saluteducacion.peso = null;    
    Array.from(document.querySelectorAll('[name=radiogroupopeso]')).forEach((x: any) => x.checked  = false);
  }
  onSelectionChangeinsomio(insomio) {
    this.saluteducacion.sufreinsomio = insomio.tipo;
  }
  desseleccionarradiogroupinsomio() {
    this.saluteducacion.sufreinsomio = null;    
    Array.from(document.querySelectorAll('[name=radiogroupinsomio]')).forEach((x: any) => x.checked  = false);
  }
  onSelectionChangeduerme(duerme) {
    this.saluteducacion.duermecompania = duerme.tipo;
  }
  desseleccionarradiogroupduerme() {
    this.saluteducacion.duermecompania = null;    
    Array.from(document.querySelectorAll('[name=radiogroupduerme]')).forEach((x: any) => x.checked  = false);
  }
  onSelectionChangesuenio(suenio) {
    this.saluteducacion.sueno = suenio.tipo;
  }
  // desseleccionarradiogrouposuenio
  desseleccionarradiogrouposuenio() {
    this.saluteducacion.sueno = null;    
    Array.from(document.querySelectorAll('[name=radiogrouposuenio]')).forEach((x: any) => x.checked  = false);
  }
  // escolaridad
  onSelectionChangerepetidocurso(repetidocurso) {
    this.escolaridad.repitecursos = repetidocurso.tipo;
  }
  desseleccionarradiogrouprepetidocurso() {
    this.escolaridad.repitecursos = null;    
    Array.from(document.querySelectorAll('[name=radiogrouprepetidocurso]')).forEach((x: any) => x.checked  = false);
  }
  onSelectionChangejardin(jardin) {
    this.escolaridad.asistiojardin = jardin.tipo;
  }
   desseleccionarradiogroupjardin() {
    this.escolaridad.asistiojardin = null;    
    Array.from(document.querySelectorAll('[name=radiogroupjardin]')).forEach((x: any) => x.checked  = false);
  }

  onSelectionChangemodalidad(modalidad) {
    this.escolaridad.modensenanza = modalidad.tipo;
  }
   desseleccionarradiogroupmodalidad() {
    this.escolaridad.modensenanza = null;    
    Array.from(document.querySelectorAll('[name=radiogroupmodalidad]')).forEach((x: any) => x.checked  = false);
  }

  onSelectionChangedisruptiva(disruptiva) {
    this.escolaridad.conductadisr = disruptiva.tipo;
  }
   desseleccionarradiogroupdisruptiva() {
    this.escolaridad.conductadisr = null;    
    Array.from(document.querySelectorAll('[name=radiogroupdisruptiva]')).forEach((x: any) => x.checked  = false);
  }

  onSelectionChangeamigos(amigos) {
    this.escolaridad.amigos = amigos.tipo;
  }
   desseleccionarradiogroupamigos() {
    this.escolaridad.amigos = null;    
    Array.from(document.querySelectorAll('[name=radiogroupamigos]')).forEach((x: any) => x.checked  = false);
  }

  onSelectionChangedificultad(dificultad) {
    this.escolaridad.difaprendizaje = dificultad.tipo;
  }
   desseleccionarradiogroupdificultad() {
    this.escolaridad.difaprendizaje = null;    
    Array.from(document.querySelectorAll('[name=radiogroupdificultad]')).forEach((x: any) => x.checked  = false);
  }

  onSelectionChangedificultadparti(dificultadparti) {
    this.escolaridad.difparticipar = dificultadparti.tipo;
  }
  desseleccionaradiogroupdificultadpartir() {
    this.escolaridad.difparticipar = null;    
    Array.from(document.querySelectorAll('[name=radiogroupdificultadparti]')).forEach((x: any) => x.checked  = false);
  }

  onSelectionChangeregularmente(regularmente) {
    this.escolaridad.asistereg = regularmente.tipo;
  }
  desseleccionarradiogroupregularmente() {
    this.escolaridad.asistereg = null;    
    Array.from(document.querySelectorAll('[name=radiogroupregularmente]')).forEach((x: any) => x.checked  = false);
  }

  onSelectionChangeagrado(agrado) {
    this.escolaridad.asisteagrado = agrado.tipo;
  }
   desseleccionarradiogroupagrado() {
    this.escolaridad.asisteagrado = null;    
    Array.from(document.querySelectorAll('[name=radiogroupagrado]')).forEach((x: any) => x.checked  = false);
  }

  onSelectionChangebanios(banios) {
    this.escolaridad.necesbano = banios.tipo;
  }
   desseleccionarradiogroupbanios() {
    this.escolaridad.necesbano = null;    
    Array.from(document.querySelectorAll('[name=radiogroupbanios]')).forEach((x: any) => x.checked  = false);
  }

  onSelectionChangevestido(vestido) {
    this.escolaridad.necesestido = vestido.tipo;
  }
   desseleccionarradiogroupvestido() {
    this.escolaridad.necesestido = null;    
    Array.from(document.querySelectorAll('[name=radiogroupvestido]')).forEach((x: any) => x.checked  = false);
  }

  onSelectionChangealiment(aliment) {
    this.escolaridad.necesalim = aliment.tipo;
  }
   desseleccionarradiogroupaliment() {
    this.escolaridad.necesalim = null;    
    Array.from(document.querySelectorAll('[name=radiogroupaliment]')).forEach((x: any) => x.checked  = false);
  }

  onSelectionChangedesempenioesco(desempenioesco) {
    this.escolaridad.evaldesempeno = desempenioesco.tipo;
  }
   desseleccionarradiogroupdesempenioesco() {
    this.escolaridad.evaldesempeno = null;    
    Array.from(document.querySelectorAll('[name=radiogroupdesempenioesco]')).forEach((x: any) => x.checked  = false);
  }

  onSelectionChangerespuestadifi(respuestadifi) {
    this.escolaridad.respdificultades = respuestadifi.tipo;
  }
   desseleccionarradiogrouprespuestadifi() {
    this.escolaridad.respdificultades = null;    
    Array.from(document.querySelectorAll('[name=radiogrouprespuestadifi]')).forEach((x: any) => x.checked  = false);
  }

  onSelectionChangefuturoesco(futuroesco) {
    this.escolaridad.expectativas = futuroesco.tipo;
  }
   desseleccionarradiogroupfuturoesco() {
    this.escolaridad.expectativas = null;    
    Array.from(document.querySelectorAll('[name=radiogroupfuturoesco]')).forEach((x: any) => x.checked  = false);
  }

  onSelectionChangerespuestaex(respuestaex) {
    this.escolaridad.respexitos = respuestaex.tipo;
  }
   desseleccionarradiogrourespuestaex() {
    this.escolaridad.respexitos = null;    
    Array.from(document.querySelectorAll('[name=radiogrourespuestaex]')).forEach((x: any) => x.checked  = false);
  }

  onSelectionChangeapoyoaprendi(apoyoaprendi) {
    this.escolaridad.apoyoaprendizaje = apoyoaprendi.tipo;
  }
   desseleccionarradiogroupapoyoaprendi() {
    this.escolaridad.apoyoaprendizaje = null;    
    Array.from(document.querySelectorAll('[name=radiogroupapoyoaprendi]')).forEach((x: any) => x.checked  = false);
  }

  saveOrUpdateSaludEducacion(): void {
    this.saluteducacion.idfichaeduc = this.idEducacion;
    if (this.isValid) {
      this.saludeducacionSrv.saveOrUpdateSalud(this.saluteducacion).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Guardado!', 'success');
        } else {
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  saveOrUpdateEscolaridadl(): void {
    this.escolaridad.idfichaeduc = this.idEducacion;
    if (this.isValid) {
      this.escolaridadSrv.saveOrUpdateEscolaridad(this.escolaridad).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Guardado!', 'success');
        } else {
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }

  saveOrUpdateDesarroSocial(): void {
    this.desarrsocial.idfichaeduc = this.idEducacion;
    if (this.isValid) {
      this.desasocialSrv.saveOrUpdateDesarrolloSocial(this.desarrsocial).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Guardado!', 'success');
        } else {
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }

  saveOrUpdateDesarroLenguaje(): void {
    this.desalenguaje.idfichaeduc = this.idEducacion;
    if (this.isValid) {
      this.desalenguajeSrv.saveOrUpdateDesalenguaje(this.desalenguaje).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Guardado!', 'success');
        } else {
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  saveOrUpdateEducacion(): void {
    this.evalueducacion.idexpediente = this.idExpediente;
    if (this.isValid) {
      this.EducacionSrv.saveOrUpdateEducacion(this.evalueducacion).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Guardado!', 'success');
          this.imprimirEducacion(this.idExpediente);
        } else {
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
}
