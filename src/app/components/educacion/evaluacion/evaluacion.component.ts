/* cSpell:disable */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OK } from '../../../models/configuration/httpstatus';
declare var swal: any;
import { 
  ExpedienteService, 
  EducacionService, 
  EvaluacionEducacionService 
} from '@services/servicio.index';
import { 
  EducacionModel, 
  EvaluacionEduModel, 
  ExpedienteModel } from '@models/model.index';

@Component({
  selector: 'app-evaluacion',
  templateUrl: './evaluacion.component.html',
  styleUrls: ['./evaluacion.component.scss']
})
export class EvaluacionComponent implements OnInit {
  total81: number;
  total8: number;
  total71: number;
  total7: number;
  total61: number;
  total6: number;
  total51: number;
  total5: number;
  total41: number;
  total4: number;
  total31: number;
  total3: number;
  total21: number;
  total11: number;
  total2: number;
  total1: number;
  idEducacion: number;
  evalueducacion: EducacionModel;
  evalucion1: EvaluacionEduModel;
  reevalucion1: EvaluacionEduModel;
  evalucion2: EvaluacionEduModel;
  reevalucion2: EvaluacionEduModel;
  evalucion3: EvaluacionEduModel;
  reevalucion3: EvaluacionEduModel;
  evalucion4: EvaluacionEduModel;
  reevalucion4: EvaluacionEduModel;
  evalucion5: EvaluacionEduModel;
  reevalucion5: EvaluacionEduModel;
  idExpediente: any;
  public expedientes: Array<ExpedienteModel>;
  verpaciente: boolean;
  public isValid: boolean = true;
  private message: string = '';
  verAtender = false;
  constructor(
    private expedienteSrv: ExpedienteService,
    private EducacionSrv: EducacionService,
    private evaluacionSrv: EvaluacionEducacionService,
    private route: ActivatedRoute,    
  ) {
    if (sessionStorage.getItem('evalucion1')) {
      this.evalucion1 = JSON.parse(sessionStorage.getItem('evalucion1'));
    } else {
      this.evalucion1 = new EvaluacionEduModel();
    }
    if (sessionStorage.getItem('reevalucion1')) {
      this.reevalucion1 = JSON.parse(sessionStorage.getItem('reevalucion1'));
    } else {
      this.reevalucion1 = new EvaluacionEduModel();
    }

    if (sessionStorage.getItem('evalucion2')) {
      this.evalucion2 = JSON.parse(sessionStorage.getItem('evalucion2'));
    } else {
      this.evalucion2 = new EvaluacionEduModel();
    }
    if (sessionStorage.getItem('reevalucion2')) {
      this.reevalucion2 = JSON.parse(sessionStorage.getItem('reevalucion2'));
    } else {
      this.reevalucion2 = new EvaluacionEduModel();
    }

    if (sessionStorage.getItem('evalucion3')) {
      this.evalucion3 = JSON.parse(sessionStorage.getItem('evalucion3'));
    } else {
      this.evalucion3 = new EvaluacionEduModel();
    }
    if (sessionStorage.getItem('reevalucion3')) {
      this.reevalucion3 = JSON.parse(sessionStorage.getItem('reevalucion3'));
    } else {
      this.reevalucion3 = new EvaluacionEduModel();
    }

    if (sessionStorage.getItem('evalucion4')) {
      this.evalucion4 = JSON.parse(sessionStorage.getItem('evalucion4'));
    } else {
      this.evalucion4 = new EvaluacionEduModel();
    }
    if (sessionStorage.getItem('reevalucion4')) {
      this.reevalucion4 = JSON.parse(sessionStorage.getItem('reevalucion4'));
    } else {
      this.reevalucion4 = new EvaluacionEduModel();
    }

    if (sessionStorage.getItem('evalucion5')) {
      this.evalucion5 = JSON.parse(sessionStorage.getItem('evalucion5'));
    } else {
      this.evalucion5 = new EvaluacionEduModel();
    }
    if (sessionStorage.getItem('reevalucion5')) {
      this.reevalucion5 = JSON.parse(sessionStorage.getItem('reevalucion5'));
    } else {
      this.reevalucion5 = new EvaluacionEduModel();
    }
  }
  ngOnInit() {
    const dato = this.route.snapshot.paramMap.get('historial');
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));       
    if(autorization === 8  || autorization === 5  || autorization === 4 ) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }
    this.verpaciente = false;
    this.imprimiExpedientexHistorial(dato);
  }
  imprimiExpedientexHistorial(histoiral) {
    this.expedienteSrv.getBusquedaxHistorial(histoiral).subscribe(
      (data: any[]) => {
        console.log(data);
        this.expedientes = data[0];
        this.idExpediente = data[0].id_expediente;
        this.verpaciente = true;
        this.imprimirEducacion(this.idExpediente);
      });
  }
  imprimirEducacion(dato) {
    this.EducacionSrv.getEducacionxExpediente(dato).subscribe(res => {
      if (res[0] === undefined) {
        this.evalueducacion = new EducacionModel();
      } else {
        this.evalueducacion = res[0];
        this.idEducacion = res[0].idfichaeduc;
        this.imprimirevaluacionEducacion1(dato);
      }
    });
  }
  
  imprimirevaluacionEducacion1(dato) {
    const numEval1 = 1;
    this.evaluacionSrv.getEvaluacionEducacionxfichaeducacionYnumEvalu(dato, numEval1).subscribe(res => {
      if (res[0] === undefined) {
        this.reevalucion1 = new EvaluacionEduModel();
      } else {
        this.reevalucion1 = res[0];
        this.sumarprimeraparteReeleccion();
        this.sumarprimerapartesegundoReeleccio();        
        if (res[1] === undefined) {
          this.reevalucion2 = new EvaluacionEduModel();
        } else {
          this.reevalucion2 = res[1];
          this.sumarsegundaparteReevalprimero();
          if (res[2] === undefined) {
            this.reevalucion3 = new EvaluacionEduModel();
          } else {
            this.reevalucion3 = res[2];
            this.sumarterceraReeleccion();
            if (res[3] === undefined) {
              this.reevalucion4 = new EvaluacionEduModel();
            } else {
              this.reevalucion4 = res[3];
              this.sumarCuartoReeleccion();
              if (res[4] === undefined) {
                this.reevalucion5 = new EvaluacionEduModel();
              } else {
                this.reevalucion5 = res[4];
                this.sumarQuintoReeleccion();
                this.sumarQuintoReeleccionsegunda();
                this.sumarQuintoReelecciontercera();
              }
            }
          }
        }
      }
    });
    const numEval0 = 0;
    this.evaluacionSrv.getEvaluacionEducacionxfichaeducacionYnumEvalu(dato, numEval0).subscribe(res => {
      if (res[0] === undefined) {
        this.evalucion1 = new EvaluacionEduModel();
      } else {
        this.evalucion1 = res[0];
        this.sumarprimeraparteprimero();
        this.sumarprimerapartesegundo();        
        if (res[1] === undefined) {
          this.evalucion2 = new EvaluacionEduModel();
        } else {
          this.evalucion2 = res[1];
          this.sumarsegundaparteprimero();
          if (res[2] === undefined) {
            this.evalucion3 = new EvaluacionEduModel();
          } else {
            this.evalucion3 = res[2];
            this.sumarterceraeleccion();
            if (res[3] === undefined) {
              this.evalucion4 = new EvaluacionEduModel();
            } else {
              this.evalucion4 = res[3];
              this.sumarCuartoeleccion();
              if (res[4] === undefined) {
                this.evalucion5 = new EvaluacionEduModel();
              } else {
                this.evalucion5 = res[4];
                this.sumarQuintoeleccion();
                this.sumarQuintoeleccionsegunda();
                this.sumarQuintoelecciontercera();
              }
            }
          }
        }
      }
    });
  }

  sumarprimeraparteprimero() {
    this.total1 = this.evalucion1.item1 + this.evalucion1.item2 + this.evalucion1.item3 + this.evalucion1.item4 + this.evalucion1.item5 + this.evalucion1.item6 + this.evalucion1.item7 + this.evalucion1.item8 + this.evalucion1.item9;
  }
  sumarprimeraparteReeleccion() {
    this.total11 = this.reevalucion1.item1 + this.reevalucion1.item2 + this.reevalucion1.item3 + this.reevalucion1.item4 + this.reevalucion1.item5 +
    this.reevalucion1.item6 + this.reevalucion1.item7 + this.reevalucion1.item8 + this.reevalucion1.item9;
  }

  sumarprimerapartesegundo() {
    this.total2 = this.evalucion1.item10 + this.evalucion1.item11 + this.evalucion1.item12 + this.evalucion1.item13 + this.evalucion1.item14 + this.evalucion1.item15 + this.evalucion1.item16 + this.evalucion1.item17;
  }
  sumarprimerapartesegundoReeleccio() {
    this.total21 = this.reevalucion1.item10 + this.reevalucion1.item11 + this.reevalucion1.item12 + this.reevalucion1.item13 + this.reevalucion1.item14 + this.reevalucion1.item15 + this.reevalucion1.item16 + this.reevalucion1.item17;
  }

  sumarsegundaparteprimero() {
    this.total3 = this.evalucion2.item1 + this.evalucion2.item2 + this.evalucion2.item3 + this.evalucion2.item4 + this.evalucion2.item5 + this.evalucion2.item6;
  }
  sumarsegundaparteReevalprimero() {
    this.total31 = this.reevalucion2.item1 + this.reevalucion2.item2 + this.reevalucion2.item3 + this.reevalucion2.item4 + this.reevalucion2.item5 + this.reevalucion2.item6;
  }
  sumarterceraeleccion() {
    this.total4 = this.evalucion3.item1 + this.evalucion3.item2 + this.evalucion3.item3 + this.evalucion3.item4 + this.evalucion3.item5 + this.evalucion3.item6 + this.evalucion3.item7 + this.evalucion3.item8 + this.evalucion3.item9 +
      this.evalucion3.item10 + this.evalucion3.item11 + this.evalucion3.item12 + this.evalucion3.item13;
  }
  sumarterceraReeleccion() {
    this.total41 = this.reevalucion3.item1 + this.reevalucion3.item2 + this.reevalucion3.item3 + this.reevalucion3.item4 + this.reevalucion3.item5 +
     this.reevalucion3.item6 + this.reevalucion3.item7 + this.reevalucion3.item8 + this.reevalucion3.item9 +
      this.reevalucion3.item10 + this.reevalucion3.item11 + this.reevalucion3.item12 + this.reevalucion3.item13;
  }

  sumarCuartoeleccion() {
    this.total5 = this.evalucion4.item1 + this.evalucion4.item2 + this.evalucion4.item3 + this.evalucion4.item4 + this.evalucion4.item5 + this.evalucion4.item6 + this.evalucion4.item7 + this.evalucion4.item8 + this.evalucion4.item9 +
      this.evalucion4.item10 + this.evalucion4.item11 + this.evalucion4.item12 + this.evalucion4.item13 + this.evalucion4.item14 + this.evalucion4.item15 + this.evalucion4.item16 + this.evalucion4.item17 + this.evalucion4.item18;
  }
  sumarCuartoReeleccion() {
    this.total51 = this.reevalucion4.item1 + this.reevalucion4.item2 + this.reevalucion4.item3 + this.reevalucion4.item4 + this.reevalucion4.item5 +
     this.reevalucion4.item6 + this.reevalucion4.item7 + this.reevalucion4.item8 + this.reevalucion4.item9 +
      this.reevalucion4.item10 + this.reevalucion4.item11 + this.reevalucion4.item12 + this.reevalucion4.item13 + this.reevalucion4.item14 +
      this.reevalucion4.item15 + this.reevalucion4.item16 + this.reevalucion4.item17 + this.reevalucion4.item18;
  }
  sumarQuintoeleccion() {
    this.total6 = this.evalucion5.item1 + this.evalucion5.item2 + this.evalucion5.item3 + this.evalucion5.item4 + this.evalucion5.item5 + this.evalucion5.item6 + this.evalucion5.item7 + this.evalucion5.item8;
  }
  sumarQuintoReeleccion() {
    this.total61 = this.reevalucion5.item1 + this.reevalucion5.item2 + this.reevalucion5.item3 + this.reevalucion5.item4 + this.reevalucion5.item5 + this.reevalucion5.item6 + this.reevalucion5.item7 + this.reevalucion5.item8;
  }

  sumarQuintoeleccionsegunda() {
    this.total7 = this.evalucion5.item9 + this.evalucion5.item10 + this.evalucion5.item11;
  }
  sumarQuintoReeleccionsegunda() {
    this.total71 = this.reevalucion5.item9 + this.reevalucion5.item10 + this.reevalucion5.item11;
  }
  sumarQuintoelecciontercera() {
    this.total8 = this.evalucion5.item12 + this.evalucion5.item13 + this.evalucion5.item14 + this.evalucion5.item15 + this.evalucion5.item16;
  }
  sumarQuintoReelecciontercera() {
    this.total81 = this.reevalucion5.item12 + this.reevalucion5.item13 + this.reevalucion5.item14 + this.reevalucion5.item15 + this.reevalucion5.item16;
  }
  saveOrUpdateEducacion1(): void {
    this.evalucion1.numevaluacion = 0;
    this.evalucion1.tipohabilidad = 0;
    this.evalucion1.idfichaeducacion = this.idEducacion;
    if (this.isValid) {
      this.evaluacionSrv.saveOrUpdateEvaluacionEducacion(this.evalucion1).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Evaluación Guardado !', 'success');          
        } else {
          this.message = res.message;
          // this.isValid = false;
          swal('Mal!', 'Los campos con * son obligatorios!', 'warning');
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }

  saveOrUpdateREEducacion1(): void {
    this.reevalucion1.numevaluacion = 1;
    this.reevalucion1.tipohabilidad = 0;
    this.reevalucion1.idfichaeducacion = this.idEducacion;
    if (this.isValid) {
      this.evaluacionSrv.saveOrUpdateEvaluacionEducacion(this.reevalucion1).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Guardado!', 'success');          
          this.imprimirevaluacionEducacion1(this.idEducacion);
        } else {
          this.message = res.message;
          // this.isValid = false;
          swal('Mal!', 'Los campos con * son obligatorios!', 'warning');
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  saveOrUpdateEducacion2(): void {
    this.evalucion2.numevaluacion = 0;
    this.evalucion2.tipohabilidad = 1;
    this.evalucion2.idfichaeducacion = this.idEducacion;
    if (this.isValid) {
      this.evaluacionSrv.saveOrUpdateEvaluacionEducacion(this.evalucion2).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Guardado!', 'success');
          this.imprimirevaluacionEducacion1(this.idEducacion);
        } else {
          this.message = res.message;
          // this.isValid = false;
          swal('Mal!', 'Los campos con * son obligatorios!', 'warning');
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  saveOrUpdateREEducacion2(): void {
    this.reevalucion2.numevaluacion = 1;
    this.reevalucion2.tipohabilidad = 1;
    this.reevalucion2.idfichaeducacion = this.idEducacion;
    if (this.isValid) {
      this.evaluacionSrv.saveOrUpdateEvaluacionEducacion(this.reevalucion2).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Guardado!', 'success').then(() => {                    
            this.imprimirevaluacionEducacion1(this.idEducacion);
          });
        } else {
          this.message = res.message;
          // this.isValid = false;
          swal('Mal!', 'Los campos con * son obligatorios!', 'warning');
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  saveOrUpdateEducacion3(): void {
    this.evalucion2.numevaluacion = 0;
    this.evalucion2.tipohabilidad = 2;
    this.evalucion2.idfichaeducacion = this.idEducacion;
    if (this.isValid) {
      this.evaluacionSrv.saveOrUpdateEvaluacionEducacion(this.evalucion3).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Guardado!', 'success');
          this.imprimirevaluacionEducacion1(this.idEducacion);
        } else {
          this.message = res.message;
          // this.isValid = false;
          swal('Mal!', 'Los campos con * son obligatorios!', 'warning');
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  saveOrUpdateREEducacion3(): void {
    this.reevalucion2.numevaluacion = 1;
    this.reevalucion2.tipohabilidad = 2;
    this.reevalucion2.idfichaeducacion = this.idEducacion;
    if (this.isValid) {
      this.evaluacionSrv.saveOrUpdateEvaluacionEducacion(this.reevalucion3).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Guardado!', 'success').then(() => {  
          this.imprimirevaluacionEducacion1(this.idEducacion);
          });
        } else {
          this.message = res.message;
          // this.isValid = false;
          swal('Mal!', 'Los campos con * son obligatorios!', 'warning');
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }

  saveOrUpdateEducacion4(): void {
    this.evalucion3.numevaluacion = 0;
    this.evalucion3.tipohabilidad = 3;
    this.evalucion3.idfichaeducacion = this.idEducacion;
    if (this.isValid) {
      this.evaluacionSrv.saveOrUpdateEvaluacionEducacion(this.evalucion4).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Guardado!', 'success').then(() => {  
          this.imprimirevaluacionEducacion1(this.idEducacion);
          });
        } else {
          this.message = res.message;
          // this.isValid = false;
          swal('Mal!', 'Los campos con * son obligatorios!', 'warning');
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  saveOrUpdateREEducacion4(): void {
    this.reevalucion3.numevaluacion = 1;
    this.reevalucion3.tipohabilidad = 3;
    this.reevalucion3.idfichaeducacion = this.idEducacion;
    if (this.isValid) {
      this.evaluacionSrv.saveOrUpdateEvaluacionEducacion(this.reevalucion4).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Guardado!', 'success').then(() => {  
          this.imprimirevaluacionEducacion1(this.idEducacion);
          });
        } else {
          this.message = res.message;
          // this.isValid = false;
          swal('Mal!', 'Los campos con * son obligatorios!', 'warning');
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }

  saveOrUpdateEducacion5(): void {
    this.evalucion4.numevaluacion = 0;
    this.evalucion4.tipohabilidad = 4;
    this.evalucion4.idfichaeducacion = this.idEducacion;
    if (this.isValid) {
      this.evaluacionSrv.saveOrUpdateEvaluacionEducacion(this.evalucion5).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Guardado!', 'success').then(() => {  
          this.imprimirevaluacionEducacion1(this.idEducacion);
          });
        } else {
          this.message = res.message;
          // this.isValid = false;
          swal('Mal!', 'Los campos con * son obligatorios!', 'warning');
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  saveOrUpdateREEducacion5(): void {
    this.reevalucion4.numevaluacion = 1;
    this.reevalucion4.tipohabilidad = 4;
    this.reevalucion4.idfichaeducacion = this.idEducacion;
    if (this.isValid) {
      this.evaluacionSrv.saveOrUpdateEvaluacionEducacion(this.reevalucion5).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Guardado!', 'success').then(() => {  
          this.imprimirevaluacionEducacion1(this.idEducacion);
          });
        } else {
          this.message = res.message;
          // this.isValid = false;
          swal('Mal!', 'Los campos con * son obligatorios!', 'warning');
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
}
