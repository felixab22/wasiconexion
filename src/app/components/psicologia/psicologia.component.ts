import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FileUploader } from 'ng2-file-upload';
import { OK } from '../../models/configuration/httpstatus';
import { URL_SERVICIOS } from '../../config/config';

declare var swal: any;
// declare var FileSaver: any;
import {
  ExpedienteService,
  EvalPsicologicaService,
  ApoderadoService,
  CompoFamiliarService,
  RefInternaService,
  FilesService
} from '@services/servicio.index';
import {
  ReferenciaImtermaModel,
  ApoderadoModel,
  FichaPsicologicaModel,
  ExpedienteModel
} from '@models/model.index';

const URL = URL_SERVICIOS + '/upload';
@Component({
  selector: 'app-psicologia',
  templateUrl: './psicologia.component.html',
  styles: []
})
export class PsicologiaComponent implements OnInit {
  documentnames: Array<any>;
  public uploader: FileUploader = new FileUploader({ url: URL });
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;

  verAtender = false;
  chekeadotiporelacion: string;
  relacionpadres: { id: number; descripcion: string; }[];
  tipfamilia: { id: number; descripcion: string; }[];
  referencia: ReferenciaImtermaModel;
  verOtroRadiorelacionPadres: boolean;
  chekeadotipoFamiliar: string;
  verOtroRadiotipoFamiliar: boolean;
  familiares: any[];
  MadreExiste: boolean;
  PadreExiste: boolean;
  padre: ApoderadoModel;
  madre: ApoderadoModel;
  apoderado: ApoderadoModel;
  idExpediente: number;
  verpaciente: boolean;
  evalpsicologico: FichaPsicologicaModel;
  public expedientes: Array<ExpedienteModel>;
  public isValid: boolean = true;
  private message: string = '';
  codigoHistorial: string;
  constructor(
    private expedienteSrv: ExpedienteService,
    private route: ActivatedRoute,
    private psicologiaSrv: EvalPsicologicaService,
    private apoderadoSrv: ApoderadoService,
    private familiaSrv: CompoFamiliarService,
    private referenciaSrv: RefInternaService,
    private _uploadFilesSrv: FilesService
  ) {
    if (sessionStorage.getItem('evalpsicologico')) {
      this.evalpsicologico = JSON.parse(sessionStorage.getItem('evalpsicologico'));
    } else {
      this.evalpsicologico = new FichaPsicologicaModel();
    }
  }
  ngOnInit() {

    this.codigoHistorial = this.route.snapshot.paramMap.get('historial');
    this.verpaciente = false;
    this.imprimiExpedientexHistorial(this.codigoHistorial);
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));
    if (autorization === 10 || autorization === 17 || autorization === 5 || autorization === 4) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }
    this.PadreExiste = false;
    this.MadreExiste = false;
    this.verOtroRadiorelacionPadres = false;
    this.verOtroRadiotipoFamiliar = false;
    this.tipfamilia = [
      { id: 1, descripcion: 'Nuclear' },
      { id: 2, descripcion: 'Monoparentales' },
      { id: 3, descripcion: 'Compuesta' },
      { id: 4, descripcion: 'Extensa' },
      { id: 5, descripcion: 'Otro' }];
    this.relacionpadres = [
      { id: 6, descripcion: 'Casados' },
      { id: 7, descripcion: 'Divorciados' },
      { id: 8, descripcion: 'Convivencia' },
      { id: 9, descripcion: 'Relacional Ocacional' },
      { id: 10, descripcion: 'Separados' },
      { id: 11, descripcion: 'Otro' }];
    // this.imprimirFiles();
  }

  imprimiExpedientexHistorial(histoiral) {
    this.expedienteSrv.getBusquedaxHistorial(histoiral).subscribe(
      (data: any[]) => {
        this.expedientes = data[0];
        this.idExpediente = data[0].id_expediente;
        this.verpaciente = true;
        this.imprimirpadres(this.idExpediente);
        this.imprimifamiliar(this.idExpediente);
        this.imprimirEvaluacionPsicologica(this.idExpediente);
        this.imprimirReferencia(this.idExpediente);
        this.imprimirFiles();
      });
  }
  imprimirReferencia(dato) {
    this.referenciaSrv.getReferenciaxExpediente(dato).subscribe(res => {
      if (res !== undefined) {
        this.evalpsicologico.idreferenciainterna = res[0].idreferinterna;
      } else {
        this.evalpsicologico.idreferenciainterna = 0;
      }
    });
  }
  imprimirpadres(dato) {
    this.apoderadoSrv.getApoderadoxExpediente(dato).subscribe(res => {
      if (res !== undefined) {
        if (res[0].tipo_apoderado === 'Padre') {
          this.PadreExiste = true;
          this.padre = res[0];
          if (res[1] !== undefined) {
            this.MadreExiste = true;
            this.madre = res[1];
          }
        } else {
          this.madre = res[0];
          this.MadreExiste = true;
          if (res[1] !== undefined) {
            this.PadreExiste = true;
            this.padre = res[1];
          }
        }
        if (res[0].tipo_apoderado === 'Apoderado') {
          this.PadreExiste = true;
          this.padre = res[0];
        }
      }
    });
  }
  imprimifamiliar(dato) {
    this.familiaSrv.getComposisionFamiliar(dato).subscribe(
      (res: any[]) => {
        if (res !== undefined) {
          this.familiares = res;
        }
      });
  }
  imprimirEvaluacionPsicologica(dato) {
    this.psicologiaSrv.getPsicologicaxExpediente(dato).subscribe(res => {
      if (res[0] === undefined) {
        this.evalpsicologico = new FichaPsicologicaModel();
      } else {
        this.evalpsicologico = res[0];
        this.chekeadotipoFamiliar = res[0].tipofamilia;
        this.chekeadotiporelacion = res[0].relacionpadres;
      }
    }
    );
  }
  imprimirFiles() {
    console.log(this.codigoHistorial);
    this._uploadFilesSrv.getListaFilesxAreayPaciente('psicologia', this.codigoHistorial).subscribe(res => {
      if (res.length === 0) {
        swal('error!', 'No existen archivos!', 'warning');
      } else {
        this.documentnames = res;
      }
    });
  }
  descargar(nombreArchivo) {
    window.open(`${URL_SERVICIOS}/file/${nombreArchivo}?nombrearea=psicologia&codigo=${this.codigoHistorial}`);
  }

  onSelectionChangeTipoFamilia(entrya) {
    if (entrya.descripcion !== 'Otro') {
      this.evalpsicologico.tipofamilia = entrya.descripcion;
      this.verOtroRadiotipoFamiliar = false;
    } else {
      this.verOtroRadiotipoFamiliar = true;
    }
  }
  onSelectionChangerelacionPadres(rela) {
    if (rela.descripcion !== 'Otro') {
      this.evalpsicologico.relacionpadres = rela.descripcion;
      this.verOtroRadiorelacionPadres = false;
    } else {
      this.verOtroRadiorelacionPadres = true;
    }
  }
  saveOrUpdateEvaluacionPsicologica(): void {
    this.evalpsicologico.idexpediente = this.idExpediente;
    if (this.isValid) {
      this.psicologiaSrv.saveOrUpdateEvalPsicologica(this.evalpsicologico).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Guardado!', 'success');
          this.imprimirEvaluacionPsicologica(this.idExpediente);
        } else {
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  saveOrUpdatePadre(): void {
    if (this.isValid) {
      this.apoderadoSrv.saveOrUpdate(this.padre).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Actualizado!', 'success');
        } else {
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  saveOrUpdateMadre(): void {
    if (this.isValid) {
      this.apoderadoSrv.saveOrUpdate(this.madre).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Actualizado!', 'success');
        } else {
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
    for (let i = 0; i < this.uploader.queue.length; i++) {
      this.uploader.queue[i].file.name = 'psicologia-' + this.codigoHistorial + '-' + this.uploader.queue[i].file.name;
    }
  }

  eliminararchivo(documento) {
    console.log(documento);
    swal({
      title: 'Esta seguro?',
      text: 'Usted esta por eliminar el archivo!',
      icon: 'warning',
      buttons: true,
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          this._uploadFilesSrv.deleteFilesxcodigo(documento, 'psicologia', this.codigoHistorial).subscribe(res => {
            if (res) {
              this.imprimirFiles();
              swal('Horario eliminado', {
                icon: 'success',
              });
            }
          });

        } else {
          swal('Sin acciones!');
        }
      });
  }
}
