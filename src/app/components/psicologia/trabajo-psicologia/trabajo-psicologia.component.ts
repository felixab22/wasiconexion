/* cSpell:disable */
import { Component, OnInit, Input } from '@angular/core';
import { OK } from '../../../models/configuration/httpstatus';
import { EvaluacionFamiliarModel } from '@models/model.index';
import { EvalFamiliaService } from '@services/servicio.index';
declare var swal: any;

@Component({
  selector: 'app-trabajo-psicologia',
  templateUrl: './trabajo-psicologia.component.html',
  styleUrls: ['./trabajo-psicologia.component.scss']
})
export class TrabajoPsicologiaComponent implements OnInit {
  @Input() idficha;
  verAtender = false;
  public isValid: boolean = true;
  personas: { id: number; descripcion: string; }[];
  today: any;
  evaluacion: EvaluacionFamiliarModel;
  evaluaciones: Array<EvaluacionFamiliarModel>;
  evaluacionesfintadas: Array<EvaluacionFamiliarModel>;
  filtradas: Array<EvaluacionFamiliarModel>;
  k: number;
  verEvalpsicologica: boolean;
  progresos: { id: number; descripcion: string; }[];
  constructor(
    public _evalSrv: EvalFamiliaService,
  ) {
    if (sessionStorage.getItem('evaluacion')) {
      this.evaluacion = JSON.parse(sessionStorage.getItem('evaluacion'));
    } else {
      this.evaluacion = new EvaluacionFamiliarModel();
    }
  }

  ngOnInit() {
    this.verEvalpsicologica = false;
    this.imprimirpruebafamiliar(this.idficha);
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));
    if (autorization === 10 || autorization === 17 || autorization === 5 || autorization === 4) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }

    // this.today = new Date().toISOString().substr(0, 10);
    // this.evaluacion.fevaluacion = this.today;
    this.personas = [
      { id: 9, descripcion: 'Paciente' },
      { id: 10, descripcion: 'Padre' },
      { id: 11, descripcion: 'Madre' },
      { id: 12, descripcion: 'Hermano' },
      { id: 13, descripcion: 'Hermana' },
      { id: 14, descripcion: 'Apoderado' },
      { id: 15, descripcion: 'Otros' }
    ];
    this.progresos = [
      { id: 16, descripcion: 'Logrado' },
      { id: 17, descripcion: 'En proceso' },
      { id: 18, descripcion: 'No logrado' }
    ];

  }
  setNewPersona(persona) {
    this.evaluacion.tipo_familia = persona;
    this.today = new Date().toISOString().substr(0, 10);
    this.evaluacion.fevaluacion = this.today;
  }
  setNewprogresos(progreso) {
    this.evaluacion.progreso = progreso;
  }
  imprimirpruebafamiliar(paciente) {
    this._evalSrv.getEvalPsicologicaFamiliarxExpediente(paciente).subscribe(res => {
      if (res[0] === undefined) {
        this.evaluacion = new EvaluacionFamiliarModel();
      } else {
        this.evaluacionesfintadas = this.evaluaciones = this.filtradas = res;
      }
    });
  }
  editarEvaluation(pruebaeditar) {
    this.verEvalpsicologica = true;
    this.evaluacion = pruebaeditar;
  }
  canceleval() {
    this.evaluacion = new EvaluacionFamiliarModel();

  }
  saveOrUpdateEvaluacionPsicologica(): void {
    this.evaluacion.idfichapsicologia = this.idficha;
    if (this.isValid) {
      this._evalSrv.saveOrUpdateEvalFamiliaPsicologica(this.evaluacion).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Guardado!', 'success').then(() => {
            this.imprimirpruebafamiliar(this.idficha);
            this.evaluacion = new EvaluacionFamiliarModel();
          });
        } else {
          // this.isValid = false;
          swal('Mal!', 'Los campos con * son obligatorios!', 'warning');
        }
      });
    } else {
      console.log('error');

    }
    sessionStorage.clear();
  }
  selectordenar(personaorden) {
    console.log(personaorden);
    if (personaorden === 'todos') {
      this.evaluacionesfintadas = this.evaluaciones;
    } else {
      this.filtradas = this.evaluaciones.filter((evaluacion: any) => {
        return evaluacion.tipo_familia === personaorden;
      });

      this.evaluacionesfintadas = [...this.filtradas];
    }
  }
}
