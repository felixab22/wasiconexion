/* cSpell:disable */
import { NgModule, NO_ERRORS_SCHEMA, LOCALE_ID } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentsComponent } from './components.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TabModule } from 'angular-tabs-component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { COMPONENTS_ROUTES } from './components.routes';

import { ServiceModule } from '../services/servicio.modele';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ListauserComponent } from './configuracion/listauser.component';
import { UserFormComponent } from './configuracion/user-form.component';
import { RolPersonalComponent } from './configuracion/rol-personal.component';
import { AreaListarComponent } from './configuracion/area-listar/area-listar.component';

import { IncripcionComponent } from './social/incripcion.component';
import { EvalSocioEconomicaComponent } from './social/eval-socio-economica.component';
import { ListaAptosComponent } from './social/lista-aptos.component';
import { AbrirExpedienteComponent } from './social/abrir-expediente.component';


import { ListaMedicamentosComponent } from './salud/lista-medicamentos.component';
import { MedicamentosFormsComponent } from './salud/medicamentos-forms.component';
import { ExpedienteComponent } from './social/expediente-list.component';
import { ExpedienteFormComponent } from './social/expediente-form.component';
import { ApoderadoComponent } from './social/apoderado.component';
import { EvarFisicaFormComponent } from './rehabilitacion/evar-fisica-form.component';
import { DiagnosticoComponent } from './rehabilitacion/diagnostico.component';
import { AnamnecisComponent } from './rehabilitacion/anamnecis.component';
import { InscripcionFormComponent } from './social/inscripcion-form.component';
import { MovArticulacionesComponent } from './rehabilitacion/mov-articulaciones.component';
import { InspeccionComponent } from './rehabilitacion/inspeccion/inspeccion.component';
import { BiometriaComponent } from './rehabilitacion/biometria/biometria.component';
import { FuerzaComponent } from './rehabilitacion/fuerza/fuerza.component';
import { EspecificaComponent } from './rehabilitacion/especifica/especifica.component';
import { ActividadesComponent } from './rehabilitacion/actividades/actividades.component';
import { PiernaComponent } from './rehabilitacion/pierna/pierna.component';
import { FinalComponent } from './rehabilitacion/final/final.component';
import { PsicologiaComponent } from './psicologia/psicologia.component';
import { ListapComponent } from './rehabilitacion/listap/listap.component';
import { ReferenciapComponent } from './rehabilitacion/referenciap/referenciap.component';
import { ReferenciadosComponent } from './psicologia/referenciados/referenciados.component';
import { ReferenciasaludComponent } from './salud/referenciasalud/referenciasalud.component';
import { SaludComponent } from './salud/salud.component';
import { AtoComponent } from './ato/ato.component';
import { ListaaComponent } from './ato/listaa/listaa.component';
import { EducacionComponent } from './educacion/educacion.component';
import { ListaeComponent } from './educacion/listae/listae.component';
import { EvaluacionComponent } from './educacion/evaluacion/evaluacion.component';
import { CentrodiaComponent } from './centrodia/centrodia.component';
import { ListadiaComponent } from './centrodia/listadia/listadia.component';
import { MaterialesComponent } from './rehabilitacion/materiales/materiales.component';
import { ListamatComponent } from './rehabilitacion/listamat/listamat.component';
import { TrabajoPsicologiaComponent } from './psicologia/trabajo-psicologia/trabajo-psicologia.component';
import { BienvenidoComponent } from './configuracion/bienvenido/bienvenido.component';
import { HorarioreahComponent } from './configuracion/horarioreah/horarioreah.component';
import { Estadistica1Component } from './configuracion/estadistica1.component';
import { HistorialPacienteComponent } from './configuracion/historial-paciente.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { IniciarAgendaComponent } from './configuracion/iniciar-agenda/iniciar-agenda.component';
import { FileUploadModule } from 'ng2-file-upload';
import { GraficasComponent } from './configuracion/graficas/graficas.component';
import { CampaniasComponent } from './salud/companias/campanias.component';
import { GuionPipe } from '@pipes/Guion.pipe';
import { VisitaComponent } from './configuracion/visitas/visita.component';
import { CorreoComponent } from './configuracion/correo.component';
import { PerfilComponent } from './configuracion/perfil/perfil.component';
import { ImagenPipe } from '@pipes/Imagen.pipe';
import { SidebarModule } from 'ng-sidebar';


@NgModule({
  imports: [
    FileUploadModule,
    RouterModule,
    CommonModule,
    MDBBootstrapModule.forRoot(),
    SidebarModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    BrowserAnimationsModule,
    ServiceModule,
    SharedModule,
    TabModule,
    // COMPONENTS_ROUTES    
  ],
  exports: [
    BienvenidoComponent,


  ],
  declarations: [
    PerfilComponent,
    VisitaComponent,
    CorreoComponent,
    GuionPipe,
    ImagenPipe,
    ComponentsComponent,
    CampaniasComponent,
    IncripcionComponent,
    ListauserComponent,
    UserFormComponent,
    EvalSocioEconomicaComponent,
    HorarioreahComponent,
    Estadistica1Component,
    HistorialPacienteComponent,
    ListaAptosComponent,
    AbrirExpedienteComponent,  
    ListaMedicamentosComponent,
    RolPersonalComponent,
    AreaListarComponent,
    MedicamentosFormsComponent,
    ExpedienteComponent,
    ExpedienteFormComponent,
    ApoderadoComponent,   
    EvarFisicaFormComponent,
    DiagnosticoComponent,
    AnamnecisComponent,
    InscripcionFormComponent,
    MovArticulacionesComponent,
    InspeccionComponent,
    BiometriaComponent,
    FuerzaComponent,
    EspecificaComponent,
    ActividadesComponent,
    PiernaComponent,
    FinalComponent,
    PsicologiaComponent,
    ListapComponent,
    ReferenciapComponent,
    ReferenciadosComponent,
    ReferenciasaludComponent,
    SaludComponent,
    AtoComponent,
    ListaaComponent,
    EducacionComponent,
    ListaeComponent,
    EvaluacionComponent,
    CentrodiaComponent,
    ListadiaComponent,
    MaterialesComponent,
    ListamatComponent,
    TrabajoPsicologiaComponent,
    BienvenidoComponent,
    IniciarAgendaComponent,
    GraficasComponent
  ],
  schemas: [NO_ERRORS_SCHEMA],
  // providers: [    
  //   {
  //   provide:  LOCALE_ID, useValue:"es"
  //   }

  // ]
})
export class ComponentsModule { }
