/* cSpell:disable */
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OK } from '@models/configuration/httpstatus';
import { ExpedienteService, RefInternaService, ATOService } from '@services/servicio.index';
import { ATOModel, ExpedienteModel } from '@models/model.index';
declare var swal: any;
@Component({
  selector: 'app-ato',
  templateUrl: './ato.component.html',
  styles: [],
})
export class AtoComponent implements OnInit {
  verAtender = false;
  idExpediente: any;
  verpaciente: boolean;
  evaluato: ATOModel;
  public expedientes: Array<ExpedienteModel>;
  public isValid: boolean = true;
  private message: string = '';
  date = new Date();
  anioactual: string;
  aniosretornos: ATOModel[];
  constructor(
    private expedienteSrv: ExpedienteService,
    private route: ActivatedRoute,
    private referenciaSrv: RefInternaService,
    private atoSrv: ATOService,
  ) {
    this.anioactual = String(this.date).substr(11, 4);
    if (sessionStorage.getItem('evalpsicologico')) {
      this.evaluato = JSON.parse(sessionStorage.getItem('evalpsicologico'));
    } else {
      this.evaluato = new ATOModel();
    }
  }

  ngOnInit() {
    const dato = this.route.snapshot.paramMap.get('historial');
    // console.log(dato);
    this.verpaciente = false;
    this.imprimiExpedientexHistorial(dato);
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));
    if (autorization === 11 || autorization === 15 || autorization === 5 || autorization === 4) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }
  }
  imprimiExpedientexHistorial(histoiral) {
    this.expedienteSrv.getBusquedaxHistorial(histoiral).subscribe(
      (data: any[]) => {
        // console.log(data);
        this.expedientes = data[0];
        this.idExpediente = data[0].id_expediente;
        this.verpaciente = true;
        this.imprimirEvaluacionAto(this.idExpediente, this.anioactual);
        this.imprimirReferencia(this.idExpediente);
        this.atoSrv.getaniosATO(this.idExpediente).subscribe(res => {
          this.aniosretornos = res;
        });
      });
  }
  imprimirReferencia(dato) {
    this.referenciaSrv.getReferenciaxExpediente(dato).subscribe(res => {
      if (res !== undefined) {
        this.evaluato.idreferenciainterna = res[0].idreferinterna;
      } else {
        this.evaluato.idreferenciainterna = 0;
      }
    });
  }
  imprimirEvaluacionAto(dato, anio) {
    this.atoSrv.getATOxExpediente(dato, anio).subscribe(res => {
      if (res[0] === undefined) {
        this.evaluato = new ATOModel();
      } else {
        this.evaluato = res[0];
      }
    });
  }
  setNewaniosselect(valor) {
    const anio = valor.substr(0, 4);
    if (this.anioactual !== anio) {
      this.verAtender = false;
    } else {
      this.verAtender = true;
    }
    this.imprimirEvaluacionAto(this.idExpediente, anio);
  }
  getaniosATO() {

  }
  saveOrUpdateEvaluacionAto(): void {
    this.evaluato.idexpediente = this.idExpediente;
    if (this.isValid) {
      this.atoSrv.saveOrUpdateAto(this.evaluato).subscribe(res => {
        if (res.responseCode === OK) {
          swal('Bien!', 'Guardado!', 'success').then(() => {
            this.imprimirEvaluacionAto(this.idExpediente, this.anioactual);
          });
        } else {
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
}
