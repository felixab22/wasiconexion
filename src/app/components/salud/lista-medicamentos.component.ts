/* cSpell:disable */
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { MedicamentosModel } from '@models/model.index';
import { MedicamentoService } from '@services/servicio.index';
declare var swal: any;
@Component({
  selector: 'app-lista-medicamentos',
  templateUrl: './lista-medicamentos.component.html',
  styles: []
})
export class ListaMedicamentosComponent implements OnInit {
  public medicamentos: Array<MedicamentosModel>;
  public p = 5;
  public term: any;
  public binding: any;

  constructor(
    private medicamentoService: MedicamentoService,
    private router: Router,
  ) {
  }

  ngOnInit() {
    this.loadMedicamento();
  }
  private loadMedicamento(): void {
    this.medicamentoService.getMedicamento().subscribe(res => {
      if (res[0] !== undefined) {
        this.medicamentos = res;
      }
    });
  }
  public editMedicamento(medicamento: MedicamentosModel): void {
    sessionStorage.setItem('medicamento', JSON.stringify(medicamento));
    this.router.navigate(['wasi/Crearmedicamento']);
  }

  public deleteMedicamento(medicamento: MedicamentosModel): void {
    if (confirm('¿Está seguro que desea eliminar?')) {
      this.medicamentoService.deleteMedicamento(medicamento);
      this.medicamentos = this.medicamentos.filter(u => medicamento.nombremedic !== u.nombremedic);
      swal('Eliminado!', 'Medicamento eliminado!', 'info');
    }

  }
  CrearMedicamento() {
    this.router.navigate(['wasi/Crearmedicamento']);
  }

  createPDF() {
    var sTable = document.getElementById('idlistamedicamentos').innerHTML;
    var style = "<style>";
    style = style + "table {width: 100%;font: 11px Calibri;}";
    style = style + "table, th, td {border: solid 1px #DDD; border-collapse: collapse;}";
    style = style + "th {color: blue;}";
    style = style + "td {color: black;}";
    style = style + ".none {display: none;}";
    style = style + "padding: 2px 3px;text-align: center;}";
    style = style + "</style>";
    // CREATE A WINDOW OBJECT.
    var win = window.open('', '', 'height=700,width=700');
    win.document.write('<html><head>');
    win.document.write('<title>LISTA DE MEDICAMENTOS</title>');   // <title> FOR PDF HEADER.
    win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');
    win.document.close(); 	// CLOSE THE CURRENT WINDOW.
    win.print();    // PRINT THE CONTENTS.
  }

  exportTableToExcel(tableID, filename = '') {
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

    // Specify file name
    filename = filename ? filename + '.xls' : 'excel_data.xls';

    // Create download link element
    downloadLink = document.createElement("a");

    document.body.appendChild(downloadLink);

    if (navigator.msSaveOrOpenBlob) {
      var blob = new Blob(['\ufeff', tableHTML], {
        type: dataType
      });
      navigator.msSaveOrOpenBlob(blob, filename);
    } else {
      // Create a link to the file
      downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

      // Setting the file name
      downloadLink.download = filename;

      //triggering the function
      downloadLink.click();
    }
  }

}
