/* cSpell:disable */
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OK } from '../../models/configuration/httpstatus';
import { FileUploader } from 'ng2-file-upload';
import { URL_SERVICIOS } from '../../config/config';
import {
  DiganosticoMedicoService,
  SaludService,
  ExpedienteService,
  ApoderadoService,
  RefInternaService,
  SocioEconomicaService,
  EvaluacionFisicaService,
  CuidadoAlimenService,
  VisitaIntegralService,
  FilesService
} from '@services/servicio.index';
import {
  CuidadoAlimenModel,
  EvaluacionFisicaModel,
  ApoderadoModel,
  DiganosticoMedicoModel,
  ExpedienteModel,
  SaludModel,
  SocioEconomicaModel,
  VisitaIntegralModel
} from '@models/model.index';
declare var swal: any;
const URL = URL_SERVICIOS + '/upload';

@Component({
  selector: 'app-salud',
  templateUrl: './salud.component.html',
  styleUrls: ['./salud.component.scss']
})
export class SaludComponent implements OnInit {

  documentnames: Array<any>;
  public uploader: FileUploader = new FileUploader({ url: URL });
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;

  sumauno: any;
  versisita: boolean;
  verbotonsegcero: boolean;
  verbotonsegdos: boolean;
  verbotonseguno: boolean;
  versegimientodos: boolean;
  versegimientouno: boolean;
  idsalud: number;
  idalimentacion: number;
  public isValid: boolean = true;
  private message: string = '';
  alimentacion: CuidadoAlimenModel;
  idterapiafisica: number;
  evaluacionfisica: EvaluacionFisicaModel;
  madre: ApoderadoModel;
  diagnosticomedic: DiganosticoMedicoModel;
  MadreExiste: boolean;
  padre: ApoderadoModel;
  PadreExiste: boolean;
  verpaciente: boolean;
  idExpediente: any;
  expedientes: ExpedienteModel;
  evalsalud: SaludModel;
  socioecos: SocioEconomicaModel;
  visita1: VisitaIntegralModel;
  visita2: VisitaIntegralModel;
  visita3: VisitaIntegralModel;
  verAtender = false;
  codigoHistorial: string;
  constructor(
    private servicioDiagnostico: DiganosticoMedicoService,
    private saludSrv: SaludService,
    private expedienteSrv: ExpedienteService,
    private apoderadoSrv: ApoderadoService,
    private socioSrv: SocioEconomicaService,
    private SrvEvalFisica: EvaluacionFisicaService,
    private AlimentSRV: CuidadoAlimenService,
    private visitaIntSrv: VisitaIntegralService,
    private route: ActivatedRoute,
    private _uploadFilesSrv: FilesService
  ) {
    if (sessionStorage.getItem('evalsalud')) {
      this.evalsalud = JSON.parse(sessionStorage.getItem('evalsalud'));
    } else {
      this.evalsalud = new SaludModel();
    }
    if (sessionStorage.getItem('socioecos')) {
      this.socioecos = JSON.parse(sessionStorage.getItem('socioecos'));
    } else {
      this.socioecos = new SocioEconomicaModel();
    }
    if (sessionStorage.getItem('alimentacion')) {
      this.alimentacion = JSON.parse(sessionStorage.getItem('alimentacion'));
    } else {
      this.alimentacion = new CuidadoAlimenModel();
    }
    if (sessionStorage.getItem('visita1')) {
      this.visita1 = JSON.parse(sessionStorage.getItem('visita1'));
    } else {
      this.visita1 = new VisitaIntegralModel();
    }
    if (sessionStorage.getItem('visita2')) {
      this.visita2 = JSON.parse(sessionStorage.getItem('visita2'));
    } else {
      this.visita2 = new VisitaIntegralModel();
    }
    if (sessionStorage.getItem('visita3')) {
      this.visita3 = JSON.parse(sessionStorage.getItem('visita3'));
    } else {
      this.visita3 = new VisitaIntegralModel();
    }
  }

  ngOnInit() {
    this.PadreExiste = false;
    this.MadreExiste = false;
    this.codigoHistorial = this.route.snapshot.paramMap.get('historial');
    this.imprimiExpedientexHistorial(this.codigoHistorial);

    const autorization = JSON.parse(localStorage.getItem('idautorizado'));
    if (autorization === 9 || autorization === 5 || autorization === 4) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }
    this.verpaciente = false;
    this.verbotonseguno = false;
    this.versegimientouno = false;
    this.versegimientodos = false;
    this.verbotonsegdos = false;
    this.verbotonsegcero = true;
  }
  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
    for (let i = 0; i < this.uploader.queue.length; i++) {
      this.uploader.queue[i].file.name = "salud-" + this.codigoHistorial + "-" + this.uploader.queue[i].file.name;
    }
  }
  descargar(nombreArchivo) {
    window.open(`${URL_SERVICIOS}/file/${nombreArchivo}?nombrearea=salud&codigo=${this.codigoHistorial}`);
  }
  imprimirFiles() {
    console.log(this.codigoHistorial);
    this._uploadFilesSrv.getListaFilesxAreayPaciente('salud', this.codigoHistorial).subscribe(res => {
      if (res.length === 0) {
        swal("error!", "No existen archivos!", "warning");
      } else {
        this.documentnames = res;
      }
    })
  }
  eliminararchivo(documento) {
    swal({
      title: "Esta seguro?",
      text: "Usted esta por eliminar el archivo!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          this._uploadFilesSrv.deleteFilesxcodigo(documento, 'salud', this.codigoHistorial).subscribe(res => {
            if (res) {
              this.imprimirFiles();
              swal("Horario eliminado", {
                icon: "success",
              });
            }
          });

        } else {
          swal("Sin acciones!");
        }
      });
  }

  PrimerSeguimientovisitauno() {
    this.versegimientouno = true;
    this.verbotonsegcero = false;
    this.verbotonseguno = true;
  }
  PrimerSeguimientovisitados() {
    this.versegimientouno = true;
    this.versegimientodos = true;
    this.verbotonsegcero = false;
    this.verbotonseguno = false;
    this.verbotonsegdos = true;
  }
  sumaruno() {
    this.visita1.puntajetotal = this.visita1.criteriouno + this.visita1.criteriodos + this.visita1.criteriotres + this.visita1.criteriocuatro;
  }
  sumardos() {
    this.visita2.puntajetotal = this.visita2.criteriouno + this.visita2.criteriodos + this.visita2.criteriotres + this.visita2.criteriocuatro;
  }
  sumartres() {
    this.visita3.puntajetotal = this.visita3.criteriouno + this.visita3.criteriodos + this.visita3.criteriotres + this.visita3.criteriocuatro;
  }
  imprimirVisitaIntegral(dato) {
    this.visitaIntSrv.getvisitaIntegral(dato).subscribe(res => {
      if (res[0] === undefined) {
        this.visita1 = new VisitaIntegralModel();
      } else {
        this.visita1 = res[0];
        if (res[1] === undefined) {
          this.visita2 = new VisitaIntegralModel();
        } else {
          this.visita2 = res[1];
          if (res[2] === undefined) {
            this.visita3 = new VisitaIntegralModel();
          } else {
            this.visita3 = res[2];
          }
        }
      }
    });
  }
  imprimiExpedientexHistorial(histoiral) {
    this.expedienteSrv.getBusquedaxHistorial(histoiral).subscribe(
      (data: any[]) => {
        this.expedientes = data[0];
        this.idExpediente = data[0].id_expediente;
        this.verpaciente = true;
        this.imprimirpadres(this.idExpediente);
        // this.imprimirReferencia(this.idExpediente);
        this.imprimirEvalSocioEconomica(this.idExpediente);
        this.imprimirFichaterapiaFisica();
        this.imprimirAlimentacion(this.idExpediente);
      });
  }

  imprimirpadres(dato) {
    this.apoderadoSrv.getApoderadoxExpediente(dato).subscribe(res => {
      if (res !== undefined) {
        if (res[0].tipo_apoderado === 'Padre') {
          this.PadreExiste = true;
          this.padre = res[0];
          if (res[1] !== undefined) {
            this.MadreExiste = true;
            this.madre = res[1];
          }
        } else {
          this.madre = res[0];
          this.MadreExiste = true;
          if (res[1] !== undefined) {
            this.PadreExiste = true;
            this.padre = res[1];
          }
        }
      }
    });
  }
  imprimirFichaterapiaFisica() {
    this.SrvEvalFisica.getEvaluacionFisicaporexpediente(this.idExpediente).subscribe(res => {
      if (res[0] !== undefined) {
        this.evaluacionfisica = res[0];
        this.evaluacionfisica.fregistro = res[0].fregistro;
        this.idterapiafisica = this.evaluacionfisica.idfichaterapiafisica;
        this.imprimirDiagnosticoMedico();
      } else {
        this.evaluacionfisica = new EvaluacionFisicaModel();
      }

    });
  }
  imprimirAlimentacion(dato) {
    this.AlimentSRV.getCuidadoAlimenxExpediente(dato).subscribe(res => {
      if (res[0] === undefined) {
        this.alimentacion = new CuidadoAlimenModel();
        this.inicialsaveOrUpdateAlimentacion();
      } else {
        this.alimentacion = res[0];
        this.idalimentacion = res[0].idcuidalim;
        this.imprimirEvaluacionSalud(this.alimentacion.idcuidalim);
      }
    });
  }
  imprimirEvaluacionSalud(dato) {
    this.saludSrv.getSaludxAlimentacion(dato).subscribe(res => {
      if (res[0] === undefined) {
        this.evalsalud = new SaludModel();
        this.versisita = false;
      } else {
        this.evalsalud = res[0];
        this.idsalud = res[0].idfichasalud;
        this.versisita = true;
        this.imprimirVisitaIntegral(this.idsalud);
      }
    });
  }
  imprimirDiagnosticoMedico() {
    this.servicioDiagnostico.getDiganosticoMedicoxfichaterapia(this.idterapiafisica).subscribe(res => {
      if (res[0] === undefined) {
        this.diagnosticomedic = new DiganosticoMedicoModel();
      } else {
        this.diagnosticomedic = res[0];
      }
    });
  }
  imprimirEvalSocioEconomica(dato) {
    this.socioSrv.getSocioEconxexpediente(dato).subscribe(res => {
      if (res[0] !== undefined) {
        this.socioecos = res[0];
      }
    });
  }
  inicialsaveOrUpdateAlimentacion(): void {
    this.alimentacion.idexpediente = this.idExpediente;
    if (this.isValid) {
      this.AlimentSRV.saveOrUpdateCuidadoAlimen(this.alimentacion).subscribe(res => {
        if (res.responseCode === OK) {
          window.location.reload();
        } else {
          this.message = res.message;
          // this.isValid = false;
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  saveOrUpdateAlimentacion(): void {
    this.alimentacion.idexpediente = this.idExpediente;
    if (this.isValid) {
      this.AlimentSRV.saveOrUpdateCuidadoAlimen(this.alimentacion).subscribe(res => {
        if (res.responseCode === OK) {
          swal("Bien!", "Guardado!", "success");
        } else {
          this.message = res.message;
          // this.isValid = false;
          swal('Mal!', 'Los campos con * son obligatorios', 'warning');
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  saveOrUpdateSalud(): void {
    this.evalsalud.idcuidalim = this.idalimentacion;
    if (this.isValid) {
      this.saludSrv.saveOrUpdateEvalSalud(this.evalsalud).subscribe(res => {
        if (res.responseCode === OK) {
          swal("Bien!", "Guardado!", "success");
        } else {
          this.message = res.message;
          // this.isValid = false;
          swal('Mal!', 'Los campos con * son obligatorios', 'warning');
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  saveOrUpdateVisita1(): void {
    this.visita1.idfichasalud = this.idsalud;
    this.visita1.numseguimiento = 0;
    if (this.isValid) {
      this.visitaIntSrv.saveOrUpdatevisitaIntegral(this.visita1).subscribe(res => {
        if (res.responseCode === OK) {
          swal("Bien!", "Guardado!", "success");
        } else {
          this.message = res.message;
          // this.isValid = false;
          swal('Mal!', 'Los campos con * son obligatorios', 'warning');
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  saveOrUpdateVisita2(): void {
    this.visita2.idfichasalud = this.idsalud;
    this.visita2.numseguimiento = 1;
    if (this.isValid) {
      this.visitaIntSrv.saveOrUpdatevisitaIntegral(this.visita2).subscribe(res => {
        if (res.responseCode === OK) {
          swal("Bien!", "Guardado!", "success");
        } else {
          this.message = res.message;
          // this.isValid = false;
          swal('Mal!', 'Los campos con * son obligatorios', 'warning');
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  saveOrUpdateVisita3(): void {
    this.visita3.idfichasalud = this.idsalud;
    this.visita3.numseguimiento = 2;
    if (this.isValid) {
      this.visitaIntSrv.saveOrUpdatevisitaIntegral(this.visita3).subscribe(res => {
        if (res.responseCode === OK) {
          swal("Bien!", "Guardado!", "success");
        } else {
          this.message = res.message;
          // this.isValid = false;
          swal('Mal!', 'Los campos con * son obligatorios', 'warning');
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
}
