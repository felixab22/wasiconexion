/* cSpell:disable */
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { OK } from '../../models/configuration/httpstatus';
import { Router } from '@angular/router';
import { MedicamentoService } from '@services/servicio.index';
import { MedicamentosModel } from '@models/model.index';

declare var swal: any;
@Component({
  selector: 'app-medicamentos-forms',
  templateUrl: './medicamentos-forms.component.html',
  styles: []
})
export class MedicamentosFormsComponent implements OnInit {
  verAtender = false;
  recibido: any;
  public medicamento: MedicamentosModel;
  // tslint:disable-next-line:no-inferrable-types
  public isValid: boolean = true;
  // tslint:disable-next-line:no-inferrable-types
  public message: string = '';
  public comprados = [];

  constructor(
    private medicamentoSer: MedicamentoService,
    private router: Router,
    
  ) {
    if (sessionStorage.getItem('medicamento')) {
      this.medicamento = JSON.parse(sessionStorage.getItem('medicamento'));
      this.recibido = this.medicamento.comprado;
    } else {
      this.medicamento = new MedicamentosModel();
    }
  }
  ngOnInit() {
    const autorization = JSON.parse(localStorage.getItem('idautorizado'));       
    if(autorization === 9 || autorization === 5  || autorization === 4 ) {
      this.verAtender = true;
    } else {
      this.verAtender = false;
    }

    this.comprados = [
      { id: 1, descripcion: 'Wasi Esperanza' },
      { id: 2, descripcion: 'Comprado' }
    ];
  }
  Cancelar() {
    this.router.navigate(['wasi/Listamedicamento']);
    sessionStorage.clear();
  }
  public saveOrUpdate(): void {
    if (this.isValid) {
      this.medicamentoSer.saveOrUpdate(this.medicamento).subscribe(res => {
        if (res.responseCode === OK) {
          swal("Bien!", "Guardado!", "success").then(() => {
          this.router.navigate(['wasi/Listamedicamento']);
          }); 
        } else {
          this.message = res.message;
          // this.isValid = false;          
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  onSelectionChange(entry) {
    this.medicamento.comprado = entry.descripcion;
  }

}
