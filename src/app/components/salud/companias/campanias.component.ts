import { Component, OnInit } from '@angular/core';
import { CampaniasModel, AtencionCampanModel } from '@models/model.index';
import { CampaniasService, AtencionCampanService } from '@services/servicio.index';
declare var swal: any;

@Component({
  selector: 'app-campanias',
  templateUrl: './campanias.component.html',
  styles: []
})
export class CampaniasComponent implements OnInit {
  listacamanias: Array<CampaniasModel>
  public isValid: boolean = true;
  public message: string = '';
  capampanis: CampaniasModel;
  fechacampania: string;
  fechahoy : any;
  today: string;
  idcapania: number;
  atention : AtencionCampanModel;
  atenciones: Array<AtencionCampanModel>;
  infoatention : AtencionCampanModel;
  lookcampania: boolean;
  veratention: boolean;
  verTablaAtention: boolean;
  constructor(
      private _campaniasSrv: CampaniasService,
      private _atentionSrv: AtencionCampanService,
  ) {
      this.capampanis = new CampaniasModel();
      this.atention = new AtencionCampanModel();
      this.infoatention = new AtencionCampanModel();
      this.today = new Date().toISOString().substr(0, 10); 
      this.fechahoy = this.today;
      this.fechacampania =  this.today;
      this.idcapania = null;
     }

  ngOnInit() {
    this.verTablaAtention = false;
    this.veratention = false;  
    this.lookcampania = false;
    this.imprimirCampanias();
  }
  Cancelar() {
    this.capampanis = new CampaniasModel();
  }
imprimirCampanias() {
  this._campaniasSrv.getCampaniaLista().subscribe(res => {
    this.listacamanias = res;             
});
}

  public saveOrUpdateCampana(): void {
  this.capampanis.fcampania = String(this.fechacampania);
    if (this.isValid) {
      this._campaniasSrv.saveOrUpdateCampania(this.capampanis).subscribe(res => {
        if (res.responseCode === 200) {
          swal('Bien!', 'Campaña guardado!', 'success').then(() => {           
            this.capampanis = new CampaniasModel();
            this.lookcampania = false;
            this.imprimirCampanias();
          });
        } else {
          this.message = res.message;
          // this.isValid = false;          
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  public saveOrUpdateAtention(){
    if(this.idcapania === null) {
      swal("Mal!", "Selecciona una campaña!", "warning");
    }   else {
      this.atention.campaniaatencion = this.idcapania;
      // this.atention.personal = 32;
      this.atention.fregistro = this.fechahoy;
      if (this.isValid) {
        this._atentionSrv.saveOrUpdateAtention(this.atention).subscribe(res => {
          if (res.responseCode === 200) {
            swal('Bien!', 'Guardado!', 'success').then(() => {             
              this.atention = new AtencionCampanModel();
              this.listarParticipantes(this.idcapania); 
            });
        } else {
          this.message = res.message;
          // this.isValid = false;          
        }
      });
    } else {
      this.message = 'Los campos con * son obligatorios';
    }
    sessionStorage.clear();
  }
  }

  setNewDistrito(valor){    
    this.idcapania = parseInt(valor);
    this.listarParticipantes(valor);
    this.veratention = true;    
  }
  listarParticipantes(data){
    this._atentionSrv.getAtencionxCampania(data).subscribe(res => {
      if(res[0] === Array[0]) {
        console.log('no existe lista');
        this.verTablaAtention = false;        
      } else {
        this.atenciones = res;
        this.verTablaAtention = true;
      }
    })
  }
  editar(paciente: AtencionCampanModel) {    
    console.log(paciente);    
    this.atention = paciente ;
    this.atention.campaniaatencion =  this.idcapania;
  } 
  informacion(paciente: AtencionCampanModel) {
        this.infoatention = paciente;
        }
  cancelar() {
    this.atention = new AtencionCampanModel();
  }
  editecampania(item: CampaniasModel) {   
    this.lookcampania = true;
    this.capampanis = item;
  }
  CreandoCampania() {
    this.lookcampania = true;
    this.capampanis = new CampaniasModel();
  }
  Cancelarcampa() {
    this.lookcampania = false;
    this.capampanis = new CampaniasModel();
  } 
  
}
