// cSpell:disable
import { BrowserModule } from '@angular/platform-browser';
import { APP_ROUTES } from './app.routes';
import { AppComponent } from './app.component';
import { NgModule, NO_ERRORS_SCHEMA, LOCALE_ID } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ServiceModule } from './services/servicio.modele';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';
import { TabModule } from 'angular-tabs-component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TokenStorage } from './core/token.storage';
import { AuthService } from './core/auth.service';
import { Interceptor } from './core/app.interceptor';
import { AuthGuard } from './core/auth.guard';
import { FileUploadModule } from 'ng2-file-upload';
import { ComponentsModule } from './components/components.module';
import { SharedModule } from './shared/shared.module';
import { LoginComponent } from './shared/login/login.component';
import { SidebarModule } from 'ng-sidebar';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    // UserComponent,      
  ],
  imports: [
    FileUploadModule,
    SharedModule,
    ComponentsModule,
    ServiceModule,
    BrowserModule,
    APP_ROUTES,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    BrowserAnimationsModule,
    TabModule,    
    MDBBootstrapModule.forRoot(),
    SidebarModule.forRoot()

  ],
  providers: [
    AuthService,
    AuthGuard,
    TokenStorage,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
    }
  ],
  schemas: [NO_ERRORS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule { }
