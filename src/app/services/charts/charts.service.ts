import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { URL_SERVICIOS } from '../../config/config';

@Injectable()
export class GraficasService {
  api = URL_SERVICIOS;


  public menus: any[] = [
    {
      id: 1,
      area: 'Social',
      tipo: 'Por tipo de familia'
    },
    {
      id: 2,
      area: 'Social',
      tipo: 'Por ubicación de la vivienda'
    },
    {
      id: 3,
      area: 'Social',
      tipo: 'Por tenencia de la vivienda'
    },
    {
      id: 4,
      area: 'Social',
      tipo: 'Por tipo de seguro de salud'
    },
    {
      id: 5,
      area: 'Social',
      tipo: 'Por nivel de educación alcanzado'
    },
    {
      id: 6,
      area: 'Social',
      tipo: 'Por tipo de instutición al que asiste'
    },
    {
      id: 7,
      area: 'Social',
      tipo: 'Por necesidad de certificado de discapacidad'
    },
    {
      id: 8,
      area: 'Social',
      tipo: 'Por diagnostico médico'
    },
    {
      id: 9,
      area: 'Social',
      tipo: 'Por Edad'
    },
    {
      id: 10,
      area: 'Social',
      tipo: 'Por sexo'
    }
  ];


  constructor(private http: HttpClient) { }

  public getgraficoTipoFamilia(): Observable<any[]> {

    return this.http.get<any[]>(this.api + '/reporteByTipoFamilia', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getgraficoUbicVivienda(): Observable<any[]> {

    return this.http.get<any[]>(this.api + '/reporteByUbicacionVivienda', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getgraficoTenenciaVivienda(): Observable<any[]> {

    return this.http.get<any[]>(this.api + '/reporteByTenenciaVivienda', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }

  public getgraficoTipoSeguro(): Observable<any[]> {
    return this.http.get<any[]>(this.api + '/reporteByTipoSeguro', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }

  public getgraficoNivelEducatito(): Observable<any[]> {
    return this.http.get<any[]>(this.api + '/reporteByNivelEducativo', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }

  public getgraficoInsAsiste(): Observable<any[]> {
    return this.http.get<any[]>(this.api + '/reporteByTipoInstEducativa', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }

  public getgraficoNecesiCertificado(): Observable<any[]> {
    return this.http.get<any[]>(this.api + '/reporteByNecesidadCertificadoDiscapacidad', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }

  public getgraficxDiagnostico(): Observable<any[]> {
    return this.http.get<any[]>(this.api + '/reporteByDiagnostico', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }

  public getgraficxEdad(): Observable<any[]> {
    return this.http.get<any[]>(this.api + '/reporteByEdadPaciente', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getgraficaxSexo(): Observable<any[]> {
    return this.http.get<any[]>(this.api + '/reporteBySexoPaciente', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }

}
