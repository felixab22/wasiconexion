import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { URL_SERVICIOS } from '../../config/config';
import { AtentionModel, InasistenciasModel } from '@models/model.index';
import { HorarioModel } from '@models/configuration/agenda.model';

@Injectable()
export class HorarioService {
  // tslint:disable-next-line:no-inferrable-types
  api = URL_SERVICIOS;
  headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

  constructor(private http: HttpClient) { }


  public getHorarioxidpersonal(personal): Observable<HorarioModel[]> {
    return this.http.get<HorarioModel[]>(this.api + `/getAgendaPersonal?idpersonal=${personal}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteAgendaxPersonal(idpersonal: number): void {
    this.http.post(this.api + `/deleteAgendaByPersonal?idpersonal=${idpersonal}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
   //    * Metodo que valida campos obligatorios
   //    * @param horario
   //    */
  public saveOrUpdateHorario(horario: HorarioModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateCitaTerapia', JSON.stringify(horario), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}
@Injectable()
export class AtentionService {
  api = URL_SERVICIOS;
  headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

  constructor(private http: HttpClient) { }

  public getAsistenciaxPersona(paciente): Observable<AtentionModel[]> {
    return this.http.get<AtentionModel[]>(this.api + `/getAsistenciasByPacientes?paciente=${paciente}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }

  /**
   //    * Metodo que valida campos obligatorios
   //    * @atetion horario
   //    */
  public saveOrUpdateAtention(atetion: AtentionModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateAsistencia', JSON.stringify(atetion), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}
@Injectable()
export class InasistenciasService {
  api = URL_SERVICIOS;
  headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

  constructor(private http: HttpClient) { }

  public getInasistencia(paciente): Observable<InasistenciasModel[]> {
    return this.http.get<InasistenciasModel[]>(this.api + `/getAsistenciasByPacientes?paciente=${paciente}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }

  public deleteInasistencia(dato: string): void {
    this.http.post(this.api + '/deleteNotificacion', JSON.stringify(dato), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
   //    * Metodo que valida campos obligatorios
   //    * @inasistencia horario
   //    */
  public saveOrUpdateInasistencia(inasistencia: InasistenciasModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateNotificacion', JSON.stringify(inasistencia), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}