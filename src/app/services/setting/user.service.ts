import { PersonalModel } from '../../models/configuration/personal.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { URL_SERVICIOS } from '../../config/config';


@Injectable()
export class UserService {
  api = URL_SERVICIOS;
  constructor(private http: HttpClient) { }

  public getUser(): Observable<PersonalModel[]> {
    return this.http.get<PersonalModel[]>(this.api + '/getPersonal', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getPersonalActivo(): Observable<PersonalModel[]> {
    return this.http.get<PersonalModel[]>(this.api + '/listPersonalActivos', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getPersonalInactivos(): Observable<PersonalModel[]> {
    return this.http.get<PersonalModel[]>(this.api + '/listPersonalInactivos', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getPersonalRehabilitacion(): Observable<PersonalModel[]> {
    return this.http.get<PersonalModel[]>(this.api + '/getPersonalByAreaRehabilitacion', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteUser(user: PersonalModel): void {
    this.http.post(this.api + '/deletePersonal', JSON.stringify(user), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  /**
   * Metodo que valida campos obligatorios
   * @param user
   */

  public saveOrUpdate(user: PersonalModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdatePersonal', JSON.stringify(user), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }

  cambiarImagen(file: File, id: any) {

  }
}




