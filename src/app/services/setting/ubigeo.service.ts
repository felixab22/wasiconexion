import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { URL_SERVICIOS } from '../../config/config';


@Injectable()
export class UbigeoService {
  api = URL_SERVICIOS;
  constructor(private http: HttpClient) { }


  public getDepartamento(): Observable<any[]> {
    return this.http.get<any[]>(this.api + '/getDepartamentos', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getprovinciaxDepartamento(depa): Observable<any[]> {
    return this.http.get<any[]>(this.api + `/getProvinciaByDepartamentos?iddepa=${depa}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getDistritoxprovincia(provincia): Observable<any[]> {
    return this.http.get<any[]>(this.api + `/getDistritoByProvincias?idprov=${provincia}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}
