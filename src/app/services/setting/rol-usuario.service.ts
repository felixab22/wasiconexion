import { RolModel } from '../../models/configuration/rol.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { URL_SERVICIOS } from '../../config/config';

@Injectable()
export class RolUsuarioService {
  public roles: ModelRoles[] = [
    { idrol: 5, descripcionrol: "Administradora", real: "Administradora" },
    { idrol: 13, descripcionrol: "Generador_de_ingresos", real: "Generador de ingresos" },
    { idrol: 7, descripcionrol: "Jefe_centro_de_día", real: "Jefe centro de día" },
    { idrol: 8, descripcionrol: "Jefe_de_educación", real: "Jefe de educación" },
    { idrol: 10, descripcionrol: "Jefe_de_psicología", real: "Jefe de psicología" },
    { idrol: 12, descripcionrol: "Jefe_de_rehabilitación", real: "Jefe de rehabilitación" },
    { idrol: 9, descripcionrol: "Jefe_de_salud", real: "Jefe de salud" },
    { idrol: 6, descripcionrol: "Jefe_de_social", real: "Jefe de social" },
    { idrol: 11, descripcionrol: "Jefe_de_terapia_ocupacional", real: "Jefe de terapia ocupacional" },
    { idrol: 4, descripcionrol: "Sistema", real: "Sistema" },
    { idrol: 18, descripcionrol: "Voluntario_centro_de_día", real: "Voluntario centro de día" },
    { idrol: 19, descripcionrol: "Voluntario_comunicación", real: "Voluntario comunicación" },
    { idrol: 17, descripcionrol: "Voluntario_psicología", real: "Voluntario psicología" },
    { idrol: 16, descripcionrol: "Voluntario_rehabilitación", real: "Voluntario rehabilitación" },
    { idrol: 15, descripcionrol: "Voluntario_terapia_ocupacional", real: "Voluntario terapia ocupacional" }
  ]




  api = URL_SERVICIOS;
  headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
  constructor(private http: HttpClient) { }

  public getRol(): Observable<RolModel[]> {
    return this.http.get<RolModel[]>(this.api + '/getRol',
      { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleterol(id: number): void {
    this.http.post(this.api + '/deleteRol',
      { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  /**
   * Metodo que valida campos obligatorios
   * @param rol
   */

  public saveOrUpdate(rol: RolModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateRol',
      JSON.stringify(rol), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }

  public buscarroles(termino: string) {
    let rolArr: ModelRoles[] = [];
    for (let rol of this.roles) {
      if (rol.descripcionrol.indexOf(termino) >= 0) {
        rolArr.push(rol);
      }
    }
    return rolArr;
  }
}
export class ModelRoles {
  idrol: number;
  descripcionrol: string;
  real: string;

}

