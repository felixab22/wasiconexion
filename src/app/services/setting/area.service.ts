import { AreaModel } from '../../models/configuration/area.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { URL_SERVICIOS } from '../../config/config';

@Injectable()
export class AreaService {
  // tslint:disable-next-line:no-inferrable-types
  api = URL_SERVICIOS;

  constructor(
    private _http: HttpClient,
  ) {

  }

  public getArea(): Observable<AreaModel[]> {
    return this._http.get<AreaModel[]>(this.api + '/getArea', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getAreareferenciados(): Observable<AreaModel[]> {
    return this._http.get<AreaModel[]>(this.api + '/getAllAreasDeReferenciaInterna', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteArea(id: number): void {
    this._http.post(this.api + '/deleteArea', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
   * Metodo que valida campos obligatorios
   * @param area
   */
  public saveOrUpdate(area: AreaModel): Observable<any> {
    return this._http.post<any>(this.api + '/saveOrUpdateArea', JSON.stringify(area), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }

}
