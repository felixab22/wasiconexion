import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { RegisUsuarioModel } from '../../models/configuration/regis-usuario.modal';
import { URL_SERVICIOS } from '../../config/config';

@Injectable()
export class RegisUsuarioService {
  // tslint:disable-next-line:no-inferrable-types
  api = URL_SERVICIOS;
  constructor(private http: HttpClient) { }

  public getPersonaxIduser(id): Observable<any> {
    return this.http.get<any>(this.api + `/ObtenerPersonalPorSuUsuario?iduser=${id}`,
      { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getListaUser(): Observable<any[]> {
    return this.http.get<any[]>(this.api + '/api/listaUsers',
      { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }

  public deletePreinscripcion(id: number): void {
    this.http.post(this.api + '/deletePreinscripcion', JSON.stringify(id),
      { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
 * Metodo que valida campos obligatorios
 * @param userpass
 */
  public saveOrUpdateUsuario(userpass: RegisUsuarioModel): Observable<any> {
    return this.http.post<any>(this.api + '/api/auth/signup',
      JSON.stringify(userpass), { headers: { 'Content-Type': 'application/json' } });
  }
}
