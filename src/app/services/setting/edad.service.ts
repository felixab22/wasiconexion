import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { URL_SERVICIOS } from '../../config/config';

@Injectable()
export class EdadService {
  api = URL_SERVICIOS;
    today: string;
  edadmeses: number = 0;
  edad: number = 0;
  edadesymeses: { 'edad': number; 'meses': number; }[];
algo: any;
  constructor(
    private _http: HttpClient,
  ) {

  }

  calcularedad(fechaNacimiento: any) {
    console.log('servicio:', fechaNacimiento);    
    this.today = new Date().toISOString().substr(0, 10);
    const aperturaanio = String(this.today);
    const nacimientoanio = String(fechaNacimiento);
    const nacimiento_anio = parseInt(nacimientoanio.substr(0, 4));
    const apertura_anio = parseInt(aperturaanio.substr(0, 4));
    const nacimiento_mes = parseInt(nacimientoanio.substr(5, 2));
    const apertura_mes = parseInt(aperturaanio.substr(5, 2));
    const nacimiento_dia = parseInt(nacimientoanio.substr(8, 2));
    const apertura_dia = parseInt(aperturaanio.substr(8, 2));

    const d = apertura_dia - nacimiento_dia;
    const m = apertura_mes - nacimiento_mes;
    const a = apertura_anio - nacimiento_anio;
    this.edad = a;
    if ( m === 1) {
      this.edadmeses =  1;      
    } else  if (m === 0) {
      // this.m = this.m - 1;
      if (d < 0) {
        this.edad = a - 1;
        this.edadmeses = 12 - 1;
      } else if (d >= 0) {
        this.edadmeses = 0;
      }
    } else if (m < 0) {       
      this.edad = a - 1;
        this.edadmeses = 12 - Math.abs(m);
      } else {
        this.edadmeses = m;
      }    
       this.edadesymeses = [{'edad': this.edad, 'meses': this.edadmeses}];    
  return JSON.stringify(this.edadesymeses);
}
}
