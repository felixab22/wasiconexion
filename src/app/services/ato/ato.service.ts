import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ATOModel } from '../../models/ato/ato.model';
import { URL_SERVICIOS } from '../../config/config';

@Injectable()
export class ATOService {
  api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }

  public getATOxExpediente(idexpediente, anio): Observable<ATOModel[]> {
    return this.http.get<ATOModel[]>
      (this.api + `/getFichaAtoPaciente?idexpediente=${idexpediente}&anio=${anio}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getaniosATO(idexpediente): Observable<any[]> {
    return this.http.get<any[]>
      (this.api + `/ListarFichaAtoPorFechas?idexpediente=${idexpediente}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteATO(id: number): void {

    this.http.post(this.api + '/deleteFichaAtoPaciente', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
   //    * Metodo que valida campos obligatorios
   //    * @param  ato
   //    */
  public saveOrUpdateAto(ato: ATOModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateFichaAto', JSON.stringify(ato), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}
