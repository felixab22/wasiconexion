import { SaludModel, VisitaIntegralModel, CuidadoAlimenModel, VisitaMedicaModel, CampaniasModel, AtencionCampanModel } from '@models/salud/salud.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { URL_SERVICIOS } from '../../config/config';

@Injectable()
export class SaludService {
  public  api = URL_SERVICIOS;
  constructor(private http: HttpClient) { }
  public getSaludxAlimentacion(dato): Observable<SaludModel[]> {
    return this.http.get<SaludModel[]>
      (this.api + `/getFichaSaludPaciente?idcuidadoalim=${dato}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteSalud(id: number): void {
    this.http.post(this.api + '/deleteFichaSalud', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param  evalsalud
  //    */
  public saveOrUpdateEvalSalud(evalsalud: SaludModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateFichaSalud', JSON.stringify(evalsalud), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}
@Injectable()
export class VisitaIntegralService {
   api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }
  public getvisitaIntegral(dato): Observable<VisitaIntegralModel[]> {
    return this.http.get<VisitaIntegralModel[]>
      (this.api + `/getVisitaDomiciliariaIntegral?idfichasalud=${dato}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deletevisitaIntegral(id: number): void {
    this.http.post(this.api + '/deleteVisitaDomiciliariaIntegral', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param  vistasitegral
  //    */
  public saveOrUpdatevisitaIntegral(vistasitegral: VisitaIntegralModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateVisitaDomiciliariaIntegral', JSON.stringify(vistasitegral), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}
@Injectable()
export class CuidadoAlimenService {
   api = URL_SERVICIOS;


  constructor(private http: HttpClient) { }
  public getCuidadoAlimenxExpediente(dato): Observable<CuidadoAlimenModel[]> {
    return this.http.get<CuidadoAlimenModel[]>
      (this.api + `/getCuidadoAlimentacion?expediente=${dato}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteCuidadoAlimen(id: number): void {
    this.http.post(this.api + '/deleteCuidadoAlimentacion', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param  alimnetacion
  //    */
  public saveOrUpdateCuidadoAlimen(alimnetacion: CuidadoAlimenModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateCuidadoAlimentacion', JSON.stringify(alimnetacion), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}
@Injectable()
export class VisitaMedicaService {
   api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }
  
  public getCuidadoAlimen(dato): Observable<VisitaMedicaModel[]> {
    return this.http.get<VisitaMedicaModel[]>
      (this.api + `/getVisitaMedicaSalud?idfichasalud=${dato}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteCuidadoAlimen(id: number): void {
    this.http.post(this.api + '/deleteVisitaMedicaSalud', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param  alimnetacion
  //    */
  public saveOrUpdateCuidadoAlimen(alimnetacion: VisitaMedicaModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateVisitaMedicaSalud', JSON.stringify(alimnetacion), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}
@Injectable()
export class CampaniasService {
   api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }
  
  public getCampaniaLista(): Observable<CampaniasModel[]> {
    return this.http.get<CampaniasModel[]>
      (this.api + '/ListaCampaniaAtencion', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteCampania(id: number): void {
    this.http.post(this.api + '/deleteCampaniaAtencion', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @campania  alimnetacion
  //    */
  public saveOrUpdateCampania(campania: CampaniasModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveCampaniaAtencion', JSON.stringify(campania), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}
@Injectable()
export class AtencionCampanService {
   api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }
  
  public getAtencionCampaxFecha(dato): Observable<AtencionCampanModel[]> {
    return this.http.get<AtencionCampanModel[]>
      (this.api + `/ListaRegistroCampaniaByFecha?fecharegistro=${dato}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getAtencionxCampania(dato): Observable<AtencionCampanModel[]> {
    return this.http.get<AtencionCampanModel[]>
      (this.api + `/ListaRegistroCampaniaByCampania?idcampania=${dato}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  } 
  /**
  //    * Metodo que valida campos obligatorios
  //    * @campania  alimnetacion
  //    */
  public saveOrUpdateAtention(campania: AtencionCampanModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveRegistroCampaniaAtencion', JSON.stringify(campania), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}