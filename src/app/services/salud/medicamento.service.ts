import { MedicamentosModel } from '../../models/salud/medicamentos.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { URL_SERVICIOS } from '../../config/config';


@Injectable()
export class MedicamentoService {
  api = URL_SERVICIOS;
  constructor(private http: HttpClient) { }

  public getMedicamento(): Observable<MedicamentosModel[]> {
    return this.http.get<MedicamentosModel[]>(this.api + '/getAllMedicamento', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteMedicamento(id: MedicamentosModel): void {
    this.http.post(this.api + '/deleteMedicamento', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
   * Metodo que valida campos obligatorios
   * @param medicamento
   */

  public saveOrUpdate(medicamento: MedicamentosModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateMedicamento', JSON.stringify(medicamento), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}


