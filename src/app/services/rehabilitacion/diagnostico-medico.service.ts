import { DiganosticoMedicoModel } from './../../models/rehabilitacion/diagnostico-medico.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { URL_SERVICIOS } from '../../config/config';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class DiganosticoMedicoService {
  // tslint:disable-next-line:no-inferrable-types
   api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }

  public getDiganosticoMedico(): Observable<DiganosticoMedicoModel[]> {
    return this.http.get<DiganosticoMedicoModel[]>(this.api + '/getPacientesAtendidos', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getDiganosticoMedicoxfichaterapia(dato): Observable<DiganosticoMedicoModel[]> {
    return this.http.get<DiganosticoMedicoModel[]>
      (this.api + `/getDiangnosticoMedicoByIdFichaTerapiaFisica?idfichaterapiafisica=${dato}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getDiganosticoMedicoxidExpediente(dato): Observable<DiganosticoMedicoModel[]> {
    return this.http.get<DiganosticoMedicoModel[]>
      (this.api + `/getDiangnosticoMedicoByIdFichaTerapiaFisica?idfichaterapiafisica=${dato}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteDiganosticoMedico(id: number): void {
    this.http.post(this.api + '/deleteDiagnosticoMedico', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param diagnostico
  //    */
  public saveOrUpdateDiganosticoMedico(diagnostico: DiganosticoMedicoModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateDiagnosticoMedico', JSON.stringify(diagnostico), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}
