import { InspbocaarribaModel, BiometriaModel } from './../../models/rehabilitacion/rehabilitacion.model';
import { EvaluacionFisicaModel } from './../../models/rehabilitacion/rehabilitacion.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ReferenciaImtermaModel } from '../../models/rehabilitacion/referencia-interna.model';
import { URL_SERVICIOS } from '../../config/config';
import { map } from 'rxjs/operators';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class EvaluacionFisicaService {
  // tslint:disable-next-line:no-inferrable-types
  api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }

  public getEvaluacionFisica(): Observable<EvaluacionFisicaModel[]> {
    return this.http.get<EvaluacionFisicaModel[]>(this.api + '/getPacientesAtendidos', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getEvaluacionFisicaporexpediente(Expediente): Observable<any[]> {
    return this.http.get<any[]>(this.api + `/getFichaTerapiaFisicaByIdExpediente?idexpediente=${Expediente}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param fisica
  //    */
  public saveOrUpdateEvaluacionFisica(fisica: EvaluacionFisicaModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateFichaTerapiaFisica', JSON.stringify(fisica), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}

export class BiometriaService {
  // tslint:disable-next-line:no-inferrable-types
  api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }

  public getEvaluacionBiometrixterapiafisica(dato, anio): Observable<BiometriaModel[]> {
    return this.http.get<BiometriaModel[]>(this.api + `/getBiometriaPaciente?idfichaterapiafisica=${dato}&anio=${anio}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param fisica
  //    */
  public saveOrUpdateBiometria(fisica: BiometriaModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrBiometriaPaciente', JSON.stringify(fisica), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getlistadeaniosBiomatria(dato): Observable<any[]> {
    return this.http.get<any[]>(this.api + `/ListarAniosDeBiometria?fichaTerapiaFisica=${dato}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}

export class BocaArribaService {
  // tslint:disable-next-line:no-inferrable-types
  api = URL_SERVICIOS;
  date = new Date();
  actual = String(this.date).substr(11, 4);
  constructor(private http: HttpClient) { }

  public getBocaArriba(): Observable<InspbocaarribaModel[]> {
    return this.http.get<InspbocaarribaModel[]>(this.api + '/getPacientesAtendidos', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  // retorna del año actual
  public getBocaArribaxfichaterapia(dato, anio = this.actual): Observable<InspbocaarribaModel[]> {
    return this.http.get<InspbocaarribaModel[]>(this.api + `/getInspBocaArribaPaciente?idfichaterapiafisica=${dato}&anio=${anio}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getlistadeaniosBocaarriba(dato): Observable<any[]> {
    return this.http.get<any[]>(this.api + `/ListarInspeccionBocaArribaPorAnios?idfichaterapia=${dato}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }

  /**
  //    * Metodo que valida campos obligatorios
  //    * @param boca
  //    */
  public saveOrUpdateBocaArriba(boca: InspbocaarribaModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrInspBocaArriba', JSON.stringify(boca), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}




export class RefInternaService {
  // tslint:disable-next-line:no-inferrable-types
  api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }

  public getReferenciaAll(): Observable<ReferenciaImtermaModel[]> {
    return this.http.get<ReferenciaImtermaModel[]>(this.api + '/getAllReferenciaInterna', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getReferenciaxExpediente(dato): Observable<ReferenciaImtermaModel[]> {
    return this.http.get<ReferenciaImtermaModel[]>(this.api + `/getReferenciaInternaPorPaciente?idexpediente=${dato}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getReferenciaxArea(dato): Observable<ReferenciaImtermaModel[]> {
    return this.http.get<ReferenciaImtermaModel[]>(this.api + `/getReferenciaInternaPorAreas?idarea=${dato}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteRefeInterna(id: number): void {
    this.http.post(this.api + '/deleteReferenciaInterna', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param boca
  //    */
  public saveOrUpdateRefInterna(boca: ReferenciaImtermaModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateReferenciaInterna', JSON.stringify(boca), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}
