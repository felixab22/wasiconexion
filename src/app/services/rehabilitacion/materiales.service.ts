import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { MaterialRegModel, PrestamoMaterialModel } from '../../models/rehabilitacion/materiales.model';
import { URL_SERVICIOS } from '../../config/config';


@Injectable()
export class MaterialRegService {
  // tslint:disable-next-line:no-inferrable-types
  api = URL_SERVICIOS;


  constructor(private http: HttpClient) { }

  public getMaterialReg(): Observable<MaterialRegModel[]> {
    return this.http.get<MaterialRegModel[]>(this.api + '/getMaterialOrtopedico', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteMaterialReg(id: number): void {
    this.http.post(this.api + '/deleteMaterialOrtopedico', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param material
  //    */
  public saveOrUpdateMaterialReg(material: MaterialRegModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateMaterialOrtopedico', JSON.stringify(material), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}


@Injectable()
export class PrestamoMaterialService {
  // tslint:disable-next-line:no-inferrable-types
  api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }

  public getMaterialxPaciente(): Observable<PrestamoMaterialModel[]> {
    return this.http.get<PrestamoMaterialModel[]>(this.api + '/getPrestamoMaterialPorPaciente', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getMaterGarantia(): Observable<PrestamoMaterialModel[]> {
    return this.http.get<PrestamoMaterialModel[]>(this.api + '/getMaterialEnGarantia', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getMaterAlquiler(): Observable<PrestamoMaterialModel[]> {
    return this.http.get<PrestamoMaterialModel[]>(this.api + '//getMaterialesEnAlquiler', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getMaterVendido(): Observable<PrestamoMaterialModel[]> {
    return this.http.get<PrestamoMaterialModel[]>(this.api + '/getMaterialVendido', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deletePrestamoMaterial(id: number): void {
    this.http.post(this.api + '/deletePrestamoMaterial', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param material
  //    */
  public saveOrUpdatePrestamoMaterial(material: PrestamoMaterialModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdatePrestamoMaterial', JSON.stringify(material), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}
