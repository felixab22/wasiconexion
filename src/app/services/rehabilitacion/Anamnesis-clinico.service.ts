import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AnamnesisClinicoModel } from '../../models/rehabilitacion/anamnesis-clinico.model';
import { URL_SERVICIOS } from '../../config/config';

@Injectable()
export class AnamnesisClinicoService {
  // tslint:disable-next-line:no-inferrable-types
   api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }

  public getAnamnesis(): Observable<AnamnesisClinicoModel[]> {
    return this.http.get<AnamnesisClinicoModel[]>(this.api + '/getPacientesAtendidos', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getAnamnesisxidfichaterapiafisica(dato): Observable<AnamnesisClinicoModel[]> {
    return this.http.get<AnamnesisClinicoModel[]>
      (this.api + `/findAnamnesisClinicoByIdFichaTerapiaFisica?idfichaterapiafisica=${dato}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteAnamnesis(id: number): void {
    this.http.post(this.api + '/deleteFichaTerapaFisica', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param  Anamnesis
  //    */
  public saveOrUpdateAnamnesisClinico(Anamnesis: AnamnesisClinicoModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateAnamnesisClinico', JSON.stringify(Anamnesis), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}
