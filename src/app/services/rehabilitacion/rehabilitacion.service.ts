import {
  AnamnesisFamiliarModel, MovilidadArticulacionesModel, SegMovilidadModel,
  FuerzaObserModel, SegFuerzaModel, EspasticidadModel, ResultadoEscalaModel, MarchaModel, PiernaModel, DiagnosticoFinalModel, PlanTrabajoModel
} from './../../models/rehabilitacion/rehabilitacion.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { DesarrolloIMDModel } from '../../models/rehabilitacion/desarrollo-imd.model';
import { URL_SERVICIOS } from '../../config/config';

@Injectable()
export class DesarrolloIMDService {
  // tslint:disable-next-line:no-inferrable-types
  api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }
  public getDesarrolloIMD(): Observable<DesarrolloIMDModel[]> {
    return this.http.get<DesarrolloIMDModel[]>(this.api + '/');
  }
  public getDesarrolloIMDxidfichaterapiafisica(dato): Observable<DesarrolloIMDModel[]> {
    return this.http.get<DesarrolloIMDModel[]>
      (this.api + `/findByDesarrolloIndByIdanamnesis?idanamnesis=${dato}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteDesarrolloIMD(id: number): void {
    this.http.post(this.api + '/deleteDesarrolloInd', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param  desarrollo
  //    */
  public saveOrUpdateDesarrolloIMD(desarrollo: DesarrolloIMDModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateDesarrolloInd', JSON.stringify(desarrollo), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}

export class AnamnecisFamiliarService {
  // tslint:disable-next-line:no-inferrable-types
  api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }
  public getAnamnecisFamiliar(): Observable<AnamnesisFamiliarModel[]> {
    return this.http.get<AnamnesisFamiliarModel[]>(this.api + '/');
  }
  public getAnamnecisFamiliarxidfichaterapiafisica(dato): Observable<AnamnesisFamiliarModel[]> {
    return this.http.get<AnamnesisFamiliarModel[]>
      (this.api + `/getAnamnesisFamiliarByIdfichaterapiafisica?idfichaterapiafisica=${dato}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteDesarrolloIMD(id: number): void {
    this.http.post(this.api + '/deleteAnamnesisFamiliar', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param  anamnesisfamiliar
  //    */
  public saveOrUpdateAnamnecisFamiliar(anamnesisfamiliar: AnamnesisFamiliarModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateAnamnesisFamiliar', JSON.stringify(anamnesisfamiliar), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}

export class MovilidadArticulacionesService {
  // tslint:disable-next-line:no-inferrable-types
  api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }

  public getMovilidadxidfichaterapiafisica(idterapia, anio): Observable<MovilidadArticulacionesModel[]> {
    return this.http.get<MovilidadArticulacionesModel[]>
      (this.api + `/getMovilidadArticulacionesByIdfichaterapiafisica?idfichaterapia=${idterapia}&anio=${anio}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getlistaAniosMovilidad(idterapia): Observable<any[]> {
    return this.http.get<any[]>
      (this.api + `/listarMovilidadArticulacionePorFechas?idficha=${idterapia}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteMovilidad(id: number): void {
    this.http.post(this.api + '/deleteMovilidadArticulaciones', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param  movilidad
  //    */
  public saveOrUpdateMovilidad(movilidad: MovilidadArticulacionesModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateMovilidadArticulaciones', JSON.stringify(movilidad), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}

export class SegMovilidadArtiService {
  // tslint:disable-next-line:no-inferrable-types
  api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }

  public getMovilidadxidmovilidadArtia(dato): Observable<SegMovilidadModel[]> {
    return this.http.get<SegMovilidadModel[]>
      (this.api + `/getMovilidadSeguimiento?movilidadArticulaciones=${dato}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteMovilidad(id: number): void {
    this.http.post(this.api + '/deleteMovilidadPaciente', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param  segmovilidad
  //    */
  public saveOrUpdateMovilidad(segmovilidad: SegMovilidadModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateMovilidadPaciente', JSON.stringify(segmovilidad), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}

export class FuerzaService {
  // tslint:disable-next-line:no-inferrable-types
  api = URL_SERVICIOS;
  constructor(private http: HttpClient) { }

  public getFuerzaxidfichaterapia(idterapia, anios): Observable<FuerzaObserModel[]> {
    return this.http.get<FuerzaObserModel[]>
      (this.api + `/getFuerzaPaciente?idfichaterapia=${idterapia}&anio=${anios}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getListaAniosFuerza(idterapia): Observable<any[]> {
    return this.http.get<any[]>
      (this.api + `/ListarFichasFuerzaPorFechas?idficha=${idterapia}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteFuerza(id: number): void {
    this.http.post(this.api + '/deleteFuerzaPaciente', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param  obserfuerza
  //    */
  public saveOrUpdateFuerzaobs(obserfuerza: FuerzaObserModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateFuerza', JSON.stringify(obserfuerza), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}

export class SegFuerzaService {
  // tslint:disable-next-line:no-inferrable-types
  api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }
  public getseguiFuerzaxFuerza(dato): Observable<SegFuerzaModel[]> {
    return this.http.get<SegFuerzaModel[]>
      (this.api + `/getSeguimientoFuerza?idfuerza=${dato}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteMovilidad(id: number): void {
    this.http.post(this.api + '/deleteSeguimientoFuerza', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param  segfuerza
  //    */
  public saveOrUpdateSegFuerza(segfuerza: SegFuerzaModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateSeguimientoFuerza', JSON.stringify(segfuerza), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}

export class EspasticidadService {
  // tslint:disable-next-line:no-inferrable-types
  api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }

  public getEspasticidadxFichaterapiafisica(idterapia, anio): Observable<EspasticidadModel[]> {
    return this.http.get<EspasticidadModel[]>
      (this.api + `/getEspasticidadPaciente?idfichaterapiafisica=${idterapia}&anio=${anio}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getAniosListaEspasticidad(idterapia): Observable<any[]> {
    return this.http.get<any[]>
      (this.api + `/ListarFechasRegistroEspasticidad?idficha=${idterapia}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteEspasticidad(id: number): void {
    this.http.post(this.api + '/deleteEspasticidadPaciente', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param  espasticidad
  //    */
  public saveOrUpdateSegEspasticidad(espasticidad: EspasticidadModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrEspasticidadPaciente', JSON.stringify(espasticidad), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}

export class ResultadoEscalaService {
  // tslint:disable-next-line:no-inferrable-types
  api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }

  public getResultadoEscalaxFichaterapiafisica(idterapia, anio): Observable<ResultadoEscalaModel[]> {
    return this.http.get<ResultadoEscalaModel[]>
      (this.api + `/getResultadoEscalaPaciente?idficha=${idterapia}&anio=${anio}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getAniosListaResultado(idterapia): Observable<any[]> {
    return this.http.get<any[]>
      (this.api + `/listarResultadoEscalaPorFechas?idficha=${idterapia}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteResultadoEscala(id: number): void {
    this.http.post(this.api + '/deleteResultadoEscalaPaciente', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param  resultado
  //    */
  public saveOrUpdateSegResultadoEscala(resultado: ResultadoEscalaModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateResultadoEscala', JSON.stringify(resultado), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}

export class MarchaService {
  // tslint:disable-next-line:no-inferrable-types
  api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }
  public getMarchaxFichaterapiafisica(idterapia, anio): Observable<MarchaModel[]> {
    return this.http.get<MarchaModel[]>
      (this.api + `/getMarchaPaciente?idficha=${idterapia}&anio=${anio}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getAniosListaMarcha(idterapia): Observable<any[]> {
    return this.http.get<any[]>
      (this.api + `/listarFichaMarchaPorFechas?idficha=${idterapia}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteMarcha(id: number): void {
    this.http.post(this.api + '/deleteMarchaPaciente', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param  marcha
  //    */
  public saveOrUpdateSegMarcha(marcha: MarchaModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateMarcha', JSON.stringify(marcha), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}

export class PiernaService {
  // tslint:disable-next-line:no-inferrable-types
  api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }
  public getPernaxMarchayPosicion(marcha, pierna): Observable<PiernaModel[]> {
    return this.http.get<PiernaModel[]>
      (this.api + `/getMarchaPiernaPaciente?idmarcha=${marcha}&posicionpierna=${pierna}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deletePierna(id: number): void {
    this.http.post(this.api + '/deleteMarchaPiernaPaciente', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param  pierna
  //    */
  public saveOrUpdatePierna(pierna: PiernaModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateMarchaPierna', JSON.stringify(pierna), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}
export class DiagnosticoFinalService {
  // tslint:disable-next-line:no-inferrable-types
  api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }
  public getDiagnosticoFinalxFichafisica(idterapia, anio): Observable<DiagnosticoFinalModel[]> {
    return this.http.get<DiagnosticoFinalModel[]>
      (this.api + `/getDiagnosticoFinalPaciente?idficha=${idterapia}&anio=${anio}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getAniosListaDiagnosticoFinal(idfichate): Observable<any[]> {
    return this.http.get<any[]>
      (this.api + `/ListarDiagnosticoFinalPorFechas?idficha=${idfichate}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteDiagnosticoFinal(id: number): void {
    this.http.post(this.api + '/deleteDiagnosticoFinalPaciente', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param  final
  //    */
  public saveOrUpdateDiagnosticoFinal(final: DiagnosticoFinalModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrDiagnosticoFinalPaciente', JSON.stringify(final), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}

export class PlanTrabajoService {
  // tslint:disable-next-line:no-inferrable-types
  api = URL_SERVICIOS;
  constructor(private http: HttpClient) { }

  public getDiagnosticoFinalxDiagFinal(dato): Observable<PlanTrabajoModel[]> {
    return this.http.get<PlanTrabajoModel[]>
      (this.api + `/getPlanTrabajoPaciente?iddiagnostico=${dato}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteDiagnosticoFinal(id: number): void {
    this.http.post(this.api + '/deletePlanTrabajoPaciente', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param  plan
  //    */
  public saveOrUpdateDiagnosticoFinal(plan: PlanTrabajoModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrPlantrabajoPaciente', JSON.stringify(plan), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}
