import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { InasistenciaModel } from '../../models/rehabilitacion/inasistencia.model';
import { URL_SERVICIOS } from '../../config/config';


@Injectable()
export class InasistenciaService {
  // tslint:disable-next-line:no-inferrable-types
  api = URL_SERVICIOS;


  constructor(private http: HttpClient) { }

  public getInasistencias(): Observable<InasistenciaModel[]> {
    return this.http.get<InasistenciaModel[]>(this.api + '/getPacientesAtendidos', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getInasistenciasexpediente(): Observable<InasistenciaModel[]> {
    return this.http.get<InasistenciaModel[]>(this.api + '/findAllPacienteNoAtendido', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteInasistencias(id: number): void {
    this.http.post(this.api + '/deleteFichaTerapaFisica', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  public getListaInasistencias(): Observable<any[]> {
    return this.http.get<any[]>(this.api + '/reporteByCantidadInasistencias', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getListaInasistenciasDetalle(): Observable<any[]> {
    return this.http.get<any[]>(this.api + '/getAllinasistenciasPacientes', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteNotifications(id: number): void {
    this.http.post(this.api + '/deleteNotificacion', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }

  /**
  //    * Metodo que valida campos obligatorios
  //    * @param asistencia
  //    */
  public saveOrUpdateInasistencias(asistencia: InasistenciaModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateInasistencia', JSON.stringify(asistencia), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}
