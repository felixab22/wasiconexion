import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { FichaPsicologicaModel, EvaluacionFamiliarModel } from '../../models/psicologia/psicologia.model';
import { URL_SERVICIOS } from '../../config/config';

@Injectable()
export class EvalPsicologicaService {
  // tslint:disable-next-line:no-inferrable-types
   api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }
  
  public getPsicologicaxExpediente(dato): Observable<FichaPsicologicaModel[]> {
    return this.http.get<FichaPsicologicaModel[]>
      (this.api + `/getFichaPsicologiaPaciente?idexpediente=${dato}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }



  public deletePsicologica(id: number): void {
    this.http.post(this.api + '/deleteFichaPsicologia', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param  evalpsicolg
  //    */
  public saveOrUpdateEvalPsicologica(evalpsicolg: FichaPsicologicaModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateFichaPsicologia', JSON.stringify(evalpsicolg), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}
export class EvalFamiliaService { 
   api = URL_SERVICIOS;
  dato: any;

  constructor(private http: HttpClient) { }

  public getEvalPsicologicaFamiliarxExpediente(dato) {
    return this.http.get<EvaluacionFamiliarModel[]>
      (this.api + `/getEvaluacionPsicologicaFamiliares?idfichapsicologia=${dato}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  
  public deleteEvalPsicologicafamiliar(id: number): void {
    this.http.post(this.api + '/deleteEvPsicologia', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param  evalfamilia
  //    */
  public saveOrUpdateEvalFamiliaPsicologica(evalfamilia: EvaluacionFamiliarModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateEvaluacionPsicologia', JSON.stringify(evalfamilia), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}
