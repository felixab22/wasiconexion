import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { EvaluacionEduModel } from '../../models/educacion/evaluacion-edu.model';
import { URL_SERVICIOS } from '../../config/config';

@Injectable()
export class EvaluacionEducacionService {
  // tslint:disable-next-line:no-inferrable-types
   api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }
  public getEvaluacionEducacionxfichaeducacionYnumEvalu(dato1, dato2): Observable<EvaluacionEduModel[]> {
    return this.http.get<EvaluacionEduModel[]>
      (this.api + `/getEvaluacionSemestral?idfichaeducacion=${dato1}&numevaluacion=${dato2}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteEvaluacionEducacion(id: number): void {
    this.http.post(this.api + '/deleteEvaluacionSemestral', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param  evaleduca
  //    */
  public saveOrUpdateEvaluacionEducacion(evaleduca: EvaluacionEduModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateEvaluacionSemestral', JSON.stringify(evaleduca), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}
