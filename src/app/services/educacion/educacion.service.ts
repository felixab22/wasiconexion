import { EducacionModel, EscolaridadModel, DesarrolloSocialModel, DesalenguajeModel, SaludActualEduModel } from './../../models/educacion/educacion.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { URL_SERVICIOS } from '../../config/config';


@Injectable()


export class EducacionService {
  // tslint:disable-next-line:no-inferrable-types
   api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }
  public getEducacionxExpediente(dato): Observable<EducacionModel[]> {
    return this.http.get<EducacionModel[]>
      (this.api + `/getFichaEducacionPaciente?idexpediente=${dato}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteEducacion(id: number): void {
    this.http.post(this.api + '/deleteFichaEducacion', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param  evaleducacion
  //    */
  public saveOrUpdateEducacion(evaleducacion: EducacionModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateFichaEducacion', JSON.stringify(evaleducacion), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}

export class EscolaridadService {
  // tslint:disable-next-line:no-inferrable-types
   api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }
  public getEscolaridadaxEducacion(dato): Observable<EscolaridadModel[]> {
    return this.http.get<EscolaridadModel[]>
      (this.api + `/getEscolaridad?idfichaeducacion=${dato}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteEscolaridad(id: number): void {
    this.http.post(this.api + '/deleteEscolaridad', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param  evalpsicolg
  //    */
  public saveOrUpdateEscolaridad(evalpsicolg: EscolaridadModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateEscolaridad', JSON.stringify(evalpsicolg), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}

export class DesarrolloSocialService {
  // tslint:disable-next-line:no-inferrable-types
   api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }
  public getDesarrolloSocialfichaeduc(dato): Observable<DesarrolloSocialModel[]> {
    return this.http.get<DesarrolloSocialModel[]>
      (this.api + `/getDesarrolloSocial?idfichaeducacion=${dato}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteDesarrolloSocial(id: number): void {
    this.http.post(this.api + '/deleteDesarrolloSocial', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param  desarrosocial
  //    */
  public saveOrUpdateDesarrolloSocial(desarrosocial: DesarrolloSocialModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateDesarrolloSocial', JSON.stringify(desarrosocial), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}
export class DesalenguajeService {
  // tslint:disable-next-line:no-inferrable-types
   api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }
  public getDesalenguajexfichaeduc(dato): Observable<DesalenguajeModel[]> {
    return this.http.get<DesalenguajeModel[]>
      (this.api + `/getDesarrolloLenguaje?idfichaeducacion=${dato}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteDesalenguaje(id: number): void {
    this.http.post(this.api + '/deleteDesarrolloLenguaje', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param  desarrolengua
  //    */
  public saveOrUpdateDesalenguaje(desarrolengua: DesalenguajeModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateDesarrolloLenguaje', JSON.stringify(desarrolengua), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}
export class SaludActualEduService {
  // tslint:disable-next-line:no-inferrable-types
   api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }
  public getSaludxfichaeduc(dato): Observable<SaludActualEduModel[]> {
    return this.http.get<SaludActualEduModel[]>
      (this.api + `getSaludActual?idfichaeducacion=${dato}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteSalud(id: number): void {
    this.http.post(this.api + '/deleteSaludActual', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  //    * Metodo que valida campos obligatorios
  //    * @param  saludeducaci
  //    */
  public saveOrUpdateSalud(saludeducaci: SaludActualEduModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateSaludActual', JSON.stringify(saludeducaci), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}
