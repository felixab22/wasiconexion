
export { FilesService } from './files/upload.service';
/* cSpell:disable */
export { AtentionService, HorarioService, InasistenciasService } from './config/agenda.service';
export { EvalFamiliaService, EvalPsicologicaService } from './psicologia/psicologia.service';
export { EdadService } from './setting/edad.service';
export { UserService } from './setting/user.service';
export { RolUsuarioService } from './setting/rol-usuario.service';
export { AreaService } from './setting/area.service';
export { MedicamentoService } from './salud/medicamento.service';
export { PreinscripcionService } from './social/preinscripcion.service';
export { ExpedienteService } from './social/expediente.service';
export { SocioEconomicaService } from './social/socio-economico.service';
export { UbigeoService } from './setting/ubigeo.service';
export { ApoderadoService } from './social/apoderado.service';
export { CompoFamiliarService } from './social/compo-familiar.service';
export { VisitaService } from './social/visita-domiciliaria.service';
export { SituaEconomicService } from './social/situacion-economica.service';
export { SituaPacientecService } from './social/situacion-paciente.service';
export { EvaluacionFisicaService, BiometriaService, BocaArribaService, RefInternaService } from './rehabilitacion/evaluacion-fisica.service';
export { InasistenciaService } from './rehabilitacion/inasistencias.service';
export { AnamnesisClinicoService } from './rehabilitacion/Anamnesis-clinico.service';
export { DiganosticoMedicoService } from './rehabilitacion/diagnostico-medico.service';
export { ReferenciaService } from './social/referencias-externas.service';
export {
    DesarrolloIMDService,
    AnamnecisFamiliarService,
    SegMovilidadArtiService,
    MovilidadArticulacionesService,
    SegFuerzaService, FuerzaService,
    EspasticidadService, ResultadoEscalaService,
    MarchaService, PiernaService, DiagnosticoFinalService,
    PlanTrabajoService
} from './rehabilitacion/rehabilitacion.service';
export { GraficasService } from './charts/charts.service';
export {
    SaludService,
    VisitaMedicaService,
    CuidadoAlimenService,
    VisitaIntegralService,
    CampaniasService,
    AtencionCampanService
} from './salud/salud.service';
export { CentroDiaService } from './centrodia/centrodia.service';
export { ATOService } from './ato/ato.service';
export { EducacionService, EscolaridadService, DesarrolloSocialService, SaludActualEduService, DesalenguajeService } from './educacion/educacion.service';
export { EvaluacionEducacionService } from './educacion/evaluacion-edu.service';
export { RegisUsuarioService } from './setting/regis-usuario.service';
export { MaterialRegService, PrestamoMaterialService } from './rehabilitacion/materiales.service';
