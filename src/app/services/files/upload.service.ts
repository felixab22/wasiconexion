import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { URL_SERVICIOS } from '../../config/config';


@Injectable()
export class FilesService {
  api = URL_SERVICIOS;
  constructor(
    private http: HttpClient
  ) { }

  public getListaFilesxAreayPaciente(nombrearea, idpaciente): Observable<any[]> {
    return this.http.get<any[]>
      (this.api + `/getAllFiles?nombrearea=${nombrearea}&codigo=${idpaciente}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteFilesxcodigo(nombrearchivo: any, nombrearea: string, id: any) {
    return this.http.post(this.api + `/deleteFile/${nombrearchivo}?nombrearea=${nombrearea}&codigo=${id}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}


