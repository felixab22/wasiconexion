import { SituacionPacienteModel } from '../../models/social/situacion-paciente';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { URL_SERVICIOS } from '../../config/config';

@Injectable()

export class SituaPacientecService {
  // tslint:disable-next-line:no-inferrable-types
   api = URL_SERVICIOS;


  constructor(private http: HttpClient) { }

  public getSituaPaciente(Expediente): Observable<SituacionPacienteModel[]> {
    return this.http.get<SituacionPacienteModel[]>(this.api + `/getSituacionPacienteByIdExpediente?idexpediente=${Expediente}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getSituaPacientelista(): Observable<any[]> {
    return this.http.get<any[]>(this.api + '/getPacienteActivoAndAceptado', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  /**
    * Metodo que valida campos obligatorios
    * @param paciente
    */
  public saveOrUpdateSituaPaciente(paciente: SituacionPacienteModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateSituacionPaciente', JSON.stringify(paciente), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}

