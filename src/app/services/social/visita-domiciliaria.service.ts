import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { VisitaDomiciliarioModel } from '../../models/social/visita-domiciliario.model';
import { URL_SERVICIOS } from '../../config/config';

@Injectable()
export class VisitaService {
  // tslint:disable-next-line:no-inferrable-types
   api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }

  public getVisita(Expediente): Observable<VisitaDomiciliarioModel[]> {
    return this.http.get<VisitaDomiciliarioModel[]>(this.api + `/getVisitaDomiciliariaByIdexpediente?idexpediente=${Expediente}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }

  public deleteVisita(id: number): void {
    this.http.post(this.api + '/deleteVisitaDomiciliaria', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
   * Metodo que valida campos obligatorios
   * @param visita
   */
  public saveOrUpdateVisita(visita: VisitaDomiciliarioModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateVisitaDomiciliaria', JSON.stringify(visita), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }

}
