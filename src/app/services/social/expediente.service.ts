import { ExpedienteModel } from '../../models/social/expediente.model';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { URL_SERVICIOS } from '../../config/config';


@Injectable()

export class ExpedienteService {
  // tslint:disable-next-line:no-inferrable-types
   api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }

  public getExpediente(): Observable<ExpedienteModel[]> {
    return this.http.get<ExpedienteModel[]>(this.api + '/getExpediente', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getBusquedaDNI(dni): Observable<ExpedienteModel[]> {
    return this.http.get<ExpedienteModel[]>(this.api + '/getExpedienteByDni?dni=' + dni, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }

  public getBusquedaxHistorial(historial): Observable<ExpedienteModel[]> {
    return this.http.get<ExpedienteModel[]>(this.api + `/getByCodigohistorial?codigo=${historial}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }

  public getExpedienteActivos(): Observable<ExpedienteModel[]> {
    return this.http.get<ExpedienteModel[]>(this.api + '/getExpedienteByPacienteActivo', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }

  public deleteExpediente(id: number): void {
    this.http.post(this.api + '/deleteExpediente', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
  * Metodo que valida campos obligatorios
  * @param expediente
  */

  public saveOrUpdate(expediente: ExpedienteModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateExpediente', JSON.stringify(expediente), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}




