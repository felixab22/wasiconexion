import { ReferenciaModel } from '../../models/social/referencia-externa.model';
import { Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { URL_SERVICIOS } from '../../config/config';

@Injectable()

export class ReferenciaService {
  // tslint:disable-next-line:no-inferrable-types
   api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }

  public getSituaReferencia(Expediente): Observable<ReferenciaModel[]> {
    return this.http.get<ReferenciaModel[]>(this.api + `/findReferenciaExternaByIdexpediente?idexpediente=${Expediente}`,
      { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteReferencia(id: number): void {
    this.http.post(this.api + '/deleteReferenciaExterna',
      JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
      * Metodo que valida campos obligatorios
      * @param referencia
      */
  public saveOrUpdateReferencia(referencia: ReferenciaModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateReferenciaExterna' , JSON.stringify(referencia),
      { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }

}
