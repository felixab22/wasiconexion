import { CompisicionFamiliarModel } from '../../models/social/compo-familiar.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { URL_SERVICIOS } from '../../config/config';

@Injectable()

export class CompoFamiliarService {
  // tslint:disable-next-line:no-inferrable-types
   api = URL_SERVICIOS;


  constructor(private http: HttpClient) { }
  public getComposisionFamiliar(Expediente): Observable<CompisicionFamiliarModel[]> {
    return this.http.get<CompisicionFamiliarModel[]>(this.api + `/getComposicionFamiliarByIdexpediente?idexpediente=${Expediente}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }

  public deleteCompoFamiliar(id: number): void {
    this.http.post(this.api + '/deleteComposicionFamiliar', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
   * Metodo que valida campos obligatorios
   * @param familiar
   */

  public saveOrUpdate(familiar: CompisicionFamiliarModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateComposicionFamiliar', JSON.stringify(familiar), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}

