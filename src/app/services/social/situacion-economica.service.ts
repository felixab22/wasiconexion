import { SituacionEconomicaModel } from '../../models/social/situacion-economica.model';

import { Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { URL_SERVICIOS } from '../../config/config';

@Injectable()

export class SituaEconomicService {
  // tslint:disable-next-line:no-inferrable-types
   api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }

  public getSituaEconomic(Expediente): Observable<SituacionEconomicaModel[]> {
    return this.http.get<SituacionEconomicaModel[]>(this.api + `/getSituacioneconByIdexpediente?idexpediente=${Expediente}`,
      { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }

  public deleteSituaEconomic(id: number): void {
    this.http.post(this.api + '/deletePersonal', JSON.stringify(id),
      { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
      * Metodo que valida campos obligatorios
      * @param situaeconomic
      */
  public saveOrUpdateeconomia(situaeconomic: SituacionEconomicaModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateSituacionecon ', JSON.stringify(situaeconomic),
    { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }

}

