import { PreInscripcionModel } from '../../models/social/preinscripcion.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { URL_SERVICIOS } from '../../config/config';



@Injectable()
export class PreinscripcionService {
  // tslint:disable-next-line:no-inferrable-types
   api = URL_SERVICIOS;
  constructor(
    private http: HttpClient,
    
    ) { 
      
    }

  public getPreinscripcion(): Observable<PreInscripcionModel[]> {
    return this.http.get<PreInscripcionModel[]>(this.api + '/getPreinscripcion', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }

  public getPreinscripciondebaja(): Observable<PreInscripcionModel[]> {
    return this.http.get<PreInscripcionModel[]>(this.api + '/getPreinscripcionDadosDeBaja', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deletePreinscripcion(id: number): void {
    this.http.post(this.api + '/deletePreinscripcion', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
 * Metodo que valida campos obligatorios
 * @param preinscripcion
 */

  public saveOrUpdate(preinscripcion: PreInscripcionModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdatePreinscripcion', JSON.stringify(preinscripcion), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken')
  }) });
  }
}


