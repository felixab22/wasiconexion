import { SocioEconomicaModel } from '../../models/social/socio-economica';
import { Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { URL_SERVICIOS } from '../../config/config';

@Injectable()

export class SocioEconomicaService {
  // tslint:disable-next-line:no-inferrable-types
   api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }

  public getSocioEcon(): Observable<SocioEconomicaModel[]> {
    return this.http.get<SocioEconomicaModel[]>(this.api + '/getFichaSocioecon', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getSocioEconxexpediente(expediente): Observable<SocioEconomicaModel[]> {
    return this.http.get<SocioEconomicaModel[]>(this.api + `/getFichaSocioEconomicaByIdexpediente?idexpediente=${expediente}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteSocioEcon(id: number): void {
    this.http.post(this.api + '/deleteFichasocioecon', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
    * Metodo que valida campos obligatorios
    * @param historia
    */
  public saveOrUpdateEvalSocioeco(historia: SocioEconomicaModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateFichasocioecon', JSON.stringify(historia), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}

