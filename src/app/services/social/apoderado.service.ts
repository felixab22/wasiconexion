import { ApoderadoModel } from '../../models/social/apoderado.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { URL_SERVICIOS } from '../../config/config';


@Injectable()
export class ApoderadoService {
   api = URL_SERVICIOS;
  constructor(private http: HttpClient) { }

  public getApoderado(): Observable<ApoderadoModel[]> {
    return this.http.get<ApoderadoModel[]>(this.api + '/getApoderado', { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public getApoderadoxExpediente(Expediente): Observable<ApoderadoModel[]> {
    return this.http.get<ApoderadoModel[]>(this.api + `/getApoderadoByIdExpediente?idexpediente=${Expediente}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteApoderado(id: number): void {
    this.http.post(this.api + '/deleteApoderado', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
   * Metodo que valida campos obligatorios
   * @param Apoderado
   */
  public saveOrUpdate(Apoderado: ApoderadoModel): Observable<any> {
    return this.http.post<any>(this.api + '/saveOrUpdateApoderado', JSON.stringify(Apoderado), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}

