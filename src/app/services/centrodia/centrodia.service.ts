import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { CentroDiaModel } from '../../models/centrodia/centrodia.model';
import { URL_SERVICIOS } from '../../config/config';

@Injectable()
export class CentroDiaService {
  // tslint:disable-next-line:no-inferrable-types
  api = URL_SERVICIOS;

  constructor(private http: HttpClient) { }

  public getcuidadoYalimentacion(dato): Observable<CentroDiaModel[]> {

    return this.http.get<CentroDiaModel[]>
      (this.api + `/getFichacentrodiaPaciente?idcuidadoalim=${dato}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  public deleteCentroDia(id: number): void {

    this.http.post(this.api + '/deleteFichacentrodia', JSON.stringify(id), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) }).subscribe();
  }
  /**
   //    * Metodo que valida campos obligatorios
   //    * @param  dia
   //    */
  public saveOrUpdateEvalCentroDia(dia: CentroDiaModel): Observable<any> {

    return this.http.post<any>(this.api + '/saveOrUpdateFichacentrodia', JSON.stringify(dia), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
}
